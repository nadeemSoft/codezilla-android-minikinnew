package com.minikin.android.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by user on 11/04/2016.
 */
public class MySession {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "MyPref";
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_LANG_COUNT_SEL = "IsLanguageCountrySelected";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_ID = "id";
    public static final String KEY_FULLNAME = "full_name";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_LANGUAGE = "language";
    public static final String KEY_COUNTRY = "country";

    public MySession(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String id, String full_name, String email, String mobile) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_ID, id);
        editor.putString(KEY_FULLNAME, full_name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.commit();
    }

    public void setLanguageCountry(String language, String country) {
        editor.putBoolean(IS_LANG_COUNT_SEL, true);
        editor.putString(KEY_LANGUAGE, language);
        editor.putString(KEY_COUNTRY, country);
        editor.commit();
    }

    public void setKeyLanguage(String language) {
        editor.putString(KEY_LANGUAGE, language);
        editor.commit();
    }

    public void setKeyCountry(String country) {
        editor.putString(KEY_COUNTRY, country);
        editor.commit();
    }

    /* public void checkLogin() {
         if (!this.IsLoggedIn()) {
             Intent intent = new Intent(_context, LoginActivity.class);
             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
             intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
             _context.startActivity(intent);
         }
     }
 */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, ""));
        user.put(KEY_ID, pref.getString(KEY_ID, ""));
        user.put(KEY_FULLNAME, pref.getString(KEY_FULLNAME, ""));
        user.put(KEY_MOBILE, pref.getString(KEY_MOBILE, ""));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, ""));
        return user;
    }

    public void logoutUser() {
        editor.putBoolean(IS_LOGIN, false);
        editor.putString(KEY_ID, "");
        editor.putString(KEY_FULLNAME, "");
        editor.putString(KEY_EMAIL, "");
        editor.putString(KEY_MOBILE, "");
        editor.commit();
    }

    public boolean isLoggedIn()

    {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public boolean isLanguageCountrySelected()

    {
        return pref.getBoolean(IS_LANG_COUNT_SEL, false);
    }

    public String getKeyFullName() {
        return pref.getString(KEY_FULLNAME, null);
    }

    public String getKeyMobile() {
        return pref.getString(KEY_MOBILE, null);
    }

    public String getKeyEmail() {
        return pref.getString(KEY_EMAIL, null);
    }

    public String getKeyId() {
        return pref.getString(KEY_ID, null);
    }

    public String getKeyCountry() {
        return pref.getString(KEY_COUNTRY, null);
    }

    public String getKeyLanguage() {
        return pref.getString(KEY_LANGUAGE, null);
    }
}