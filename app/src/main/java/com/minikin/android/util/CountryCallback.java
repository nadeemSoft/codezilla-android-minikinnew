package com.minikin.android.util;

import com.minikin.android.model.AreaModel;

import java.util.ArrayList;

public interface CountryCallback {
    void setItemList(int position, ArrayList<AreaModel> list);
}
