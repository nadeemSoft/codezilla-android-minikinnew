package com.minikin.android.util;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by codezilla-16 on 8/3/18.
 */

public class AppConstants {

    public static final String SHAREDPREFERENEKEY = "UserData";
    public static final String ALLOWED_URI_CHARS = "@#&=*+-_.,:!?()/~'%";

    public static final String BASE_URL = "http://132.148.0.36/minikin_ws/";
    public static final String GET_SPLASH_SCREEN = BASE_URL + "splash_screen.php?";
    public static final String GET_REGISTRATION = BASE_URL + "register.php?";
    public static final String GET_LOGIN = BASE_URL + "login.php?";
    // public static final String GET_CATEGORY = BASE_URL + "cats.php?"; //OLd
    public static final String GET_CATEGORY = BASE_URL + "categories?"; //OLd

    // http://minikin.co/minikin_ws/categories?lang=en
    //public static final String GET_CATEGORY = BASE_URL +"categories.php";

    //http://132.148.0.36/minikin_ws/products.php
    public static final String GET_PRODUCT = BASE_URL + "products.php";
    public static final String GET_MYPROFILE = BASE_URL + "my_profile.php";
    public static final String GET_CHANGE_PASSWORD = BASE_URL + "change_password.php";
    public static final String GET_VERIFY_OLD_PASSWORD = BASE_URL + "verify_old_password.php";
    public static final String GET_TERMS_AND_CONDITION = BASE_URL + "terms_and_conditions.php";
    public static final String GET_DELIVERY_POLICY = BASE_URL + "delivery_policy.php";
    public static final String GET_WHO_WE_ARE = BASE_URL + "who_we_are.php";
    public static final String GET_JOIN_US = BASE_URL + "register.php";
    public static final String GET_MY_ADDRESS = BASE_URL + "my_address.php";
    public static final String GET_MY_ADD_NEW_ADDRESS = BASE_URL + "add_address.php";
    public static final String GET_RETURN_EXCHANGE = BASE_URL + "returns_exchange.php";

    // http://132.148.0.36/minikin_ws/cart_items.php?device_id=device_id
    public static final String GET_MY_CART = BASE_URL + "cart_items.php";

    public static final String GET_MY_CART_COUNT = BASE_URL + "cart_count.php";

    public static final String GET_REMOVE_ITEM_CART = BASE_URL + "remove_item.php";
    public static final String GET_PRODUCT_DETAIL = BASE_URL + "product_details.php";
    public static final String GET_GIFT_PACKAGE = BASE_URL + "gift_packages.php";
    public static final String GET_MY_ORDER = BASE_URL + "my_orders.php";

    //http://132.148.0.36/minikin_ws/add_to_cart.php?product_id=6&device_id=device_id&quantity=2

    public static final String GET_ADD_ITEM_CARD = BASE_URL + "add_to_cart.php";
    public static final String GET_RELETED_ITEMS = BASE_URL + "related_items.php";
    public static final String GET_PRODUCT_STOCK = BASE_URL + "product_stock.php";

    // http://132.148.0.36/minikin_ws/checkout.php?as_gift=1&gift_card=1&card_from=khalid&card_to=any&customer_id=1&address_id=2&package_id=3&coupon_code=MINIKINCOUPON1
    public static final String MEMBER_CHECK_OUT = BASE_URL + "checkout.php";

    public static final String GUEST_CHECK_OUT = BASE_URL + "checkout.php";
    //http://132.148.0.36/minikin_ws/checkout.php?device_id=device_id&title=address%20title&area_id=4&block=address%20block&street=address%20street&avenue=adddress%20avenue&building=address%20building&floor=address%20floor&apartment=address%20apartment&notes=notes&mobile=mobile&as_guest=1&as_gift=1&gift_card=1&card_from=khalid&card_to=any

    public static final String GET_AREA = BASE_URL + "areas.php";
    public static final String GET_REMOVE_ADDRESS = BASE_URL + "remove_address.php";
    public static final String GET_EDIT_ADDRESS = BASE_URL + "add_address.php";
    public static final String GET_ORDER_CONFIRM = BASE_URL + "confirm_order.php";
    public static final String GET_VALID_COUPON = BASE_URL + "validate_coupon.php";
    public static final String GET_FILTER = BASE_URL + "filter_attributes.php";
    public static final String GET_MY_FAVROIT = BASE_URL + "my_favorites.php";
    public static final String GET_ADD_TO_MYFAVROIT = BASE_URL + "add_to_favorite.php";
    public static final String GET_REMOVE_FAVROIT = BASE_URL + "remove_favorite.php";
    public static final String GET_CONTACT_US = BASE_URL + "contact_us.php";
    public static final String GET_ORDER_DETAIL = BASE_URL + "order_details.php";
    public static final String GET_ORDER_CONFIRMATION = BASE_URL + "confirm_order.php";

    public static final String GET_FORGET_PASSWORD = BASE_URL + "forget_password.php";
    public static final String GET_SOCIAL_MEDIA = BASE_URL + "social_media.php";
    public static final String GET_SEARCH = BASE_URL + "product_search.php";
    public static final String GET_LIVE_CHAT = "https://tawk.to/minikin";


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
