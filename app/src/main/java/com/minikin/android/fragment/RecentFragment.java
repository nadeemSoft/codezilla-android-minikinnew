package com.minikin.android.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.minikin.android.R;
import com.minikin.android.activity.MyOrderActivity;
import com.minikin.android.adapter.MyOrderAdapter;

public class RecentFragment extends Fragment {
    RecyclerView rvRecent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recent, container, false);
        rvRecent = view.findViewById(R.id.rv_recent);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvRecent.setLayoutManager(mLayoutManager);
       // rvRecent.setAdapter(new MyOrderAdapter(MyOrderActivity.myOrderModelList, R.layout.rowitem_my_order, getActivity()));
        return view;

    }
}
