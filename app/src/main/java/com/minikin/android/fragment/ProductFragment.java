package com.minikin.android.fragment;


import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.AdapterFilterSubCategory;
import com.minikin.android.adapter.ProductAdapter;
import com.minikin.android.model.ProductModel;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductFragment extends Fragment {
    RecyclerView rv;
    String productId = "0";
    ProgressDialog pd;
    ArrayList<ProductModel> productList;
    ProductAdapter productAdapter;
    SharedPreferences sp = null;
    private SharedPreferences.Editor editor;
    private SharedPreferences pref;
    private String WhichLanguage = "";
    Bundle bundle = null;




    public ProductFragment() {
        // Required empty public constructor

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         bundle = getArguments();
        productId = bundle.getString("productId");
        Log.e("productIdinfrag", productId);
        pd = new ProgressDialog(getContext());
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        sp = getContext().getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        productList = new ArrayList<>();
        pref = getActivity().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        setLangRecreate(WhichLanguage);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.frag_prod, container, false);
        rv = view.findViewById(R.id.frag_prod_rv);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        String size = sp.getString("sizeFilterCategory","");

        if (productId.equalsIgnoreCase("000")) {
            getProductWithNew();

        } else if(productId.equalsIgnoreCase("filter")){
            String priceMinimum = bundle.getString("priceMinimum");
            String priceMaximum = bundle.getString("priceMaximum");

            if(!priceMinimum.equalsIgnoreCase(null) && !priceMaximum.equalsIgnoreCase(null)){
                getProductWithNewFilterAttribute(size,priceMinimum,priceMaximum);

            }else {
                Toast.makeText(getContext(),"Please set price",Toast.LENGTH_SHORT).show();
            }



        }else {
            getProduct();

        }
    }

    private void getProduct() {

        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    rv.removeAllViews();

                    Log.e("ProductResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        productList.clear();
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objCat = jsonArrayResult.getJSONObject(i);
                            String product_id = objCat.getString("product_id");
                            String name = objCat.getString("name");
                            String description = objCat.getString("description");
                            String price = objCat.getString("price");
                            String image = objCat.getString("image");
                            String has_offer = objCat.getString("has_offer");
                            String offer_price = objCat.getString("offer_price");

                            //ProductModel productModel = new ProductModel(product_id, name, description, price, image, has_offer, offer_price);
                            //productList.add(productModel);

                        }
                        pd.dismiss();
                        productAdapter = new ProductAdapter(productList, R.layout.rowitem_pro, getActivity());
                        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(productAdapter);


                    } else {
                        pd.dismiss();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                    }
                    pd.dismiss();


                    //startActivity(new Intent(activity, LoginActivity.class));


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");

                    // Toast.makeText(SignUpActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                //params.put("new", sp.getString("WhichLanguage", ""));
                params.put("cat_id", productId);
                //params.put("atribute_id", sp.getString("WhichLanguage", ""));

                Log.e("HomeParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);


    }

    private void getProductWithNew() {

        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {

                    Log.e("ProductResponseWithNew", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        productList.clear();
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objCat = jsonArrayResult.getJSONObject(i);
                            String product_id = objCat.getString("product_id");
                            String name = objCat.getString("name");
                            String description = objCat.getString("description");
                            String price = objCat.getString("price");
                            String image = objCat.getString("image");
                            String has_offer = objCat.getString("has_offer");
                            String offer_price = objCat.getString("offer_price");

                           // ProductModel productModel = new ProductModel(product_id, name, description, price, image, has_offer, offer_price);
                           // productList.add(productModel);

                        }
                        pd.dismiss();
                        productAdapter = new ProductAdapter(productList, R.layout.rowitem_pro, getActivity());
                        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(productAdapter);


                    } else {
                        pd.dismiss();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                    }
                    pd.dismiss();


                    //startActivity(new Intent(activity, LoginActivity.class));


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");

                    // Toast.makeText(SignUpActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("new", "1");
                params.put("cat_id", productId);
                //params.put("atribute_id", sp.getString("WhichLanguage", ""));

                Log.e("HomeParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);


    }






    private void getProductWithNewFilterAttribute(final String size, final String priceMinimum, final String priceMaximum) {
        final int sizeInt = Integer.parseInt(size);

        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    rv.removeAllViews();

                    Log.e("ProductResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        productList.clear();
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objCat = jsonArrayResult.getJSONObject(i);
                            String product_id = objCat.getString("product_id");
                            String name = objCat.getString("name");
                            String description = objCat.getString("description");
                            String price = objCat.getString("price");
                            String image = objCat.getString("image");
                            String has_offer = objCat.getString("has_offer");
                            String offer_price = objCat.getString("offer_price");

                           // ProductModel productModel = new ProductModel(product_id, name, description, price, image, has_offer, offer_price);
                          //  productList.add(productModel);

                        }
                        pd.dismiss();
                        productAdapter = new ProductAdapter(productList, R.layout.rowitem_pro, getActivity());
                        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
                        rv.setLayoutManager(layoutManager);
                        rv.setAdapter(productAdapter);


                    } else {
                        pd.dismiss();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

                    }
                    pd.dismiss();


                    //startActivity(new Intent(activity, LoginActivity.class));


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");

                    // Toast.makeText(SignUpActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("new", "1");
                params.put("cat_id", productId);
                Log.e("SIZEATTRI", sizeInt + "");
                for(int i = 0; i<sizeInt;i++){
                    params.put("atribute["+(i+1)+"]", AdapterFilterSubCategory.addIdList.get(i)+"");

                    }
                params.put("min_price", priceMinimum);
                params.put("max_price", priceMaximum);
                Log.e("filterParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        requestQueue.add(request);


    }






    public void setLangRecreate(String langval) {
        Configuration config = getActivity().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getActivity().getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());
        Typeface fontEn = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontAr = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);


        } else if (langval.equalsIgnoreCase("en")) {

            getActivity().getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
    }

}
