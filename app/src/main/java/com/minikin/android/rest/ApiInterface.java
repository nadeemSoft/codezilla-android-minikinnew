package com.minikin.android.rest;

/**
 * Created by cube_dev on 10/24/2017.
 */

import android.database.Observable;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("change_pass_cust")
    Call<ResponseBody> changePassword(@Header("access_token") String access_token, @Query("oldpass_cust") String oldPass, @Query("npass_cust") String newPass);

    @GET("login")
    Call<ResponseBody> login(@Query("email") String email, @Query("password") String password
            , @Query("device_token") String deviceToken);

    @GET("register")
    Call<ResponseBody> signUp(@Query("uname") String uname, @Query("email") String email
            , @Query("password") String password, @Query("device_token") String deviceToken);

    @GET("cust_address")
    Call<ResponseBody> addAddress(@Header("access_token") String access_token, @Query("cust_id") String cust_id
            , @Query("address_type") String type, @Query("city") String city
            , @Query("area") String area, @Query("street") String street
            , @Query("landmark") String landmark, @Query("postcode") String postcode, @Query("full_address") String full_address
            , @Query("latitude") Double latitude, @Query("longitude") Double longitude
    );


    @GET("edit_cust_address")
    Call<ResponseBody> editAddress(@Header("access_token") String access_token, @Query("cust_id") String cust_id
            , @Query("address_type") String type, @Query("city") String city
            , @Query("area") String area, @Query("street") String street
            , @Query("landmark") String landmark, @Query("postcode") String postcode, @Query("full_address") String full_address
            , @Query("latitude") Double latitude, @Query("longitude") Double longitude
    );

    @GET("get_cust_address")
    Call<ResponseBody> getAddress(@Header("access_token") String access_token, @Query("cust_id") String cust_id
            , @Query("address_type") String type);

    @GET("get_profile")
    Call<ResponseBody> getProfile(@Header("access_token") String access_token, @Query("id") String user_id);

    @Multipart
    @POST("edit_cust")
    Call<ResponseBody> editProfile(@Header("access_token") String access_token, @Part MultipartBody.Part file
            , @Part("cust_id") RequestBody cust_id, @Part("cust_name") RequestBody cust_name);

    @GET("find_restaurant")
    Call<ResponseBody> findRestaurant(@Header("access_token") String access_token, @Query("latitude") String latitude
            , @Query("longitude") String longitude);

    @GET("get_product_restaurant")
    Call<ResponseBody> restaurantDetail(@Header("access_token") String access_token, @Query("restaurant_id") String id, @Query("cust_id") String cust_id);

    @GET("get_likes")
    Call<ResponseBody> favRestaurant(@Header("access_token") String access_token, @Query("cust_id") String cust_id);

    @GET("cust_like")
    Call<ResponseBody> like(@Header("access_token") String access_token, @Query("restaurant_id") String restaurant_id
            , @Query("cust_id") String cust_id, @Query("likes") String likes);

    @Multipart
    @POST("add_order")
    Call<ResponseBody> placeOrder(@Header("access_token") String access_token
            , @Part("restaurant_id") RequestBody restaurant_id, @Part("cust_id") RequestBody cust_id
            , @Part("subtotal") RequestBody subtotal, @Part("taxes") RequestBody taxes
            , @Part("delivery_charges") RequestBody delivery_charges, @Part("total_amount") RequestBody total_amount
            , @Part("address_type") RequestBody address_type, @Part("latitude") RequestBody latitude
            , @Part("longitude") RequestBody longitude, @Part("is_payment_done") RequestBody is_payment_done
            , @Part("product_items") RequestBody product_items
    );

    @GET("cust_order_status")
    Call<ResponseBody> changeOrderStatus(@Header("access_token") String access_token, @Query("order_id") String order_id
            , @Query("status") String status);

    @GET("past_order")
    Call<ResponseBody> pastOrder(@Header("access_token") String access_token, @Query("cust_id") String cust_id);

    /*@GET("movie/top_rated")
    Call<MoviesResponse> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/top_rated")
    Call<ResponseBody> getTopRatedMoviesRAW(@Query("api_key") String apiKey);

    @GET("movie/{id}")
    Call<MoviesResponse> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);*/
}
