package com.minikin.android.model;

import org.json.JSONArray;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductDetailsModel implements Serializable{
    String productId, name , description , price , mainImage , hasOffer, offerPrice , store;
    ArrayList<SetAttributeModel> setAttributeModelsList = new ArrayList<>();
    JSONArray jsonArray;

    public ProductDetailsModel(String productId, String name, String description, String price, String mainImage, String hasOffer, String offerPrice, String store, ArrayList<SetAttributeModel> setAttributeModelsList, JSONArray jsonArray) {
        this.productId = productId;
        this.name = name;
        this.description = description;
        this.price = price;
        this.mainImage = mainImage;
        this.hasOffer = hasOffer;
        this.offerPrice = offerPrice;
        this.store = store;
        this.setAttributeModelsList = setAttributeModelsList;
        this.jsonArray = jsonArray;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public String getHasOffer() {
        return hasOffer;
    }

    public void setHasOffer(String hasOffer) {
        this.hasOffer = hasOffer;
    }

    public String getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(String offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public ArrayList<SetAttributeModel> getSetAttributeModelsList() {
        return setAttributeModelsList;
    }

    public void setSetAttributeModelsList(ArrayList<SetAttributeModel> setAttributeModelsList) {
        this.setAttributeModelsList = setAttributeModelsList;
    }

    public JSONArray getJsonArray() {
        return jsonArray;
    }

    public void setJsonArray(JSONArray jsonArray) {
        this.jsonArray = jsonArray;
    }
}
