package com.minikin.android.model;

import java.io.Serializable;

public class AddressModel implements Serializable {
    String address_id,area_id , title , area_name, block , street , avenue, building, floor , apartment , notes , delivery_charge;

    public AddressModel(String address_id, String area_id, String title, String area_name, String block, String street, String avenue, String building, String floor, String apartment, String notes, String delivery_charge) {
        this.address_id = address_id;
        this.area_id = area_id;
        this.title = title;
        this.area_name = area_name;
        this.block = block;
        this.street = street;
        this.avenue = avenue;
        this.building = building;
        this.floor = floor;
        this.apartment = apartment;
        this.notes = notes;
        this.delivery_charge = delivery_charge;
    }

    public String getAddress_id() {
        return address_id;
    }

    public void setAddress_id(String address_id) {
        this.address_id = address_id;
    }

    public String getArea_id() {
        return area_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getAvenue() {
        return avenue;
    }

    public void setAvenue(String avenue) {
        this.avenue = avenue;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDelivery_charge() {
        return delivery_charge;
    }

    public void setDelivery_charge(String delivery_charge) {
        this.delivery_charge = delivery_charge;
    }
}
