package com.minikin.android.model;

import java.io.Serializable;

public class OrderDetailProductsModel implements Serializable {
    String orderProductId,orderName,orderStoreName,orderPrice,orderImage,orderQuantity,orderCartAmount;

    public OrderDetailProductsModel(String orderProductId, String orderName, String orderStoreName, String orderPrice, String orderImage, String orderQuantity, String orderCartAmount) {
        this.orderProductId = orderProductId;
        this.orderName = orderName;
        this.orderStoreName = orderStoreName;
        this.orderPrice = orderPrice;
        this.orderImage = orderImage;
        this.orderQuantity = orderQuantity;
        this.orderCartAmount = orderCartAmount;
    }


    public String getOrderProductId() {
        return orderProductId;
    }

    public void setOrderProductId(String orderProductId) {
        this.orderProductId = orderProductId;
    }

    public String getOrderName() {
        return orderName;
    }

    public void setOrderName(String orderName) {
        this.orderName = orderName;
    }

    public String getOrderStoreName() {
        return orderStoreName;
    }

    public void setOrderStoreName(String orderStoreName) {
        this.orderStoreName = orderStoreName;
    }

    public String getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getOrderImage() {
        return orderImage;
    }

    public void setOrderImage(String orderImage) {
        this.orderImage = orderImage;
    }

    public String getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getOrderCartAmount() {
        return orderCartAmount;
    }

    public void setOrderCartAmount(String orderCartAmount) {
        this.orderCartAmount = orderCartAmount;
    }
}
