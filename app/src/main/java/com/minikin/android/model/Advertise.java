package com.minikin.android.model;

import java.io.Serializable;

public class Advertise implements Serializable {
    String status,message,image,title;

    public Advertise(String status, String message, String image, String title) {
        this.status = status;
        this.message = message;
        this.image = image;
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
