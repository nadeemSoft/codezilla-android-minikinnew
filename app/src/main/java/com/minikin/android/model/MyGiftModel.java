package com.minikin.android.model;

import java.io.Serializable;

public class MyGiftModel implements Serializable {
    String packageId,name,brief,price,packagImageId;

    public MyGiftModel(String packageId, String name, String brief, String price, String packagImageId) {
        this.packageId = packageId;
        this.name = name;
        this.brief = brief;
        this.price = price;
        this.packagImageId = packagImageId;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrief() {
        return brief;
    }

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPackagImageId() {
        return packagImageId;
    }

    public void setPackagImageId(String packagImageId) {
        this.packagImageId = packagImageId;
    }
}
