package com.minikin.android.model;

import java.io.Serializable;
import java.util.ArrayList;

public class SetAttributeModel implements Serializable {
    String setId, setName;
    ArrayList<AttributesModel> attributesModelsLisy = new ArrayList<>();

    public SetAttributeModel(String setId, String setName, ArrayList<AttributesModel> attributesModelsLisy) {
        this.setId = setId;
        this.setName = setName;
        this.attributesModelsLisy = attributesModelsLisy;
    }

    public String getSetId() {
        return setId;
    }

    public void setSetId(String setId) {
        this.setId = setId;
    }

    public String getSetName() {
        return setName;
    }

    public void setSetName(String setName) {
        this.setName = setName;
    }

    public ArrayList<AttributesModel> getAttributesModelsLisy() {
        return attributesModelsLisy;
    }

    public void setAttributesModelsLisy(ArrayList<AttributesModel> attributesModelsLisy) {
        this.attributesModelsLisy = attributesModelsLisy;
    }
}
