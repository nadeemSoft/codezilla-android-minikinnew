package com.minikin.android.model;

public class SubCategoryModel {
    String subCategory,subName,subImage;

    public SubCategoryModel(String subCategory, String subName, String subImage) {
        this.subCategory = subCategory;
        this.subName = subName;
        this.subImage = subImage;
    }

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getSubName() {
        return subName;
    }

    public void setSubName(String subName) {
        this.subName = subName;
    }

    public String getSubImage() {
        return subImage;
    }

    public void setSubImage(String subImage) {
        this.subImage = subImage;
    }
}
