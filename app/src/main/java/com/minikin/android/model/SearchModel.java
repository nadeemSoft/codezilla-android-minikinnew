package com.minikin.android.model;

import java.io.Serializable;

public class SearchModel implements Serializable {
    String productId,productName,productDescriptiob,productPrice,productImage,productHasOffer,productOfferPrice,productFlag;

    public SearchModel(String productId, String productName, String productDescriptiob, String productPrice, String productImage, String productHasOffer, String productOfferPrice, String productFlag) {
        this.productId = productId;
        this.productName = productName;
        this.productDescriptiob = productDescriptiob;
        this.productPrice = productPrice;
        this.productImage = productImage;
        this.productHasOffer = productHasOffer;
        this.productOfferPrice = productOfferPrice;
        this.productFlag = productFlag;
    }


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescriptiob() {
        return productDescriptiob;
    }

    public void setProductDescriptiob(String productDescriptiob) {
        this.productDescriptiob = productDescriptiob;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductHasOffer() {
        return productHasOffer;
    }

    public void setProductHasOffer(String productHasOffer) {
        this.productHasOffer = productHasOffer;
    }

    public String getProductOfferPrice() {
        return productOfferPrice;
    }

    public void setProductOfferPrice(String productOfferPrice) {
        this.productOfferPrice = productOfferPrice;
    }

    public String getProductFlag() {
        return productFlag;
    }

    public void setProductFlag(String productFlag) {
        this.productFlag = productFlag;
    }
}
