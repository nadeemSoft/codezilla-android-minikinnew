package com.minikin.android.model;

import java.io.Serializable;

public class SocialMediaModel implements Serializable{
    String title,link;

    public SocialMediaModel(String title, String link) {
        this.title = title;
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
