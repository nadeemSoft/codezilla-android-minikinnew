package com.minikin.android.model;

import java.io.Serializable;

public class AttributesModel implements Serializable {

    String attributeId, attribute_name;
    private boolean isSelected;

    public AttributesModel(String attributeId, String attribute_name) {
        this.attributeId = attributeId;
        this.attribute_name = attribute_name;
    }

    public String getAttributeId() {
        return attributeId;
    }

    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    public String getAttribute_name() {
        return attribute_name;
    }

    public void setAttribute_name(String attribute_name) {
        this.attribute_name = attribute_name;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
