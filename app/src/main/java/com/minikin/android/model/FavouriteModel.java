package com.minikin.android.model;

import java.io.Serializable;

public class FavouriteModel implements Serializable {
    String favId,productId,favName,favPrice,favImage,favStore;

    public FavouriteModel(String favId, String productId, String favName, String favPrice, String favImage, String favStore) {
        this.favId = favId;
        this.productId = productId;
        this.favName = favName;
        this.favPrice = favPrice;
        this.favImage = favImage;
        this.favStore = favStore;
    }

    public String getFavId() {
        return favId;
    }

    public void setFavId(String favId) {
        this.favId = favId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getFavName() {
        return favName;
    }

    public void setFavName(String favName) {
        this.favName = favName;
    }

    public String getFavPrice() {
        return favPrice;
    }

    public void setFavPrice(String favPrice) {
        this.favPrice = favPrice;
    }

    public String getFavImage() {
        return favImage;
    }

    public void setFavImage(String favImage) {
        this.favImage = favImage;
    }

    public String getFavStore() {
        return favStore;
    }

    public void setFavStore(String favStore) {
        this.favStore = favStore;
    }
}
