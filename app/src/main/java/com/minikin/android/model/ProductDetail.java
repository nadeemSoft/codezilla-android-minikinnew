package com.minikin.android.model;

import java.util.List;

public class ProductDetail {

    private String product_id;
    private String name;
    private String description;
    private String price;
    private String main_image;
    private boolean has_offer;
    private String offer_price;
    private String store;
    private List<Attributes> attributes;
    private List<String> images;

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMain_image() {
        return main_image;
    }

    public void setMain_image(String main_image) {
        this.main_image = main_image;
    }

    public boolean isHas_offer() {
        return has_offer;
    }

    public void setHas_offer(boolean has_offer) {
        this.has_offer = has_offer;
    }

    public String getOffer_price() {
        return offer_price;
    }

    public void setOffer_price(String offer_price) {
        this.offer_price = offer_price;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public List<Attributes> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attributes> attributes) {
        this.attributes = attributes;
    }

    public static class Attributes {


        private String set_id;
        private String set_name;
        private List<SetAttributes> set_attributes;

        public String getSet_id() {
            return set_id;
        }

        public void setSet_id(String set_id) {
            this.set_id = set_id;
        }

        public String getSet_name() {
            return set_name;
        }

        public void setSet_name(String set_name) {
            this.set_name = set_name;
        }

        public List<SetAttributes> getSet_attributes() {
            return set_attributes;
        }

        public void setSet_attributes(List<SetAttributes> set_attributes) {
            this.set_attributes = set_attributes;
        }

        public static class SetAttributes {

            private String attribute_id;
            private String attribute_name;
            private Boolean selected = false;

            public String getAttribute_id() {
                return attribute_id;
            }

            public void setAttribute_id(String attribute_id) {
                this.attribute_id = attribute_id;
            }

            public String getAttribute_name() {
                return attribute_name;
            }

            public void setAttribute_name(String attribute_name) {
                this.attribute_name = attribute_name;
            }

            public Boolean getSelected() {
                return selected;
            }

            public void setSelected(Boolean selected) {
                this.selected = selected;
            }
        }
    }

}
