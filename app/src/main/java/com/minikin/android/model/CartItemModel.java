package com.minikin.android.model;

import java.io.Serializable;

public class CartItemModel implements Serializable {
    String cartId,productId,quantity,productPrice,productImage,productName,storeName,attritute1,attritute2,total;

    public CartItemModel(String cartId, String productId, String quantity, String productPrice, String productImage, String productName, String storeName, String attritute1, String attritute2, String total) {
        this.cartId = cartId;
        this.productId = productId;
        this.quantity = quantity;
        this.productPrice = productPrice;
        this.productImage = productImage;
        this.productName = productName;
        this.storeName = storeName;
        this.attritute1 = attritute1;
        this.attritute2 = attritute2;
        this.total = total;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAttritute1() {
        return attritute1;
    }

    public void setAttritute1(String attritute1) {
        this.attritute1 = attritute1;
    }

    public String getAttritute2() {
        return attritute2;
    }

    public void setAttritute2(String attritute2) {
        this.attritute2 = attritute2;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
