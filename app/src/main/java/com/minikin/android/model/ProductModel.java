package com.minikin.android.model;

import java.io.Serializable;

public class ProductModel implements Serializable{
    String productId,productName,productDescription,productPrice,productImage,productHasOffer,productOfferPrice,flag;



    public ProductModel(String productId, String productName, String productDescription, String productPrice, String productImage, String productHasOffer, String productOfferPrice, String flag) {
        this.productId = productId;
        this.productName = productName;
        this.productDescription = productDescription;
        this.productPrice = productPrice;
        this.productImage = productImage;
        this.productHasOffer = productHasOffer;
        this.productOfferPrice = productOfferPrice;
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductHasOffer() {
        return productHasOffer;
    }

    public void setProductHasOffer(String productHasOffer) {
        this.productHasOffer = productHasOffer;
    }

    public String getProductOfferPrice() {
        return productOfferPrice;
    }

    public void setProductOfferPrice(String productOfferPrice) {
        this.productOfferPrice = productOfferPrice;
    }
}
