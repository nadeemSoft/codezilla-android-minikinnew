package com.minikin.android.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.util.MySession;

import java.util.Locale;

public class AddAddressActivity extends AppCompatActivity {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    private Activity activity = AddAddressActivity.this;
    private MySession mySession;
    private TextView tvTittle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_add_address);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        tvTittle = findViewById(R.id.toolbar_two_tv_title);
        mySession = new MySession(activity);
        setLangRecreate(WhichLanguage);

    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        Typeface fontEn = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontAr = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/Avenir Roman.otf");
        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);
            tvTittle.setTypeface(fontAr);

        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvTittle.setTypeface(fontEn);

        }
    }
}
