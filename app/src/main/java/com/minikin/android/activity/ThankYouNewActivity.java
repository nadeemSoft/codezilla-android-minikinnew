package com.minikin.android.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.minikin.android.R;

public class ThankYouNewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you_new);
    }
}
