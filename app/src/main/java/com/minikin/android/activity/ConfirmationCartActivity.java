package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.AdapterOrderDetail;
import com.minikin.android.adapter.AddressOrderDetail;
import com.minikin.android.adapter.GiftAdapterOrderDetail;
import com.minikin.android.model.CartItemModel;
import com.minikin.android.model.MyGiftModel;
import com.minikin.android.model.OrderDetailProductsModel;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ConfirmationCartActivity extends BaseActivity {
    String getGiftOPtion, orderId;
    private RelativeLayout rl_back, rl_menu, rlSearh;
    private FrameLayout rlCart;

    private RecyclerView rvAddress, rvGift, rvMainCart;
    private ProgressDialog pd;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    private SharedPreferences sp = null;
    private Activity activity = ConfirmationCartActivity.this;
    private CardView cardView;
    private TextView tvConfirmProductName, tvConfirmStoreName, tvConfirmPrice, tvCouponSubTotal, tvCouponDelivery, tvCouponWrapgift, tvCouponDiscount, tvCouponTotalMain, tvBillToInvoice, tvBillToBill, tvBillToDate, tvBillToStatus;
    private TextView tvShipToBill, tvShipToDate, tvShipToStatus, tvShipToInvoice, tvShipToAddress, tvConfirmPay, tvToolBar, tvSubTotalTitle, tvDeliveryTitle, tvWrapGiftTitle, tvCouponDiscountTitle, tvTotalTitle;
    private TextView tvBillTo, tvShipTo, tvBillInvoiceTitle, tvShipInvoiceTitle, tvBillBillTitle, tvShipBillTitle, tvBillDateTilte, tvShipDateTitle, tvBillStastusTitle, tvShipStatusTitle, tvShipAddressTitle;
    private ImageView ivConfirmProduct;
    private TextView tvConfirmSubTotalKdTitle, tvConfirmDeliveryKdTitle, tvConfirmPackagingKdTitle, tvConfirmCouponDiscountKdTitle, tvConfirmGrandTotalKdTitle;
    private LinearLayout llConfirmCouponGiftDelivery;
    private ArrayList<CartItemModel> cartItemList;
    private ArrayList<MyGiftModel> myGiftModelsList;
    private ArrayList<OrderDetailProductsModel> orderDetailProductsModelList;
    private JSONArray jsonArrayAddress = null;
    DecimalFormat precision = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_confirmation_cart, contentFrameLayout);
        initViews();

        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        precision = new DecimalFormat("0.000");

        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        setLangRecreate(WhichLanguage);
        cartItemList = new ArrayList<>();
        myGiftModelsList = new ArrayList<>();
        orderDetailProductsModelList = new ArrayList<>();

        Intent intent = getIntent();
        orderId = intent.getStringExtra("orderId");
        Log.e("OrderID", orderId);


        getGiftOPtion = sp.getString("sendGiftOption", "");
        Log.e("sendGiftOption", getGiftOPtion);
        if (getGiftOPtion.equalsIgnoreCase("visible")) {
            cardView.setVisibility(View.GONE);
            Log.e("sendGiftOption", getGiftOPtion);

        } else if (getGiftOPtion.equalsIgnoreCase("invisible")) {
            cardView.setVisibility(View.GONE);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //orderId = "70";
        // orderId = "110";

        getOrderDetail(orderId);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.toolbar_two_rl_back:
                Intent inHome = new Intent(this, HomeActivityNew.class);
                startActivity(inHome);
                finish();
                break;

            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
                break;
            case R.id.tv_confirm_pay:
                getConfirmandPay(orderId);
                break;
        }
    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        // Typeface fontEn = Typeface.createFromAsset(ConfirmationCartActivity.this.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(ConfirmationCartActivity.this.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);


            tvToolBar.setText("الدفع");


            tvSubTotalTitle.setText("حاصل الجمع");
            tvDeliveryTitle.setText("تكلفه التوصيل");
            tvWrapGiftTitle.setText("تكلفه التغليف");
            tvCouponDiscountTitle.setText("كود الخصم");
            tvTotalTitle.setText("الاجمالي");

          /*  tvConfirmSubTotalKdTitle.setText("د.ك");
            tvConfirmDeliveryKdTitle.setText("د.ك");
            tvConfirmPackagingKdTitle.setText("د.ك");
            tvConfirmCouponDiscountKdTitle.setText("د.ك");
            tvConfirmGrandTotalKdTitle.setText("د.ك");*/

            tvConfirmPay.setText("تأكيد والدفع");


            tvToolBar.setTypeface(fontEnMedium);
            tvShipToAddress.setTypeface(fontEn);
            tvShipToStatus.setTypeface(fontEn);
            tvShipToDate.setTypeface(fontEn);
            tvShipToBill.setTypeface(fontEn);
            tvShipToInvoice.setTypeface(fontEn);
            tvBillToStatus.setTypeface(fontEn);
            tvBillToDate.setTypeface(fontEn);
            tvBillToInvoice.setTypeface(fontEn);
            tvConfirmPay.setTypeface(fontEn);

            tvCouponTotalMain.setTypeface(fontEn);

            tvConfirmProductName.setTypeface(fontEn);
            tvConfirmPrice.setTypeface(fontEn);
            tvCouponDelivery.setTypeface(fontEn);
            tvCouponSubTotal.setTypeface(fontEn);
            tvBillToBill.setTypeface(fontEn);
            tvConfirmStoreName.setTypeface(fontEn);
            tvCouponWrapgift.setTypeface(fontEn);
            tvCouponDiscount.setTypeface(fontEn);

            tvSubTotalTitle.setTypeface(fontEn);
            tvDeliveryTitle.setTypeface(fontEn);
            tvWrapGiftTitle.setTypeface(fontEn);
            tvCouponDiscountTitle.setTypeface(fontEn);

            tvTotalTitle.setTypeface(fontEnMedium);

            tvBillTo.setTypeface(fontEn);
            tvShipTo.setTypeface(fontEn);
            tvBillInvoiceTitle.setTypeface(fontEn);
            tvShipInvoiceTitle.setTypeface(fontEn);
            tvBillBillTitle.setTypeface(fontEn);
            tvShipBillTitle.setTypeface(fontEn);
            tvBillDateTilte.setTypeface(fontEn);
            tvShipDateTitle.setTypeface(fontEn);
            tvBillStastusTitle.setTypeface(fontEn);
            tvShipStatusTitle.setTypeface(fontEn);
            tvShipAddressTitle.setTypeface(fontEn);


            tvConfirmSubTotalKdTitle.setTypeface(fontEn);
            tvConfirmDeliveryKdTitle.setTypeface(fontEn);
            tvConfirmPackagingKdTitle.setTypeface(fontEn);
            tvConfirmCouponDiscountKdTitle.setTypeface(fontEn);
            tvConfirmGrandTotalKdTitle.setTypeface(fontEn);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvToolBar.setTypeface(fontEnMedium);
            tvShipToAddress.setTypeface(fontEn);
            tvShipToStatus.setTypeface(fontEn);
            tvShipToDate.setTypeface(fontEn);
            tvShipToBill.setTypeface(fontEn);
            tvShipToInvoice.setTypeface(fontEn);
            tvBillToStatus.setTypeface(fontEn);
            tvBillToDate.setTypeface(fontEn);
            tvBillToInvoice.setTypeface(fontEn);
            tvConfirmPay.setTypeface(fontEn);

            tvCouponTotalMain.setTypeface(fontEn);

            tvConfirmProductName.setTypeface(fontEn);
            tvConfirmPrice.setTypeface(fontEn);
            tvCouponDelivery.setTypeface(fontEn);
            tvCouponSubTotal.setTypeface(fontEn);
            tvBillToBill.setTypeface(fontEn);
            tvConfirmStoreName.setTypeface(fontEn);
            tvCouponWrapgift.setTypeface(fontEn);
            tvCouponDiscount.setTypeface(fontEn);

            tvSubTotalTitle.setTypeface(fontEn);
            tvDeliveryTitle.setTypeface(fontEn);
            tvWrapGiftTitle.setTypeface(fontEn);
            tvCouponDiscountTitle.setTypeface(fontEn);

            tvTotalTitle.setTypeface(fontEnMedium);

            tvBillTo.setTypeface(fontEn);
            tvShipTo.setTypeface(fontEn);
            tvBillInvoiceTitle.setTypeface(fontEn);
            tvShipInvoiceTitle.setTypeface(fontEn);
            tvBillBillTitle.setTypeface(fontEn);
            tvShipBillTitle.setTypeface(fontEn);
            tvBillDateTilte.setTypeface(fontEn);
            tvShipDateTitle.setTypeface(fontEn);
            tvBillStastusTitle.setTypeface(fontEn);
            tvShipStatusTitle.setTypeface(fontEn);
            tvShipAddressTitle.setTypeface(fontEn);


            tvConfirmSubTotalKdTitle.setTypeface(fontEn);
            tvConfirmDeliveryKdTitle.setTypeface(fontEn);
            tvConfirmPackagingKdTitle.setTypeface(fontEn);
            tvConfirmCouponDiscountKdTitle.setTypeface(fontEn);
            tvConfirmGrandTotalKdTitle.setTypeface(fontEn);

        }
    }

    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_back.setOnClickListener(this);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        llConfirmCouponGiftDelivery = findViewById(R.id.ll_visible);
        llConfirmCouponGiftDelivery.setVisibility(View.GONE);
        rl_menu.setOnClickListener(this);
        tvToolBar = findViewById(R.id.toolbar_two_tv_title);
        tvToolBar.setText("" + "CHECKOUT");
        ivConfirmProduct = findViewById(R.id.iv_confirm_product_image);
        rvAddress = findViewById(R.id.rv_address);
        rvGift = findViewById(R.id.rv_gift);
        rvMainCart = findViewById(R.id.my_main_cart);
        cardView = findViewById(R.id.cardView_send_gift_card_option);
        cardView.setVisibility(View.GONE);
        tvConfirmProductName = findViewById(R.id.tv_confirm_product_name);
        tvConfirmStoreName = findViewById(R.id.tv_confirm_store_name);
        tvConfirmPrice = findViewById(R.id.tv_confirm_price);
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setVisibility(View.GONE);
        rlSearh = findViewById(R.id.rl_search_at_toolbar);
        rlSearh.setVisibility(View.GONE);

        tvCouponSubTotal = findViewById(R.id.tv_confirm_sub_total);
        tvCouponDelivery = findViewById(R.id.tv_confirm_sub_delivery);
        tvCouponWrapgift = findViewById(R.id.tv_confirm_sub_wrap_gift);
        tvCouponDiscount = findViewById(R.id.tv_confirm_sub_coupon_discount);
        tvCouponTotalMain = findViewById(R.id.tv_confirm_sub_total_Main);

        tvBillToInvoice = findViewById(R.id.tv_confirm_invoice_bill_to);
        tvBillToBill = findViewById(R.id.tv_confirm_bill_bill_to);
        tvBillToDate = findViewById(R.id.tv_confirm_date_bill_to);
        tvBillToStatus = findViewById(R.id.tv_confirm_status_bill_to);

        tvShipToInvoice = findViewById(R.id.tv_confirm_invoice_ship_to);
        tvShipToBill = findViewById(R.id.tv_confirm_bill_ship_to);
        tvShipToDate = findViewById(R.id.tv_confirm_date_ship_to);
        tvShipToStatus = findViewById(R.id.tv_confirm_status_ship_to);
        tvShipToAddress = findViewById(R.id.tv_confirm_address_ship_to);
        tvConfirmPay = findViewById(R.id.tv_confirm_pay);
        tvConfirmPay.setOnClickListener(this);
        tvConfirmPay.setVisibility(View.GONE);


        tvSubTotalTitle = findViewById(R.id.tv_sub_total_Title);
        tvDeliveryTitle = findViewById(R.id.tv_delivery_title);
        tvWrapGiftTitle = findViewById(R.id.tv_Wrap_Gift_title);
        tvCouponDiscountTitle = findViewById(R.id.tv_cou_Discount_title);
        tvTotalTitle = findViewById(R.id.tv_totalSub_title);


        tvBillTo = findViewById(R.id.tv_bill_to_title);
        tvShipTo = findViewById(R.id.tv_ship_to_title);
        tvBillInvoiceTitle = findViewById(R.id.tv_bill_invoice_title);
        tvShipInvoiceTitle = findViewById(R.id.tv_ship_invoice_title);
        tvBillBillTitle = findViewById(R.id.tv_bill__bill_to_title);
        tvShipBillTitle = findViewById(R.id.tv_ship_bill_title);
        tvBillDateTilte = findViewById(R.id.tv_bill_date_title);
        tvShipDateTitle = findViewById(R.id.tv_ship_date_title);
        tvBillStastusTitle = findViewById(R.id.tv_bill_status_title);
        tvShipStatusTitle = findViewById(R.id.tv_ship_status_title);
        tvShipAddressTitle = findViewById(R.id.tv_ship_address_title);

        tvConfirmSubTotalKdTitle = findViewById(R.id.tv_confirm_sub_total_kdTitle);
        tvConfirmDeliveryKdTitle = findViewById(R.id.tv_confirm_sub_delivery_kd);
        tvConfirmPackagingKdTitle = findViewById(R.id.tv_confirm_sub_wrap_gift_kd);
        tvConfirmCouponDiscountKdTitle = findViewById(R.id.tv_confirm_coupon_discount_kd);
        tvConfirmGrandTotalKdTitle = findViewById(R.id.tv_confirm_total_Main_kd);

    }


    private void getOrderDetail(final String idOrder) {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_ORDER_DETAIL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseOrderDetail", response);
                orderDetailProductsModelList.clear();
                String cart_amount = null;
                myGiftModelsList.clear();
                jsonArrayAddress = new JSONArray();
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONObject resultJsonObj = objects.getJSONObject("result");
                        String asGift = resultJsonObj.getString("as_gift");
                        String coupon_discount = resultJsonObj.getString("coupon_discount");
                        String delivery_charge = resultJsonObj.getString("delivery_charge");
                        String total_amount = resultJsonObj.getString("total_amount");


                        JSONObject addressJsonObject = resultJsonObj.getJSONObject("address");
                        jsonArrayAddress.put(addressJsonObject);
                        String address_id = addressJsonObject.getString("address_id");
                        String title = addressJsonObject.getString("title");
                        String area_id = addressJsonObject.getString("area_id");
                        String area_name = addressJsonObject.getString("area_name");
                        String block = addressJsonObject.getString("block");
                        String street = addressJsonObject.getString("street");
                        String building = addressJsonObject.getString("building");
                        String floor = addressJsonObject.getString("floor");
                        String apartment = addressJsonObject.getString("apartment");

                        JSONArray jsonArray = resultJsonObj.getJSONArray("products");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectProduct = jsonArray.getJSONObject(i);
                            String product_id = jsonObjectProduct.getString("product_id");
                            String name = jsonObjectProduct.getString("name");
                            String main_image = jsonObjectProduct.getString("main_image");
                            String price = jsonObjectProduct.getString("price");
                            String quantity = jsonObjectProduct.getString("quantity");
                            cart_amount = jsonObjectProduct.getString("cart_amount");
                            String store = jsonObjectProduct.getString("store");

                            OrderDetailProductsModel model = new OrderDetailProductsModel(product_id, name, store, price, main_image, quantity, cart_amount);
                            orderDetailProductsModelList.add(model);
                        }

                        JSONObject objectPackage = resultJsonObj.getJSONObject("package");
                        String package_id = objectPackage.getString("package_id");
                        String price = objectPackage.getString("price");
                        String name = objectPackage.getString("name");
                        String brief = objectPackage.getString("brief");
                        String image = objectPackage.getString("image");


                        MyGiftModel myGiftModel = new MyGiftModel(package_id, name, brief, price, image);
                        myGiftModelsList.add(myGiftModel);


                        RecyclerView.LayoutManager mLayoutManagerAddress = new LinearLayoutManager(getApplicationContext());
                        rvAddress.setLayoutManager(mLayoutManagerAddress);
                        rvAddress.setAdapter(new AddressOrderDetail(jsonArrayAddress, R.layout.adapter_address_confirm_select, activity));
                        rvAddress.setNestedScrollingEnabled(false);

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        rvMainCart.setLayoutManager(mLayoutManager);
                        rvMainCart.setAdapter(new AdapterOrderDetail(orderDetailProductsModelList, activity));
                        rvMainCart.setNestedScrollingEnabled(false);

                        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getApplicationContext());
                        rvGift.setLayoutManager(mLayoutManager3);
                        rvGift.setAdapter(new GiftAdapterOrderDetail(myGiftModelsList, R.layout.adapter_gift_confirm_select, activity));
                        rvGift.setNestedScrollingEnabled(false);

                        llConfirmCouponGiftDelivery.setVisibility(View.VISIBLE);
                        tvConfirmPay.setVisibility(View.VISIBLE);


                        AddTotal(price, coupon_discount, delivery_charge, cart_amount, total_amount);


                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        Toast.makeText(ConfirmationCartActivity.this, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ConfirmationCartActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", pref.getString("WhichLanguage", ""));
                params.put("order_id", idOrder);

                Log.e("OrderDetailParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationCartActivity.this);
        requestQueue.add(request);

    }


    private void getConfirmandPay(final String idOred) {
        Intent intent = new Intent(activity, KNetActivity.class);
        intent.putExtra("orderId", idOred);
        startActivity(intent);
        finish();

        //pd.show();

        /*StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_ORDER_CONFIRMATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseConfirmPay", response);

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");

                    if (status.equalsIgnoreCase("true")) {
                        pd.dismiss();
                        Toast.makeText(ConfirmationCartActivity.this, message, Toast.LENGTH_SHORT).show();
                       *//* Intent intent = new Intent(ConfirmationCartActivity.this, OrderPlacedActivity.class);
                        startActivity(intent);
                        finish();*//*

                        Intent intent = new Intent(activity, KNetActivity.class);
                        intent.putExtra("orderId", orderId);
                        startActivity(intent);
                        finish();

                    } else {
                        pd.dismiss();
                        Toast.makeText(ConfirmationCartActivity.this, message, Toast.LENGTH_SHORT).show();
                        finish();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ConfirmationCartActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();
                Toast.makeText(ConfirmationCartActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("order_id", idOred);

                Log.e("confirm.PARAMS", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ConfirmationCartActivity.this);
        requestQueue.add(request);*/


    }

    private void AddTotal(String afGift, String coupDiscount, String deliveryAddress, String cart_amount, String total_amount) {

        // String itemPrice = sp.getString("totalPrice", "");
        //String delivery = sp.getString("delivery_charge", "");
        // String giftPrice = sp.getString("gift_charge", "");

        double afGiftD = Double.parseDouble(afGift);
        double coupDiscountD = Double.parseDouble(coupDiscount);
        double deliveryAddressD = Double.parseDouble(deliveryAddress);
        double cartAmountD = Double.parseDouble(cart_amount);
        double totalAmountD = Double.parseDouble(total_amount);


        Log.e("lang", WhichLanguage);


        if (WhichLanguage.equalsIgnoreCase("en")) {

            tvCouponSubTotal.setText("" + precision.format(cartAmountD));
            tvCouponDelivery.setText("" + precision.format(deliveryAddressD));
            tvCouponWrapgift.setText("" + precision.format(afGiftD));
            tvCouponDiscount.setText("" + precision.format(coupDiscountD));

            tvCouponTotalMain.setText("" + precision.format(totalAmountD));


        } else if (WhichLanguage.equalsIgnoreCase("ar")) {
            tvCouponWrapgift.setText(afGift);
            tvCouponDiscount.setText(coupDiscount);
            tvCouponDelivery.setText(deliveryAddress);

            tvCouponSubTotal.setText(cart_amount);

            tvCouponTotalMain.setText(total_amount);


        }

        //Double totalPrice = (Double.parseDouble(cart_amount) + Double.parseDouble(deliveryAddress) + Double.parseDouble(afGift) - Double.parseDouble(coupDiscount));
        // Log.e("totalPrice", totalPrice + "");

      /*  tvCouponSubTotal.setText("" + cartAmountD);
        tvCouponDelivery.setText("" + deliveryAddressD);
        tvCouponWrapgift.setText("" + afGiftD);
        tvCouponDiscount.setText("" + coupDiscountD);

        tvCouponTotalMain.setText("" + totalAmountD);*/


    }
}
