package com.minikin.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.minikin.android.R;

import java.util.Locale;

public class OrderPlacedActivity extends BaseActivity {
    private Activity activity = OrderPlacedActivity.this;
    private RelativeLayout rl_back, rl_menu, rlSearch;
    private FrameLayout rlCart;

    private TextView tv_title;
    private TextView tvOrderNoTitle, tvOrderStatusTitle, tvOrderPaymentTitle, tvOrderDateTitle, tvOrderAmountTitle;
    private TextView tvOrderNo, tvOrderStatus, tvOrderPayment, tvOrderDate, tvOrderAmount;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.act_order_placed, contentFrameLayout);
        initViews();
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        setLangRecreate(WhichLanguage);

    }

    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tv_title.setText("Order Placed");
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setVisibility(View.GONE);
        rlSearch = findViewById(R.id.rl_search_at_toolbar);
        rlSearch.setVisibility(View.GONE);
        tvOrderNoTitle = findViewById(R.id.tv_order_no_title);
        tvOrderStatusTitle = findViewById(R.id.tv_order_status_order_conf_title);
        tvOrderPaymentTitle = findViewById(R.id.tv_order_payment_title);
        tvOrderDateTitle = findViewById(R.id.tv_order_date_title);
        tvOrderAmountTitle = findViewById(R.id.tv_order_amount_title);
        tvOrderNo = findViewById(R.id.tv_order_no);
        tvOrderStatus = findViewById(R.id.tv_order_status);
        tvOrderPayment = findViewById(R.id.tv_order_patment_id);
        tvOrderDate = findViewById(R.id.tv_order_date);
        tvOrderAmount = findViewById(R.id.tv_order_amount);
        rl_back.setOnClickListener(this);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rl_menu.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.toolbar_two_rl_back:
                Intent intent = new Intent(this, HomeActivityNew.class);
                startActivity(intent);
                finish();
                break;
            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
                break;


        }
    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
       // Typeface fontEn = Typeface.createFromAsset(OrderPlacedActivity.this.getAssets(), "fonts/Avenir Roman Font Download.otf");

        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");

        Typeface fontAr = Typeface.createFromAsset(OrderPlacedActivity.this.getAssets(), "fonts/Avenir Roman.otf");
        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);

            tvOrderNoTitle.setTypeface(fontAr);
            tvOrderStatusTitle.setTypeface(fontAr);
            tvOrderPaymentTitle.setTypeface(fontAr);
            tvOrderDateTitle.setTypeface(fontAr);
            tvOrderAmountTitle.setTypeface(fontAr);
            tvOrderNo.setTypeface(fontAr);
            tvOrderStatus.setTypeface(fontAr);
            tvOrderPayment.setTypeface(fontAr);
            tvOrderDate.setTypeface(fontAr);
            tvOrderAmount.setTypeface(fontAr);
            tv_title.setTypeface(fontAr);

        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvOrderNoTitle.setTypeface(fontEn);
            tvOrderStatusTitle.setTypeface(fontEn);
            tvOrderPaymentTitle.setTypeface(fontEn);
            tvOrderDateTitle.setTypeface(fontEn);
            tvOrderAmountTitle.setTypeface(fontEn);
            tvOrderNo.setTypeface(fontEn);
            tvOrderStatus.setTypeface(fontEn);
            tvOrderPayment.setTypeface(fontEn);
            tvOrderDate.setTypeface(fontEn);
            tvOrderAmount.setTypeface(fontEn);
            tv_title.setTypeface(fontEnMedium);


        }
    }
}
