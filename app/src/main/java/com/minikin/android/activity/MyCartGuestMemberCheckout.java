package com.minikin.android.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.AdapterGiftSelect;
import com.minikin.android.adapter.CountryDialogAdapter;
import com.minikin.android.model.AreaModel;
import com.minikin.android.model.MyGiftModel;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.CountryCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class MyCartGuestMemberCheckout extends BaseActivity {
    private Activity activity = MyCartGuestMemberCheckout.this;
    private String areaId, chkPayment = "";
    double couponDis = 0.000, deliPr = 0.000;

    String email = null;
    private RecyclerView rvGuestGiftPackaging;
    private LinearLayout llGiftFromToShow = null;

    private LinearLayout llShippingAddressGuest = null, llShoppingAddressShow = null;
    private EditText etAddressName, etBlock, etStreet, etAvenue, etBuilding, etFloor, etApartment, etExtra, etFromGuest, etToGuest, etCouponGuest, etMobileGuest, etEamilAddressNew;
    private TextView tvAreaGuest, tvAddShippingAddress, tvSelectShippingAddressTitle, tvTitle, tvSendGiftTitle, tvAddressTitle, tvPaymentTitle, tvKnet, tvCashOnDelivery, tvGuestChekOut, tvCartCounter, tvGiftOptionAsGuest;
    private TextView tvSubTotalTitleGuest, tvSubTotalGuest, tvSubDeliveryTitleGuest, tvSubdeliveryGuest, tvWrapGiftTitleGuest, tvWrapGiftGuest, tvCouponDisTitleGuest, tvCouponDiscountGuest, tvTotalSubTitleGuest, tvTotalMainGuest;
    private Button btnApplyCoupon;
    private LinearLayout llCashOnDelivery, llKnet;
    private ProgressDialog pd;
    private ArrayList<AreaModel> areaList = null;
    private SharedPreferences sp = null;
    private SharedPreferences.Editor editorSp = null;
    private SharedPreferences pref;
    private String WhichLanguage = "";
    private CardView cardViewSubTotal, cvCoupon;
    private RadioButton radioButtonKPay, radioButtonCashOnDelivery, radioGiftYes, radioGiftNo;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private ArrayList<MyGiftModel> myGiftModelsListGuest;
    private TextView tvConfirmSubTotalKdTitle, tvConfirmDeliveryKdTitle, tvConfirmPackagingKdTitle, tvConfirmCouponDiscountKdTitle, tvConfirmGrandTotalKdTitle;
    private RelativeLayout rl_back, rl_menu;
    private FrameLayout rlCart;
    DecimalFormat precision = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Runtime.getRuntime().maxMemory();

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_my_cart_guest_member, contentFrameLayout);
        initWidgets();
        precision  = new DecimalFormat("0.000");

        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        areaList = new ArrayList<>();
        myGiftModelsListGuest = new ArrayList<>();
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editorSp = sp.edit();
        setLangRecreate(WhichLanguage);
        getAreaId();

        radioButtonKPay.setChecked(true);
        radioButtonCashOnDelivery.setChecked(false);
        chkPayment = "2";
        editorSp.putString("paymentDetails", String.valueOf(2));
        editorSp.commit();

    }

    @Override
    protected void onResume() {
        super.onResume();
        tvCartCounter.setText("" + HomeActivityNew.cartCounter);

    }

    private void initWidgets() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rl_menu.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setOnClickListener(this);
        rlCart.setVisibility(View.GONE);
        tvTitle = findViewById(R.id.toolbar_two_tv_title);
        tvTitle.setText("Guest Checkout");
        llShippingAddressGuest = findViewById(R.id.ll_shipping_address_guest);
        llShoppingAddressShow = findViewById(R.id.ll_shipping_address_guest_Show);
        etAddressName = findViewById(R.id.et_address_name_guest);
        tvAreaGuest = findViewById(R.id.tv_area_name_guest);
        etBlock = findViewById(R.id.et_block_guest);
        etStreet = findViewById(R.id.et_street_guest);
        etAvenue = findViewById(R.id.et_avenue_guest);
        tvGiftOptionAsGuest = findViewById(R.id.tv_gift_option_guest);
        llGiftFromToShow = findViewById(R.id.ll_gift_from_to_show);

        tvCartCounter = findViewById(R.id.tv_cart_counter);
        etBuilding = findViewById(R.id.et_building_guest);
        etFloor = findViewById(R.id.et_floor_guest);
        etApartment = findViewById(R.id.et_aparment_guest);
        etExtra = findViewById(R.id.et_extra_guest);
        etMobileGuest = findViewById(R.id.et_mobile_no_guest);
        tvAddShippingAddress = findViewById(R.id.tv_add_address_guest);
        etEamilAddressNew = findViewById(R.id.et_email_adddress_new);
        tvSelectShippingAddressTitle = findViewById(R.id.tv_select_shipping_add_title_guest);
        cardViewSubTotal = findViewById(R.id.cv_sub_total);
        cardViewSubTotal.setVisibility(View.GONE);
        cvCoupon = findViewById(R.id.cv_coupon);
        cvCoupon.setVisibility(View.GONE);

        rvGuestGiftPackaging = findViewById(R.id.rv_gift_guest_checkout);

        radioGiftNo = findViewById(R.id.rdNo_guest);
        radioGiftYes = findViewById(R.id.rdYes_guest);
        radioGiftYes.setOnClickListener(this);
        radioGiftNo.setOnClickListener(this);
        tvSendGiftTitle = findViewById(R.id.tv_send_giftCard);
        etFromGuest = findViewById(R.id.etFrom_guest);
        etToGuest = findViewById(R.id.etTo_guest);
        tvAddressTitle = findViewById(R.id.tv_select_address_title);
        etCouponGuest = findViewById(R.id.et_coupon_guest);
        btnApplyCoupon = findViewById(R.id.btn_coupon_guest);
        tvPaymentTitle = findViewById(R.id.tv_payment_details_title_guest);
        tvKnet = findViewById(R.id.tv_kNet_guest);
        tvCashOnDelivery = findViewById(R.id.tv_Cash_Devivery_title_guest);

        tvSubTotalTitleGuest = findViewById(R.id.tv_sub_total_Title_guest);
        tvSubTotalGuest = findViewById(R.id.tv_sub_total_guest);
        tvSubDeliveryTitleGuest = findViewById(R.id.tv_delivery_title_guest);
        tvSubdeliveryGuest = findViewById(R.id.tv_sub_delivery_guest);
        tvWrapGiftTitleGuest = findViewById(R.id.tv_Wrap_Gift_title_guest);
        tvWrapGiftGuest = findViewById(R.id.tv_sub_wrap_gift_guest);
        tvCouponDisTitleGuest = findViewById(R.id.tv_coupon_Discount_title_guest);
        tvCouponDiscountGuest = findViewById(R.id.tv_sub_coupon_discount_guest);
        tvTotalSubTitleGuest = findViewById(R.id.tv_totalSub_title_guest);
        tvTotalMainGuest = findViewById(R.id.tv_sub_total_Main_guest);
        tvGuestChekOut = findViewById(R.id.tv_next_product_guest);
        llCashOnDelivery = findViewById(R.id.ll_radio_cash_guest);
        llKnet = findViewById(R.id.ll_radio_k_guest);

        radioButtonKPay = findViewById(R.id.radio_k_net_guest);
        radioButtonCashOnDelivery = findViewById(R.id.radio_cash_Delivery_guest);

        llShoppingAddressShow.setVisibility(View.GONE);
        llCashOnDelivery.setOnClickListener(this);
        llKnet.setOnClickListener(this);
        tvGuestChekOut.setOnClickListener(this);
        btnApplyCoupon.setOnClickListener(this);
        llShoppingAddressShow.setOnClickListener(this);
        tvAreaGuest.setOnClickListener(this);
        tvAddShippingAddress.setOnClickListener(this);


        tvConfirmSubTotalKdTitle = findViewById(R.id.tv_confirm_sub_total_kdTitle);
        tvConfirmDeliveryKdTitle = findViewById(R.id.tv_confirm_sub_delivery_kd);
        tvConfirmPackagingKdTitle = findViewById(R.id.tv_confirm_sub_wrap_gift_kd);
        tvConfirmCouponDiscountKdTitle = findViewById(R.id.tv_confirm_coupon_discount_kd);
        tvConfirmGrandTotalKdTitle = findViewById(R.id.tv_confirm_total_Main_kd);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {

            case R.id.toolbar_two_rl_back:
                finish();
                break;

            case R.id.toolbar_rl_menutwo:
                //openDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));

                    drawer.closeDrawer(GravityCompat.START);

                } else {

                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;

            case R.id.ll_radio_cash_guest:
                radioButtonKPay.setChecked(false);
                radioButtonCashOnDelivery.setChecked(true);
                chkPayment = "1";
                editorSp.putString("paymentDetails", String.valueOf(1));
                editorSp.commit();
                break;
            case R.id.ll_radio_k_guest:
                radioButtonKPay.setChecked(true);
                radioButtonCashOnDelivery.setChecked(false);
                chkPayment = "2";
                editorSp.putString("paymentDetails", String.valueOf(2));
                editorSp.commit();
                break;

            case R.id.rl_cart_icon_toolbar:
                finish();
                break;

            case R.id.ll_shipping_address_guest_Show:
                llShoppingAddressShow.setVisibility(View.GONE);
                llShippingAddressGuest.setVisibility(View.VISIBLE);
                break;

            case R.id.tv_area_name_guest:
                showDialog(activity, areaList);
                break;
            case R.id.tv_add_address_guest:
                llShoppingAddressShow.setVisibility(View.VISIBLE);

                llShippingAddressGuest.setVisibility(View.GONE);
                break;
            case R.id.tv_next_product_guest:

                if (WhichLanguage.equalsIgnoreCase("en")){

                    if (getValidationCheckOut()) {

                        if (radioGiftYes.isChecked()) {



                            Log.e("packageIDGuest", sp.getString("packageIdGuest", ""));


                            String urlYes = null;
                            try {
                                urlYes = AppConstants.GUEST_CHECK_OUT + "?device_id=" + sp.getString("deviceId", "") + "&title=" + tvAddressTitle.getText().toString()
                                        + "&area_id=" + URLEncoder.encode(areaId, "utf-8") + "&block=" + etBlock.getText().toString() + "&street=" + etStreet.getText().toString() + "&avenue=" + etAvenue.getText().toString()
                                        + "&building=" + etBuilding.getText().toString() + "&floor=" + etFloor.getText().toString() + "&apartment=" + etApartment.getText().toString() + "&notes=" + etExtra.getText().toString() + "&mobile=" + etMobileGuest.getText().toString()
                                        + "&as_guest=" + "1" + "&as_gift=" + "1" + "&gift_card=" + "1" + "&card_from=" + etFromGuest.getText().toString() + "&card_to" + etToGuest.getText().toString() + "&package_id=" + sp.getString("packageIdGuest", "") + "&lang=" + pref.getString("WhichLanguage", "");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                                Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();

                            }

                            Log.e("urlYes", urlYes);
                            getCheckOutAsGuest(urlYes);

                        } else if (radioGiftNo.isChecked()) {
                            Log.e("packageIDGuest", sp.getString("packageIdGuest", ""));

                            String urlNo = null;
                            try {
                                urlNo = AppConstants.GUEST_CHECK_OUT + "?device_id=" + sp.getString("deviceId", "") + "&title=" + tvAddressTitle.getText().toString()
                                        + "&area_id=" + URLEncoder.encode(areaId, "utf-8") + "&block=" + etBlock.getText().toString() + "&street=" + etStreet.getText().toString() + "&avenue=" + etAvenue.getText().toString()
                                        + "&building=" + etBuilding.getText().toString() + "&floor=" + etFloor.getText().toString() + "&apartment=" + etApartment.getText().toString() + "&notes=" + etExtra.getText().toString() + "&mobile=" + etMobileGuest.getText().toString()
                                        + "&as_guest=" + "1"  + "&as_gift=" + "0" + "&gift_card=" + "0"  + "&package_id=" + sp.getString("packageIdGuest", "") +"&lang=" + pref.getString("WhichLanguage", "");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                                Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                            }

                            Log.e("urlNo", urlNo);
                            getCheckOutAsGuest(urlNo);

                        }
                    }

                }else {
                    if (getValidationCheckOutUrdu()) {

                        if (radioGiftYes.isChecked()) {



                            Log.e("packageIDGuest", sp.getString("packageIdGuest", ""));


                            String urlYes = null;
                            try {
                                urlYes = AppConstants.GUEST_CHECK_OUT + "?device_id=" + sp.getString("deviceId", "") + "&title=" + tvAddressTitle.getText().toString()
                                        + "&area_id=" + URLEncoder.encode(areaId, "utf-8") + "&block=" + etBlock.getText().toString() + "&street=" + etStreet.getText().toString() + "&avenue=" + etAvenue.getText().toString()
                                        + "&building=" + etBuilding.getText().toString() + "&floor=" + etFloor.getText().toString() + "&apartment=" + etApartment.getText().toString() + "&notes=" + etExtra.getText().toString() + "&mobile=" + etMobileGuest.getText().toString()
                                        + "&as_guest=" + "1" + "&as_gift=" + "1" + "&gift_card=" + "1" + "&card_from=" + etFromGuest.getText().toString() + "&card_to" + etToGuest.getText().toString() + "&package_id=" + sp.getString("packageIdGuest", "") + "&lang=" + pref.getString("WhichLanguage", "");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                                Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();

                            }

                            Log.e("urlYes", urlYes);
                            getCheckOutAsGuest(urlYes);

                        } else if (radioGiftNo.isChecked()) {
                            Log.e("packageIDGuest", sp.getString("packageIdGuest", ""));

                            String urlNo = null;
                            try {
                                urlNo = AppConstants.GUEST_CHECK_OUT + "?device_id=" + sp.getString("deviceId", "") + "&title=" + tvAddressTitle.getText().toString()
                                        + "&area_id=" + URLEncoder.encode(areaId, "utf-8") + "&block=" + etBlock.getText().toString() + "&street=" + etStreet.getText().toString() + "&avenue=" + etAvenue.getText().toString()
                                        + "&building=" + etBuilding.getText().toString() + "&floor=" + etFloor.getText().toString() + "&apartment=" + etApartment.getText().toString() + "&notes=" + etExtra.getText().toString() + "&mobile=" + etMobileGuest.getText().toString()
                                        + "&as_guest=" + "1"  + "&as_gift=" + "0" + "&gift_card=" + "0"  + "&package_id=" + sp.getString("packageIdGuest", "") + "&lang=" + pref.getString("WhichLanguage", "");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                                Toast.makeText(activity, "" + e, Toast.LENGTH_SHORT).show();
                            }

                            Log.e("urlNo", urlNo);
                            getCheckOutAsGuest(urlNo);

                        }
                    }

                }

                break;

            case R.id.rdNo_guest:
                llGiftFromToShow.setVisibility(View.GONE);

                break;
            case R.id.rdYes_guest:
                llGiftFromToShow.setVisibility(View.VISIBLE);

                break;


        }
    }

    private Boolean getValidationCheckOut() {
        email = etEamilAddressNew.getText().toString().trim();
        if (etAddressName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter a Address Title", Toast.LENGTH_SHORT).show();
            return false;
        } else if (tvAreaGuest.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Select Area", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etBlock.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter a Valid Block", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etStreet.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter a Valid Street", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etBuilding.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter a Building", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etMobileGuest.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter a Mobile Number", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etMobileGuest.getText().toString().length() != 8) {
            Toast.makeText(activity, "Please Enter a Correct Mobile Number", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etEamilAddressNew.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter a Email Address", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!email.matches(emailPattern)) {
            Toast.makeText(activity, "Please Enter Valid Email Address", Toast.LENGTH_SHORT).show();
            return false;
        } else if (chkPayment.equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Select Payment", Toast.LENGTH_SHORT).show();
            return false;

        }


        return true;
    }


    private Boolean getValidationCheckOutUrdu() {
        email = etEamilAddressNew.getText().toString().trim();
        if (etAddressName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "يرحى إدخال اسم للعنوان", Toast.LENGTH_SHORT).show();
            return false;
        } else if (tvAreaGuest.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "اسم المنطقة", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etBlock.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "يرجى إدخال رقم القطعة الصحيح", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etStreet.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "يرجى إدخال رقم الشارع الصحيح", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etBuilding.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter a Building", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etMobileGuest.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "يرجى إدخال رقم الهاتف", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etMobileGuest.getText().toString().length() != 8) {
            Toast.makeText(activity, "يرجى إدخال رقم هاتف صحيح", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etEamilAddressNew.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "أدخل البريد الإلكتروني", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!email.matches(emailPattern)) {
            Toast.makeText(activity, "يرجى إدخال بريد إلكتروني صحيح", Toast.LENGTH_SHORT).show();
            return false;
        } else if (chkPayment.equalsIgnoreCase("")) {
            Toast.makeText(activity, "يرجى اختيار الدفع", Toast.LENGTH_SHORT).show();
            return false;

        }


        return true;
    }


    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);

            tvTitle.setText("ضيف المحاسبة");
            tvAddressTitle.setHint("عنوان التوصيل");
            etAddressName.setHint("اسم العنوان");
            tvAreaGuest.setHint("المنطقه");
            etBlock.setHint("القطعه ");
            etAvenue.setHint("الجادة");
            etStreet.setHint("الشارع");
            etFloor.setHint("الطابق ");
            etBuilding.setHint("المنزل");
            etMobileGuest.setHint("رقم الهاتف");
            etApartment.setHint("الشقه");
            etEamilAddressNew.setHint("أدخل البريد الإلكتروني");
            etExtra.setHint("تفاصيل اضافيه");
            tvPaymentTitle.setText("خيارات الدفع");
            tvSendGiftTitle.setText("بطاقه التهادي ");
            radioGiftNo.setText("لا");
            radioGiftYes.setText("نعم ");
            tvGiftOptionAsGuest.setText("خيار التغليف");

            tvGuestChekOut.setText("قم بالطلب");

            etFromGuest.setHint("من");
            etToGuest.setHint("الى");


            tvTitle.setTypeface(fontEnMedium);
            tvAreaGuest.setTypeface(fontEn);
            tvAddShippingAddress.setTypeface(fontEn);
            tvSelectShippingAddressTitle.setTypeface(fontEn);
            etExtra.setTypeface(fontEn);
            etApartment.setTypeface(fontEn);
            etFloor.setTypeface(fontEn);
            etBuilding.setTypeface(fontEn);
            etAvenue.setTypeface(fontEn);
            etStreet.setTypeface(fontEn);
            etBlock.setTypeface(fontEn);
            etAddressName.setTypeface(fontEn);
            tvSendGiftTitle.setTypeface(fontEn);
            etToGuest.setTypeface(fontEn);
            etFromGuest.setTypeface(fontEn);
            tvAddressTitle.setTypeface(fontEnMedium);
            etCouponGuest.setTypeface(fontEn);
            btnApplyCoupon.setTypeface(fontEn);
            tvPaymentTitle.setTypeface(fontEnMedium);
            etMobileGuest.setTypeface(fontEn);
            tvKnet.setTypeface(fontEn);
            tvCashOnDelivery.setTypeface(fontEn);
            tvGuestChekOut.setTypeface(fontEn);
            radioGiftNo.setTypeface(fontEn);
            radioGiftYes.setTypeface(fontEn);

            tvSubTotalTitleGuest.setTypeface(fontEn);
            tvSubTotalGuest.setTypeface(fontEn);
            tvSubDeliveryTitleGuest.setTypeface(fontEn);
            tvSubdeliveryGuest.setTypeface(fontEn);
            tvWrapGiftTitleGuest.setTypeface(fontEn);
            tvWrapGiftGuest.setTypeface(fontEn);
            tvCouponDisTitleGuest.setTypeface(fontEn);
            tvCouponDiscountGuest.setTypeface(fontEn);
            tvTotalSubTitleGuest.setTypeface(fontEn);
            tvTotalMainGuest.setTypeface(fontEn);
            etEamilAddressNew.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);
            tvGiftOptionAsGuest.setTypeface(fontEnMedium);


            tvConfirmSubTotalKdTitle.setTypeface(fontEn);
            tvConfirmDeliveryKdTitle.setTypeface(fontEn);
            tvConfirmPackagingKdTitle.setTypeface(fontEn);
            tvConfirmCouponDiscountKdTitle.setTypeface(fontEn);
            tvConfirmGrandTotalKdTitle.setTypeface(fontEn);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            tvTitle.setTypeface(fontEnMedium);
            tvAreaGuest.setTypeface(fontEn);
            tvAddShippingAddress.setTypeface(fontEn);
            tvSelectShippingAddressTitle.setTypeface(fontEn);
            etExtra.setTypeface(fontEn);
            etApartment.setTypeface(fontEn);
            etFloor.setTypeface(fontEn);
            etBuilding.setTypeface(fontEn);
            etAvenue.setTypeface(fontEn);
            etStreet.setTypeface(fontEn);
            etBlock.setTypeface(fontEn);
            etAddressName.setTypeface(fontEn);
            tvSendGiftTitle.setTypeface(fontEn);
            etToGuest.setTypeface(fontEn);
            etFromGuest.setTypeface(fontEn);
            tvAddressTitle.setTypeface(fontEnMedium);
            etCouponGuest.setTypeface(fontEn);
            btnApplyCoupon.setTypeface(fontEn);
            tvPaymentTitle.setTypeface(fontEnMedium);
            etMobileGuest.setTypeface(fontEn);
            tvKnet.setTypeface(fontEn);
            tvCashOnDelivery.setTypeface(fontEn);
            tvGuestChekOut.setTypeface(fontEn);
            radioGiftNo.setTypeface(fontEn);
            radioGiftYes.setTypeface(fontEn);

            tvSubTotalTitleGuest.setTypeface(fontEn);
            tvSubTotalGuest.setTypeface(fontEn);
            tvSubDeliveryTitleGuest.setTypeface(fontEn);
            tvSubdeliveryGuest.setTypeface(fontEn);
            tvWrapGiftTitleGuest.setTypeface(fontEn);
            tvWrapGiftGuest.setTypeface(fontEn);
            tvCouponDisTitleGuest.setTypeface(fontEn);
            tvCouponDiscountGuest.setTypeface(fontEn);
            tvTotalSubTitleGuest.setTypeface(fontEn);
            tvTotalMainGuest.setTypeface(fontEn);
            etEamilAddressNew.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);
            tvGiftOptionAsGuest.setTypeface(fontEnMedium);


            tvConfirmSubTotalKdTitle.setTypeface(fontEn);
            tvConfirmDeliveryKdTitle.setTypeface(fontEn);
            tvConfirmPackagingKdTitle.setTypeface(fontEn);
            tvConfirmCouponDiscountKdTitle.setTypeface(fontEn);
            tvConfirmGrandTotalKdTitle.setTypeface(fontEn);


        }
    }


    private void getAreaId() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.GET, AppConstants.GET_AREA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseArea", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArray = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectResponse = jsonArray.getJSONObject(i);
                            String area_id = jsonObjectResponse.getString("area_id");
                            String area_name = jsonObjectResponse.getString("area_name");

                            AreaModel areaModel = new AreaModel(area_id, area_name);
                            areaList.add(areaModel);


                        }


                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    }

                    getMyGift();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                Log.e("AreaPArams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void showDialog(Activity activity, ArrayList<AreaModel> list) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_country);

        final EditText searchText = (EditText) dialog.findViewById(R.id.country_dialog_search_text);
        ImageButton searchBtn = (ImageButton) dialog.findViewById(R.id.country_dialog_search_btn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestFocus(searchText);
            }
        });

        ListView listView = (ListView) dialog.findViewById(R.id.country_dialog_list_view);

        final CountryDialogAdapter adapter = new CountryDialogAdapter(activity, list, new CountryCallback() {
            @Override
            public void setItemList(int position, ArrayList<AreaModel> list) {
                AreaModel areaModel = list.get(position);
                String areaName = areaModel.getAreaName();
                areaId = areaModel.getAreaId();
                tvAreaGuest.setText("" + areaName);

                dialog.dismiss();

            }


        });
        listView.setAdapter(adapter);
        dialog.show();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });

        searchText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    private void getCheckOutAsGuest(String url) {
        Log.e("URLCHEK",url);

        /*String urlEncoded = Uri.encode(url, ALLOWED_URI_CHARS);
        Log.e("urlEncode",urlEncoded);*/

        pd.show();


        StringRequest request = new StringRequest(Request.Method.GET, url.trim(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseCheckOut", response);

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    String order_id = objects.getString("order_id");

                    if (status.equalsIgnoreCase("true")) {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(activity, ConfirmationCartActivity.class);
                        intent.putExtra("orderId", order_id);
                        startActivity(intent);
                        finish();



                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                        finish();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }


        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);


    }


    private void getMyGift() {

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_GIFT_PACKAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseMyGift", response);
                myGiftModelsListGuest.clear();
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArray = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String package_id = jsonObject.getString("package_id");
                            String name = jsonObject.getString("name");
                            String brief = jsonObject.getString("brief");
                            String price = jsonObject.getString("price");
                            String image = jsonObject.getString("image");

                            MyGiftModel myGiftModel = new MyGiftModel(package_id, name, brief, price, image);
                            myGiftModelsListGuest.add(myGiftModel);

                            //SetDataFromApi();
                        }

                        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getApplicationContext());
                        rvGuestGiftPackaging.setLayoutManager(mLayoutManager3);
                        rvGuestGiftPackaging.setAdapter(new AdapterGiftSelect(myGiftModelsListGuest, R.layout.rowitem_gift, activity));
                        rvGuestGiftPackaging.setNestedScrollingEnabled(false);
                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("lang", pref.getString("WhichLanguage", ""));

                Log.e("langauge", params + "");

                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }


    public void getGiftPriceGuest(int pos) {
        MyGiftModel myGiftModel = myGiftModelsListGuest.get(pos);
        double giftPriceDouble = Double.parseDouble(myGiftModel.getPrice());
        tvWrapGiftGuest.setText(""+precision.format(giftPriceDouble));

        editorSp.putString("delivery_charge", sp.getString("delivery_charge",""));
        editorSp.putString("gift_charge", myGiftModel.getPrice());
        editorSp.commit();
       // AddTotalPrice();


    }



    /*private void AddTotalPrice() {

        String itemPrice = sp.getString("totalPrice", "");
        String delivery = sp.getString("delivery_charge", "");
        if(delivery.equalsIgnoreCase("")){
            delivery = "0.000";
            editorSp.putString("delivery_charge", delivery);
            editorSp.commit();

        }else {

            deliPr = Double.parseDouble(delivery);
            tvSubdeliveryGuest.setText(""+precision.format(deliPr));

        }
        String giftPrice = sp.getString("giftPriceGuest", "");
        String couponDiscount = sp.getString("couponDiscount", "");
        if(couponDiscount.equalsIgnoreCase("")){
            couponDiscount = "0.000";
            editorSp.putString("couponDiscount", couponDiscount);
            editorSp.commit();

            couponDis = Double.parseDouble(couponDiscount);

            tvCouponDiscountGuest.setText(""+precision.format(couponDis));


        }else {
            tvCouponDiscountGuest.setText(""+precision.format(couponDis));

        }

        Log.e("delivery....", delivery + "");
        Log.e("giftPrice....", giftPrice + "");
        Log.e("itemPrice....", itemPrice + "");

        if(!delivery.equalsIgnoreCase("") && !giftPrice.equalsIgnoreCase("") && !couponDiscount.equalsIgnoreCase("")){
            double itmPr = Double.parseDouble(itemPrice);
            tvSubTotalGuest.setText(""+   precision.format(itmPr));
            Double totalPrice = (Double.parseDouble(itemPrice) + Double.parseDouble(delivery) + Double.parseDouble(giftPrice) - Double.parseDouble(couponDiscount));
            //  Float totalPrice = (Float.parseFloat(itemPrice) + Float.parseFloat(delivery) + Float.parseFloat(giftPrice));

            Log.e("totalPrice", totalPrice + "");


            tvTotalMainGuest.setText(""+precision.format(totalPrice));

        }else {
            double itemPr = Double.parseDouble(itemPrice);
            tvSubTotalGuest.setText(""+precision.format(itemPr));

            Toast.makeText(activity,"Delivery or Gift Price is less than 0",Toast.LENGTH_SHORT).show();
        }



    }
*/




    /*private void getCouponValid() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_VALID_COUPON, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Responsecoupon", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {


                        JSONObject jsonObject = objects.getJSONObject("result");
                        String coupon_id = jsonObject.getString("coupon_id");
                        String discount = jsonObject.getString("discount");
                        String discount_value = jsonObject.getString("discount_value");
                        String min_price = jsonObject.getString("min_price");
                        editorSp.putString("couponDiscount", discount);
                        editorSp.commit();


                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                params.put("device_id", sp.getString("deviceId", ""));
                params.put("coupon_code", etCouponGuest.getText().toString());
                Log.e("CouponParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);
    }*/


}
