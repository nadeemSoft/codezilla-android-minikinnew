package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.AdapterAttributesTwo;
import com.minikin.android.adapter.ProductDetailsAdapter;
import com.minikin.android.adapter.SlidingImageAdapter;
import com.minikin.android.model.ProductDetail;
import com.minikin.android.model.ProductDetailsModel;
import com.minikin.android.model.ReletedItemModel;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.RecyclerItemClickListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;

public class ProductDetailActivity extends BaseActivity {
    int count, chkFav = 0;
    String productId, loginValue;
    private RecyclerView rv, rvAttributes;
    private RelativeLayout rl_back, rl_menu, rlSelectQtySub, rlSelectQtyAdd;
    private TextView tv_title, tvDetailsPrice, tvDetailStoreName, tvDetailDescriptionl, tvAddCard, tvSelectQtyCount, tvPopupSuccessfull, tvContinueShop, tvGoCart;
    private TextView tvDelivery, tvSameDay, tvOrdered, tvMayALso, tvAge,tvAgeNew, tvStoreNameBy, tvCartCounter, tvShowOfferPrice;
    private RelativeLayout rl_vp_size;
    private FrameLayout rlCart, frameOfferPrice = null;

    private Activity activity = ProductDetailActivity.this;
    private ViewPager vp;
    private CircleIndicator cir_ind;
    private LinearLayout.LayoutParams layoutParams;
    private LinearLayout llPopUp, llShopping, llAddTocard;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Toolbar tb;
    private ProgressDialog pd;
    private SharedPreferences sp = null;
    private String WhichLanguage = "";
    private ArrayList<ProductDetailsModel> productDetailList;
    private ProductDetailsAdapter productDetailsAdapter;
    private ArrayList<ReletedItemModel> reletedItemList;
    private AdapterAttributesTwo adapterAttributesTwo;
    private ImageView ivFavroit;
    int attributeCount = 0;
    DecimalFormat precision;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        //  this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.act_prod_detail, contentFrameLayout);
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        initViews();
        precision = new DecimalFormat("0.000");
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        loginValue = sp.getString("loginValue", "");
        setLangRecreate(WhichLanguage);
        productDetailList = new ArrayList<>();
        reletedItemList = new ArrayList<>();
        Intent intent = getIntent();
        productId = intent.getStringExtra("product_id");
        Log.e("ProductIdDetail", productId);
        tvCartCounter.setText("" + HomeActivityNew.cartCounter);


    }

    @Override
    protected void onResume() {
        super.onResume();
        tvAddCard.setEnabled(true);
        tvSelectQtyCount.setText("1");
        getProductDetail(productId);
    }


    private void initViews() {
        tb = findViewById(R.id.toolbar);
        setSupportActionBar(tb);
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_back.setOnClickListener(this);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rl_menu.setOnClickListener(this);
        rlSelectQtySub = findViewById(R.id.lay_select_qty_rl_sub);
        rlSelectQtySub.setOnClickListener(this);
        rlSelectQtyAdd = findViewById(R.id.lay_select_qty_rl_add);
        rlSelectQtyAdd.setOnClickListener(this);
        tvSelectQtyCount = findViewById(R.id.tv_select_qty_count);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tvDetailsPrice = findViewById(R.id.tv_detail_price);
        tvDetailStoreName = findViewById(R.id.tv_detail_store_name);
        tvDetailDescriptionl = findViewById(R.id.tv_detail_description);
        tvAddCard = findViewById(R.id.tv_add_card);
        tvStoreNameBy = findViewById(R.id.tv_detail_store_by);
        tvCartCounter = findViewById(R.id.tv_cart_counter);
        ivFavroit = findViewById(R.id.iv_favroite);
        ivFavroit.setOnClickListener(this);
        frameOfferPrice = findViewById(R.id.frame_offer_price);

        tvAddCard.setOnClickListener(this);
        tvDelivery = findViewById(R.id.tv_deleivery);
        tvOrdered = findViewById(R.id.tv_orderedBefore);
        tvMayALso = findViewById(R.id.tv_may_also);
        tvSameDay = findViewById(R.id.tv_same);
        tvAge = findViewById(R.id.tv_age);
        tvAgeNew = findViewById(R.id.tv_age2);
        tvPopupSuccessfull = findViewById(R.id.tv_popup_cart_successfull);
        tvContinueShop = findViewById(R.id.tv_cart_continue_sho);
        tvGoCart = findViewById(R.id.tv_go_to_cart);
        rl_vp_size = findViewById(R.id.act_sch_detail_rl_vp_size);
        vp = findViewById(R.id.act_sch_detail_vp);
        cir_ind = findViewById(R.id.act_sch_detail_cir_ind);
        rv = findViewById(R.id.act_pro_detail_rv);
        rvAttributes = findViewById(R.id.rv_attributes);
        llShopping = findViewById(R.id.ll_shopping);
        llShopping.setOnClickListener(this);
        llAddTocard = findViewById(R.id.ll_add_to_card);
        llAddTocard.setOnClickListener(this);
        llPopUp = findViewById(R.id.ll_pop_up);
        llPopUp.setOnClickListener(this);
        llPopUp.setVisibility(View.GONE);
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setOnClickListener(this);
        tvShowOfferPrice = findViewById(R.id.tv_show_price_offer_detail);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        int height = displaymetrics.heightPixels;
        int MyHeight = ((width * 3) / 4);
        layoutParams = new LinearLayout.LayoutParams(width, MyHeight);
        rl_vp_size.setLayoutParams(layoutParams);

        rv.addOnItemTouchListener(new RecyclerItemClickListener(this, rv, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ReletedItemModel reletedItemModel = reletedItemList.get(position);
                // getProductDetail(reletedItemModel.getProductId());

                Intent intent = new Intent(getApplicationContext(), ProductDetailActivity.class);
                intent.putExtra("product_id", reletedItemModel.getProductId());
                startActivity(intent);

            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));


    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.toolbar_two_rl_back:
                finish();
                break;
            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;
            case R.id.tv_add_card:

                /* if (loginValue.equalsIgnoreCase("login")) {*/
                if (adapterAttributesTwo.attributesList.size() != 0) {


                    List<String> stringList = new ArrayList<>();

                    Log.e("SIZE", ">>>" + adapterAttributesTwo.attributesList.size());
                    for (int i = 0; i < adapterAttributesTwo.attributesList.size(); i++) {

                        ProductDetail.Attributes attributes = adapterAttributesTwo.attributesList.get(i);

                        inner:
                        for (int j = 0; j < attributes.getSet_attributes().size(); j++) {

                            ProductDetail.Attributes.SetAttributes setAttributes = attributes.getSet_attributes().get(j);

                            if (setAttributes.getSelected()) {

                                stringList.add(setAttributes.getAttribute_id());
                                Log.e("Selected Attribute Id's", setAttributes.getAttribute_id());
                                break inner;
                            }
                        }

                    }

                    if (stringList.size() == attributeCount) {

                        if (!tvSelectQtyCount.getText().toString().equalsIgnoreCase("0")) {

                            getProductStock(productId, stringList);


                        } else {

                            if (WhichLanguage.equalsIgnoreCase("en")){
                                Toast.makeText(ProductDetailActivity.this, "Please Select Item Quantity", Toast.LENGTH_SHORT).show();

                            }else {
                                Toast.makeText(ProductDetailActivity.this, "يرجى اختيار الكمية", Toast.LENGTH_SHORT).show();

                            }
                        }

                    } else {
                        Toast.makeText(activity, "Please Select All Attributes", Toast.LENGTH_SHORT).show();
                    }
                } else {

                    if (!tvSelectQtyCount.getText().toString().equalsIgnoreCase("0")) {


                        getProductStockWithoutAtrribute(productId);


                    } else {

                        if (WhichLanguage.equalsIgnoreCase("en")){
                            Toast.makeText(ProductDetailActivity.this, "Please Select Item Quantity", Toast.LENGTH_SHORT).show();

                        }else {
                            Toast.makeText(ProductDetailActivity.this, "يرجى اختيار الكمية", Toast.LENGTH_SHORT).show();

                        }
                    }


                }


               /* } else if (!loginValue.equalsIgnoreCase("login")) {
                    Intent intent = new Intent(activity, LoginActivity.class);
                    startActivity(intent);
                }*/
                break;
            case R.id.ll_pop_up:
                //nestedScrollView.setVisibility(View.VISIBLE);
                //llPopUp.setVisibility(View.GONE);
                break;
            case R.id.ll_add_to_card:
                Intent intentMy = new Intent(activity, MyCartActivityNew.class);
                startActivity(intentMy);
               /* if (loginValue.equalsIgnoreCase("login")) {
                    Intent intent = new Intent(activity, MyCartActivityNew.class);
                    startActivity(intent);
                    llPopUp.setVisibility(View.GONE);
                } else if (!loginValue.equalsIgnoreCase("login")) {
                    Intent intent = new Intent(activity, MyCartGuestMemberCheckout.class);
                    startActivity(intent);
                    llPopUp.setVisibility(View.GONE);

                }*/

                break;
            case R.id.ll_shopping:
                llPopUp.setVisibility(View.GONE);
                tvAddCard.setEnabled(true);
                finish();


                break;
            case R.id.lay_select_qty_rl_sub:
                if (count > 0) {
                    count = count - 1;
                    tvSelectQtyCount.setText("" + count);

                }
                break;
            case R.id.lay_select_qty_rl_add:
                count = count + 1;
                tvSelectQtyCount.setText("" + count);
                break;
            case R.id.iv_favroite:
                addToMyFavroit();
                break;

            case R.id.rl_cart_icon_toolbar:
                Intent intentMyC = new Intent(activity, MyCartActivityNew.class);
                startActivity(intentMyC);
                break;


        }


    }


    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/Avenir Roman Font Download.otf");

        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");

        Typeface fontAr = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/Avenir Roman.otf");
        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);



            tvMayALso.setText("منتجات قد تعجبك");
            tvSameDay.setText("التسليم في نفس اليوم");
            tvGoCart.setText("الذهاب الى سلة المشتريات ");
            tvPopupSuccessfull.setText("نجاح");
            tvContinueShop.setText("الاستمرار في التسوق");
            tvDelivery.setText("نفسه");
            tvAddCard.setText(" اضف الى السلة ");
            tvOrdered.setText("في حال الطلب قبل الساعه ٦ مساءا");
            tvAge.setText("عمر");
            tvStoreNameBy.setText("بواسطة");
            tvAgeNew.setTypeface(fontEn);

            tvSelectQtyCount.setTypeface(fontEnMedium);
            tvAddCard.setTypeface(fontEn);
            tvDetailsPrice.setTypeface(fontEn);
            tv_title.setTypeface(fontEnMedium);
            tvDetailDescriptionl.setTypeface(fontEn);
            tvDetailStoreName.setTypeface(fontEnMedium);
            tvDelivery.setTypeface(fontEn);
            tvMayALso.setTypeface(fontEn);
            tvSameDay.setTypeface(fontEn);
            tvOrdered.setTypeface(fontEn);
            tvAge.setTypeface(fontEn);
            tvGoCart.setTypeface(fontEn);
            tvPopupSuccessfull.setTypeface(fontEn);
            tvContinueShop.setTypeface(fontEn);
            tvStoreNameBy.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            tvSelectQtyCount.setTypeface(fontEnMedium);
            tvAddCard.setTypeface(fontEn);
            tvDetailsPrice.setTypeface(fontEn);
            tv_title.setTypeface(fontEnMedium);
            tvDetailDescriptionl.setTypeface(fontEn);
            tvDetailStoreName.setTypeface(fontEnMedium);
            tvDelivery.setTypeface(fontEn);
            tvMayALso.setTypeface(fontEn);
            tvSameDay.setTypeface(fontEn);
            tvOrdered.setTypeface(fontEn);
            tvAge.setTypeface(fontEn);
            tvGoCart.setTypeface(fontEn);
            tvPopupSuccessfull.setTypeface(fontEn);
            tvContinueShop.setTypeface(fontEn);
            tvStoreNameBy.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);
            tvAgeNew.setTypeface(fontEn);



        }
    }

    private void getProductDetail(final String idProduct) {

        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_PRODUCT_DETAIL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("detailResponse", response + "");

                productDetailList.clear();

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {

                        JSONArray resultJsonArray = objects.getJSONArray("result");
                        JSONObject finalObject = resultJsonArray.getJSONObject(0);

                        Gson gson = new Gson();
                        ProductDetail productDetail = gson.fromJson(finalObject.toString(), ProductDetail.class);

                        attributeCount = productDetail.getAttributes().size();
                        Log.e("ATTRIBUTECOUNT..", attributeCount + "");


                        adapterAttributesTwo = new AdapterAttributesTwo(activity, productDetail.getAttributes());
                        LinearLayoutManager linearLayoutManagerAtt = new LinearLayoutManager(ProductDetailActivity.this, LinearLayoutManager.VERTICAL, false);
                        rvAttributes.setLayoutManager(linearLayoutManagerAtt);
                        rvAttributes.setAdapter(adapterAttributesTwo);
                        rvAttributes.setNestedScrollingEnabled(false);


                        double price = Double.parseDouble(productDetail.getPrice());


                        if (WhichLanguage.equalsIgnoreCase("en")) {

                            if (productDetail.isHas_offer()) {
                                frameOfferPrice.setVisibility(View.VISIBLE);
                                tvShowOfferPrice.setVisibility(View.VISIBLE);
                                double priceOffer = Double.parseDouble(productDetail.getOffer_price());

                                tvDetailsPrice.setText("KD" + " " + precision.format(priceOffer));
                                tvShowOfferPrice.setText("KD" + " " + precision.format(price));


                            } else if (!productDetail.isHas_offer()) {
                                frameOfferPrice.setVisibility(View.GONE);
                                tvShowOfferPrice.setVisibility(View.GONE);
                                // tvShowOfferPrice.setText("KD"+" "+precision.format(price));
                                tvDetailsPrice.setText("KD" + " " + precision.format(price));

                            }


                            // tvDetailsPrice.setText("KD" + " " + precision.format(price));
                            tvDetailDescriptionl.setText("" + productDetail.getDescription());
                            tvDetailStoreName.setText("" + productDetail.getStore());
                            tv_title.setText("" + productDetail.getName());

                            List imageList = productDetail.getImages();
                            Log.e("CHECKIMAGLIST THREE", imageList + "");
                            if (imageList.size() != 0) {
                                JSONArray jsonArrayJ = new JSONArray();
                                for (int img = 0; img < imageList.size(); img++) {
                                    jsonArrayJ.put(imageList.get(img));

                                }

                                vp.setAdapter(new SlidingImageAdapter(activity, jsonArrayJ));

                            } else {
                                JSONArray jsonArray1 = new JSONArray();
                                jsonArray1.put(productDetail.getMain_image());
                                vp.setAdapter(new SlidingImageAdapter(activity, jsonArray1));

                            }

                            cir_ind.setViewPager(vp);


                        } else if (WhichLanguage.equalsIgnoreCase("ar")) {


                            if (productDetail.isHas_offer()) {
                                frameOfferPrice.setVisibility(View.VISIBLE);
                                tvShowOfferPrice.setVisibility(View.VISIBLE);
                                double priceOffer = Double.parseDouble(productDetail.getOffer_price());

                                tvDetailsPrice.setText("KD" + " " + productDetail.getOffer_price());
                                tvShowOfferPrice.setText("KD" + " " + productDetail.getPrice());



                              /*  tvDetailsPrice.setText("KD" + " " + precision.format(priceOffer));
                                tvShowOfferPrice.setText("KD" + " " + precision.format(price));
*/
                               /* tvDetailsPrice.setText("د.ك" + " " + precision.format(priceOffer));
                                tvShowOfferPrice.setText("د.ك" + " " + precision.format(price));*/


                            } else if (!productDetail.isHas_offer()) {
                                frameOfferPrice.setVisibility(View.GONE);
                                tvShowOfferPrice.setVisibility(View.GONE);
                                //tvDetailsPrice.setText("د.ك" + " " + precision.format(price));
                                tvDetailsPrice.setText("KD" + " " + productDetail.getPrice());


                            }


                            // tvDetailsPrice.setText("KD" + " " + precision.format(price));
                            tvDetailDescriptionl.setText("" + productDetail.getDescription());
                            tvDetailStoreName.setText("" + productDetail.getStore());
                            tv_title.setText("" + productDetail.getName());

                            List imageList = productDetail.getImages();
                            Log.e("CHECKIMAGLIST THREE", imageList + "");
                            if (imageList.size() != 0) {
                                JSONArray jsonArrayJ = new JSONArray();
                                for (int img = 0; img < imageList.size(); img++) {
                                    jsonArrayJ.put(imageList.get(img));

                                }

                                vp.setAdapter(new SlidingImageAdapter(activity, jsonArrayJ));

                            } else {
                                JSONArray jsonArray1 = new JSONArray();
                                jsonArray1.put(productDetail.getMain_image());
                                vp.setAdapter(new SlidingImageAdapter(activity, jsonArray1));

                            }

                            cir_ind.setViewPager(vp);


                        }


                    }


                    getReletedItem(productId);


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ProductDetailActivity.this, "Exception One" + e, Toast.LENGTH_SHORT).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("product_id", idProduct);
                //params.put("atribute_id", sp.getString("WhichLanguage", ""));

                Log.e("detailParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetailActivity.this);
        requestQueue.add(request);


    }


    private void getReletedItem(final String idProduct) {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_RELETED_ITEMS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ReletedResponse", response + "");
                reletedItemList.clear();

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray resultJsonArray = objects.getJSONArray("result");

                        for (int i = 0; i < resultJsonArray.length(); i++) {
                            JSONObject jsonObjectResponse = resultJsonArray.getJSONObject(i);
                            String productIdReleted = jsonObjectResponse.getString("product_id");
                            String nameReleted = jsonObjectResponse.getString("name");
                            String imageReleted = jsonObjectResponse.getString("image");

                            ReletedItemModel reletedItemModel = new ReletedItemModel(productIdReleted, nameReleted, imageReleted);
                            reletedItemList.add(reletedItemModel);

                        }

                    } else {
                        pd.dismiss();
                        Toast.makeText(ProductDetailActivity.this, message, Toast.LENGTH_SHORT).show();

                    }


                    productDetailsAdapter = new ProductDetailsAdapter(reletedItemList, R.layout.rowitem_pro_hori, activity);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ProductDetailActivity.this, LinearLayoutManager.HORIZONTAL, false);
                    rv.setLayoutManager(linearLayoutManager);
                    rv.setAdapter(productDetailsAdapter);
                    rv.setNestedScrollingEnabled(false);
                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ProductDetailActivity.this, "Exception Four" + e, Toast.LENGTH_SHORT).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("product_id", idProduct);
                Log.e("CardParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetailActivity.this);
        requestQueue.add(request);
    }


    private void getProductStock(final String idProduct, final List<String> stringList) {

        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_PRODUCT_STOCK, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("StockResponse", response + "");

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    String stock = objects.getString("stock");

                    if (status.equalsIgnoreCase("true") || !stock.equalsIgnoreCase("0")) {
                        addItemInCart(idProduct, stringList);

                    } else {
                        pd.dismiss();
                        Toast.makeText(ProductDetailActivity.this, message, Toast.LENGTH_SHORT).show();

                    }

                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ProductDetailActivity.this, "Exception Two" + e, Toast.LENGTH_SHORT).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("product_id", idProduct);
                for (int i = 0; i < stringList.size(); i++) {
                    params.put("attribute_" + i + 1, stringList.get(i));
                }
                Log.e("StokParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetailActivity.this);
        requestQueue.add(request);
    }


    private void addItemInCart(final String idProduct, final List<String> stringList) {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_ADD_ITEM_CARD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("CardAddResponse", response + "");

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        pd.dismiss();
                        // Toast.makeText(ProductDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                        llPopUp.setVisibility(View.VISIBLE);
                        llPopUp.setFocusable(true);
                        tvAddCard.setEnabled(false);

                        getCartForCounter();


                    } else {
                        pd.dismiss();
                        Toast.makeText(ProductDetailActivity.this, message, Toast.LENGTH_SHORT).show();

                    }
                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ProductDetailActivity.this, "Exception Three" + e, Toast.LENGTH_SHORT).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                params.put("device_id", sp.getString("deviceId", ""));
                params.put("product_id", idProduct);
                for (int i = 0; i < stringList.size(); i++) {
                    params.put("attribute_" + i + 1, stringList.get(i));
                }
                params.put("quantity", tvSelectQtyCount.getText().toString());
                Log.e("ADD TO Params", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetailActivity.this);
        requestQueue.add(request);

    }


    private void addItemInCartWithoutAtrribute(final String idProduct) {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_ADD_ITEM_CARD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("CardAddResponse", response + "");

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        pd.dismiss();
                        //Toast.makeText(ProductDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                        llPopUp.setVisibility(View.VISIBLE);
                        llPopUp.setFocusable(true);
                        tvAddCard.setEnabled(false);

                        getCartForCounter();


                    } else {
                        pd.dismiss();
                        Toast.makeText(ProductDetailActivity.this, message, Toast.LENGTH_SHORT).show();

                    }
                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ProductDetailActivity.this, "Exception Three" + e, Toast.LENGTH_SHORT).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                params.put("product_id", idProduct);
                params.put("device_id", sp.getString("deviceId", ""));
                params.put("quantity", tvSelectQtyCount.getText().toString());
                Log.e("CardParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetailActivity.this);
        requestQueue.add(request);

    }


    private void getProductStockWithoutAtrribute(final String idProduct) {

        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_PRODUCT_STOCK, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("StockResponse", response + "");

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    String stock = objects.getString("stock");

                    if (status.equalsIgnoreCase("true") || !stock.equalsIgnoreCase("0")) {
                        addItemInCartWithoutAtrribute(idProduct);

                    } else {
                        pd.dismiss();
                        Toast.makeText(ProductDetailActivity.this, message, Toast.LENGTH_SHORT).show();

                    }

                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ProductDetailActivity.this, "Exception Two" + e, Toast.LENGTH_SHORT).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("product_id", idProduct);
                params.put("device_id", sp.getString("deviceId", ""));
                Log.e("CardParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetailActivity.this);
        requestQueue.add(request);
    }


    public void addToMyFavroit() {

        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_ADD_TO_MYFAVROIT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("addToFavroitResponse", response + "");

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        pd.dismiss();
                        Toast.makeText(ProductDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                        // Intent inFav = new Intent(ProductDetailActivity.this, MyFavroiteActivity.class);
                        // startActivity(inFav);
                    } else {
                        pd.dismiss();
                        Toast.makeText(ProductDetailActivity.this, message, Toast.LENGTH_SHORT).show();

                    }

                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ProductDetailActivity.this, "Exception Two" + e, Toast.LENGTH_SHORT).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("product_id", productId);
                params.put("customer_id", sp.getString("customerId", ""));
                Log.e("addFavParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetailActivity.this);
        requestQueue.add(request);

    }

    public void removeFavroit() {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_REMOVE_FAVROIT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("REMOVEFavroitResponse", response + "");

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        pd.dismiss();
                        Toast.makeText(ProductDetailActivity.this, message, Toast.LENGTH_SHORT).show();
                    } else {
                        pd.dismiss();
                        Toast.makeText(ProductDetailActivity.this, message, Toast.LENGTH_SHORT).show();

                    }

                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ProductDetailActivity.this, "Exception Two" + e, Toast.LENGTH_SHORT).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("product_id", productId);
                params.put("customer_id", sp.getString("customerId", ""));
                Log.e("removeFavParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ProductDetailActivity.this);
        requestQueue.add(request);
    }


    public void getCartForCounter() {

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_MY_CART_COUNT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseCart", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String count = objects.getString("count");

                    if (status.equalsIgnoreCase("true")) {
                        HomeActivityNew.cartCounter = Integer.parseInt(count);
                        Log.e("cartArray", count + "");
                        tvCartCounter.setText("" + HomeActivityNew.cartCounter);

                        pd.dismiss();
                    } else {
                        pd.dismiss();
                        //finish();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                params.put("device_id", sp.getString("deviceId", ""));
                Log.e("MyCartParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }

}
