package com.minikin.android.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.MySession;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvRegister, tvLogin, tvTitle, tvForgetTitle, tvForgetClcik, tvLoginGuest;
    private EditText etPass, etUserName, etEmailDialog;
    private Activity activity = LoginActivity.this;
    private MySession mySession;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    private ImageView ivBack;
    private ProgressDialog pd;
    private String userName, password;
    private SharedPreferences sp = null;
    private SharedPreferences.Editor editorUser;
    private LinearLayout llForgetPassword;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.act_login);
        initViews();
        mySession = new MySession(activity);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        setLangRecreate(WhichLanguage);
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editorUser = sp.edit();
        pd = new ProgressDialog(LoginActivity.this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.act_login_tv_register:
                startActivity(new Intent(activity, SignUpActivity.class));
                break;

            case R.id.act_login_tv_login:
                getDataFromUser();
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.ll_forget_pw:
                showUpdateDialog();
                break;
            case R.id.act_login_tv_login_guest:

                if (WhichLanguage.equalsIgnoreCase("ar")) {

                    if (HomeActivityNew.cartCounter != 0) {
                        startActivity(new Intent(activity, MyCartGuestMemberCheckout.class));

                    } else if (HomeActivityNew.cartCounter == 0) {
                        Toast.makeText(activity, "السلة فارغة", Toast.LENGTH_SHORT).show();

                    }

                } else if (WhichLanguage.equalsIgnoreCase("en")) {

                    if (HomeActivityNew.cartCounter != 0) {
                        startActivity(new Intent(activity, MyCartGuestMemberCheckout.class));

                    } else if (HomeActivityNew.cartCounter == 0) {
                        Toast.makeText(activity, "Cart is Empty", Toast.LENGTH_SHORT).show();

                    }

                }

                break;
        }
    }

    private void initViews() {
        tvRegister = findViewById(R.id.act_login_tv_register);
        tvTitle = findViewById(R.id.tv_title_login);
        tvForgetTitle = findViewById(R.id.tv_forget_PW);
        tvForgetClcik = findViewById(R.id.tv_fw_click);
        tvLoginGuest = findViewById(R.id.act_login_tv_login_guest);
        tvLogin = findViewById(R.id.act_login_tv_login);
        ivBack = (ImageView) findViewById(R.id.iv_back);
        etPass = findViewById(R.id.act_login_et_pass);
        etUserName = findViewById(R.id.act_login_et_uname);
        etUserName.setHint(getResources().getString(R.string.HintUserNameLogin).toUpperCase());
        etPass.setHint(getResources().getString(R.string.HintPasswordLogin).toUpperCase());
        llForgetPassword = findViewById(R.id.ll_forget_pw);

        tvRegister.setOnClickListener(this);
        tvLogin.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        llForgetPassword.setOnClickListener(this);
        tvLoginGuest.setOnClickListener(this);

    }

    private boolean checkValidation() {
        if (etUserName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Enter Username", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etPass.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Enter Password", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean checkValidationUrdu() {
        if (etUserName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "أدخل اسم المستخدم", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etPass.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "أدخل الرمز السري", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);


            tvTitle.setText("تسجيل الدخول");

            etUserName.setHint("اسم المستخدم");
            etPass.setHint("كلمة السر");

            tvLogin.setText("تسجيل الدخول");
            tvForgetTitle.setText("نسيت كلمة السر؟");
            tvRegister.setText("التسجيل");
            tvLoginGuest.setText("طلب من غير تسجيل");

            tvTitle.setTypeface(fontEnMedium);
            tvLogin.setTypeface(fontEn);
            tvRegister.setTypeface(fontEnBold);
            tvForgetTitle.setTypeface(fontEnBold);
            tvForgetClcik.setTypeface(fontEnBold);
            tvLoginGuest.setTypeface(fontEnMedium);
            etPass.setTypeface(fontEn);
            etUserName.setTypeface(fontEn);


            /*tvTitle.setTypeface(fontAr);
            tvLogin.setTypeface(fontAr);
            tvRegister.setTypeface(fontAr);
            tvForgetTitle.setTypeface(fontAr);
            tvForgetClcik.setTypeface(fontAr);
            tvLoginGuest.setTypeface(fontAr);
            etPass.setTypeface(fontAr);
            etUserName.setTypeface(fontAr);*/

        } else if (langval.equalsIgnoreCase("en")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            tvTitle.setTypeface(fontEnMedium);
            tvLogin.setTypeface(fontEn);
            tvRegister.setTypeface(fontEnBold);
            tvForgetTitle.setTypeface(fontEnBold);
            tvForgetClcik.setTypeface(fontEnBold);
            tvLoginGuest.setTypeface(fontEnMedium);
            etPass.setTypeface(fontEn);
            etUserName.setTypeface(fontEn);


        }
    }

    private void getDataFromUser() {
        userName = etUserName.getText().toString();
        password = etPass.getText().toString();

        if (WhichLanguage.equalsIgnoreCase("ar")) {

            if (checkValidationUrdu()) {
                pd.show();
                userLogin();

            }

        } else if (WhichLanguage.equalsIgnoreCase("en")) {

            if (checkValidation()) {
                pd.show();
                userLogin();

            }
        }


    }

    private void userLogin() {
        Log.e("url", AppConstants.GET_LOGIN + "");

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e(" LoginResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");

                    if (status.equalsIgnoreCase("true")) {
                        JSONObject jsonObjResult = objects.getJSONObject("result");
                        String customerId = jsonObjResult.getString("customer_id");
                        String customerName = jsonObjResult.getString("customer_name");
                        String customerEmail = jsonObjResult.getString("email");
                        String usernameResult = jsonObjResult.getString("username");

                        editorUser.putString("customerId", customerId);
                        editorUser.putString("customerName", customerName);
                        editorUser.putString("customerEmail", customerEmail);
                        editorUser.putString("usernameResult", usernameResult);
                        editorUser.putString("loginValue", "login");
                        editorUser.commit();
                        pd.dismiss();
                        Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_LONG).show();
                        startActivity(new Intent(activity, HomeActivityNew.class));
                        finish();

                    } else if (status.equalsIgnoreCase("false")) {
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();

                        pd.dismiss();


                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(LoginActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", String.valueOf(error));
            /*    String body;
                //get status code here
                String statusCode = String.valueOf(error.networkResponse.statusCode);
                //get response body and parse with appropriate encoding
                if (error.networkResponse.data != null) {
                    try {
                        body = new String(error.networkResponse.data, "UTF-8");
                        Log.e("errorin", body);

                        Toast.makeText(LoginActivity.this, "Invalid Login" + body, Toast.LENGTH_LONG).show();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }*/


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", pref.getString("WhichLanguage", ""));
                params.put("username", userName);
                params.put("password", password);
                Log.e("LoginParams", params + "");

                return params;
            }


        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(request);


    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(activity, SplashActivity.class);
        startActivity(intent);
        finish();
    }


    public void showUpdateDialog() {

        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_layout);

        LinearLayout linearLayout = dialog.findViewById(R.id.ll_item_increase);
        linearLayout.setVisibility(View.VISIBLE);

        TextView tvTitleDialog = dialog.findViewById(R.id.dialog_titile);
        TextView tvMessage = dialog.findViewById(R.id.dialog_message);
        Button yes = dialog.findViewById(R.id.dialog_positive_btn);
        yes.setText("Send");
        Button no = dialog.findViewById(R.id.dialog_negative_btn);
        etEmailDialog = dialog.findViewById(R.id.et_eamil_dialog);
        tvTitleDialog.setText("Reset Your Password?");
        tvMessage.setText("In order to receive your password by email,please enter the email address you provided during the registration process.");

        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");


        if (WhichLanguage.equalsIgnoreCase("ar")) {
            //((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

            no.setText("لا");
            yes.setText("نعم ");
            tvTitleDialog.setText("نسيت كلمة المرور؟");
            tvMessage.setText(" لتلقي كلمة المرور الخاصة بك عن طريق البريد الإلكتروني ، يرجى إدخال عنوان البريد الإلكتروني الذي قدمته أثناء عملية التسجيل.");

            etEmailDialog.setHint("أدخل البريد الإلكتروني");

            tvTitleDialog.setTypeface(fontEnMedium);
            tvMessage.setTypeface(fontEn);
            yes.setTypeface(fontEn);
            etEmailDialog.setTypeface(fontEn);
            no.setTypeface(fontEn);

        } else if (WhichLanguage.equalsIgnoreCase("en")) {
            //((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvTitleDialog.setTypeface(fontEnMedium);
            tvMessage.setTypeface(fontEn);
            yes.setTypeface(fontEn);
            etEmailDialog.setTypeface(fontEn);
            no.setTypeface(fontEn);


        }


        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailIdStr = etEmailDialog.getText().toString().trim();

                if (WhichLanguage.equalsIgnoreCase("en")){
                    if (chekEmailValidtion(emailIdStr)) {
                        getForgetPassword(emailIdStr);

                    }
                }else {
                    if (chekEmailValidtionUrdu(emailIdStr)) {
                        getForgetPassword(emailIdStr);

                    }
                }



            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });


        dialog.show();

    }

    private boolean chekEmailValidtion(String emailIdStr) {
        if (etEmailDialog.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Enter Email", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!emailIdStr.matches(emailPattern)) {
            Toast.makeText(activity, "Please Enter Valid Email", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }

    private boolean chekEmailValidtionUrdu(String emailIdStr) {
        if (etEmailDialog.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "أدخل البريد الإلكتروني", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!emailIdStr.matches(emailPattern)) {
            Toast.makeText(activity, "يرجى إدخال بريد إلكتروني صحيح", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }


    private void getForgetPassword(final String userEmail) {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_FORGET_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("forgetResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");

                    if (status.equalsIgnoreCase("true")) {
                        pd.dismiss();
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();

                    } else if (status.equalsIgnoreCase("false")) {
                        pd.dismiss();
                        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();


                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(LoginActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", String.valueOf(error));

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("email", userEmail);
                Log.e("forgetParams", params + "");

                return params;
            }


        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(request);
    }
}


