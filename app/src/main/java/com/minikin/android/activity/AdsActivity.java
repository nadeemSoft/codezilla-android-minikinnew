package com.minikin.android.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.ViewPagerAdapter;
import com.minikin.android.model.Advertise;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class AdsActivity extends AppCompatActivity implements View.OnClickListener {
    public static AdsActivity adsActivity;
    private String loginValue;
    private TextView tvSkeep;
    private ViewPager viewPager;
    private LinearLayout sliderDotsPanel;
    private ProgressDialog pd;
    private FrameLayout frameLayout;
    private ArrayList<Advertise> advertisesList = null;
    private Timer timer;
    private int dotsCount;
    private ImageView[] dots;
    private SharedPreferences sp = null;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    Handler handler;
    Runnable runnable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.act_ads);
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        initViews();

        adsActivity = this;
        loginValue = sp.getString("LoginValue", "");
        Log.e("loginValue", loginValue);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();

        WhichLanguage = pref.getString("WhichLanguage", "");
        advertisesList = new ArrayList<>();
        pd = new ProgressDialog(AdsActivity.this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);


        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (handler != null){
                    handler.removeCallbacks(runnable);

                }
                return false;
            }
        });



    }

    @Override
    protected void onResume() {
        super.onResume();
        setLangRecreate(WhichLanguage);
        getSplash();

    }



    private void initViews() {
        tvSkeep = findViewById(R.id.act_ads_tv_skip);
        tvSkeep.setOnClickListener(this);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        sliderDotsPanel = (LinearLayout) findViewById(R.id.slider_dots);
        frameLayout = findViewById(R.id.frame_ads);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.act_ads_tv_skip:
                handler.removeCallbacks(runnable);
                Intent intent = new Intent(AdsActivity.this, HomeActivityNew.class);
                startActivity(intent);
                finish();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (handler != null){
            handler.removeCallbacks(runnable);
            finish();

        }
    }


    private void getSplash() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_SPLASH_SCREEN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                advertisesList.clear();
                try {
                    Log.e(" AdvertiseResponse", response);
                    JSONObject objects = new JSONObject(response);

                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    JSONArray jsonArray = objects.getJSONArray("result");


                    if (status.equalsIgnoreCase("true")) {

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            String image = obj.getString("img");
                            String title = obj.getString("title");
                            Advertise advertise = new Advertise(status, message, image, title);
                            advertisesList.add(advertise);

                        }
                        setViewPager();
                        pd.dismiss();

                        handler = new Handler();
                        handler.postDelayed(runnable = new Runnable() {
                              @Override
                              public void run() {
                                  Intent intent = new Intent(AdsActivity.this, HomeActivityNew.class);
                                  startActivity(intent);
                                  finish();

                              }
                          }, 5000);

                    }else {
                        pd.dismiss();
                        Toast.makeText(AdsActivity.this, ""+message , Toast.LENGTH_LONG).show();
                        handler = new Handler();
                        handler.postDelayed(runnable = new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(AdsActivity.this, HomeActivityNew.class);
                                startActivity(intent);
                                finish();

                            }
                        }, 5000);


                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(AdsActivity.this, "Exceotion" + "" + e, Toast.LENGTH_LONG).show();


                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", String.valueOf(error));
                Toast.makeText(AdsActivity.this, "" + error, Toast.LENGTH_LONG).show();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", WhichLanguage);
                Log.e("addparams", params + "");

                return params;
            }


        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);


        RequestQueue requestQueue = Volley.newRequestQueue(AdsActivity.this);
        requestQueue.add(request);

    }


    private void setViewPager() {

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(AdsActivity.this, advertisesList);
        viewPager.setAdapter(viewPagerAdapter);

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                viewPager.post(new Runnable() {

                    @Override
                    public void run() {
                        viewPager.setCurrentItem((viewPager.getCurrentItem() + 1) % advertisesList.size());
                    }
                });
            }
        };
        timer = new Timer();
        timer.schedule(timerTask, 3000, 3000);

        sliderDotsPanel.removeAllViews();

        dotsCount = viewPagerAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {

            dots[i] = new ImageView(AdsActivity.this);

            dots[i].setImageDrawable(ContextCompat.getDrawable(AdsActivity.this, R.drawable.non_active_dots));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            sliderDotsPanel.addView(dots[i], params);

        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(AdsActivity.this, R.drawable.active_dots));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(AdsActivity.this, R.drawable.non_active_dots));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(AdsActivity.this, R.drawable.active_dots));


            }

            @Override
            public void onPageScrollStateChanged(int state) {


            }
        });
    }


    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        Typeface fontEn = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/AmmanV3Serif-Medium.otf");
        Typeface fontEnBold = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/Avenir.ttc");


        Typeface fontAr = Typeface.createFromAsset(getBaseContext().getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
           tvSkeep.setText("تخطي"+"");
           // tvSkeep.setTypeface(fontAr);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvSkeep.setTypeface(fontEnMedium);


        }
    }

}
