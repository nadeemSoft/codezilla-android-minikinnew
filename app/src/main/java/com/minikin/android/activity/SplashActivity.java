package com.minikin.android.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.test.mock.MockPackageManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.minikin.android.R;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.MySession;

import io.fabric.sdk.android.Fabric;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity implements View.OnClickListener {
    String loginValue;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private static final int REQUEST_CODE_PERMISSION = 2;

    private String[
            ] mPermission = {Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE

    };

    private TimerTask tt;
    private Timer timer;
    private Activity activity = SplashActivity.this;
    private MySession mySession;
    private TextView tvEnglish, tvUrdu;
    private String WhichLanguage = "";
    private LinearLayout llEnglishUrdu;
    SharedPreferences sp = null;
    SharedPreferences.Editor editorUser;
    String chkTos = "1";
    String UpdatechkTos = "2";
    String tos;
    int count = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.act_splash);
        mySession = new MySession(activity);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editorUser = sp.edit();
        tvEnglish = (TextView) findViewById(R.id.tv_english);
        tvEnglish.setOnClickListener(this);
        tvUrdu = (TextView) findViewById(R.id.tv_urdu);
        llEnglishUrdu = findViewById(R.id.ll_english_urdu);
        tvUrdu.setOnClickListener(this);
        loginValue = sp.getString("loginValue", "");


        tt = new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(activity, AdsActivity.class);
                startActivity(intent);
            }
        };


        try {
            if (ActivityCompat.checkSelfPermission(this, mPermission[0])
                    != MockPackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(this, mPermission[1])
                            != MockPackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this, mPermission, REQUEST_CODE_PERMISSION);

                // If any permission aboe not allowed by user, this condition will execute every tim, else your else part will work
            } else {

                timer = new Timer();
                timer.schedule(tt, 5000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        tos = sp.getString("ToastCount", "1");
        Log.e("TOS", tos);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Req Code", "" + requestCode);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.length == 2 &&
                    grantResults[0] == MockPackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == MockPackageManager.PERMISSION_GRANTED) {

                timer = new Timer();
                timer.schedule(tt, 5000);
            } else {
                Toast.makeText(SplashActivity.this, "Denied", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_english:
                editor.putString("WhichLanguage", "en");
                editorUser.putString("WhichLanguage", "en");
                editor.commit();
                editorUser.commit();
                String tostSma = "English Language";
                showToast(tostSma);
                break;
            case R.id.tv_urdu:
                editor.putString("WhichLanguage", "ar");
                editorUser.putString("WhichLanguage", "ar");
                editor.commit();
                editorUser.commit();
                String tostSmaU = "Urdu Language";
                showToast(tostSmaU);
                break;
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        setLangRecreate(WhichLanguage);


    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        Typeface fontEn = Typeface.createFromAsset(SplashActivity.this.getAssets(), "fonts/AmmanV3Serif-Medium.otf");
        Typeface fontEnBold = Typeface.createFromAsset(SplashActivity.this.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(SplashActivity.this.getAssets(), "fonts/Avenir.ttc");


        Typeface fontAr = Typeface.createFromAsset(SplashActivity.this.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            tvUrdu.setTypeface(fontAr);
            tvEnglish.setTypeface(fontAr);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvEnglish.setTypeface(fontEnMedium);
            tvUrdu.setTypeface(fontEnMedium);
        }
    }

    private void showToast(String sms) {
        if (tos.equalsIgnoreCase(chkTos)) {
            chkTos = "2";
            editorUser.putString("ToastCount", chkTos);
            editorUser.commit();
            Toast.makeText(SplashActivity.this, sms, Toast.LENGTH_SHORT).show();


        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        System.exit(0);


    }
}
