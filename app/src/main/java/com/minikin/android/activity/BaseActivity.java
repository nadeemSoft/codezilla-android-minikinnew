package com.minikin.android.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.NavigationAdapter;
import com.minikin.android.model.SocialMediaModel;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.MySession;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    String loginValue;
    String sociallink = "";

    ActionBarDrawerToggle actionBarDrawerToggle;
    Toolbar toolbar;
    private RecyclerView nav_rv;
    private SharedPreferences sp = null;
    private Activity activity = BaseActivity.this;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ImageView ivFb, ivInsta, ivTwitter;
    private RelativeLayout rl_menu, rl_back;
    private Toolbar tb;
    public DrawerLayout drawer;
    private MySession mySession;
    private TextView tvLanguage;
    private TextView tvUserNameNavigation = null;
    public FrameLayout framSearchFilterIcon;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private SharedPreferences.Editor editorSp;

    private String WhichLanguage = "";
    ImageView ivNavigation, ivHome;
    private ProgressDialog pd;
    private ArrayList<SocialMediaModel> socialMediaList = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        //  this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.act_home);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);

        editor = pref.edit();
        editorSp = sp.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");

        initViews();
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        socialMediaList = new ArrayList<>();
        loginValue = sp.getString("loginValue", "");

        Log.e("USERNAME.", sp.getString("usernameResult", ""));
        String name = sp.getString("usernameResult", "");

        Log.e("name", name);


        //  Typeface fontEn = Typeface.createFromAsset(BaseActivity.this.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(BaseActivity.this.getAssets(), "fonts/Avenir Roman.otf");
        if (WhichLanguage.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            tvLanguage.setText("English");
            tvLanguage.setTypeface(fontEn);
            tvUserNameNavigation.setTypeface(fontAr);


        } else if (WhichLanguage.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvLanguage.setText("فارسی");

            tvLanguage.setTypeface(fontAr);
            tvUserNameNavigation.setTypeface(fontEn);


        }


        getSocialMedia();


        if (name.equalsIgnoreCase("")) {
            if (WhichLanguage.equalsIgnoreCase("en")) {
                tvUserNameNavigation.setText("Hello  Guest");

            } else if (WhichLanguage.equalsIgnoreCase("ar")) {

                tvUserNameNavigation.setText(" زائر مرحباً ");


            }

        } else {

            if (WhichLanguage.equalsIgnoreCase("en")) {
                tvUserNameNavigation.setText("Hello " + name);

            } else if (WhichLanguage.equalsIgnoreCase("ar")) {
                tvUserNameNavigation.setText("مرحباً " + name);

            }


        }


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void initViews() {
        tb = findViewById(R.id.toolbar);
        setSupportActionBar(tb);
        drawer = findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(actionBarDrawerToggle);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getBackground().setAlpha(255);
        rl_back = findViewById(R.id.toolbar_rl_back);
        rl_menu = findViewById(R.id.toolbar_rl_menu);
        rl_back.setVisibility(View.INVISIBLE);

        ivNavigation = findViewById(R.id.iv_navigation);
        View headerLayout = navigationView.getHeaderView(0);
        ivHome = headerLayout.findViewById(R.id.iv_home);
        nav_rv = headerLayout.findViewById(R.id.nav_rv);
        ivFb = (ImageView) headerLayout.findViewById(R.id.iv_fb);
        ivInsta = (ImageView) headerLayout.findViewById(R.id.iv_insta);
        ivTwitter = (ImageView) headerLayout.findViewById(R.id.iv_twitter);
        tvLanguage = (TextView) headerLayout.findViewById(R.id.tv_lang);
        tvLanguage.setOnClickListener(this);
        framSearchFilterIcon = findViewById(R.id.frame_search_icon);
        framSearchFilterIcon.setVisibility(View.GONE);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        nav_rv.setLayoutManager(mLayoutManager);
        nav_rv.setAdapter(new NavigationAdapter(R.layout.rowitem_nav, activity));
        nav_rv.setNestedScrollingEnabled(false);

        tvUserNameNavigation = headerLayout.findViewById(R.id.tv_user_name_navigation);

        //click listener
        rl_menu.setOnClickListener(this);
        ivHome.setOnClickListener(this);
        ivFb.setOnClickListener(this);
        ivInsta.setOnClickListener(this);
        ivTwitter.setOnClickListener(this);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }


    public void menuItemClick(int position) {
        Intent intent;
        switch (position) {
            case 0:
                intent = new Intent(BaseActivity.this, HomeActivityNew.class);
                startActivity(intent);
                finish();

                break;
            case 1:
                if (loginValue.equalsIgnoreCase("login")) {

                    final Dialog dialog = new Dialog(activity);
                    dialog.setCancelable(true);
                    dialog.setContentView(R.layout.dialog_layout);

                    TextView tvTitleDialog = dialog.findViewById(R.id.dialog_titile);
                    TextView tvMessage = dialog.findViewById(R.id.dialog_message);
                    Button yes = dialog.findViewById(R.id.dialog_positive_btn);
                    Button no = dialog.findViewById(R.id.dialog_negative_btn);
                    tvTitleDialog.setText("Minikin");
                    tvMessage.setText("Are You Sure? You want to Logout from the App..");

                    Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman Font Download.otf");
                    Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");


                    if (WhichLanguage.equalsIgnoreCase("ar")) {
                        //((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

                        tvTitleDialog.setText("مينيكن");
                        tvMessage.setText("هل انت متأكد من تسجيل الخروج من التطبيق");
                        yes.setText("نعم ");
                        no.setText("لا");


                    } else if (WhichLanguage.equalsIgnoreCase("en")) {
                        //((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                        tvTitleDialog.setTypeface(fontEn);
                        tvMessage.setTypeface(fontEn);
                        yes.setTypeface(fontEn);
                        no.setTypeface(fontEn);


                    }


                    yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            SharedPreferences preferences = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear();
                            editor.commit();

                            SharedPreferences preferencesLan = getSharedPreferences("LanguagePref", MODE_PRIVATE);
                            SharedPreferences.Editor editorLan = preferencesLan.edit();
                            editorLan.clear();
                            editorLan.commit();

                            finishAffinity();
                            Intent intentLogin = new Intent(activity, SplashActivity.class);
                            //intentLogin.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intentLogin);
                            finish();
                            dialog.dismiss();

                            //tvUserNameNavigation.setText("Guest");


                        }
                    });

                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();

                        }
                    });

                    dialog.show();


                } else if (!loginValue.equalsIgnoreCase("login")) {
                    intent = new Intent(activity, LoginActivity.class);
                    startActivity(intent);

                }


                break;

            case 2:
                if (loginValue.equalsIgnoreCase("login")) {
                    intent = new Intent(activity, MyAccountActivity.class);
                    startActivity(intent);
                } else if (!loginValue.equalsIgnoreCase("login")) {
                    intent = new Intent(activity, LoginActivity.class);
                    startActivity(intent);
                }
                break;
            case 3:
                //framSearchFilterIcon.setVisibility(View.GONE);
                if (loginValue.equalsIgnoreCase("login")) {
                    intent = new Intent(activity, MyFavroiteActivity.class);
                    startActivity(intent);
                } else if (!loginValue.equalsIgnoreCase("login")) {
                    intent = new Intent(activity, LoginActivity.class);
                    startActivity(intent);
                }

                break;
            case 4:
                // framSearchFilterIcon.setVisibility(View.GONE);

                if (loginValue.equalsIgnoreCase("login")) {
                    intent = new Intent(activity, MyOrderActivity.class);
                    startActivity(intent);
                } else if (!loginValue.equalsIgnoreCase("login")) {
                    intent = new Intent(activity, LoginActivity.class);
                    startActivity(intent);
                }
                break;
            case 5:
                // framSearchFilterIcon.setVisibility(View.GONE);
                intent = new Intent(activity, MyCartActivityNew.class);
                startActivity(intent);
               /* if (loginValue.equalsIgnoreCase("login")) {
                    intent = new Intent(activity,MyCartActivityNew.class);
                    startActivity(intent);
                } else if (!loginValue.equalsIgnoreCase("login")) {
                    intent = new Intent(activity, MyCartGuestMemberCheckout.class);
                    startActivity(intent);
                }*/
                break;
            case 6:
                intent = new Intent(activity, JoinUsActivity.class);
                startActivity(intent);

                break;
            case 7:
                intent = new Intent(activity, ContactUsActivity.class);
                startActivity(intent);

                break;
            case 8:

                intent = new Intent(activity, LiveChatActivity.class);
                startActivity(intent);


                break;
            case 9:
                intent = new Intent(activity, TermsAndConditionActivity.class);
                startActivity(intent);

                break;
            case 10:

                intent = new Intent(activity, ReturnExchangeActivity.class);
                startActivity(intent);
                break;
            case 11:
                intent = new Intent(activity, DeliveryPolicyActivity.class);
                startActivity(intent);

                break;
            case 12:

                intent = new Intent(activity, WhoWeAreActivity.class);
                startActivity(intent);
                break;

        }

        if (drawer.isDrawerOpen(GravityCompat.START)) {

            drawer.closeDrawer(GravityCompat.START);
        } else {

            drawer.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.toolbar_rl_menu:

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));

                    drawer.closeDrawer(GravityCompat.START);

                } else {

                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;
            case R.id.iv_fb:
                getLinkFromSocialBase("facebook");
                break;
            case R.id.iv_insta:
                getLinkFromSocialBase("instagram");
                break;
            case R.id.iv_twitter:
                getLinkFromSocialBase("twitter");
                break;
            case R.id.iv_home:
                /*Intent intent = new Intent(BaseActivity.this, HomeActivityNew.class);
                startActivity(intent);
                finish();*/
                break;

            case R.id.tv_lang:
                if (WhichLanguage.equalsIgnoreCase("en")){
                    editor.putString("WhichLanguage", "ar");
                    editorSp.putString("WhichLanguage", "ar");
                    editorSp.commit();
                    editor.commit();

                }else if (WhichLanguage.equalsIgnoreCase("ar")){
                    editor.putString("WhichLanguage", "en");
                    editorSp.putString("WhichLanguage", "en");
                    editorSp.commit();
                    editor.commit();



                }
                finish();
                Intent i = new Intent(BaseActivity.this,HomeActivityNew.class);
                startActivity(i);
                break;
        }
    }

    private void getSocialMedia() {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_SOCIAL_MEDIA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                socialMediaList.clear();
                try {
                    Log.e("SocialMediaResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objCat = jsonArrayResult.getJSONObject(i);
                            String title = objCat.getString("title");
                            String link = objCat.getString("link");

                            SocialMediaModel socialMediaModel = new SocialMediaModel(title, link);
                            socialMediaList.add(socialMediaModel);

                        }
                        pd.dismiss();

                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                    }
                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_LONG).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_LONG).show();


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));

                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }

    public void getLinkFromSocialBase(String titleget) {
        Log.e("titleGet", titleget);
        Log.e("socialMediaListSIZE", socialMediaList.size() + "");

        for (int i = 0; i < socialMediaList.size(); i++) {

            SocialMediaModel model = socialMediaList.get(i);
            String title = model.getTitle();
            if (title.equalsIgnoreCase(titleget)) {
                Log.e("titlechek", title);

                sociallink = model.getLink();
                Log.e("socialMediaLINK", sociallink + "");

                Intent browserIntentFb = new Intent(Intent.ACTION_VIEW, Uri.parse(sociallink));
                startActivity(browserIntentFb);
                break;

            }

            Log.e("socialMediaListSIZEDown", socialMediaList.size() + "");

            continue;


        }


    }


}
