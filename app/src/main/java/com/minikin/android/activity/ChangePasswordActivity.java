package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.MySession;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {
    private RelativeLayout rl_back, rl_menu,rlSearch;
    private FrameLayout rlCart;

    private TextView tv_title, tvSubmit,tvCartCounter;
    private EditText etOldPw, etNewPw, etConfirmNewPw;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    private Activity activity = ChangePasswordActivity.this;
    private MySession mySession;
    private ProgressDialog pd;
    private SharedPreferences sp = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        //  this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        mySession = new MySession(activity);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.act_change_pass, contentFrameLayout);
        initViews();
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        setLangRecreate(WhichLanguage);
        tvCartCounter.setText(""+HomeActivityNew.cartCounter);

    }

    private void initViews() {

        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rlCart  = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setOnClickListener(this);
        rlSearch = findViewById(R.id.rl_search_at_toolbar);
        rlSearch.setVisibility(View.GONE);
        etOldPw = findViewById(R.id.et_old_pw);
        etNewPw = findViewById(R.id.et_new_pw);
        etConfirmNewPw = findViewById(R.id.et_confirm_new_pw);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tv_title.setText("Change Password");
        tvSubmit = findViewById(R.id.tv_submit);
        tvCartCounter = findViewById(R.id.tv_cart_counter);

        tvSubmit.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_menu.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.toolbar_two_rl_back:
                finish();
                break;

            case R.id.toolbar_rl_menutwo:
                //openDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));

                    drawer.closeDrawer(GravityCompat.START);

                } else {

                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;

            case R.id.tv_submit:
                if (chekValidation()) {
                    getOldPasswword();


                }
                break;
            case R.id.rl_cart_icon_toolbar:
                if (loginValue.equalsIgnoreCase("login")) {
                    Intent intent = new Intent(activity, MyCartActivityNew.class);
                    startActivity(intent);
                } else if (!loginValue.equalsIgnoreCase("login")) {
                    Intent intentl = new Intent(activity, LoginActivity.class);
                    startActivity(intentl);
                }
                break;

        }
    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        //Typeface fontEn = Typeface.createFromAsset(ChangePasswordActivity.this.getAssets(), "fonts/Avenir Roman Font Download.otf");

        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(ChangePasswordActivity.this.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);
            //tv_title.setTypeface(fontAr);
           // etOldPw.setTypeface(fontAr);
           // etConfirmNewPw.setTypeface(fontAr);
          //  etNewPw.setTypeface(fontAr);
          //  tvSubmit.setTypeface(fontAr);
            tvCartCounter.setTypeface(fontAr);

            tv_title.setText("تغيير كلمة السر");
            etOldPw.setHint("كلمة السر السابقه");
            etNewPw.setHint(" كلمة السر الجديدة");
            etConfirmNewPw.setHint("تأكيد كلمة السر الجديدة");
            tvSubmit.setText("اختار");


        } else if (langval.equalsIgnoreCase("en")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tv_title.setTypeface(fontEnMedium);
            etOldPw.setTypeface(fontEn);
            etConfirmNewPw.setTypeface(fontEn);
            etNewPw.setTypeface(fontEn);
            tvSubmit.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);

        }
    }

    private boolean chekValidation() {
        if (etOldPw.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Enter Old Password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etNewPw.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Enter New Password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etConfirmNewPw.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Enter Confirm Password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!etNewPw.getText().toString().equalsIgnoreCase(etConfirmNewPw.getText().toString())) {
            Toast.makeText(activity, " Confirm Password is Wrong", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;


    }

    private void getOldPasswword() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_VERIFY_OLD_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseOldPassword", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        //pd.dismiss();
                        // Toast.makeText(ChangePasswordActivity.this, message, Toast.LENGTH_LONG).show();
                        getChangePasswword();
                    } else {
                        pd.dismiss();
                        Toast.makeText(ChangePasswordActivity.this, message, Toast.LENGTH_LONG).show();
                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ChangePasswordActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("old_password", etOldPw.getText().toString());
                params.put("customer_id", sp.getString("customerId", ""));
                Log.e("OldPasswordParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ChangePasswordActivity.this);
        requestQueue.add(request);

    }

    private void getChangePasswword() {

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_CHANGE_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseChangePassword", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    pd.dismiss();
                    Toast.makeText(ChangePasswordActivity.this, message, Toast.LENGTH_LONG).show();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ChangePasswordActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("password", etConfirmNewPw.getText().toString());
                params.put("customer_id", sp.getString("customerId", ""));
                Log.e("ChangePaaasswordParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ChangePasswordActivity.this);
        requestQueue.add(request);

    }
}
