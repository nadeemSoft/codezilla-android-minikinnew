package com.minikin.android.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.CountryDialogAdapter;
import com.minikin.android.model.AddressModel;
import com.minikin.android.model.AreaModel;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.CountryCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AddNewAddressActivity extends BaseActivity implements View.OnClickListener {
    String cart = null;
    private Activity activity = AddNewAddressActivity.this;
    private RelativeLayout rl_back, rl_menu, rlSearch;
    private FrameLayout rlCart;

    private TextView tv_title, tvAddNewAddress, tvArea, tvUpdateAddress, tvCartCounter;
    private EditText etAddressName, etBlock, etStreet, etAvenue, etBuilding, etFloor, etApartment, etExtra, etNumber;
    private String areaId;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private SharedPreferences sp = null;
    private ArrayList<AreaModel> areaList = null;
    private String WhichLanguage = "";
    private ProgressDialog pd;
    private AddressModel model;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_add_new_address, contentFrameLayout);
        initViews();
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        areaList = new ArrayList<>();
        setLangRecreate(WhichLanguage);
        tvCartCounter.setText("" + HomeActivityNew.cartCounter);


        //get Intent for update profile Or add new Address

        // Intent intent = getIntent();
        Bundle extras = getIntent().getExtras();
        String chkAddress = extras.getString("Addnew");

        //String chkAddress = intent.getStringExtra("Addnew");
        if (chkAddress.equalsIgnoreCase("NewAddress")) {
            cart = extras.getString("callActivity");
            getAreaId();

        } else if (chkAddress.equalsIgnoreCase("EditAddress")) {
            getAreaId();
            tvAddNewAddress.setVisibility(View.GONE);
            tvUpdateAddress.setVisibility(View.VISIBLE);
            if (WhichLanguage.equalsIgnoreCase("en")) {
                tv_title.setText("Address Update");

            } else {

                tv_title.setText("تحديث العنوان");

            }
            tvArea.setEnabled(true);
            int pos = extras.getInt("position");
            Log.e("chkEditPostion", String.valueOf(pos));
            model = MyAddressActivity.addressModelArrayList.get(pos);
            etAddressName.setText(model.getTitle());
            areaId = model.getArea_id();
            tvArea.setText(model.getArea_name());
            etBlock.setText(model.getBlock());
            etStreet.setText(model.getStreet());
            etAvenue.setText(model.getAvenue());
            etBuilding.setText(model.getBuilding());
            etFloor.setText(model.getFloor());
            etApartment.setText(model.getApartment());
            etExtra.setText(model.getNotes());

        }


    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {

            case R.id.toolbar_two_rl_back:
                finish();
                break;
            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));

                    drawer.closeDrawer(GravityCompat.START);

                } else {

                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;
            case R.id.tv_add:

                if (WhichLanguage.equalsIgnoreCase("en")){
                    if (chechkValidation()) {
                        getAddMyNewAddress();

                    }
                }else {
                    if (chechkValidationUrdu()) {
                        getAddMyNewAddress();

                    }
                }

                break;
            case R.id.tv_update:

                if (WhichLanguage.equalsIgnoreCase("en")){
                    if (chechkValidation()) {
                        UpadteAddress(model.getAddress_id());

                    }
                }else {
                    if (chechkValidationUrdu()) {
                        UpadteAddress(model.getAddress_id());

                    }
                }



                break;
            case R.id.et_area_name:
                showDialog(AddNewAddressActivity.this, areaList);
                break;
            case R.id.rl_cart_icon_toolbar:
                Intent intent = new Intent(activity, MyCartActivityNew.class);
                startActivity(intent);
                /*if (loginValue.equalsIgnoreCase("login")) {
                    Intent intent = new Intent(activity, MyCartActivityNew.class);
                    startActivity(intent);
                } else if (!loginValue.equalsIgnoreCase("login")) {
                    Intent intentl = new Intent(activity, LoginActivity.class);
                    startActivity(intentl);
                }*/
                break;
        }
    }


    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tv_title.setText("Add New Address");
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setOnClickListener(this);
        rlSearch = findViewById(R.id.rl_search_at_toolbar);
        rlSearch.setVisibility(View.GONE);
        tvAddNewAddress = findViewById(R.id.tv_add);
        tvCartCounter = findViewById(R.id.tv_cart_counter);

        tvUpdateAddress = findViewById(R.id.tv_update);
        tvUpdateAddress.setVisibility(View.GONE);
        etAddressName = findViewById(R.id.et_address_name);
        tvArea = findViewById(R.id.et_area_name);
        etBlock = findViewById(R.id.et_block);
        etStreet = findViewById(R.id.et_street);
        etAvenue = findViewById(R.id.et_avenue);
        etBuilding = findViewById(R.id.et_building);
        etFloor = findViewById(R.id.et_floor);
        etApartment = findViewById(R.id.et_aparment);
        etExtra = findViewById(R.id.et_extra);
        etNumber = findViewById(R.id.et_mobile_address);
        rl_back.setOnClickListener(this);
        rl_menu.setOnClickListener(this);
        tvAddNewAddress.setOnClickListener(this);
        tvUpdateAddress.setOnClickListener(this);
        tvArea.setOnClickListener(this);
    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        // Typeface fontEn = Typeface.createFromAsset(AddNewAddressActivity.this.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(AddNewAddressActivity.this.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);
            //tvArea.setTypeface(fontAr);
            // tvUpdateAddress.setTypeface(fontAr);
            //tvAddNewAddress.setTypeface(fontAr);

            // tv_title.setTypeface(fontEnMedium);
            etExtra.setTypeface(fontAr);
            etApartment.setTypeface(fontAr);
            etFloor.setTypeface(fontAr);
            etBuilding.setTypeface(fontAr);
            etAvenue.setTypeface(fontAr);
            etStreet.setTypeface(fontAr);
            etBlock.setTypeface(fontAr);
            // etAddressName.setTypeface(fontAr);
            etNumber.setTypeface(fontAr);
            tvCartCounter.setTypeface(fontAr);

            etAddressName.setHint("اسم العنوان" + "*");
            tvArea.setHint("المنطقه" + "*");
            etBlock.setHint("القطعه " + "*");
            etStreet.setHint("الشارع" + "*");
            etAvenue.setHint("الجادة");
            etBuilding.setHint("المنزل" + "*");
            etFloor.setHint("الطابق");
            etApartment.setHint("الشقه");
            etNumber.setHint("رقم الهاتف"+"*");
            etExtra.setHint("تفاصيل اضافيه");


            tvAddNewAddress.setText("اضافه");
            tv_title.setText("اضافة كلمة السر الجديدة");
            tvUpdateAddress.setText("حدث العنوان");


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvArea.setTypeface(fontEn);
            tvUpdateAddress.setTypeface(fontEn);
            tvAddNewAddress.setTypeface(fontEn);
            tv_title.setTypeface(fontEnMedium);
            etExtra.setTypeface(fontEn);
            etApartment.setTypeface(fontEn);
            etFloor.setTypeface(fontEn);
            etBuilding.setTypeface(fontEn);
            etAvenue.setTypeface(fontEn);
            etStreet.setTypeface(fontEn);
            etBlock.setTypeface(fontEn);
            etAddressName.setTypeface(fontEn);
            etNumber.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);


        }
    }

    private boolean chechkValidation() {
        if (etAddressName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(AddNewAddressActivity.this, "Please Enter Address Name", Toast.LENGTH_SHORT).show();
            return false;

        } else if (tvArea.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(AddNewAddressActivity.this, "Please Enter Area", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etBlock.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(AddNewAddressActivity.this, "Please Enter A valid Block", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etStreet.getText().toString().matches("")) {
            Toast.makeText(AddNewAddressActivity.this, "Please Enter A vaFlid Street", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etNumber.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(AddNewAddressActivity.this, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etNumber.getText().toString().length() != 8) {
            Toast.makeText(AddNewAddressActivity.this, "Please Enter A Valid Mobile Number", Toast.LENGTH_SHORT).show();
            return false;

        }
        return true;
    }

    private boolean chechkValidationUrdu() {
        if (etAddressName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(AddNewAddressActivity.this, "يرحى إدخال اسم للعنوان", Toast.LENGTH_SHORT).show();
            return false;

        } else if (tvArea.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(AddNewAddressActivity.this, "اسم المنطقة", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etBlock.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(AddNewAddressActivity.this, "يرجى إدخال رقم القطعة الصحيح", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etStreet.getText().toString().matches("")) {
            Toast.makeText(AddNewAddressActivity.this, "يرجى إدخال رقم الشارع الصحيح", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etNumber.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(AddNewAddressActivity.this, "يرجى إدخال رقم الهاتف", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etNumber.getText().toString().length() != 8) {
            Toast.makeText(AddNewAddressActivity.this, "يرجى إدخال رقم هاتف صحيح", Toast.LENGTH_SHORT).show();
            return false;

        }
        return true;
    }

    private void getAddMyNewAddress() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_MY_ADD_NEW_ADDRESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseMyAddAddress", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("ok")) {
                        pd.dismiss();
                        Toast.makeText(AddNewAddressActivity.this, message, Toast.LENGTH_SHORT).show();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

                        Log.e("Values", cart + "");

                        if (cart.equalsIgnoreCase("cart")) {
                            Intent intent = new Intent(AddNewAddressActivity.this, CheckOutActivityNew.class);
                            //intent.putExtra("AddressSelectV","DONE");
                            //startActivity(intent);
                            setResult(Activity.RESULT_OK, intent);
                            finish();

                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                        } else if (cart.equalsIgnoreCase("my")) {
                            finish();

                        }

                    } else {
                        pd.dismiss();
                        Toast.makeText(AddNewAddressActivity.this, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("PRINTADDRESS", e + "");
                    Toast.makeText(AddNewAddressActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                params.put("title", etAddressName.getText().toString());
                params.put("area_id", areaId);
                params.put("block", etBlock.getText().toString());
                params.put("street", etStreet.getText().toString());
                params.put("avenue", etAvenue.getText().toString());
                params.put("building", etBuilding.getText().toString());
                params.put("floor", etFloor.getText().toString());
                params.put("apartment", etApartment.getText().toString());
                params.put("mobile", etNumber.getText().toString());
                params.put("notes", etExtra.getText().toString());
                Log.e("MynewAddressParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(AddNewAddressActivity.this);
        requestQueue.add(request);
    }

    private void getAreaId() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.GET, AppConstants.GET_AREA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseArea", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArray = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectResponse = jsonArray.getJSONObject(i);
                            String area_id = jsonObjectResponse.getString("area_id");
                            String area_name = jsonObjectResponse.getString("area_name");

                            AreaModel areaModel = new AreaModel(area_id, area_name);
                            areaList.add(areaModel);
                            pd.dismiss();

                        }


                    } else {
                        pd.dismiss();
                        Toast.makeText(AddNewAddressActivity.this, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(AddNewAddressActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                Log.e("AreaPArams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(AddNewAddressActivity.this);
        requestQueue.add(request);
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void showDialog(Activity activity, ArrayList<AreaModel> list) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_country);

        final EditText searchText = (EditText) dialog.findViewById(R.id.country_dialog_search_text);
        ImageButton searchBtn = (ImageButton) dialog.findViewById(R.id.country_dialog_search_btn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestFocus(searchText);
            }
        });

        ListView listView = (ListView) dialog.findViewById(R.id.country_dialog_list_view);

        final CountryDialogAdapter adapter = new CountryDialogAdapter(AddNewAddressActivity.this, list, new CountryCallback() {
            @Override
            public void setItemList(int position, ArrayList<AreaModel> list) {
                AreaModel areaModel = list.get(position);
                String areaName = areaModel.getAreaName();
                areaId = areaModel.getAreaId();
                tvArea.setText("" + areaName);

                dialog.dismiss();

            }


        });
        listView.setAdapter(adapter);
        dialog.show();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            }
        });

        searchText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void UpadteAddress(final String addressId) {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_EDIT_ADDRESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseMyAddAddress", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("ok")) {
                        pd.dismiss();
                        Toast.makeText(AddNewAddressActivity.this, message, Toast.LENGTH_SHORT).show();
                        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                        finish();

                    } else {
                        pd.dismiss();
                        Toast.makeText(AddNewAddressActivity.this, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(AddNewAddressActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("address_id", addressId);
                params.put("customer_id", sp.getString("customerId", ""));
                params.put("title", etAddressName.getText().toString());
                params.put("area_id", areaId);
                params.put("block", etBlock.getText().toString());
                params.put("street", etStreet.getText().toString());
                params.put("avenue", etAvenue.getText().toString());
                params.put("building", etBuilding.getText().toString());
                params.put("floor", etFloor.getText().toString());
                params.put("apartment", etApartment.getText().toString());
                params.put("notes", etExtra.getText().toString());
                Log.e("MynewAddressParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);
        RequestQueue requestQueue = Volley.newRequestQueue(AddNewAddressActivity.this);
        requestQueue.add(request);
    }

}
