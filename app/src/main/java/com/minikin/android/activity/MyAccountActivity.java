package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.network.NetworkManager;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.MySession;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MyAccountActivity extends BaseActivity {
    private Activity activity = MyAccountActivity.this;
    private MySession mySession;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    private RelativeLayout rl_back, rl_menu;
    private TextView tv_title, tvCartCounter;
    private LinearLayout linearMyName, linearMyEmail;
    private TextView tvChangePw, tvMyAddress;
    private EditText etName, etEmail;
    private SharedPreferences sp = null;
    private NetworkManager networkManager;
    private ProgressDialog pd;
    private RelativeLayout rlSearch;
    private FrameLayout rlCart;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // requestWindowFeature(Window.FEATURE_NO_TITLE);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.act_my_acc, contentFrameLayout);
        initViews();
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        setLangRecreate(WhichLanguage);
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        mySession = new MySession(activity);
        networkManager = new NetworkManager(this);
        pd = new ProgressDialog(MyAccountActivity.this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        Log.e("tokenChk", sp.getString("access_token", ""));

    }

    @Override
    protected void onResume() {
        super.onResume();


        getMyProfile();
    }

    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_back.setOnClickListener(this);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rl_menu.setOnClickListener(this);
        rlSearch = findViewById(R.id.rl_search_at_toolbar);
        rlSearch.setVisibility(View.GONE);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tv_title.setText("My Account");
        tvCartCounter = findViewById(R.id.tv_cart_counter);
        linearMyName = (LinearLayout) findViewById(R.id.linear_et_name);
        linearMyEmail = (LinearLayout) findViewById(R.id.linear_email);
        etEmail = (EditText) findViewById(R.id.et_email);
        etName = (EditText) findViewById(R.id.et_name);
        tvChangePw = (TextView) findViewById(R.id.tv_change_pw);
        tvMyAddress = (TextView) findViewById(R.id.tv_my_address);
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setOnClickListener(this);
        tvMyAddress.setOnClickListener(this);
        tvChangePw.setOnClickListener(this);

    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        // Typeface fontEn = Typeface.createFromAsset(MyAccountActivity.this.getAssets(), "fonts/Avenir Roman Font Download.otf");

        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");

        Typeface fontAr = Typeface.createFromAsset(MyAccountActivity.this.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);
            //tv_title.setTypeface(fontAr);
            //tvChangePw.setTypeface(fontAr);
           // tvMyAddress.setTypeface(fontAr);
            etEmail.setTypeface(fontAr);
            etName.setTypeface(fontAr);
            tvCartCounter.setTypeface(fontAr);

            tv_title.setText("الملف الشخصي");

            tvMyAddress.setText("العنوان");
            tvChangePw.setText("تغيير كلمة السر");

        } else if (langval.equalsIgnoreCase("en")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tv_title.setTypeface(fontEnMedium);
            tvChangePw.setTypeface(fontEn);
            tvMyAddress.setTypeface(fontEn);
            etEmail.setTypeface(fontEn);
            etName.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);

        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {

            case R.id.toolbar_two_rl_back:
                finish();
                break;
            case R.id.tv_my_address:
                startActivity(new Intent(activity, MyAddressActivity.class));
                break;

            case R.id.tv_change_pw:
                startActivity(new Intent(activity, ChangePasswordActivity.class));
                break;
            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
                break;

            case R.id.rl_cart_icon_toolbar:
                if (loginValue.equalsIgnoreCase("login")) {
                    Intent intent = new Intent(activity, MyCartActivityNew.class);
                    startActivity(intent);
                } else if (!loginValue.equalsIgnoreCase("login")) {
                    Intent intentl = new Intent(activity, LoginActivity.class);
                    startActivity(intentl);
                }
                break;
        }
    }


    private void getMyProfile() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_MYPROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseProfile", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONObject jsonObjectResponse = objects.getJSONObject("result");
                        String name = jsonObjectResponse.getString("name");
                        String email = jsonObjectResponse.getString("email");
                        String username = jsonObjectResponse.getString("username");

                        etName.setText("" + username);
                        etEmail.setText("" + email);
                        etName.setEnabled(false);
                        etEmail.setEnabled(false);
                        tvCartCounter.setText("" + HomeActivityNew.cartCounter);

                        pd.dismiss();
                    } else {
                        pd.dismiss();
                        Toast.makeText(MyAccountActivity.this, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(MyAccountActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));

                Log.e("MyProfileParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(MyAccountActivity.this);
        requestQueue.add(request);
    }


}
