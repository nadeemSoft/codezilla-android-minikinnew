package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.util.AppConstants;

import java.net.URL;
import java.util.Locale;


public class KNetActivity extends BaseActivity {
    String orderId = null;
    private WebView webView = null;
    private TextView tv_title, tvDescrption, tvCartCounter;
    private SharedPreferences.Editor editor;
    private SharedPreferences pref;
    private RelativeLayout rl_back, rl_menu;
    private FrameLayout rlCart;
    private String WhichLanguage = "";
    private Activity activity = KNetActivity.this;
    private ProgressDialog pd;
    private SharedPreferences sp = null;
    private URL myURL = null;
    String strUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_k_net, contentFrameLayout);
        initViews();
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.show();

        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        setLangRecreate(WhichLanguage);
        Intent intent = getIntent();
        orderId = intent.getStringExtra("orderId");

        strUrl = AppConstants.GET_ORDER_CONFIRMATION + "?order_id=" + orderId;
        Log.e("strUrl", strUrl);



        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setWebViewClient(new WebViewClient(){

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                pd.show();
                view.loadUrl(url);

                return true;
            }
            @Override
            public void onPageFinished(WebView view, final String url) {
                pd.dismiss();
            }
        });

        webView.loadUrl(strUrl);



    }




    @Override
    protected void onResume() {
        super.onResume();



    }

    private void initViews() {

        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_back.setOnClickListener(this);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rl_menu.setOnClickListener(this);
        tvDescrption = findViewById(R.id.tv_terms_description);
        tvCartCounter = findViewById(R.id.tv_cart_counter);
        webView = findViewById(R.id.webView_k_net);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tv_title.setText("Order Placed");
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.toolbar_two_rl_back:
                Intent intent = new Intent(activity, HomeActivityNew.class);
                startActivity(intent);
                finish();
                break;
            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));

                    drawer.closeDrawer(GravityCompat.START);

                } else {

                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;



        }
    }


    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        // Typeface fontEn = Typeface.createFromAsset(TermsAndConditionActivity.this.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");
        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);
            tv_title.setText("تم الطلب!");
            tv_title.setTypeface(fontEnMedium);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tv_title.setTypeface(fontEnMedium);


        }
    }
}
