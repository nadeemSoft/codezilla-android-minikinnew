package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.AdapterMyFavroit;
import com.minikin.android.model.FavouriteModel;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MyFavroiteActivity extends BaseActivity {
    RecyclerView rvFavroite;
    private ProgressDialog pd;
    private TextView tvTitle;
    private RelativeLayout rl_back, rl_menu;
    private FrameLayout rlCart;

    private SharedPreferences sp = null;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    private Context context = MyFavroiteActivity.this;
    private Activity activity = MyFavroiteActivity.this;
    private TextView tvCartCounter = null;

    ArrayList<FavouriteModel> favouriteModelList = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_my_favroite, contentFrameLayout);
        initViews();
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        setLangRecreate(WhichLanguage);
        favouriteModelList = new ArrayList<>();
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getMyFavroit();


    }


    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.toolbar_two_rl_back:
                finish();
                break;

            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;

            case R.id.rl_cart_icon_toolbar:
                if (loginValue.equalsIgnoreCase("login")) {
                    Intent intent = new Intent(activity, MyCartActivityNew.class);
                    startActivity(intent);
                } else if (!loginValue.equalsIgnoreCase("login")) {
                    Intent intentl = new Intent(activity, LoginActivity.class);
                    startActivity(intentl);
                }
                break;

        }
    }

    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        tvTitle = findViewById(R.id.toolbar_two_tv_title);
        tvCartCounter = findViewById(R.id.tv_cart_counter);
        tvTitle.setText("My Favourites");
        rvFavroite = findViewById(R.id.rv_favroite);
        rlCart  = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_menu.setOnClickListener(this);


    }


    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
       // Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);
           // tvTitle.setTypeface(fontAr);
            tvCartCounter.setTypeface(fontAr);
            tvTitle.setText("المفضله");


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvTitle.setTypeface(fontEnMedium);
            tvCartCounter.setTypeface(fontEn);



        }
    }


    private void getMyFavroit() {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_MY_FAVROIT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("fAVROITResponse", response + "");
                favouriteModelList.clear();
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray resultJsonArray = objects.getJSONArray("result");

                        for (int i = 0; i < resultJsonArray.length(); i++) {
                            JSONObject jsonObjectResponse = resultJsonArray.getJSONObject(i);
                            String favoriteidFav = jsonObjectResponse.getString("favorite_id");
                            String productidFav = jsonObjectResponse.getString("product_id");
                            String name = jsonObjectResponse.getString("name");
                            String price = jsonObjectResponse.getString("price");
                            String image = jsonObjectResponse.getString("image");
                            String store = jsonObjectResponse.getString("store");

                            FavouriteModel favouriteModel = new FavouriteModel(favoriteidFav, productidFav, name, price, image, store);
                            favouriteModelList.add(favouriteModel);


                        }

                        LinearLayoutManager mLayoutManager3 = new LinearLayoutManager(activity);
                        rvFavroite.setLayoutManager(mLayoutManager3);
                        rvFavroite.setAdapter(new AdapterMyFavroit(activity, favouriteModelList));
                        rvFavroite.setNestedScrollingEnabled(false);

                    } else {
                        pd.dismiss();
                        Toast.makeText(MyFavroiteActivity.this, message, Toast.LENGTH_SHORT).show();

                    }

                    Log.e("cartCounter",HomeActivityNew.cartCounter+"");

                  tvCartCounter.setText(""+HomeActivityNew.cartCounter);


                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(MyFavroiteActivity.this, "Exception Two" + e, Toast.LENGTH_SHORT).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                Log.e("FAVParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(MyFavroiteActivity.this);
        requestQueue.add(request);

    }
}
