package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.MySession;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    String userName, password, emaiId, confirmPassword, mobileNo;
    private Activity activity = SignUpActivity.this;
    private MySession mySession;
    private TextView tvRegister, tvTitleRegister;
    private EditText etUserName, etPass, etEmail, etConfirmPassword, etMobileNo;
    private CheckBox cb;
    private ImageView ivBack;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    private ProgressDialog pd;
    private SharedPreferences sp = null;
    private SharedPreferences.Editor editorUser;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.act_sign_up);
        initViews();
        mySession = new MySession(activity);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        setLangRecreate(WhichLanguage);
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editorUser = sp.edit();
        pd = new ProgressDialog(SignUpActivity.this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);

    }

    private void initViews() {
        tvRegister = findViewById(R.id.act_sign_up_tv_register);
        tvRegister.setOnClickListener(this);
        tvTitleRegister = findViewById(R.id.tv_title_register);
        etUserName = findViewById(R.id.act_sign_up_et_uname);
        etEmail = findViewById(R.id.act_sign_up_et_email);
        etPass = findViewById(R.id.et_password);
        etConfirmPassword = findViewById(R.id.et_confirm_pw);
        etMobileNo = findViewById(R.id.et_mobile);
        cb = findViewById(R.id.act_sign_up_cb);
        cb.setOnClickListener(this);
        cb.setButtonDrawable(R.drawable.unselected);

        ivBack = (ImageView) findViewById(R.id.iv_back);
        ivBack.setOnClickListener(this);
        etUserName.setHint(getResources().getString(R.string.HintUserName).toUpperCase());
        etEmail.setHint(getResources().getString(R.string.HintEmail).toUpperCase());
        etMobileNo.setHint(getResources().getString(R.string.HintMobile).toUpperCase());
        etPass.setHint(getResources().getString(R.string.HintPassword).toUpperCase());
        etConfirmPassword.setHint(getResources().getString(R.string.HintPasswordConfirm).toUpperCase());

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.act_sign_up_tv_register:
                getDataFromUser();
                break;
            case R.id.iv_back:
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.act_sign_up_cb:
                if (cb.isChecked()) {
                    cb.setButtonDrawable(R.drawable.chekboxselected);
                    Intent intentTerms = new Intent(SignUpActivity.this, TermsAndConditionActivity.class);
                    startActivity(intentTerms);

                } else if (!cb.isChecked()) {
                    cb.setButtonDrawable(R.drawable.unselected);

                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
        startActivity(intent);
        super.onBackPressed();

    }

    private boolean checkValidation() {
        String mailId = etEmail.getText().toString().trim();
        if (etUserName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Enter Username", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etEmail.getText().toString().equalsIgnoreCase("")) {

            Toast.makeText(activity, "Enter Email Address", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!mailId.matches(emailPattern)) {
            Toast.makeText(activity, "Please Enter Valid Email Address", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etPass.getText().toString().equalsIgnoreCase("")) {

            Toast.makeText(activity, "Enter password", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etConfirmPassword.getText().toString().equalsIgnoreCase("")) {

            Toast.makeText(activity, "Enter Confirm Password ", Toast.LENGTH_SHORT).show();
            return false;

        } else if (!etPass.getText().toString().equalsIgnoreCase(etConfirmPassword.getText().toString())) {
            Toast.makeText(activity, " Confirm Password is Wrong", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!cb.isChecked()) {
            Toast.makeText(activity, "Please Accept Terms & Conditions", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private boolean checkValidationUrdu() {
        String mailId = etEmail.getText().toString().trim();
        if (etUserName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "أدخل اسم المستخدم", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etEmail.getText().toString().equalsIgnoreCase("")) {

            Toast.makeText(activity, "أدخل البريد الإلكتروني", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!mailId.matches(emailPattern)) {
            Toast.makeText(activity, "يرجى إدخال بريد إلكتروني صحيح", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etPass.getText().toString().equalsIgnoreCase("")) {

            Toast.makeText(activity, "أدخل الرمز السري", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etConfirmPassword.getText().toString().equalsIgnoreCase("")) {

            Toast.makeText(activity, "تأكيد كلمة المرور ", Toast.LENGTH_SHORT).show();
            return false;

        } else if (!etPass.getText().toString().equalsIgnoreCase(etConfirmPassword.getText().toString())) {
            Toast.makeText(activity, " تأكيد كلمة المرور خاطئ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!cb.isChecked()) {
            Toast.makeText(activity, "يرجى الموافقة على الشروط والأحكام", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(SignUpActivity.this.getAssets(), "fonts/Avenir Roman.otf");
        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);


            tvTitleRegister.setText("التسجيل");
            etUserName.setHint("اسم المستخدم");
            etEmail.setHint("البريد الإلكتروني");
            etPass.setHint("أدخل الرمز السري");
            etConfirmPassword.setHint("تأكيد كلمة المرور");
            tvRegister.setText("التسجيل");
            cb.setText("الشروط والاحكام");



            tvTitleRegister.setTypeface(fontEnMedium);
            tvRegister.setTypeface(fontEn);
            etUserName.setTypeface(fontEn);
            etEmail.setTypeface(fontEn);
            etPass.setTypeface(fontEn);
            etConfirmPassword.setTypeface(fontEn);
            cb.setTypeface(fontEnBold);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvTitleRegister.setTypeface(fontEnMedium);
            tvRegister.setTypeface(fontEn);
            etUserName.setTypeface(fontEn);
            etEmail.setTypeface(fontEn);
            etPass.setTypeface(fontEn);
            etConfirmPassword.setTypeface(fontEn);
            cb.setTypeface(fontEnBold);

        }
    }


    private void getDataFromUser() {
        userName = etUserName.getText().toString();
        emaiId = etEmail.getText().toString();
        password = etPass.getText().toString();
        mobileNo = etMobileNo.getText().toString();
        confirmPassword = etConfirmPassword.getText().toString();

        if (WhichLanguage.equalsIgnoreCase("en")) {
            if (checkValidation()) {
                pd.show();

                editorUser.putString("username", userName);
                editorUser.putString("password", password);
                editorUser.putString("emaiId", emaiId);
                editorUser.commit();
                userRegistration();


            }

        } else if (WhichLanguage.equalsIgnoreCase("ar")) {
            if (checkValidationUrdu()) {
                pd.show();

                editorUser.putString("username", userName);
                editorUser.putString("password", password);
                editorUser.putString("emaiId", emaiId);
                editorUser.commit();
                userRegistration();


            }


        }


    }

    private void userRegistration() {

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_REGISTRATION, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e(" SignUpResponse", response);

                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONObject jsonObjResult = objects.getJSONObject("result");
                        String customerId = jsonObjResult.getString("customer_id");
                        String customerName = jsonObjResult.getString("customer_name");
                        String customerEmail = jsonObjResult.getString("email");
                        String usernameResult = jsonObjResult.getString("username");
                        editorUser.putString("customerId", customerId);
                        editorUser.putString("customerName", customerName);
                        editorUser.putString("customerEmail", customerEmail);
                        editorUser.putString("usernameResult", usernameResult);
                        editorUser.putString("loginValue", "login");
                        editorUser.commit();
                        pd.dismiss();

                        if (WhichLanguage.equalsIgnoreCase("en")){
                            Toast.makeText(SignUpActivity.this, "Thank you for Registering... ", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(activity, LoginActivity.class));
                            finish();
                        }else {
                            Toast.makeText(SignUpActivity.this, "شكراً لتسجيلكم... ", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(activity, LoginActivity.class));
                            finish();
                        }


                    } else if (status.equalsIgnoreCase("false")) {
                        JSONObject jsonObjectErros = objects.getJSONObject("errors");
                        pd.dismiss();
                        Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_LONG).show();
                        /*startActivity(new Intent(activity, LoginActivity.class));
                        finish();*/

                    }


                    //startActivity(new Intent(activity, LoginActivity.class));


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");
                    Toast.makeText(SignUpActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");
                Toast.makeText(SignUpActivity.this, "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("username", sp.getString("username", ""));
                params.put("email", sp.getString("emaiId", ""));
                params.put("password", sp.getString("password", ""));
                Log.e("sinupParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(SignUpActivity.this);
        requestQueue.add(request);


    }

}


