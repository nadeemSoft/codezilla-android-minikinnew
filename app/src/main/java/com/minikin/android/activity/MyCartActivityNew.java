package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.CartAdapter;
import com.minikin.android.model.CartItemModel;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.MySession;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MyCartActivityNew extends BaseActivity {
    double totalPrice = 0.000;
    long quantitlyLong;
    double productPriceDouble = 0.000;
    double price;
    private RecyclerView rvMainCart;
    private Activity activity = MyCartActivityNew.this;
    private MySession mySession;
    private RelativeLayout rl_back, rl_menu, rlCart;
    private TextView tvTitleMain, tvCheckout, tvSubTotalTitle, tvCartEmptyText;
    private TextView tvSubTotalMainCart;
    private TextView tvCartCounter;
    private SharedPreferences pref;
    private RelativeLayout rlSearchIconToolbar;
    private FrameLayout rlCartIconToollbar;
    private String WhichLanguage = "";
    private ProgressDialog pd;
    private SharedPreferences sp = null;
    private ArrayList<CartItemModel> cartItemList;
    private LinearLayout llSubTotalCart, llCartEmpty;
    SharedPreferences.Editor editor;
    CartAdapter cartAdapter;
    private DecimalFormat precision;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_my_cart_new, contentFrameLayout);
        initViews();
        precision = new DecimalFormat("0.000");

        /*NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMAN);
         precision = (DecimalFormat)nf;*/

        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editor = sp.edit();
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);

        cartItemList = new ArrayList<>();
        mySession = new MySession(activity);
        setLangRecreate(WhichLanguage);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCart();

    }

    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rl_menu.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        tvCheckout = findViewById(R.id.tv_next_cart);
        tvCheckout.setOnClickListener(this);
        tvCheckout.setVisibility(View.GONE);
        tvCartCounter = findViewById(R.id.tv_cart_counter);
        tvTitleMain = findViewById(R.id.toolbar_two_tv_title);
        rvMainCart = findViewById(R.id.my_main_cart);
        tvCartEmptyText = findViewById(R.id.tv_cart_empty_text);
        tvSubTotalMainCart = findViewById(R.id.tv_total_subtotal_cart);
        tvSubTotalTitle = findViewById(R.id.tv_total_cart_title);
        rlCartIconToollbar = findViewById(R.id.rl_cart_icon_toolbar);
        rlSearchIconToolbar = findViewById(R.id.rl_search_at_toolbar);
        llSubTotalCart = findViewById(R.id.ll_subTotal_cart);
        llCartEmpty = findViewById(R.id.ll_cart_empty_text);

        llSubTotalCart.setVisibility(View.GONE);
        rlSearchIconToolbar.setVisibility(View.GONE);
        rlCartIconToollbar.setVisibility(View.GONE);


    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {

            case R.id.toolbar_two_rl_back:
                finish();
                break;

            case R.id.act_my_cart_tv_checkout:

                //startActivity(new Intent(activity, CheckoutActivity.class));
                break;
            case R.id.toolbar_rl_menutwo:
                //openDrawer();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));

                    drawer.closeDrawer(GravityCompat.START);

                } else {

                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;

            case R.id.iv_home:
                Intent intenth = new Intent(activity, HomeActivityNew.class);
                startActivity(intenth);
                finish();
                break;

            case R.id.tv_next_cart:

                if (loginValue.equalsIgnoreCase("login")) {
                    Intent intentCartNext = new Intent(activity, CheckOutActivityNew.class);
                    startActivity(intentCartNext);
                } else if (!loginValue.equalsIgnoreCase("login")) {
                    Intent intentl = new Intent(activity, LoginActivity.class);
                    startActivity(intentl);
                }

                break;

            case R.id.rl_cart_icon_toolbar:
                Intent intent = new Intent(activity, MyCartActivityNew.class);
                startActivity(intent);
               /* if (loginValue.equalsIgnoreCase("login")) {
                    Intent intent = new Intent(activity, MyCartActivityNew.class);
                    startActivity(intent);
                } else if (!loginValue.equalsIgnoreCase("login")) {
                    Intent intentl = new Intent(activity, LoginActivity.class);
                    startActivity(intentl);
                }*/


                break;


        }
    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        // Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);

            tvTitleMain.setText("سلة المشتريات");
            tvSubTotalTitle.setText("حاصل الجمع  : ");
            tvCheckout.setText("الدفع");
            tvCartEmptyText.setText("للأسف السلة فارغة!");

            tvTitleMain.setTypeface(fontEnMedium);
            tvSubTotalTitle.setTypeface(fontEn);
            tvSubTotalMainCart.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);
            tvCheckout.setTypeface(fontEn);
            tvCartEmptyText.setTypeface(fontEn);
        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvTitleMain.setTypeface(fontEnMedium);
            tvSubTotalMainCart.setTypeface(fontEn);
            tvSubTotalTitle.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);
            tvCheckout.setTypeface(fontEn);
            tvCartEmptyText.setTypeface(fontEn);


        }
    }


    private void getCart() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_MY_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseCart", response);
                cartItemList.clear();
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray resultJsonArray = objects.getJSONArray("result");
                        for (int i = 0; i < resultJsonArray.length(); i++) {
                            JSONObject jsonObjectResponse = resultJsonArray.getJSONObject(i);
                            String cart_id = jsonObjectResponse.getString("cart_id");
                            String product_id = jsonObjectResponse.getString("product_id");
                            String quantity = jsonObjectResponse.getString("quantity");
                            String product_price = jsonObjectResponse.getString("product_price");
                            String product_img = jsonObjectResponse.getString("product_img");
                            String product_name = jsonObjectResponse.getString("product_name");
                            String store_name = jsonObjectResponse.getString("store_name");
                            String attribute_1 = jsonObjectResponse.getString("attribute_1");
                            String attribute_2 = jsonObjectResponse.getString("attribute_2");
                            String total = jsonObjectResponse.getString("total");

                            CartItemModel cartItem = new CartItemModel(cart_id, product_id, quantity, product_price, product_img, product_name, store_name, attribute_1, attribute_2, total);
                            cartItemList.add(cartItem);
                        }
                        llCartEmpty.setVisibility(View.GONE);

                        tvCheckout.setVisibility(View.VISIBLE);
                        llSubTotalCart.setVisibility(View.VISIBLE);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                        rvMainCart.setLayoutManager(mLayoutManager);
                        cartAdapter = new CartAdapter(cartItemList, R.layout.rowitem_adapter_my_cart, activity);
                        rvMainCart.setAdapter(cartAdapter);
                        rvMainCart.setNestedScrollingEnabled(false);
                        // tvSubTotalMainCart.setText("KD"+" "+CartAdapter.totalPrice);
                        computePrice();
                        pd.dismiss();
                    } else {
                        pd.dismiss();

                        llCartEmpty.setVisibility(View.VISIBLE);
                        llSubTotalCart.setVisibility(View.GONE);
                        tvCheckout.setVisibility(View.GONE);


                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                        //finish();

                    }
                    pd.dismiss();

                } catch (Exception e) {
                    pd.dismiss();
                    //Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                params.put("device_id", sp.getString("deviceId", ""));
                Log.e("MyCartParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }


    public void computePrice() {

        for (int i = 0; i < cartAdapter.cartItemList.size(); i++) {
            final CartItemModel cartItem = cartAdapter.cartItemList.get(i);
            quantitlyLong = Long.parseLong(((cartItem.getQuantity())));
            productPriceDouble = Double.valueOf((cartItem.getProductPrice()));
            Log.e("QUANTITY", quantitlyLong + "");
            Log.e("PRICE", productPriceDouble + "");

            if (quantitlyLong != 0) {
                totalPrice = (totalPrice + (quantitlyLong * productPriceDouble));
                Log.e("totalPrice", totalPrice + "");
                editor.putString("totalPrice", totalPrice + "");
                editor.commit();
            } else if (quantitlyLong == 0) {
                totalPrice = (totalPrice + productPriceDouble);
                editor.putString("totalPrice", totalPrice + "");
                editor.commit();

            }

        }



        if (WhichLanguage.equalsIgnoreCase("ar")) {

            Log.e("lang", WhichLanguage);
            Configuration config = getBaseContext().getResources().getConfiguration();
            Locale locale = new Locale("en");
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

            precision = new DecimalFormat("0.000");

            tvSubTotalMainCart.setText("KD" + "   " + precision.format(totalPrice));


            Log.e("lang", WhichLanguage);
            Configuration config1 = getBaseContext().getResources().getConfiguration();
            Locale locale1 = new Locale("ar");
            Locale.setDefault(locale1);
            config1.locale = locale1;
            getBaseContext().getResources().updateConfiguration(config1, getBaseContext().getResources().getDisplayMetrics());



           /* String tota =precision.format(totalPrice);


            try {

                String[] str = tota.split("\\.");
                tvSubTotalMainCart.setText("KD" + "   " + str[0] + ".000");

            }catch (Exception e){
                tvSubTotalMainCart.setText("KD" + "   " + tota + ".000");

            }*/


        } else if (WhichLanguage.equalsIgnoreCase("en")) {
            tvSubTotalMainCart.setText("KD" + "   " + precision.format(totalPrice));

        }
        totalPrice = 0.000;

    }


    public void getCartForCounter() {

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_MY_CART_COUNT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseCart", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String count = objects.getString("count");

                    if (status.equalsIgnoreCase("true")) {
                        HomeActivityNew.cartCounter = Integer.parseInt(count);
                        Log.e("cartArray", count + "");
                        tvCartCounter.setText("" + HomeActivityNew.cartCounter);

                        pd.dismiss();
                    } else {
                        pd.dismiss();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                params.put("device_id", sp.getString("deviceId", ""));
                Log.e("MyCartParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }


}
