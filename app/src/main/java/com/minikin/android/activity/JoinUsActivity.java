package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.util.AppConstants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class JoinUsActivity extends BaseActivity {

    private RelativeLayout rl_back, rl_menu;
    private FrameLayout rlCart;

    private TextView tv_title, tvSubmit, tvCartCounter;
    private EditText etStoreOfficerName, etContactName, etEmail, etMobile, etTelephone, etWebsite, etSince, etFieldBusiness, etExtra;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    private Activity activity = JoinUsActivity.this;
    private ProgressDialog pd;
    private SharedPreferences sp = null;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.act_join_us, contentFrameLayout);
        initViews();
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        setLangRecreate(WhichLanguage);
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);

    }

    @Override
    protected void onResume() {
        super.onResume();

        tvCartCounter.setText("" + HomeActivityNew.cartCounter);
    }

    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tv_title.setText("Join Us - Stores");
        tvSubmit = findViewById(R.id.tv_join_submit);
        etStoreOfficerName = findViewById(R.id.et_store_officer_name);
        etContactName = findViewById(R.id.et_join_contact_name);
        etEmail = findViewById(R.id.et_join_email);
        etMobile = findViewById(R.id.et_join_mobile_no);
        etTelephone = findViewById(R.id.et_join_telephone_no);
        etWebsite = findViewById(R.id.et_join_website);
        etSince = findViewById(R.id.et_join_sience);
        etFieldBusiness = findViewById(R.id.et_join_field_business);
        etExtra = findViewById(R.id.et_join_extra);
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        tvCartCounter = findViewById(R.id.tv_cart_counter);

        rlCart.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_menu.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.toolbar_two_rl_back:
                finish();
                break;

            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;
            case R.id.tv_join_submit:

                if (WhichLanguage.equalsIgnoreCase("en")) {

                    if (chechkValidation()) {
                        getJoinUs();
                    }

                } else if (WhichLanguage.equalsIgnoreCase("ar")) {


                    if (chechkValidationUrdu()) {
                        getJoinUs();
                    }

                }

                break;

            case R.id.rl_cart_icon_toolbar:
                Intent intent = new Intent(activity, MyCartActivityNew.class);
                startActivity(intent);
                break;

        }
    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(JoinUsActivity.this.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(JoinUsActivity.this.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);

            tv_title.setText("انظم إلى متاجرنا");
            etMobile.setHint("رقم الهاتف");
            etExtra.setHint("تفاصيل اضافيه");
            tvSubmit.setText("اختار");
            etStoreOfficerName.setHint("الاسم الرسمي للمتجر");
            etEmail.setHint("البريد الإلكتروني");
            etContactName.setHint("الاسم");
            etTelephone.setHint("رقم الهاتف");
            etSince.setHint("منذ");
            etWebsite.setHint("الموقع الرسمي");
            etFieldBusiness.setHint("مجال العمل");

            tv_title.setTypeface(fontEnMedium);
            tvSubmit.setTypeface(fontEn);
            etStoreOfficerName.setTypeface(fontEn);
            etContactName.setTypeface(fontEn);
            etEmail.setTypeface(fontEn);
            etMobile.setTypeface(fontEn);
            etTelephone.setTypeface(fontEn);
            etWebsite.setTypeface(fontEn);
            etSince.setTypeface(fontEn);
            etFieldBusiness.setTypeface(fontEn);
            etExtra.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tv_title.setTypeface(fontEnMedium);
            tvSubmit.setTypeface(fontEn);
            etStoreOfficerName.setTypeface(fontEn);
            etContactName.setTypeface(fontEn);
            etEmail.setTypeface(fontEn);
            etMobile.setTypeface(fontEn);
            etTelephone.setTypeface(fontEn);
            etWebsite.setTypeface(fontEn);
            etSince.setTypeface(fontEn);
            etFieldBusiness.setTypeface(fontEn);
            etExtra.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);


        }
    }

    private boolean chechkValidation() {
        if (etStoreOfficerName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(JoinUsActivity.this, "Please Enter Store Name", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etContactName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(JoinUsActivity.this, "Please Enter Contact Name", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etEmail.getText().toString().trim().equalsIgnoreCase("")) {
            Toast.makeText(JoinUsActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!etEmail.getText().toString().trim().matches(emailPattern)) {
            Toast.makeText(JoinUsActivity.this, "Please Enter Valid Email", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etMobile.getText().toString().matches("") || etMobile.getText().length() != 10) {
            Toast.makeText(JoinUsActivity.this, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etTelephone.getText().toString().matches("")) {
            Toast.makeText(JoinUsActivity.this, "Please Enter Telephone Number", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean chechkValidationUrdu() {
        if (etStoreOfficerName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(JoinUsActivity.this, "يرجى إدخال اسم المتجر", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etContactName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(JoinUsActivity.this, "يرجى إدخال اسم الشخص", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etEmail.getText().toString().trim().equalsIgnoreCase("")) {
            Toast.makeText(JoinUsActivity.this, "يرجى إدخال البريد الإلكتروني", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!etEmail.getText().toString().trim().matches(emailPattern)) {
            Toast.makeText(JoinUsActivity.this, "يرجى إدخال بريد إلكتروني صحيح", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etMobile.getText().toString().matches("")) {
            Toast.makeText(JoinUsActivity.this, "يرجى إدخال رقم الهاتف", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etMobile.getText().length() != 10) {
            Toast.makeText(JoinUsActivity.this, "يرجى إدخال رقم هاتف صحيح", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etTelephone.getText().toString().matches("")) {
            Toast.makeText(JoinUsActivity.this, "يرجى إدخال رقم الهاتف", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void getJoinUs() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_JOIN_US, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseJoinUs", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONObject jsonObjectResponse = objects.getJSONObject("result");
                        String customer_id = jsonObjectResponse.getString("customer_id");
                        String customer_name = jsonObjectResponse.getString("customer_name");
                        String email = jsonObjectResponse.getString("email");
                        String username = jsonObjectResponse.getString("username");

                        pd.dismiss();
                        Toast.makeText(JoinUsActivity.this, message, Toast.LENGTH_SHORT).show();

                    } else {
                        pd.dismiss();
                        Toast.makeText(JoinUsActivity.this, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(JoinUsActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("store_name", etStoreOfficerName.getText().toString());
                params.put("contact_name", etContactName.getText().toString());
                params.put("email", etEmail.getText().toString().trim());
                params.put("mobile", etMobile.getText().toString());
                params.put("telephone", etTelephone.getText().toString());
                params.put("website", etWebsite.getText().toString());
                params.put("since", etSince.getText().toString());
                params.put("desc", etExtra.getText().toString());
                params.put("username", sp.getString("usernameResult", ""));
                Log.e("JoinUsParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(JoinUsActivity.this);
        requestQueue.add(request);
    }
}

