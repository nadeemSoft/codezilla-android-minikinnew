package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.util.AppConstants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ReturnExchangeActivity  extends BaseActivity {

    private RelativeLayout rl_back, rl_menu;
    private FrameLayout rlCart;

    private TextView tv_title, tvDescrption,tvCartCounter;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    private Activity activity = ReturnExchangeActivity.this;
    private ProgressDialog pd;
    private SharedPreferences sp = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_return_exchange, contentFrameLayout);
        initViews();
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        setLangRecreate(WhichLanguage);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
    }


    @Override
    protected void onResume() {
        super.onResume();
        tvCartCounter.setText(""+HomeActivityNew.cartCounter);

        getReturnExchange();

    }

    private void initViews() {

        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_back.setOnClickListener(this);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rl_menu.setOnClickListener(this);
        tvDescrption = findViewById(R.id.tv_terms_description);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tvCartCounter = findViewById(R.id.tv_cart_counter);
        tv_title.setText("Return & Exchange");
        rlCart  = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setOnClickListener(this);
    }





    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.toolbar_two_rl_back:
                finish();
                break;
            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));

                    drawer.closeDrawer(GravityCompat.START);

                } else {

                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;
            case R.id.rl_cart_icon_toolbar:
                Intent intent = new Intent(activity, MyCartActivityNew.class);
                startActivity(intent);
                break;


        }
    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        // Typeface fontEn = Typeface.createFromAsset(TermsAndConditionActivity.this.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");



        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");
        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);


            tv_title.setText("الاسترجاع والاستبدال");

            //tv_title.setTypeface(fontAr);
            tvDescrption.setTypeface(fontAr);
            tvCartCounter.setTypeface(fontEn);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tv_title.setTypeface(fontEnMedium);
            tvDescrption.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);

        }
    }


    private void getReturnExchange() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.GET, AppConstants.GET_RETURN_EXCHANGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponsReturn", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONObject jsonObjectResponse = objects.getJSONObject("result");
                        String title = jsonObjectResponse.getString("title");
                        String content = jsonObjectResponse.getString("content");
                        tvDescrption.setText("" + content);
                        pd.dismiss();
                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                Log.e("TermsAndConditiom", params + "");
                return params;
            }

        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }








}
