package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.AdapterCategoryHome3;
import com.minikin.android.model.AttributesModel;
import com.minikin.android.model.CategoryModel;
import com.minikin.android.model.SetAttributeModel;
import com.minikin.android.model.SubCategoryModel;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class HomeActivityNew extends BaseActivity {
    public static int cartCounter = 0;
    private Activity activity = HomeActivityNew.this;
    private String android_id = null;
    private LinearLayout llMainToolbar;
    int newSearch = 0;
    private SharedPreferences pref;
    private String WhichLanguage = "";
    private ProgressDialog pd;
    private SharedPreferences sp = null;
    private SharedPreferences.Editor editorSp;
    private RelativeLayout rl_back, rl_menu, rlSearh;
    private TextView tv_title, tvCartCounter;
    private EditText etSearchNew;
    private ImageView ivFilter, ivCard, ivSearchNew;
    private ArrayList<SubCategoryModel> subCategoryList;
    public ArrayList<CategoryModel> categoriesList;
    private RecyclerView rvCategory;
    private FrameLayout rlCart, frameEIcon, framEIconEnglish;
    AdapterCategoryHome3 categoryHome3;
    public static ArrayList<SetAttributeModel> attributeMainList;
    boolean isEtSerachShowing;
    private SharedPreferences.Editor editorUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_home3);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_home3, contentFrameLayout);
        initViews();
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);

        editorUser = pref.edit();
        WhichLanguage = sp.getString("WhichLanguage", "en");
        attributeMainList = new ArrayList<>();
        setLangRecreate(WhichLanguage);

        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        categoriesList = new ArrayList<>();
        android_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        editorSp = sp.edit();
        Log.e("DeviceId..", android_id);
        editorSp.putString("deviceId", android_id);
        editorSp.commit();


        getCategory(); //GET CATEGORY AND GET FILTER DATA


        //getCategoryNew();


       /* etNewSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    // do your stuff here
                    Log.e("TEXTSEARCH", etNewSearch.getText().toString());

                    if (etNewSearch.length() >= 0 && !etNewSearch.getText().toString().trim().equals("")) {
                        Intent intentSearch = new Intent(activity, SearchActivity.class);
                        intentSearch.putExtra("Text", etNewSearch.getText().toString());
                        startActivity(intentSearch);
                        etNewSearch.setText("");

                    }

                }
                return false;
            }
        });*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        llMainToolbar.setVisibility(View.VISIBLE);
        // etNewSearch.setVisibility(View.GONE);
        getCartCount();


    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.toolbar_two_rl_back:
                finish();
                break;

            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;


            case R.id.rl_cart_icon_toolbar:
                Intent intent = new Intent(activity, MyCartActivityNew.class);
                startActivity(intent);
                break;

            case R.id.rl_search_at_toolbar:
                if (newSearch == 0) {
                    isEtSerachShowing = true;
                    newSearch = 1;
                    //llMainToolbar.setVisibility(View.GONE);
                    // llNewSearch.setVisibility(View.VISIBLE);
                    //etNewSearch.setVisibility(View.VISIBLE);
                    //etNewSearch.requestFocus();
                    //  InputMethodManager imm = (InputMethodManager) getSystemService(activity.INPUT_METHOD_SERVICE);
                    // imm.showSoftInput(etNewSearch, InputMethodManager.SHOW_IMPLICIT);
                } else if (newSearch == 1) {
                    isEtSerachShowing = false;
                   /* getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    InputMethodManager imm = (InputMethodManager) getSystemService(activity.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(etNewSearch, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    newSearch = 0;
                    etNewSearch.setVisibility(View.GONE);
                    etNewSearch.clearFocus();*/


                }

                break;

            case R.id.iv_search_new:
                llMainToolbar.setVisibility(View.VISIBLE);
                etSearchNew.clearFocus();
                break;

            case R.id.frame_e_icon:
                if (WhichLanguage.equalsIgnoreCase("en")) {
                    editorUser.putString("WhichLanguage", "ar");
                    editorSp.putString("WhichLanguage", "ar");
                    editorSp.commit();
                    editorUser.commit();
                    finishAffinity();
                    Intent intent1 = new Intent(HomeActivityNew.this, HomeActivityNew.class);
                    startActivity(intent1);

                } else if (WhichLanguage.equalsIgnoreCase("ar")) {
                    editorUser.putString("WhichLanguage", "en");
                    editorSp.putString("WhichLanguage", "en");
                    editorSp.commit();
                    editorUser.commit();

                    finishAffinity();
                    Intent intent1 = new Intent(HomeActivityNew.this, HomeActivityNew.class);
                    startActivity(intent1);

                }

                break;

            case R.id.frame_e_icon_english:
                if (WhichLanguage.equalsIgnoreCase("en")) {
                    editorUser.putString("WhichLanguage", "ar");
                    editorUser.commit();
                    editorSp.putString("WhichLanguage", "ar");
                    editorSp.commit();

                } else if (WhichLanguage.equalsIgnoreCase("ar")) {
                    editorUser.putString("WhichLanguage", "en");
                    editorUser.commit();
                    editorSp.putString("WhichLanguage", "en");
                    editorSp.commit();

                }
                finish();
                Intent intent2 = new Intent(HomeActivityNew.this, HomeActivityNew.class);
                startActivity(intent2);
                break;


        }

    }

    @Override
    public void onBackPressed() {
        if (isEtSerachShowing) {
            llMainToolbar.setVisibility(View.VISIBLE);
            isEtSerachShowing = false;
        } else {

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            finishAffinity();
            System.exit(0);
            super.onBackPressed();


        }


    }

    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rlSearh = findViewById(R.id.rl_search_at_toolbar);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tv_title.setText("Home");
        rvCategory = findViewById(R.id.rv_category_home_3);
        etSearchNew = findViewById(R.id.et_search_new);
        frameEIcon = findViewById(R.id.frame_e_icon);
        frameEIcon.setOnClickListener(this);
        framEIconEnglish = findViewById(R.id.frame_e_icon_english);
        framEIconEnglish.setOnClickListener(this);

        ivSearchNew = findViewById(R.id.iv_search_new);
        //etNewSearch = findViewById(R.id.et_search_new_show);


        llMainToolbar = findViewById(R.id.ll_main_toolbar);
        tvCartCounter = findViewById(R.id.tv_cart_counter);


        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_back.setVisibility(View.GONE);
        rlSearh.setVisibility(View.GONE);

        rl_menu.setOnClickListener(this);
        rlSearh.setOnClickListener(this);
        ivSearchNew.setOnClickListener(this);


    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");

        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);

            tv_title.setTypeface(fontEnMedium);
            tvCartCounter.setTypeface(fontEn);
            tv_title.setText("الصفحة الرئيسيه");
            framEIconEnglish.setVisibility(View.VISIBLE);
            frameEIcon.setVisibility(View.GONE);
            Log.e("ARABIC",":AR");


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tv_title.setTypeface(fontEnMedium);
            tvCartCounter.setTypeface(fontEn);
            frameEIcon.setVisibility(View.VISIBLE);
            //framEIconEnglish.setVisibility(View.GONE);

            Log.e("ENGLISH","EN");


        }
    }


    private void getCategory() {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_CATEGORY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                categoriesList.clear();

                try {
                    Log.e("CategoryResponseHOme3", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objCat = jsonArrayResult.getJSONObject(i);
                            String catId = objCat.getString("cat_id");
                            String catName = objCat.getString("cat_name");
                            String catImg = objCat.getString("cat_img");

                            subCategoryList = new ArrayList<>();
                            subCategoryList.clear();

                            JSONArray jsonArraySubCategory = objCat.getJSONArray("sub_cats");
                            for (int j = 0; j < jsonArraySubCategory.length(); j++) {
                                JSONObject objSub = jsonArraySubCategory.getJSONObject(j);
                                String subId = objSub.getString("sub_id");
                                String subName = objSub.getString("sub_name");
                                String subImg = objSub.getString("sub_img");

                                SubCategoryModel subCategoryModel = new SubCategoryModel(subId, subName, subImg);
                                subCategoryList.add(subCategoryModel);
                            }

                            CategoryModel categoryOld = new CategoryModel(catId, catName, catImg, subCategoryList);
                            categoriesList.add(categoryOld);


                        }

                        setAdapter(categoriesList);

                        getCartCount();


                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                    }

                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_LONG).show();


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                Log.e("HomeParams", params + "");
                return params;
            }


        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);


    }


    private void getCategoryNew() {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_CATEGORY, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                categoriesList.clear();

                try {
                    Log.e("CategoryResponseHOme3", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objCat = jsonArrayResult.getJSONObject(i);
                            String catId = objCat.getString("cat_id");
                            String catName = objCat.getString("cat_name");
                            String catImg = objCat.getString("cat_img");


                            /*CategoryModel category = new CategoryModel(catId, catName, catImg, subCategoryList);
                            categoriesList.add(category);*/

                            /*CategoryModelNew categoryModelNew = new CategoryModelNew(catId, catName, catImg);
                            categoriesList.add(categoryModelNew);*/


                        }


                        pd.dismiss();
                        //setAdapter(categoriesList);


                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                    }

                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_LONG).show();


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                Log.e("HomeParams", params + "");
                return params;
            }


        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);


    }


    private void setAdapter(ArrayList<CategoryModel> categoriesList) {

        categoryHome3 = new AdapterCategoryHome3(activity, categoriesList);
        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getApplicationContext());
        rvCategory.setLayoutManager(mLayoutManager3);
        rvCategory.setAdapter(categoryHome3);
        rvCategory.setNestedScrollingEnabled(false);


       /* categoryNew = new AdapterCategoryNew(activity, categoriesList);
        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getApplicationContext());
        rvCategory.setLayoutManager(mLayoutManager3);
        rvCategory.setAdapter(categoryNew);
        //rvCategory.setNestedScrollingEnabled(false);*/

    }


    private void getFilteData() {
        String urlFilter = AppConstants.GET_FILTER + "?lang=" + sp.getString("WhichLanguage", "");
        Log.e("filterUrl", urlFilter + "");

        StringRequest request = new StringRequest(Request.Method.GET, urlFilter, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    attributeMainList.clear();
                    Log.e("FilterResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objectFilter = jsonArrayResult.getJSONObject(i);
                            String setId = objectFilter.getString("set_id");
                            String setName = objectFilter.getString("set_name");

                            JSONArray jsonArraySetAttributes = objectFilter.getJSONArray("set_attributes");

                            ArrayList<AttributesModel> attributesModelArrayList = new ArrayList<>();
                            attributesModelArrayList.clear();

                            for (int j = 0; j < jsonArraySetAttributes.length(); j++) {

                                JSONObject jsonObjectSetAttributes = jsonArraySetAttributes.getJSONObject(j);
                                String attribute_id = jsonObjectSetAttributes.getString("attribute_id");
                                String attribute_name = jsonObjectSetAttributes.getString("attribute_name");

                                AttributesModel attributesModel = new AttributesModel(attribute_id, attribute_name);
                                attributesModelArrayList.add(attributesModel);
                            }

                            SetAttributeModel setAttributeModel = new SetAttributeModel(setId, setName, attributesModelArrayList);
                            attributeMainList.add(setAttributeModel);
                        }


                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                    }

                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_LONG).show();


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");
                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                Log.e("HomeParams", params + "");
                return params;
            }


        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }


    public void getCartCount() {

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_MY_CART_COUNT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseCartCOUNT", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    if (status.equalsIgnoreCase("true")) {
                        cartCounter = Integer.parseInt(objects.getString("count"));
                        Log.e("cartCountArray", cartCounter + "");
                        tvCartCounter.setText("" + cartCounter);

                        pd.dismiss();
                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, "count is Null", Toast.LENGTH_SHORT).show();
                        //finish();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                params.put("device_id", sp.getString("deviceId", ""));
                Log.e("MyCartParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }
}
