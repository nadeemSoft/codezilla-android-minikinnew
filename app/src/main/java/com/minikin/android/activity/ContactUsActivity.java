package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.model.SocialMediaModel;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ContactUsActivity extends BaseActivity {
    String sociallink = "";

    private RelativeLayout rl_back, rl_menu;
    private FrameLayout rlCart;

    private TextView tvTitle, tvSubmit, tvCartCounter;
    private EditText etUserName, etEmail, etMobileNo, etComment;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    private Activity activity = ContactUsActivity.this;
    private ProgressDialog pd;
    private ImageView ivFb, ivInsta, ivTwitter;
    private SharedPreferences sp = null;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private ArrayList<SocialMediaModel> socialMediaList = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.act_cont_us, contentFrameLayout);
        initViews();
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        socialMediaList = new ArrayList<>();
        getSocialMediaContact();
        setLangRecreate(WhichLanguage);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("sociallink", sociallink);
        tvCartCounter.setText("" + HomeActivityNew.cartCounter);

    }

    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        tvTitle = findViewById(R.id.toolbar_two_tv_title);
        tvTitle.setText("Contact Us");
        tvSubmit = findViewById(R.id.tv_contact_submit);
        etUserName = findViewById(R.id.et_contact_userName);
        etEmail = findViewById(R.id.et_contact_mail);
        etMobileNo = findViewById(R.id.et_contact_mobile_number);
        etComment = findViewById(R.id.et_contact_comment);
        ivFb = (ImageView) findViewById(R.id.iv_fb_contact);
        ivInsta = (ImageView) findViewById(R.id.iv_insta_contact);
        ivTwitter = (ImageView) findViewById(R.id.iv_twitter_contact);
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        tvCartCounter = findViewById(R.id.tv_cart_counter);
        rlCart.setOnClickListener(this);
        ivTwitter.setOnClickListener(this);
        rl_back.setOnClickListener(this);
        rl_menu.setOnClickListener(this);
        ivFb.setOnClickListener(this);
        ivInsta.setOnClickListener(this);
        tvSubmit.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.toolbar_two_rl_back:
                finish();
                break;
            case R.id.iv_fb_contact:
                getLinkFromSocialContact("facebook");
                break;
            case R.id.iv_insta_contact:
                getLinkFromSocialContact("instagram");

                break;
            case R.id.iv_twitter_contact:
                getLinkFromSocialContact("twitteter");
                break;

            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;
            case R.id.tv_contact_submit:

                if (WhichLanguage.equalsIgnoreCase("en")){

                    if (chechkValidation()) {
                        getContactUs();
                    }

                } else if (WhichLanguage.equalsIgnoreCase("ar")){



                    if (chechkValidationUrdu()) {
                        getContactUs();
                    }

                }
                break;


            case R.id.rl_cart_icon_toolbar:
                Intent intent = new Intent(activity, MyCartActivityNew.class);
                startActivity(intent);
                break;

        }
    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        //  Typeface fontEn = Typeface.createFromAsset(ContactUsActivity.this.getAssets(), "fonts/Avenir Roman Font Download.otf");

        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(ContactUsActivity.this.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);

            tvTitle.setText(" اتصل بنا");

            etUserName.setHint("اسم");
            etEmail.setHint("البريد الإلكتروني");
            etMobileNo.setHint("رقم الهاتف");
            tvSubmit.setHint("اختار");
            etComment.setHint("ملاحظات");


            // tvTitle.setTypeface(fontAr);
            // etUserName.setTypeface(fontAr);
            // etEmail.setTypeface(fontAr);
            etMobileNo.setTypeface(fontAr);
            etComment.setTypeface(fontAr);
            tvSubmit.setTypeface(fontAr);
            tvCartCounter.setTypeface(fontAr);

        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvTitle.setTypeface(fontEnMedium);
            etUserName.setTypeface(fontEn);
            etEmail.setTypeface(fontEn);
            etMobileNo.setTypeface(fontEn);
            etComment.setTypeface(fontEn);
            tvSubmit.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);


        }
    }

    private boolean chechkValidation() {
        if (etUserName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(ContactUsActivity.this, "Please Enter Name", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etEmail.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(ContactUsActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!etEmail.getText().toString().trim().matches(emailPattern)) {
            Toast.makeText(ContactUsActivity.this, "Please Enter Valid Email", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etMobileNo.getText().toString().matches("")) {
            Toast.makeText(ContactUsActivity.this, "Please Enter Mobile Number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etMobileNo.getText().length() != 10) {
            Toast.makeText(ContactUsActivity.this, "Please Enter Valid 10 digit Mobile Number", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etComment.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(ContactUsActivity.this, "Please Enter Comment", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }




    private boolean chechkValidationUrdu() {
        if (etUserName.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(ContactUsActivity.this, "يرجى إدخال الاسم", Toast.LENGTH_SHORT).show();
            return false;

        } else if (etEmail.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(ContactUsActivity.this, "يرجى إدخال البريد الإلكتروني", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!etEmail.getText().toString().trim().matches(emailPattern)) {
            Toast.makeText(ContactUsActivity.this, "يرجى إدخال بريد إلكتروني صحيح", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etMobileNo.getText().toString().matches("")) {
            Toast.makeText(ContactUsActivity.this, "يرجى إدخال رقم الهاتف", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etMobileNo.getText().length() != 10) {
            Toast.makeText(ContactUsActivity.this, "يرجى إدخال رقم هاتف صحيح", Toast.LENGTH_SHORT).show();
            return false;
        } else if (etComment.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(ContactUsActivity.this, "يرجى اضافة ملاحظاتك", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    public void getContactUs() {

        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_CONTACT_US, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseContactUs", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {

                        pd.dismiss();
                        Toast.makeText(ContactUsActivity.this, message, Toast.LENGTH_LONG).show();
                        etUserName.setText("");
                        etEmail.setText("");
                        etMobileNo.setText("");
                        etComment.setText("");

                    } else {
                        JSONObject errors = objects.getJSONObject("errors");


                        pd.dismiss();
                        Toast.makeText(ContactUsActivity.this, errors.toString(), Toast.LENGTH_LONG).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(ContactUsActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("name", etUserName.getText().toString());
                params.put("mobile", etMobileNo.getText().toString());
                params.put("email", etEmail.getText().toString().trim());
                params.put("message", etComment.getText().toString());
                Log.e("ContactUsParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(ContactUsActivity.this);
        requestQueue.add(request);

    }

    private void getSocialMediaContact() {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_SOCIAL_MEDIA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                socialMediaList.clear();
                try {
                    Log.e("SocialMediaResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objCat = jsonArrayResult.getJSONObject(i);
                            String title = objCat.getString("title");
                            String link = objCat.getString("link");

                            SocialMediaModel socialMediaModel = new SocialMediaModel(title, link);
                            socialMediaList.add(socialMediaModel);

                        }
                        pd.dismiss();

                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                    }
                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_LONG).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_LONG).show();


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));

                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }


    public void getLinkFromSocialContact(String titleget) {
        Log.e("titleGet", titleget);
        Log.e("socialMediaListSIZE", socialMediaList.size() + "");

        for (int i = 0; i < socialMediaList.size(); i++) {

            SocialMediaModel model = socialMediaList.get(i);
            String title = model.getTitle();
            if (title.equalsIgnoreCase(titleget)) {
                Log.e("titlechek", title);

                sociallink = model.getLink();
                Log.e("socialMediaLINK", sociallink + "");

                Intent browserIntentFb = new Intent(Intent.ACTION_VIEW, Uri.parse(sociallink));
                startActivity(browserIntentFb);
                break;

            } /*else {
                Toast.makeText(activity, "Link is not available", Toast.LENGTH_LONG).show();

            }*/

            Log.e("socialMediaListSIZEDown", socialMediaList.size() + "");

            continue;


        }


    }
}
