package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.MyAddressAdapter;
import com.minikin.android.model.AddressModel;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.MySession;
import com.minikin.android.util.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MyAddressActivity extends BaseActivity implements View.OnClickListener {
    private RecyclerView rv;
    private Activity activity = MyAddressActivity.this;
    private MySession mySession;
    private RelativeLayout rl_back, rl_menu,rlSearch;
    private FrameLayout rlCart;

    public static TextView tv_title, tvAddNewAddress;
    public static ArrayList<AddressModel> addressModelArrayList = null;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private SharedPreferences.Editor editorUser;
    private TextView tvCartCounter;

    private SharedPreferences sp = null;
    private String WhichLanguage = "";
    private ProgressDialog pd;
    JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.act_my_add, contentFrameLayout);
        initViews();
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editorUser = sp.edit();
        setLangRecreate(WhichLanguage);

        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        addressModelArrayList = new ArrayList<>();
        mySession = new MySession(activity);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (WhichLanguage.equalsIgnoreCase("en")){
            tvAddNewAddress.setText("ADD NEW ADDRESS");

        }else if (WhichLanguage.equalsIgnoreCase("ar")){
            tvAddNewAddress.setText("اضافة كلمة السر الجديدة");

        }

        getMyAddress();
    }

    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_back.setOnClickListener(this);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rl_menu.setOnClickListener(this);
        rlSearch = findViewById(R.id.rl_search_at_toolbar);
        rlSearch.setVisibility(View.GONE);
        rlCart  = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setOnClickListener(this);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tv_title.setText("My Addresses");
        tvAddNewAddress = findViewById(R.id.tv_add_new_address);

        tvCartCounter = findViewById(R.id.tv_cart_counter);

        tvAddNewAddress.setOnClickListener(this);
        rv = findViewById(R.id.act_my_add_rv);
        rv.addOnItemTouchListener(new RecyclerItemClickListener(activity, rv, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

            }

            @Override
            public void onLongItemClick(View view, int position) {
                // do whatever
            }
        }));
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {

            case R.id.toolbar_two_rl_back:
                finish();
                break;
            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
                break;
            case R.id.tv_add_new_address:
                Intent intent = new Intent(MyAddressActivity.this, AddNewAddressActivity.class);
                Bundle extras = new Bundle();
                extras.putString("Addnew", "NewAddress");
                extras.putString("callActivity", "my");
                intent.putExtras(extras);
                startActivity(intent);
                break;
            case R.id.rl_cart_icon_toolbar:
                Intent intentCart = new Intent(activity, MyCartActivityNew.class);
                startActivity(intentCart);
                break;
        }
    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(MyAddressActivity.this.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(MyAddressActivity.this.getAssets(), "fonts/Avenir Roman.otf");
        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);
            tvAddNewAddress.setTypeface(fontAr);
           // tv_title.setTypeface(fontAr);
            tv_title.setText("العنوان");
            tvCartCounter.setTypeface(fontAr);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvAddNewAddress.setTypeface(fontEn);
            tv_title.setTypeface(fontEnMedium);
            tvCartCounter.setTypeface(fontEn);

        }
    }

    private void getMyAddress() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_MY_ADDRESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                addressModelArrayList.clear();
                Log.e("ResponseMyAddress", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        jsonArray = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectResponse = jsonArray.getJSONObject(i);
                            String address_id = jsonObjectResponse.getString("address_id");
                            String area_id = jsonObjectResponse.getString("area_id");
                            String title = jsonObjectResponse.getString("title");
                            String area_name = jsonObjectResponse.getString("area_name");
                            String block = jsonObjectResponse.getString("block");
                            String street = jsonObjectResponse.getString("street");
                            String avenue = jsonObjectResponse.getString("avenue");
                            String building = jsonObjectResponse.getString("building");
                            String floor = jsonObjectResponse.getString("floor");
                            String apartment = jsonObjectResponse.getString("apartment");
                            String notes = jsonObjectResponse.getString("notes");
                            String delivery_charge = jsonObjectResponse.getString("delivery_charge");


                            AddressModel addressModel = new AddressModel(address_id, area_id, title, area_name, block, street, avenue, building, floor, apartment, notes, delivery_charge);
                            addressModelArrayList.add(addressModel);
                            pd.dismiss();

                        }

                        setAdapter(jsonArray);
                        tvCartCounter.setText(""+HomeActivityNew.cartCounter);

                        pd.dismiss();
                    } else {
                        pd.dismiss();
                        Toast.makeText(MyAddressActivity.this, message, Toast.LENGTH_SHORT).show();
                        tvAddNewAddress.setText("NO ADDRESS FOUND");

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(MyAddressActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                Log.e("MyAddressParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(MyAddressActivity.this);
        requestQueue.add(request);
    }

    private void setAdapter(JSONArray jsonArray) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(mLayoutManager);
        rv.setAdapter(new MyAddressAdapter(jsonArray, R.layout.rowitem_my_add, activity));
    }


}
