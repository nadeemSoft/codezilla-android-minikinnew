package com.minikin.android.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.AdapterFilterNewMainCategory;
import com.minikin.android.adapter.AdapterFilterSubCategory;
import com.minikin.android.adapter.AdapterHome3SubCategory;
import com.minikin.android.adapter.ProductAdapter;
import com.minikin.android.adapter.SearchAdapter;
import com.minikin.android.model.AttributesModel;
import com.minikin.android.model.ProductModel;
import com.minikin.android.model.SearchModel;
import com.minikin.android.model.SetAttributeModel;
import com.minikin.android.model.SubCategoryModel;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.RecyclerItemClickListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ProductShowActivity extends BaseActivity {
    boolean isEtSerachShowing, visibleView = false;
    int newSearch = 0;
    private LinearLayout llNewSearch, llMainToolbar;


    public int positionIndex;
    int clickWhat = 1, clickHighPrice = 1, clickLowPrice = 1, clickPopularity = 1, clickDiscount = 1;
    int shortValue = 0, viewSet = 0;
    private TextView tvWhatNew, tvHighPrice, tvLowPrice, tvPopularity, tvDiscount, tvSortDone, tvSortReset, tvCartCounter, tvShortNameNew;
    String titleName, productId, showData;
    public ArrayList<SetAttributeModel> attributeMainList;
    private Activity activity = ProductShowActivity.this;
    private SharedPreferences pref;
    private String WhichLanguage = "";
    private ProgressDialog pd;
    private SharedPreferences sp = null;
    private RelativeLayout rl_back, rl_menu, rlSearh;
    private FrameLayout rlCart;

    private TextView tv_title, tvNewShort, tvNewFilter;
    ArrayList<ProductModel> productList;
    //private ImageView ivSearchNew;

    private LinearLayout llSort, llFilter;
    private RecyclerView rvShowProduct;
    private AdapterFilterSubCategory adapterFilterSubCategory = null;
    private AdapterFilterNewMainCategory adapterFilterNewMainCategory = null;
    private Dialog dialog, dialogShort;
    private ProductAdapter productAdapter;

    private List<String> stringList;
    private HashMap<String, String> stringHashMap;
    private EditText etSearchNew, etNewSearch = null;
    private ArrayList<SearchModel> searchList = null;
    private SearchAdapter searchAdapter = null;
    private LinearLayout llShowSubCategoryDropDown = null;
    private ArrayList<SubCategoryModel> arrayListSubCategory = null;
    private AdapterHome3SubCategory adapterHome3SubCategory = null;
    private PopupWindow popup = null;
    private TextView tvShowPopUpName = null;
    private FrameLayout frameEIcon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Runtime.getRuntime().maxMemory();

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_home4, contentFrameLayout);
        initViews();
        searchList = new ArrayList<>();

        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        setLangRecreate(WhichLanguage);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        productList = new ArrayList<>();


        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);

        Intent intent = getIntent();
        productId = intent.getStringExtra("id");
        titleName = intent.getStringExtra("titleName");
        showData = intent.getStringExtra("showDate");


        arrayListSubCategory = getArrayList("SubCategoryList");
        Log.e("list", arrayListSubCategory.size() + "");
        // Log.e("listName", arrayList.get(0).getSubName()+ "");


        Log.e("titleName", titleName);
        Log.e("id", productId);
        Log.e("showData", showData);
        if (arrayListSubCategory.size() != 0) {
            llShowSubCategoryDropDown.setVisibility(View.VISIBLE);
            tvShowPopUpName.setText("" + titleName);
            viewShowSubcategoryDialog();


        } else if (arrayListSubCategory.size() == 0) {
            llShowSubCategoryDropDown.setVisibility(View.GONE);

        }


        attributeMainList = new ArrayList<>();

        stringList = new ArrayList<>();
        stringHashMap = new HashMap<>();

        getProduct();
        tv_title.setText(titleName);


        etNewSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    // do your stuff here
                    Log.e("TEXTSEARCH", etNewSearch.getText().toString());

                    if (etNewSearch.length() >= 0 && !etNewSearch.getText().toString().trim().equals("")) {
                       /* Intent intentSearch = new Intent(activity, SearchActivity.class);
                        intentSearch.putExtra("Text", etNewSearch.getText().toString());
                        startActivity(intentSearch);*/
                        getSearchData(etNewSearch.getText().toString());

                        etNewSearch.setText("");


                    }

                }
                return false;
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        llMainToolbar.setVisibility(View.VISIBLE);
        llNewSearch.setVisibility(View.GONE);
        etNewSearch.setVisibility(View.GONE);

        tvCartCounter.setText("" + HomeActivityNew.cartCounter);


    }

    public ArrayList<SubCategoryModel> getArrayList(String key) {
        Gson gson = new Gson();
        String json = sp.getString(key, null);
        Type type = new TypeToken<ArrayList<SubCategoryModel>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.toolbar_two_rl_back:
                finish();
                break;

            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;

            case R.id.ll_sort_new:
                if (dialogShort != null) {

                    if (visibleView) {

                        if (viewSet != 0) {

                            Log.e("ViewSetTOP", viewSet + "");

                            Log.e("shortValueTop", shortValue + "");


                            if (viewSet == 1) {
                                tvWhatNew.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                                tvLowPrice.setBackgroundResource(0);
                                tvPopularity.setBackgroundResource(0);
                                tvDiscount.setBackgroundResource(0);
                                tvHighPrice.setBackgroundResource(0);
                                dialogShort.show();


                            } else if (viewSet == 2) {

                                tvHighPrice.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                                tvLowPrice.setBackgroundResource(0);
                                tvWhatNew.setBackgroundResource(0);
                                tvPopularity.setBackgroundResource(0);
                                tvDiscount.setBackgroundResource(0);

                                dialogShort.show();

                            } else if (viewSet == 3) {
                                tvLowPrice.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                                tvHighPrice.setBackgroundResource(0);
                                tvWhatNew.setBackgroundResource(0);
                                tvPopularity.setBackgroundResource(0);
                                tvDiscount.setBackgroundResource(0);

                                dialogShort.show();


                            } else if (viewSet == 4) {
                                tvPopularity.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                                tvHighPrice.setBackgroundResource(0);
                                tvWhatNew.setBackgroundResource(0);
                                tvLowPrice.setBackgroundResource(0);
                                tvDiscount.setBackgroundResource(0);

                                dialogShort.show();

                            } else if (viewSet == 5) {
                                tvDiscount.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                                tvHighPrice.setBackgroundResource(0);
                                tvWhatNew.setBackgroundResource(0);
                                tvLowPrice.setBackgroundResource(0);
                                tvPopularity.setBackgroundResource(0);

                                dialogShort.show();

                            }

                        } else {
                            shortValue = 0;
                            tvDiscount.setBackgroundResource(0);
                            tvHighPrice.setBackgroundResource(0);
                            tvWhatNew.setBackgroundResource(0);
                            tvLowPrice.setBackgroundResource(0);
                            tvPopularity.setBackgroundResource(0);
                            dialogShort.show();
                        }


                    } else if (!visibleView) {
                        dialogShort.show();

                    }


                }
                break;

            case R.id.ll_filter_new:

                if (dialog != null) {

                    dialog.show();
                }
                break;


            case R.id.rl_cart_icon_toolbar:
                Intent intent = new Intent(activity, MyCartActivityNew.class);
                startActivity(intent);
                break;

            case R.id.rl_search_at_toolbar:
                if (newSearch == 0) {
                    llShowSubCategoryDropDown.setVisibility(View.GONE);
                    isEtSerachShowing = true;
                    newSearch = 1;
                    //llMainToolbar.setVisibility(View.GONE);
                    // llNewSearch.setVisibility(View.VISIBLE);
                    etNewSearch.setVisibility(View.VISIBLE);
                    etNewSearch.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(activity.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(etNewSearch, InputMethodManager.SHOW_IMPLICIT);
                } else if (newSearch == 1) {
                    isEtSerachShowing = false;
                    if (arrayListSubCategory.size() != 0) {
                        llShowSubCategoryDropDown.setVisibility(View.VISIBLE);

                    } else {
                        llShowSubCategoryDropDown.setVisibility(View.GONE);

                    }

                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                    InputMethodManager imm = (InputMethodManager) getSystemService(activity.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(etNewSearch, InputMethodManager.HIDE_IMPLICIT_ONLY);
                    InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                    newSearch = 0;
                    etNewSearch.setVisibility(View.GONE);
                    etNewSearch.clearFocus();


                }


                break;

            case R.id.ll_show_sub_cate_show_drop:
                //dialogShowSubCategory.show();
                popup.showAsDropDown(view);

                break;
        }

    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rlSearh = findViewById(R.id.rl_search_at_toolbar);
        rlSearh.setVisibility(View.VISIBLE);
        rlSearh.setOnClickListener(this);
        frameEIcon = findViewById(R.id.frame_e_icon);
        frameEIcon.setVisibility(View.GONE);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tvNewShort = findViewById(R.id.tv_new_sort);
        tvNewFilter = findViewById(R.id.tv_new_filter);
        rvShowProduct = findViewById(R.id.rv_show_prod);
        llFilter = findViewById(R.id.ll_filter_new);
        llSort = findViewById(R.id.ll_sort_new);
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setOnClickListener(this);
        etSearchNew = findViewById(R.id.et_search_new);
        etNewSearch = findViewById(R.id.et_search_new_show);

        tvCartCounter = findViewById(R.id.tv_cart_counter);
        llShowSubCategoryDropDown = findViewById(R.id.ll_show_sub_cate_show_drop);
        llShowSubCategoryDropDown.setOnClickListener(this);
        tvShowPopUpName = findViewById(R.id.tv_sub_cat_show_new);


        // ivSearchNew = findViewById(R.id.iv_search_new);

        llNewSearch = findViewById(R.id.ll_search_new);
        llMainToolbar = findViewById(R.id.ll_main_toolbar);

        rl_back.setOnClickListener(this);
        rl_menu.setOnClickListener(this);
        llSort.setOnClickListener(this);
        llFilter.setOnClickListener(this);
        //   ivSearchNew.setOnClickListener(this);


    }

    private void getProduct() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    rvShowProduct.removeAllViews();
                    productList.clear();

                    Log.e("ProductResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objCat = jsonArrayResult.getJSONObject(i);
                            String product_id = objCat.getString("product_id");
                            String name = objCat.getString("name");
                            String description = objCat.getString("description");
                            String price = objCat.getString("price");
                            String image = objCat.getString("image");
                            String has_offer = objCat.getString("has_offer");
                            String offer_price = objCat.getString("offer_price");
                            String flag = objCat.getString("flag");

                            ProductModel productModel = new ProductModel(product_id, name, description, price, image, has_offer, offer_price, flag);
                            productList.add(productModel);

                        }
                        viewShort();

                        productAdapter = new ProductAdapter(productList, R.layout.rowitem_pro, activity);
                        GridLayoutManager layoutManager = new GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false);
                        rvShowProduct.setLayoutManager(layoutManager);
                        rvShowProduct.setAdapter(productAdapter);
                        rvShowProduct.setNestedScrollingEnabled(false);

                        llFilter.setEnabled(true);
                        llSort.setEnabled(true);


                        getFilteData();


                    } else {
                        pd.dismiss();
                        llFilter.setEnabled(false);
                        llSort.setEnabled(false);
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                    }


                    //startActivity(new Intent(activity, LoginActivity.class));


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");

                    // Toast.makeText(SignUpActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("cat_id", productId);
                Log.e("ProductParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);


    }


    public void getProductWithSubCategory(final String pId, String subName) {

        tvShowPopUpName.setText(subName);
        popup.dismiss();
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    rvShowProduct.removeAllViews();
                    productList.clear();

                    Log.e("ProductResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objCat = jsonArrayResult.getJSONObject(i);
                            String product_id = objCat.getString("product_id");
                            String name = objCat.getString("name");
                            String description = objCat.getString("description");
                            String price = objCat.getString("price");
                            String image = objCat.getString("image");
                            String has_offer = objCat.getString("has_offer");
                            String offer_price = objCat.getString("offer_price");
                            String flag = objCat.getString("flag");

                            ProductModel productModel = new ProductModel(product_id, name, description, price, image, has_offer, offer_price, flag);
                            productList.add(productModel);

                        }
                        viewShort();

                        productAdapter = new ProductAdapter(productList, R.layout.rowitem_pro, activity);
                        GridLayoutManager layoutManager = new GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false);
                        rvShowProduct.setLayoutManager(layoutManager);
                        rvShowProduct.setAdapter(productAdapter);
                        rvShowProduct.setNestedScrollingEnabled(false);

                        llFilter.setEnabled(true);
                        llSort.setEnabled(true);


                        getFilteData();


                    } else {
                        pd.dismiss();
                        llFilter.setEnabled(false);
                        llSort.setEnabled(false);
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                    }


                    //startActivity(new Intent(activity, LoginActivity.class));


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");

                    // Toast.makeText(SignUpActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("cat_id", pId);
                Log.e("ProductParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);


    }


    private void viewShort() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        dialogShort = new Dialog(activity);
        dialogShort.setContentView(R.layout.short_layout_new);
        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogShort.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialogShort.getWindow().setAttributes(lp);

        tvShortNameNew = dialogShort.findViewById(R.id.tv_adapter_short_name);
        tvWhatNew = dialogShort.findViewById(R.id.tv_whats_New);
        tvHighPrice = dialogShort.findViewById(R.id.tv_price_high);
        tvLowPrice = dialogShort.findViewById(R.id.tv_price_low);
        tvPopularity = dialogShort.findViewById(R.id.tv_popularity_sort);
        tvDiscount = dialogShort.findViewById(R.id.tv_discount_sort);
        tvSortDone = dialogShort.findViewById(R.id.tv_sort_done);
        tvSortReset = dialogShort.findViewById(R.id.tv_sort_reset);
        tvSortReset.setEnabled(false);

        if (WhichLanguage.equalsIgnoreCase("ar")) {
            tvShortNameNew.setText("ترتيب حسب");
            tvWhatNew.setText("الجديد");
            tvHighPrice.setText("الاسعار من الاعلى الى الادني");
            tvLowPrice.setText("الاسعار من الادنى الى الاعلى ");
            tvPopularity.setText("الاكثر شهرة");
            tvDiscount.setText("تخفيض");
            tvSortReset.setText("اعادة");
            tvSortDone.setText("الانتهاء");


        }


        tvWhatNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (clickWhat == 1) {
                    clickWhat = 2;
                    shortValue = 1;

                    clickHighPrice = 1;
                    clickLowPrice = 1;
                    clickDiscount = 1;
                    clickPopularity = 1;
                    visibleView = true;

                    tvWhatNew.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));

                    tvHighPrice.setBackgroundResource(0);
                    tvLowPrice.setBackgroundResource(0);
                    tvPopularity.setBackgroundResource(0);
                    tvDiscount.setBackgroundResource(0);


                } else if (clickWhat == 2) {
                    clickWhat = 1;
                    shortValue = 0;
                    visibleView = false;
                    tvWhatNew.setBackgroundResource(0);


                }


            }
        });


        tvHighPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (clickHighPrice == 1) {
                    clickHighPrice = 2;
                    shortValue = 2;

                    clickWhat = 1;
                    clickLowPrice = 1;
                    clickDiscount = 1;
                    clickPopularity = 1;
                    visibleView = true;


                    tvHighPrice.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));

                    tvWhatNew.setBackgroundResource(0);
                    tvLowPrice.setBackgroundResource(0);
                    tvPopularity.setBackgroundResource(0);
                    tvDiscount.setBackgroundResource(0);


                } else if (clickHighPrice == 2) {
                    clickHighPrice = 1;
                    shortValue = 0;

                    visibleView = false;


                    tvHighPrice.setBackgroundResource(0);

                }


            }
        });


        tvLowPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (clickLowPrice == 1) {
                    clickLowPrice = 2;
                    shortValue = 3;

                    clickWhat = 1;
                    clickHighPrice = 1;
                    clickDiscount = 1;
                    clickPopularity = 1;
                    visibleView = true;


                    tvLowPrice.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));

                    tvWhatNew.setBackgroundResource(0);
                    tvPopularity.setBackgroundResource(0);
                    tvDiscount.setBackgroundResource(0);
                    tvHighPrice.setBackgroundResource(0);


                } else if (clickLowPrice == 2) {
                    clickLowPrice = 1;
                    shortValue = 0;
                    visibleView = false;


                    tvLowPrice.setBackgroundResource(0);


                }
            }
        });


        tvPopularity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (clickPopularity == 1) {
                    clickPopularity = 2;
                    shortValue = 4;


                    clickWhat = 1;
                    clickHighPrice = 1;
                    clickDiscount = 1;
                    clickLowPrice = 1;
                    visibleView = true;


                    tvPopularity.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));

                    tvWhatNew.setBackgroundResource(0);
                    tvLowPrice.setBackgroundResource(0);
                    tvDiscount.setBackgroundResource(0);
                    tvHighPrice.setBackgroundResource(0);


                } else if (clickPopularity == 2) {
                    clickPopularity = 1;
                    shortValue = 0;

                    visibleView = false;


                    tvPopularity.setBackgroundResource(0);

                }
            }
        });


        tvDiscount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (clickDiscount == 1) {
                    clickDiscount = 2;
                    shortValue = 5;

                    clickWhat = 1;
                    clickHighPrice = 1;
                    clickPopularity = 1;
                    clickLowPrice = 1;
                    visibleView = true;


                    tvDiscount.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));

                    tvWhatNew.setBackgroundResource(0);
                    tvPopularity.setBackgroundResource(0);
                    tvLowPrice.setBackgroundResource(0);
                    tvHighPrice.setBackgroundResource(0);


                } else if (clickDiscount == 2) {
                    clickDiscount = 1;
                    shortValue = 0;

                    visibleView = false;


                    tvDiscount.setBackgroundResource(0);

                }
            }
        });


        tvSortDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shortValue != 0 && visibleView) {
                    tvSortReset.setEnabled(true);
                    visibleView = false;
                    viewSet = shortValue;
                    Log.e("ViewSet", viewSet + "");


                    if (viewSet == 1) {
                        tvWhatNew.setEnabled(false);

                        tvHighPrice.setEnabled(true);
                        tvLowPrice.setEnabled(true);
                        tvPopularity.setEnabled(true);
                        tvDiscount.setEnabled(true);


                    } else if (viewSet == 2) {
                        tvWhatNew.setEnabled(true);
                        tvHighPrice.setEnabled(false);

                        tvLowPrice.setEnabled(true);
                        tvPopularity.setEnabled(true);
                        tvDiscount.setEnabled(true);
                    } else if (viewSet == 3) {
                        tvWhatNew.setEnabled(true);
                        tvHighPrice.setEnabled(true);
                        tvLowPrice.setEnabled(false);

                        tvPopularity.setEnabled(true);
                        tvDiscount.setEnabled(true);
                    } else if (viewSet == 4) {
                        tvWhatNew.setEnabled(true);
                        tvHighPrice.setEnabled(true);
                        tvLowPrice.setEnabled(true);
                        tvPopularity.setEnabled(false);

                        tvDiscount.setEnabled(true);
                    } else if (viewSet == 5) {
                        tvWhatNew.setEnabled(true);
                        tvHighPrice.setEnabled(true);
                        tvLowPrice.setEnabled(true);
                        tvPopularity.setEnabled(true);
                        tvDiscount.setEnabled(false);

                    }


                    getSort();

                    dialogShort.dismiss();


                } else if (shortValue == 0) {
                    Toast.makeText(ProductShowActivity.this, "Please Select at Least one Sort", Toast.LENGTH_SHORT).show();
                    dialogShort.dismiss();


                }

            }
        });

        tvSortReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("SortReset", shortValue + "");
                if (shortValue > 0) {
                    visibleView = false;
                    shortValue = 0;
                    viewSet = 0;


                    getProduct();
                    dialogShort.dismiss();


                } else {
                    dialogShort.dismiss();
                }

            }
        });


    }


    private void showFIlter() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        dialog = new Dialog(activity);
        dialog.setContentView(R.layout.filter_view_new);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        RecyclerView rvFilterMainCategory = dialog.findViewById(R.id.rv_filter_new_main_attribute);

        final RecyclerView rvFilterChildCategory = dialog.findViewById(R.id.rv_filter_new_child_attribute);

        TextView tvReset = dialog.findViewById(R.id.tv_filter_new_reset);
        TextView tvDone = dialog.findViewById(R.id.tv_filter_new_done);
        if (WhichLanguage.equalsIgnoreCase("ar")) {

            tvReset.setText("اعادة");
            tvDone.setText("الانتهاء");


        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvFilterMainCategory.setLayoutManager(mLayoutManager);
        rvFilterMainCategory.setNestedScrollingEnabled(false);
        adapterFilterNewMainCategory = new AdapterFilterNewMainCategory(activity, attributeMainList);
        rvFilterMainCategory.setAdapter(adapterFilterNewMainCategory);
        final ArrayList<AttributesModel> attributesSubCategoryList = attributeMainList.get(0).getAttributesModelsLisy();


        rvFilterMainCategory.addOnItemTouchListener(new RecyclerItemClickListener(activity, rvFilterMainCategory, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                positionIndex = position;

                ArrayList<AttributesModel> attributesSubCategoryList = attributeMainList.get(position).getAttributesModelsLisy();
                adapterFilterSubCategory = new AdapterFilterSubCategory(activity, attributesSubCategoryList);

                rvFilterChildCategory.setAdapter(adapterFilterSubCategory);

                adapterFilterSubCategory.notifyDataSetChanged();
                adapterFilterNewMainCategory.poss = position;
                adapterFilterNewMainCategory.notifyDataSetChanged();

            }

            @Override
            public void onLongItemClick(View view, int position) {
                // do whatever
            }
        }));


        rvFilterChildCategory.addOnItemTouchListener(new RecyclerItemClickListener(activity, rvFilterChildCategory, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (attributeMainList.get(positionIndex).getAttributesModelsLisy().get(position).isSelected()) {

                    attributeMainList.get(positionIndex).getAttributesModelsLisy().get(position).setSelected(false);
                } else {

                    for (int i = 0; i < attributeMainList.get(positionIndex).getAttributesModelsLisy().size(); i++) {

                        if (i == position) {

                            attributeMainList.get(positionIndex).getAttributesModelsLisy().get(i).setSelected(true);
                        } else {
                            attributeMainList.get(positionIndex).getAttributesModelsLisy().get(i).setSelected(false);
                        }
                    }
                }

                ArrayList<AttributesModel> attributesSubCategoryList = attributeMainList.get(positionIndex).getAttributesModelsLisy();
                adapterFilterSubCategory = new AdapterFilterSubCategory(activity, attributesSubCategoryList);
                rvFilterChildCategory.setAdapter(adapterFilterSubCategory);
                //adapterFilterSubCategory.notifyDataSetChanged();

            }

            @Override
            public void onLongItemClick(View view, int position) {
                // do whatever
            }
        }));

        RecyclerView.LayoutManager mLayoutManagerSub = new LinearLayoutManager(getApplicationContext());
        rvFilterChildCategory.setLayoutManager(mLayoutManagerSub);
        rvFilterChildCategory.setAdapter(new AdapterFilterSubCategory(activity, attributesSubCategoryList));
        rvFilterChildCategory.setNestedScrollingEnabled(false);


        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stringList = new ArrayList<>();// clea old list
                stringHashMap = new HashMap<>();

                for (int i = 0; i < attributeMainList.size(); i++) {

                    for (int j = 0; j < attributeMainList.get(i).getAttributesModelsLisy().size(); j++) {

                        if (attributeMainList.get(i).getAttributesModelsLisy().get(j).isSelected()) {

                            stringList.add(attributeMainList.get(i).getSetId());
                            stringHashMap.put(attributeMainList.get(i).getSetId(), attributeMainList.get(i).getAttributesModelsLisy().get(j).getAttributeId());
                            break;
                        }

                    }

                }


                if (stringList.size() > 0 && stringHashMap.size() > 0) {
                    getProductWithNewFilterAttribute();

                } else {
                    Toast.makeText(activity, "Please Select Attribute", Toast.LENGTH_SHORT).show();
                }




               /* Map<String, String> params = new HashMap<>();


                for (int i = 0; i < stringList.size(); i++) {

                    Log.e("Set_id", stringList.get(i));
                    Log.e("Attribute_id", stringHashMap.get(stringList.get(i)));

                    params.put("atribute["+(i+1)+"]",stringHashMap.get(stringList.get(i)));
                   // params.put("atribute["+ stringList.get(i)+"]",stringHashMap.get(stringList.get(i)));
                    //params.put("atribute["+(i+1)+"]", AdapterFilterSubCategory.addIdList.get(i)+"");

                }*/
                dialog.dismiss();
            }
        });


        tvReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (stringList.size() > 0 && stringHashMap.size() > 0) {

                    stringList = new ArrayList<>();// clea old list
                    stringHashMap = new HashMap<>();


                    getProduct();
                    adapterFilterNewMainCategory.poss = -1;
                    adapterFilterNewMainCategory.notifyDataSetChanged();

                }


                dialog.dismiss();

            }
        });

    }


    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");

        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);

            tvNewFilter.setText("فرر");
            tvNewShort.setText("ترتيب");
            etNewSearch.setHint("بحث");
            tv_title.setTypeface(fontEnMedium);
            tvNewFilter.setTypeface(fontEn);
            tvNewShort.setTypeface(fontEn);
            etNewSearch.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tv_title.setTypeface(fontEnMedium);
            tvNewFilter.setTypeface(fontEn);
            tvNewShort.setTypeface(fontEn);
            etNewSearch.setTypeface(fontEn);
            tvCartCounter.setTypeface(fontEn);


        }
    }


    private void viewShowSubcategoryDialog() {

        popup = new PopupWindow(activity);
        View layout = getLayoutInflater().inflate(R.layout.sub_cate_dialog_show, null);
        popup.setContentView(layout);
        // Set content width and height
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        // popup.setWidth(500);
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);


        RecyclerView rvShowSubCategory = layout.findViewById(R.id.rv_sub_category_dialog);


        adapterHome3SubCategory = new AdapterHome3SubCategory(activity, arrayListSubCategory);
        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(activity);
        rvShowSubCategory.setLayoutManager(mLayoutManager3);
        rvShowSubCategory.setAdapter(adapterHome3SubCategory);

        popup.setBackgroundDrawable(new BitmapDrawable());
        // popup.showAsDropDown(layout, 1, 1);


    }


    private void getFilteData() {
        String urlFilter = AppConstants.GET_FILTER + "?lang=" + sp.getString("WhichLanguage", "");
        Log.e("filterUrl", urlFilter + "");

        StringRequest request = new StringRequest(Request.Method.GET, urlFilter, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    attributeMainList.clear();
                    Log.e("FilterResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objectFilter = jsonArrayResult.getJSONObject(i);
                            String setId = objectFilter.getString("set_id");
                            String setName = objectFilter.getString("set_name");

                            JSONArray jsonArraySetAttributes = objectFilter.getJSONArray("set_attributes");

                            ArrayList<AttributesModel> attributesModelArrayList = new ArrayList<>();
                            attributesModelArrayList.clear();

                            for (int j = 0; j < jsonArraySetAttributes.length(); j++) {

                                JSONObject jsonObjectSetAttributes = jsonArraySetAttributes.getJSONObject(j);
                                String attribute_id = jsonObjectSetAttributes.getString("attribute_id");
                                String attribute_name = jsonObjectSetAttributes.getString("attribute_name");

                                AttributesModel attributesModel = new AttributesModel(attribute_id, attribute_name);
                                attributesModelArrayList.add(attributesModel);
                            }

                            SetAttributeModel setAttributeModel = new SetAttributeModel(setId, setName, attributesModelArrayList);
                            attributeMainList.add(setAttributeModel);
                        }


                        showFIlter();
                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                    }

                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_LONG).show();


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");
                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                Log.e("filterParams", params + "");
                return params;
            }


        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }


    private void getProductWithNewFilterAttribute() {
        //final int sizeInt = Integer.parseInt(size);

        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    rvShowProduct.removeAllViews();
                    productList.clear();

                    Log.e("ProductResponseAtti..", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        productList.clear();
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objCat = jsonArrayResult.getJSONObject(i);
                            String product_id = objCat.getString("product_id");
                            String name = objCat.getString("name");
                            String description = objCat.getString("description");
                            String price = objCat.getString("price");
                            String image = objCat.getString("image");
                            String has_offer = objCat.getString("has_offer");
                            String offer_price = objCat.getString("offer_price");

                            String flag = objCat.getString("flag");

                            ProductModel productModel = new ProductModel(product_id, name, description, price, image, has_offer, offer_price, flag);
                            productList.add(productModel);

                        }
                        productAdapter = new ProductAdapter(productList, R.layout.rowitem_pro, activity);
                        GridLayoutManager layoutManager = new GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false);
                        rvShowProduct.setLayoutManager(layoutManager);
                        rvShowProduct.setAdapter(productAdapter);
                        rvShowProduct.setNestedScrollingEnabled(false);
                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                    }


                    //startActivity(new Intent(activity, LoginActivity.class));


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");

                    // Toast.makeText(SignUpActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("cat_id", productId);
                for (int i = 0; i < stringList.size(); i++) {

                    params.put("atribute_id[" + (i + 1) + "]", stringHashMap.get(stringList.get(i)));

                }

                Log.e("filterParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);


    }


    private void getSort() {

        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_PRODUCT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    rvShowProduct.removeAllViews();
                    productList.clear();

                    Log.e("ProductResponseAtti..", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objCat = jsonArrayResult.getJSONObject(i);
                            String product_id = objCat.getString("product_id");
                            String name = objCat.getString("name");
                            String description = objCat.getString("description");
                            String price = objCat.getString("price");
                            String image = objCat.getString("image");
                            String has_offer = objCat.getString("has_offer");
                            String offer_price = objCat.getString("offer_price");
                            String flag = objCat.getString("flag");
                            ProductModel productModel = new ProductModel(product_id, name, description, price, image, has_offer, offer_price, flag);
                            productList.add(productModel);

                        }

                        productAdapter = new ProductAdapter(productList, R.layout.rowitem_pro, activity);
                        GridLayoutManager layoutManager = new GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false);
                        rvShowProduct.setLayoutManager(layoutManager);
                        rvShowProduct.setAdapter(productAdapter);
                        rvShowProduct.setNestedScrollingEnabled(false);


                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                    }
                    pd.dismiss();


                    //startActivity(new Intent(activity, LoginActivity.class));


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");

                    // Toast.makeText(SignUpActivity.this, "Exception" + e, Toast.LENGTH_LONG).show();

                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("cat_id", productId);
                params.put("sort", shortValue + "");

                Log.e("SortParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);


    }


    private void getSearchData(final String textEdit) {

        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_SEARCH, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                searchList.clear();
                rvShowProduct.removeAllViews();

                try {
                    Log.e("SearchResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objSearch = jsonArrayResult.getJSONObject(i);
                            String product_id = objSearch.getString("product_id");
                            String name = objSearch.getString("name");
                            String description = objSearch.getString("description");
                            String price = objSearch.getString("price");
                            String image = objSearch.getString("image");
                            String has_offer = objSearch.getString("has_offer");
                            String offer_price = objSearch.getString("offer_price");
                            String flag = objSearch.getString("flag");

                            SearchModel serarchModel = new SearchModel(product_id, name, description, price, image, has_offer, offer_price, flag);
                            searchList.add(serarchModel);
                        }

                        viewShort();


                        searchAdapter = new SearchAdapter(searchList, R.layout.rowitem_pro, activity);
                        GridLayoutManager layoutManager = new GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false);
                        rvShowProduct.setLayoutManager(layoutManager);
                        rvShowProduct.setItemViewCacheSize(searchList.size());
                        rvShowProduct.setAdapter(searchAdapter);
                        rvShowProduct.setNestedScrollingEnabled(false);

                        tv_title.setText("Searching...");

                        llFilter.setEnabled(true);
                        llSort.setEnabled(true);

                      //  getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                       /* InputMethodManager imm = (InputMethodManager) getSystemService(activity.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(etNewSearch, InputMethodManager.HIDE_IMPLICIT_ONLY);*/
                        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                        newSearch = 0;
                        etNewSearch.setVisibility(View.GONE);
                        etNewSearch.clearFocus();
                        getFilteData();




                    } else {
                        pd.dismiss();
                        llFilter.setEnabled(false);
                        llSort.setEnabled(false);
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();


                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_LONG).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_LONG).show();


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("key_word", textEdit);
                Log.e("searchParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }


}
