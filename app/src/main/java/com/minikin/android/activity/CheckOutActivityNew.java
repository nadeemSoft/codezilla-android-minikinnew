package com.minikin.android.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.GiftAdapter;
import com.minikin.android.adapter.MyAddressAdapterCart;
import com.minikin.android.model.CartItemModel;
import com.minikin.android.model.MyGiftModel;
import com.minikin.android.model.ProductDetailsModel;
import com.minikin.android.util.AppConstants;
import com.minikin.android.util.MySession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class CheckOutActivityNew extends BaseActivity {
    int count;
    String productId, chkPayment = "";
    String asGift = "0", cardFrom = "0", cardTo = "0";
    double couponDis = 0.000, deliPr = 0.000, couponDiscountNew = 0.000, giftPriceNew = 0.000, deliveryPriceNew = 0.000;
    private RecyclerView rvAddress, rvGift;
    private Activity activity = CheckOutActivityNew.this;
    private MySession mySession;
    private RelativeLayout rl_back, rl_menu, rlSearch;
    private FrameLayout rlCart;

    private TextView tvTitleMain, tvProductName, tvProductStoreName, tvProductPrice, tvSelectQtyCount, tvSubTotal, tvSubDeliver, tvSubWrapGift, tvSubCouponDiscount, tvSubTotalMain, tvCheckout, tvSelectanAddres, tvAddNewAddress, tvGiftType, tvSendGift;
    private TextView tvSubTotalTitle, tvDeliveryTitle, tvWrapGiftTitle, tvCouponDiscountTitle, tvTotalTitle, tvKnetTitle, tvCashTitle;
    public TextView tvSelectShippingAddress, tvSelectShippingAddTitle, tvTitleChekout;
    private TextView tvPaymentDetailsTitle, tvCartCounter, tvSubTotalKdTitle, tvDeliveryChargesKdTitle, tvPackagingServiceKdTitle, tvCouponKdTitle, tvTotalMainKdtitle;
    private RelativeLayout rlClose, rlSelectQtySub, rlSelectQtyAdd, rlCouponApplyBtn;
    private ImageView ivProduct;
    private EditText etCoupon, etGiftFrom, etGiftTo;
    private RadioGroup rgGift;
    private RadioButton radioButtonKPay, radioButtonCashOnDelivery, rdGiftYes, rdNo;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private SharedPreferences.Editor editorSp;
    private String WhichLanguage = "";
    private ProgressDialog pd;
    private Button btnCoupon;
    private LinearLayout llK, llCash, llSelectAddress, llAddressDone;
    private ArrayList<ProductDetailsModel> productDetailList;
    private ArrayList<MyGiftModel> myGiftModelsList;
    private SharedPreferences sp = null;
    private ArrayList<CartItemModel> cartItemList;
    private LinearLayout llSelectShippingAddress, llGiftCardOptionFromTo;
    public Dialog dialogAddress;
    JSONArray jsonArrayAddress;
    DecimalFormat precision = null;
    int firstValue = 0;
    Double totalPriceMainNew;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_check_out_new);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_check_out_new, contentFrameLayout);
        initViews();
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        editorSp = sp.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        editorSp.putString("sendGiftOption", "invisible");
        editorSp.commit();
        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        productDetailList = new ArrayList<>();
        myGiftModelsList = new ArrayList<>();
        cartItemList = new ArrayList<>();
        mySession = new MySession(activity);
        editorSp.putString("couponDiscount", "0.000");
        editorSp.commit();

        precision = new DecimalFormat("0.000");

        setLangRecreate(WhichLanguage);

        tvCartCounter.setText("" + HomeActivityNew.cartCounter);

        radioButtonKPay.setChecked(true);
        chkPayment = "2";
        editorSp.putString("paymentDetails", String.valueOf(2));
        editorSp.commit();


        rgGift.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rdYes_New) {
                    editorSp.putString("sendGiftOption", "visible");
                    editorSp.commit();
                    asGift = "1";
                    Log.e("sendGiftOptionYES", asGift + "");

                    llGiftCardOptionFromTo.setVisibility(View.VISIBLE);

                } else if (checkedId == R.id.rdNo_New) {
                    editorSp.putString("sendGiftOption", "invisible");
                    editorSp.commit();
                    asGift = "0";
                    Log.e("sendGiftOptionNo", asGift + "");

                    llGiftCardOptionFromTo.setVisibility(View.GONE);


                }

            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();


        getMyAddress();

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {

            case R.id.toolbar_two_rl_back:
                finish();
                break;

            case R.id.act_my_cart_tv_checkout:

                //startActivity(new Intent(activity, CheckoutActivity.class));
                break;
            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {

                    drawer.closeDrawer(GravityCompat.START);

                } else {

                    drawer.openDrawer(GravityCompat.START);
                }
                break;
            case R.id.lay_select_qty_rl_sub:
                if (count > 0) {
                    count = count - 1;
                    tvSelectQtyCount.setText("" + count);
                }
                break;
            case R.id.lay_select_qty_rl_add:
                count = count + 1;
                tvSelectQtyCount.setText("" + count);
                break;
            case R.id.ll_radio_k:
                radioButtonKPay.setChecked(true);
                radioButtonCashOnDelivery.setChecked(false);
                chkPayment = "2";
                editorSp.putString("paymentDetails", String.valueOf(2));
                editorSp.commit();
                break;
            case R.id.ll_radio_cash:
                radioButtonKPay.setChecked(false);
                radioButtonCashOnDelivery.setChecked(true);
                chkPayment = "1";
                editorSp.putString("paymentDetails", String.valueOf(1));
                editorSp.commit();
                break;
            case R.id.tv_next_product:

                if (WhichLanguage.equalsIgnoreCase("en")) {

                    if (getValidatioCheckOut()) {

                        if (asGift.equalsIgnoreCase("1")) {
                            cardFrom = etGiftFrom.getText().toString();
                            cardTo = etGiftTo.getText().toString();
                        } else if (asGift.equalsIgnoreCase("0")) {
                            cardFrom = "";
                            cardTo = "";
                        }

                        Log.e("asGift", asGift);
                        Log.e("cardFrom", cardFrom);
                        Log.e("cardTo", cardTo);


                        getCheckOut(asGift, cardFrom, cardTo);

                    }

                } else if (WhichLanguage.equalsIgnoreCase("ar")) {
                    if (getValidatioCheckOutUrdu()) {

                        if (asGift.equalsIgnoreCase("1")) {
                            cardFrom = etGiftFrom.getText().toString();
                            cardTo = etGiftTo.getText().toString();
                        } else if (asGift.equalsIgnoreCase("0")) {
                            cardFrom = "";
                            cardTo = "";
                        }

                        Log.e("asGift", asGift);
                        Log.e("cardFrom", cardFrom);
                        Log.e("cardTo", cardTo);


                        getCheckOut(asGift, cardFrom, cardTo);

                    }
                }

                break;
            case R.id.btn_coupon:
                if (!etCoupon.getText().toString().equalsIgnoreCase("")) {
                    getCouponValid();

                } else {

                    if (WhichLanguage.equalsIgnoreCase("en")) {
                        Toast.makeText(activity, "Please Enter Coupon Code", Toast.LENGTH_SHORT).show();

                    } else {
                        Toast.makeText(activity, "يرجى إدخال رقم الكوبون", Toast.LENGTH_SHORT).show();

                    }

                }
                break;
            case R.id.ll_select_address:

                Intent intent = new Intent(activity, AddNewAddressActivity.class);
                Bundle extras = new Bundle();
                extras.putString("Addnew", "NewAddress");
                extras.putString("callActivity", "cart");
                intent.putExtras(extras);
                startActivityForResult(intent, 123);

                break;
            case R.id.iv_home:
                Intent intenth = new Intent(activity, HomeActivityNew.class);
                startActivity(intenth);
                finish();
                break;

            case R.id.ll_shipping_address:
                if (dialogAddress != null) {
                    dialogAddress.show();

                }
                break;
        }
    }

    private void initViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rl_menu.setOnClickListener(this);
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setVisibility(View.GONE);
        rlSearch = findViewById(R.id.rl_search_at_toolbar);
        rlSearch.setVisibility(View.GONE);
        tvCartCounter = findViewById(R.id.tv_cart_counter);

        tvTitleChekout = findViewById(R.id.toolbar_two_tv_title);
        tvTitleChekout.setText("Checkout");
        rvAddress = findViewById(R.id.rv_address);
        rvGift = findViewById(R.id.rv_gift);
        tvCheckout = findViewById(R.id.tv_next_product);
        tvCheckout.setOnClickListener(this);
        tvTitleMain = findViewById(R.id.toolbar_two_tv_title);
        tvProductName = findViewById(R.id.tv_product_name);
        tvSelectanAddres = findViewById(R.id.tv_select_address);
        tvProductStoreName = findViewById(R.id.tv_product_store_name);
        tvProductPrice = findViewById(R.id.tv_product_price);
        tvGiftType = findViewById(R.id.tv_gift_type);
        tvAddNewAddress = findViewById(R.id.tv_add_address_new);
        tvSubTotal = findViewById(R.id.tv_sub_total);
        tvSubDeliver = findViewById(R.id.tv_sub_delivery);
        tvSubWrapGift = findViewById(R.id.tv_sub_wrap_gift);
        tvSubCouponDiscount = findViewById(R.id.tv_sub_coupon_discount);
        tvSubTotalMain = findViewById(R.id.tv_sub_total_Main);
        tvSendGift = findViewById(R.id.tv_send_giftCard);
        tvCheckout = findViewById(R.id.tv_next_product);
        tvSelectQtyCount = findViewById(R.id.tv_select_qty_count);
        ivProduct = findViewById(R.id.iv_product_image);
        etCoupon = findViewById(R.id.et_coupon);
        etGiftFrom = findViewById(R.id.etFrom);
        etGiftTo = findViewById(R.id.etTo);
        btnCoupon = findViewById(R.id.btn_coupon);
        btnCoupon.setOnClickListener(this);
        rlClose = findViewById(R.id.rl_close);
        rlSelectQtySub = findViewById(R.id.lay_select_qty_rl_sub);
        rlSelectQtySub.setOnClickListener(this);
        rlSelectQtyAdd = findViewById(R.id.lay_select_qty_rl_add);
        rlSelectQtyAdd.setOnClickListener(this);
        rlCouponApplyBtn = findViewById(R.id.rl_coupon_apply_btn);
        rlCouponApplyBtn.setOnClickListener(this);


        llGiftCardOptionFromTo = findViewById(R.id.ll_gift_option_checkout);

        llSelectAddress = findViewById(R.id.ll_select_address);
        llSelectAddress.setOnClickListener(this);
        llAddressDone = findViewById(R.id.ll_address_done);

        rgGift = (RadioGroup) findViewById(R.id.rg_New);

        rdGiftYes = findViewById(R.id.rdYes_New);
        rdNo = findViewById(R.id.rdNo_New);

        radioButtonKPay = findViewById(R.id.radio_k_net);
        radioButtonCashOnDelivery = findViewById(R.id.radio_cash_Delivery);
        tvSubTotalTitle = findViewById(R.id.tv_sub_total_Title);
        tvDeliveryTitle = findViewById(R.id.tv_delivery_title);
        tvWrapGiftTitle = findViewById(R.id.tv_Wrap_Gift_title);
        tvCouponDiscountTitle = findViewById(R.id.tv_cou_Discount_title);
        tvTotalTitle = findViewById(R.id.tv_totalSub_title);
        tvKnetTitle = findViewById(R.id.tv_kNet);
        tvCashTitle = findViewById(R.id.tv_Cash_Devivery_title);

        tvPaymentDetailsTitle = findViewById(R.id.tv_payment_details_title);

        tvSelectShippingAddress = findViewById(R.id.tv_select_shipping_add);
        tvSelectShippingAddTitle = findViewById(R.id.tv_select_shipping_add_title);
        llSelectShippingAddress = findViewById(R.id.ll_shipping_address);
        llSelectShippingAddress.setOnClickListener(this);
        llCash = findViewById(R.id.ll_radio_cash);
        llCash.setOnClickListener(this);
        llK = findViewById(R.id.ll_radio_k);
        llK.setOnClickListener(this);
        rl_back.setOnClickListener(this);


        tvSubTotalKdTitle = findViewById(R.id.tv_sub_total_kdTitle);
        tvDeliveryChargesKdTitle = findViewById(R.id.tv_sub_delivery_kd);
        tvPackagingServiceKdTitle = findViewById(R.id.tv_sub_wrap_gift_kd);
        tvCouponKdTitle = findViewById(R.id.tv_sub_coupon_discount_kd);
        tvTotalMainKdtitle = findViewById(R.id.tv_sub_total_Main_kd);

    }


    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        // Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);


            tvTitleChekout.setText(" الدفع");
            tvGiftType.setText("خيار التغليف");
            tvSendGift.setText("بطاقه التهادي");
            rdGiftYes.setText("نعم");
            rdNo.setText("لا");
            etGiftFrom.setHint("من");
            etGiftTo.setHint("الى");
            tvSelectanAddres.setText("عنوان التوصيل");
            tvSelectShippingAddress.setHint("اختيار عنوان التوصيل ");
            etCoupon.setHint("كود الخصم");
            btnCoupon.setText("تطبيق");
            tvPaymentDetailsTitle.setText(" خيارات الدفع");

           /* tvSubTotalKdTitle.setText("د.ك");
            tvDeliveryChargesKdTitle.setText("د.ك");
            tvPackagingServiceKdTitle.setText("د.ك");
            tvCouponKdTitle.setText("د.ك");
            tvTotalMainKdtitle.setText("د.ك");*/

            tvSubTotalTitle.setText("حاصل الجمع");
            tvDeliveryTitle.setText("تكلفه التوصيل");
            tvWrapGiftTitle.setText("تكلفه التغليف ");
            tvCouponDiscountTitle.setText("كود الخصم");
            tvTotalTitle.setText("الاجمالي");
            tvAddNewAddress.setText("يرجى إضافة عنوان");
            tvCheckout.setText("قم بالطلب");

            tvTitleMain.setTypeface(fontEnMedium);
            tvSelectQtyCount.setTypeface(fontEn);
            tvProductPrice.setTypeface(fontEn);
            tvProductStoreName.setTypeface(fontEn);
            tvProductName.setTypeface(fontEn);
            tvSubTotalMain.setTypeface(fontEn);
            tvSubCouponDiscount.setTypeface(fontEn);
            tvSubWrapGift.setTypeface(fontEn);
            tvSubTotal.setTypeface(fontEn);
            tvSubDeliver.setTypeface(fontEn);
            tvCheckout.setTypeface(fontEn);
            etCoupon.setTypeface(fontEn);
            etGiftFrom.setTypeface(fontEn);
            etGiftTo.setTypeface(fontEn);
            tvAddNewAddress.setTypeface(fontEn);
            tvSelectanAddres.setTypeface(fontEnMedium);
            tvGiftType.setTypeface(fontEnMedium);
            tvSendGift.setTypeface(fontEn);

            //21/june

            tvSubTotalTitle.setTypeface(fontEn);
            tvDeliveryTitle.setTypeface(fontEn);
            tvWrapGiftTitle.setTypeface(fontEn);
            tvCouponDiscountTitle.setTypeface(fontEn);
            tvTotalTitle.setTypeface(fontEn);
            tvKnetTitle.setTypeface(fontEn);
            tvCashTitle.setTypeface(fontEn);


            tvPaymentDetailsTitle.setTypeface(fontEnMedium);


            tvSelectShippingAddress.setTypeface(fontEn);
            tvSelectShippingAddTitle.setTypeface(fontEn);

            tvTitleChekout.setTypeface(fontEnMedium);
            tvCartCounter.setTypeface(fontEn);

            tvSubTotalKdTitle.setTypeface(fontEn);
            tvDeliveryChargesKdTitle.setTypeface(fontEn);
            tvPackagingServiceKdTitle.setTypeface(fontEn);
            tvCouponKdTitle.setTypeface(fontEn);
            tvTotalMainKdtitle.setTypeface(fontEn);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tvTitleMain.setTypeface(fontEnMedium);
            tvSelectQtyCount.setTypeface(fontEn);
            tvProductPrice.setTypeface(fontEn);
            tvProductStoreName.setTypeface(fontEn);
            tvProductName.setTypeface(fontEn);
            tvSubTotalMain.setTypeface(fontEn);
            tvSubCouponDiscount.setTypeface(fontEn);
            tvSubWrapGift.setTypeface(fontEn);
            tvSubTotal.setTypeface(fontEn);
            tvSubDeliver.setTypeface(fontEn);
            tvCheckout.setTypeface(fontEn);
            etCoupon.setTypeface(fontEn);
            etGiftFrom.setTypeface(fontEn);
            etGiftTo.setTypeface(fontEn);
            tvAddNewAddress.setTypeface(fontEn);
            tvSelectanAddres.setTypeface(fontEnMedium);
            tvGiftType.setTypeface(fontEnMedium);
            tvSendGift.setTypeface(fontEn);

            //21/june

            tvSubTotalTitle.setTypeface(fontEn);
            tvDeliveryTitle.setTypeface(fontEn);
            tvWrapGiftTitle.setTypeface(fontEn);
            tvCouponDiscountTitle.setTypeface(fontEn);
            tvTotalTitle.setTypeface(fontEn);
            tvKnetTitle.setTypeface(fontEn);
            tvCashTitle.setTypeface(fontEn);


            tvPaymentDetailsTitle.setTypeface(fontEnMedium);


            tvSelectShippingAddress.setTypeface(fontEn);
            tvSelectShippingAddTitle.setTypeface(fontEn);

            tvTitleChekout.setTypeface(fontEnMedium);
            tvCartCounter.setTypeface(fontEn);

            tvSubTotalKdTitle.setTypeface(fontEn);
            tvDeliveryChargesKdTitle.setTypeface(fontEn);
            tvPackagingServiceKdTitle.setTypeface(fontEn);
            tvCouponKdTitle.setTypeface(fontEn);
            tvTotalMainKdtitle.setTypeface(fontEn);


        }
    }


    private void getMyAddress() {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_MY_ADDRESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseMyAddress", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        llSelectAddress.setVisibility(View.GONE);
                        llAddressDone.setVisibility(View.VISIBLE);
                        jsonArrayAddress = objects.getJSONArray("result");
                        setAdapter(jsonArrayAddress);
                        getMyGift();
                        Dialogshow();


                    } else {
                        pd.dismiss();
                        llSelectAddress.setVisibility(View.VISIBLE);
                        llAddressDone.setVisibility(View.GONE);
                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", pref.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                Log.e("MyAddressParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);
    }

    private void getMyGift() {

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_GIFT_PACKAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseMyGift", response);
                myGiftModelsList.clear();
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArray = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String package_id = jsonObject.getString("package_id");
                            String name = jsonObject.getString("name");
                            String brief = jsonObject.getString("brief");
                            String price = jsonObject.getString("price");
                            String image = jsonObject.getString("image");

                            MyGiftModel myGiftModel = new MyGiftModel(package_id, name, brief, price, image);
                            myGiftModelsList.add(myGiftModel);

                            //SetDataFromApi();
                        }

                        RecyclerView.LayoutManager mLayoutManager3 = new LinearLayoutManager(getApplicationContext());
                        rvGift.setLayoutManager(mLayoutManager3);
                        rvGift.setAdapter(new GiftAdapter(myGiftModelsList, R.layout.rowitem_gift, activity));
                        rvGift.setNestedScrollingEnabled(false);
                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", pref.getString("WhichLanguage", ""));

                Log.e("giftParams", params + "");

                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }


    private void setAdapter(JSONArray jsonArray) {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvAddress.setLayoutManager(mLayoutManager);
        rvAddress.setAdapter(new MyAddressAdapterCart(jsonArray, R.layout.adapter_address_cart, activity));
        rvAddress.setNestedScrollingEnabled(false);

    }

    public void getAddressPrice(int pos) {
        try {
            JSONObject object = jsonArrayAddress.getJSONObject(pos);
            String delivery_charge = object.getString("delivery_charge");
            tvSubDeliver.setText("" + delivery_charge);
            editorSp.putString("delivery_charge", object.getString("delivery_charge"));
            editorSp.commit();
            //AddTotalPrice();
            addTotalViaAddress();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    public void getGiftPrice(int pos) {
        MyGiftModel myGiftModel = myGiftModelsList.get(pos);
        tvSubWrapGift.setText("" + myGiftModel.getPrice());
        editorSp.putString("delivery_charge", sp.getString("delivery_charge", ""));
        editorSp.putString("gift_charge", myGiftModel.getPrice());
        editorSp.commit();
        //AddTotalPrice();
        addTotalViaGift();


    }


    private void getCouponValid() {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_VALID_COUPON, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Responsecoupon", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        editorSp.putString("couponCode", etCoupon.getText().toString());
                        editor.commit();
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                        JSONObject jsonObject = objects.getJSONObject("result");
                        String coupon_id = jsonObject.getString("coupon_id");
                        String discount = jsonObject.getString("discount");
                        String discount_value = jsonObject.getString("discount_value");
                        String min_price = jsonObject.getString("min_price");
                        editorSp.putString("couponDiscount", discount);
                        editorSp.commit();

                        tvSubCouponDiscount.setText("" + discount);
                        //AddTotalPrice();
                        addTotalViaGift();


                        pd.dismiss();


                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {
            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                params.put("coupon_code", etCoupon.getText().toString());
                Log.e("CouponParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);
    }




    private void getCheckOut(final String giftAs, final String fromCard, final String toCard) {

        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.MEMBER_CHECK_OUT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseCheckOut", response);

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    String order_id = objects.getString("order_id");

                    if (status.equalsIgnoreCase("true")) {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                      /*  Intent intent = new Intent(activity, ConfirmationCartActivity.class);
                        intent.putExtra("orderId", order_id);
                        startActivity(intent);
                        finish();*/


                        Intent intent = new Intent(activity, KNetActivity.class);
                        intent.putExtra("orderId", order_id);
                        startActivity(intent);
                        finish();
                        HomeActivityNew.cartCounter = 0;
                        editorSp.putString("couponDiscount", "0.000");
                        editorSp.commit();

                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
                        finish();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_SHORT).show();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("address_id", sp.getString("addressIdConfirm", "")); //address_id
                params.put("as_gift", giftAs);
                params.put("package_id", sp.getString("packageID", ""));
                params.put("gift_card", giftAs);

                params.put("card_from", fromCard);
                params.put("card_to", toCard);


                params.put("coupon_code", sp.getString("couponCode", ""));
                params.put("payment_method", sp.getString("paymentDetails", ""));
                params.put("customer_id", sp.getString("customerId", ""));

                Log.e("CHECKOUT PARAMS", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);


    }


    private void Dialogshow() {

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        dialogAddress = new Dialog(activity);
        dialogAddress.setContentView(R.layout.bill_shipp_address_layout);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogAddress.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;

        dialogAddress.getWindow().setAttributes(lp);
        dialogAddress.setCancelable(true);


        // set the custom dialog components - text, image and button

        RecyclerView rv = (RecyclerView) dialogAddress.findViewById(R.id.rv_address);
        TextView tvAddNewAddressNew = (TextView) dialogAddress.findViewById(R.id.tv_add_new_address_new_inAddress);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(mLayoutManager);
        rv.setAdapter(new MyAddressAdapterCart(jsonArrayAddress, R.layout.adapter_address_cart, activity));
        rv.setNestedScrollingEnabled(false);

        if (WhichLanguage.equalsIgnoreCase("ar")) {
            tvAddNewAddressNew.setText("اضافة كلمة السر الجديدة");
        } else {
            tvAddNewAddressNew.setText("Add New Address");
        }

        tvAddNewAddressNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, AddNewAddressActivity.class);
                // intent.putExtra("Addnew", "NewAddress");
                Bundle extras = new Bundle();
                extras.putString("Addnew", "NewAddress");
                extras.putString("callActivity", "cart");
                intent.putExtras(extras);
                startActivityForResult(intent, 123);
                dialogAddress.dismiss();
                // finish();


            }
        });
        /*rv.addOnItemTouchListener(new RecyclerItemClickListener(activity, rv, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                try {
                    JSONObject  jsonObjectResponse = jsonArrayAddress.getJSONObject(position);
                    String address_id = jsonObjectResponse.getString("address_id");
                    String area_id = jsonObjectResponse.getString("area_id");
                    String title = jsonObjectResponse.getString("title");
                    String area_name = jsonObjectResponse.getString("area_name");
                    String block = jsonObjectResponse.getString("block");
                    String street = jsonObjectResponse.getString("street");
                    String avenue = jsonObjectResponse.getString("avenue");
                    String building = jsonObjectResponse.getString("building");
                    String floor = jsonObjectResponse.getString("floor");
                    String apartment = jsonObjectResponse.getString("apartment");
                    String notes = jsonObjectResponse.getString("notes");
                    String delivery_charge = jsonObjectResponse.getString("delivery_charge");

                    tvSelectShippingAddress.setText("" + area_name + "," + "Block" + " " + block + " ," + street + " St.," + "Building" + " " + building + " ," + "Floor " + floor);

                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onLongItemClick(View view, int position) {
                // do whatever
            }


        }));
*/


        //dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.parseColor("#A6000000")));


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 123) {

            if (resultCode == Activity.RESULT_OK) {
                getMyAddressNewAdded();


                //String result=data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }

        }
    }

    private Boolean getValidatioCheckOut() {
        if (tvSelectShippingAddress.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Select Shipping Address", Toast.LENGTH_SHORT).show();
            return false;


        } else if (chkPayment.equalsIgnoreCase("")) {
            Toast.makeText(activity, "Please Select Payment", Toast.LENGTH_SHORT).show();
            return false;

        }


        return true;
    }

    private Boolean getValidatioCheckOutUrdu() {
        if (tvSelectShippingAddress.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(activity, "يرجى أختيار عنوان", Toast.LENGTH_SHORT).show();
            return false;


        } else if (chkPayment.equalsIgnoreCase("")) {
            Toast.makeText(activity, "يرجى اختيار الدفع", Toast.LENGTH_SHORT).show();
            return false;

        }


        return true;
    }


    public void getMyAddressNewAdded() {

        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_MY_ADDRESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseMyAddress", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    pd.dismiss();
                    if (status.equalsIgnoreCase("true")) {
                        int pos = 0;
                        try {
                            JSONArray jsonArrayAddress = objects.getJSONArray("result");
                            Log.e("JSONADDRESSLENG", (jsonArrayAddress.length() - 1) + "");
                            if ((jsonArrayAddress.length() - 1 != 0)) {

                                pos = (jsonArrayAddress.length() - 1);

                            } else {
                                pos = 0;
                            }

                            JSONObject jsonObjectPrice = jsonArrayAddress.getJSONObject(pos);
                            editorSp.putString("address_Charge", jsonObjectPrice.getString("delivery_charge"));
                            editorSp.putString("selectAddress", String.valueOf(pos));
                            Log.e("selectAddress", pos + "");
                            editorSp.commit();
                            String address_id = jsonObjectPrice.getString("address_id");
                            editorSp.putString("addressIdConfirm", address_id);
                            editorSp.commit();
                            String area_id = jsonObjectPrice.getString("area_id");
                            String title = jsonObjectPrice.getString("title");
                            String area_name = jsonObjectPrice.getString("area_name");
                            String block = jsonObjectPrice.getString("block");
                            String street = jsonObjectPrice.getString("street");
                            String avenue = jsonObjectPrice.getString("avenue");
                            String building = jsonObjectPrice.getString("building");
                            String floor = jsonObjectPrice.getString("floor");
                            String apartment = jsonObjectPrice.getString("apartment");
                            String notes = jsonObjectPrice.getString("notes");
                            String delivery_charge = jsonObjectPrice.getString("delivery_charge");
                            tvSelectShippingAddTitle.setVisibility(View.VISIBLE);
                            tvSelectShippingAddTitle.setText(title);
                            tvSelectShippingAddress.setText("" + area_name + "," + "Block" + " " + block + " ," + street + " St.," + "Building" + " " + building + " ," + "Floor " + floor);
                            getAddressPrice(pos);


                        } catch (JSONException e) {
                            pd.dismiss();
                            Log.e("JSONExceptionAdd", e + "");
                            Toast.makeText(activity, "ExceptionPrice" + e, Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        pd.dismiss();
                        llSelectAddress.setVisibility(View.VISIBLE);
                        llAddressDone.setVisibility(View.GONE);
                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");

                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
                Log.e("MyAddressParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);


    }


    public void addTotalViaGift() {

        if (WhichLanguage.equalsIgnoreCase("en")) {


            String itemPrice = sp.getString("totalPrice", "");
            String giftPrice = sp.getString("gift_charge", "");

            giftPriceNew = Double.parseDouble(giftPrice);

            String couponDiscount = sp.getString("couponDiscount", "");
            if (couponDiscount.equalsIgnoreCase("")) {
                couponDiscount = "0.000";
                editorSp.putString("couponDiscount", couponDiscount);
                editorSp.commit();
                couponDiscountNew = Double.parseDouble(couponDiscount);
                tvSubCouponDiscount.setText("" + precision.format(couponDis));


            } else {
                tvSubCouponDiscount.setText("" + precision.format(couponDis));
            }


            if (!giftPrice.equalsIgnoreCase("") && !couponDiscount.equalsIgnoreCase("")) {
                double itmPr = Double.parseDouble(itemPrice);
                tvSubTotal.setText("" + precision.format(itmPr));
                totalPriceMainNew = (Double.parseDouble(itemPrice) + deliveryPriceNew + Double.parseDouble(giftPrice) - Double.parseDouble(couponDiscount));


                Log.e("totalPrice", totalPriceMainNew + "");

                tvSubTotalMain.setText("" + precision.format(totalPriceMainNew));

            } else {
                Toast.makeText(activity, "Delivery or Gift Price is less than 0", Toast.LENGTH_SHORT).show();
            }


        } else if (WhichLanguage.equalsIgnoreCase("ar")) {


            Log.e("lang", WhichLanguage);
            Configuration config = getBaseContext().getResources().getConfiguration();
            Locale locale = new Locale("en");
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            precision = new DecimalFormat("0.000");



            String itemPrice = sp.getString("totalPrice", "");
            String giftPrice = sp.getString("gift_charge", "");

            giftPriceNew = Double.parseDouble(giftPrice);

            String couponDiscount = sp.getString("couponDiscount", "");
            if (couponDiscount.equalsIgnoreCase("")) {
                couponDiscount = "0.000";
                editorSp.putString("couponDiscount", couponDiscount);
                editorSp.commit();
                couponDiscountNew = Double.parseDouble(couponDiscount);
                tvSubCouponDiscount.setText("" + precision.format(couponDis));


            } else {
                tvSubCouponDiscount.setText("" + precision.format(couponDis));
            }


            if (!giftPrice.equalsIgnoreCase("") && !couponDiscount.equalsIgnoreCase("")) {
                double itmPr = Double.parseDouble(itemPrice);
                tvSubTotal.setText("" + precision.format(itmPr));
                totalPriceMainNew = (Double.parseDouble(itemPrice) + deliveryPriceNew + Double.parseDouble(giftPrice) - Double.parseDouble(couponDiscount));


                Log.e("totalPrice", totalPriceMainNew + "");

                tvSubTotalMain.setText("" + precision.format(totalPriceMainNew));

            } else {
                Toast.makeText(activity, "Delivery or Gift Price is less than 0", Toast.LENGTH_SHORT).show();
            }


            Log.e("lang", WhichLanguage);
            Configuration config1 = getBaseContext().getResources().getConfiguration();
            Locale locale1 = new Locale("ar");
            Locale.setDefault(locale1);
            config1.locale = locale1;
            getBaseContext().getResources().updateConfiguration(config1, getBaseContext().getResources().getDisplayMetrics());


        }

    }


    private void addTotalViaAddress() {

        if (WhichLanguage.equalsIgnoreCase("en")) {

            String itemPrice = sp.getString("totalPrice", "");

            String delivery = sp.getString("delivery_charge", "");

            deliveryPriceNew = Double.parseDouble(delivery);
            tvSubDeliver.setText("" + precision.format(deliveryPriceNew));


            totalPriceMainNew = (Double.parseDouble(itemPrice) + deliveryPriceNew + giftPriceNew - couponDiscountNew);

            //totalPriceMainNew = totalPriceMainNew + deliveryPriceNew;

            tvSubTotalMain.setText("" + precision.format(totalPriceMainNew));


        } else if (WhichLanguage.equalsIgnoreCase("ar")) {


            Log.e("lang", WhichLanguage);
            Configuration config = getBaseContext().getResources().getConfiguration();
            Locale locale = new Locale("en");
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            precision = new DecimalFormat("0.000");

            String itemPrice = sp.getString("totalPrice", "");

            String delivery = sp.getString("delivery_charge", "");

            deliveryPriceNew = Double.parseDouble(delivery);
            tvSubDeliver.setText("" + precision.format(deliveryPriceNew));


            totalPriceMainNew = (Double.parseDouble(itemPrice) + deliveryPriceNew + giftPriceNew - couponDiscountNew);

            //totalPriceMainNew = totalPriceMainNew + deliveryPriceNew;

            tvSubTotalMain.setText("" + precision.format(totalPriceMainNew));




            Log.e("lang", WhichLanguage);
            Configuration config1 = getBaseContext().getResources().getConfiguration();
            Locale locale1 = new Locale("ar");
            Locale.setDefault(locale1);
            config1.locale = locale1;
            getBaseContext().getResources().updateConfiguration(config1, getBaseContext().getResources().getDisplayMetrics());


        }

    }
}





    /*private void AddTotalPriceNew(int newPos) {

        if (WhichLanguage.equalsIgnoreCase("en")){

            String itemPrice = sp.getString("totalPrice", "");
            String delivery = sp.getString("delivery_charge", "");

            if (delivery.equalsIgnoreCase("")) {
                delivery = "0";
                editorSp.putString("delivery_charge", delivery);
                editorSp.commit();

            } else {
                deliPr = Double.parseDouble(delivery);
                if (newPos != 0){
                    tvSubDeliver.setText("" + precision.format(deliPr));

                }

            }
            String giftPrice = sp.getString("gift_charge", "");
            String couponDiscount = sp.getString("couponDiscount", "");
            if (couponDiscount.equalsIgnoreCase("")) {
                couponDiscount = "0.000";
                editorSp.putString("couponDiscount", couponDiscount);
                editorSp.commit();
                couponDis = Double.parseDouble(couponDiscount);
                tvSubCouponDiscount.setText("" + precision.format(couponDis));


            } else {
                tvSubCouponDiscount.setText("" + precision.format(couponDis));
            }

            Log.e("delivery....", delivery + "");
            Log.e("giftPrice....", giftPrice + "");

            if (!delivery.equalsIgnoreCase("") && !giftPrice.equalsIgnoreCase("") && !couponDiscount.equalsIgnoreCase("")) {
                double itmPr = Double.parseDouble(itemPrice);
                tvSubTotal.setText("" + precision.format(itmPr));
                Double totalPrice = (Double.parseDouble(itemPrice) + Double.parseDouble(delivery) + Double.parseDouble(giftPrice) - Double.parseDouble(couponDiscount));
                //  Float totalPrice = (Float.parseFloat(itemPrice) + Float.parseFloat(delivery) + Float.parseFloat(giftPrice));

                Log.e("totalPrice", totalPrice + "");

                tvSubTotalMain.setText("" + precision.format(totalPrice));

            } else {
                Toast.makeText(activity, "Delivery or Gift Price is less than 0", Toast.LENGTH_SHORT).show();
            }


        }else if (WhichLanguage.equalsIgnoreCase("ar")){


            Log.e("lang", WhichLanguage);
            Configuration config = getBaseContext().getResources().getConfiguration();
            Locale locale = new Locale("en");
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            precision = new DecimalFormat("0.000");

            String itemPrice = sp.getString("totalPrice", "");
            String delivery = sp.getString("delivery_charge", "");
            if (delivery.equalsIgnoreCase("")) {
                delivery = "0";
                editorSp.putString("delivery_charge", delivery);
                editorSp.commit();

            } else {
                deliPr = Double.parseDouble(delivery);
                tvSubDeliver.setText("" + precision.format(deliPr));

            }
            String giftPrice = sp.getString("gift_charge", "");
            String couponDiscount = sp.getString("couponDiscount", "");
            if (couponDiscount.equalsIgnoreCase("")) {
                couponDiscount = "0.000";
                editorSp.putString("couponDiscount", couponDiscount);
                editorSp.commit();
                couponDis = Double.parseDouble(couponDiscount);
                tvSubCouponDiscount.setText("" + precision.format(couponDis));


            } else {
                tvSubCouponDiscount.setText("" + precision.format(couponDis));
            }

            Log.e("delivery....", delivery + "");
            Log.e("giftPrice....", giftPrice + "");

            if (!delivery.equalsIgnoreCase("") && !giftPrice.equalsIgnoreCase("") && !couponDiscount.equalsIgnoreCase("")) {
                double itmPr = Double.parseDouble(itemPrice);
                tvSubTotal.setText("" + precision.format(itmPr));
                Double totalPrice = (Double.parseDouble(itemPrice) + Double.parseDouble(delivery) + Double.parseDouble(giftPrice) - Double.parseDouble(couponDiscount));
                //  Float totalPrice = (Float.parseFloat(itemPrice) + Float.parseFloat(delivery) + Float.parseFloat(giftPrice));

                Log.e("totalPrice", totalPrice + "");

                tvSubTotalMain.setText("" + precision.format(totalPrice));

            } else {
                Toast.makeText(activity, "Delivery or Gift Price is less than 0", Toast.LENGTH_SHORT).show();
            }


            Log.e("lang", WhichLanguage);
            Configuration config1 = getBaseContext().getResources().getConfiguration();
            Locale locale1 = new Locale("ar");
            Locale.setDefault(locale1);
            config1.locale = locale1;
            getBaseContext().getResources().updateConfiguration(config1, getBaseContext().getResources().getDisplayMetrics());


        }

    }*/
