package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.SearchAdapter;
import com.minikin.android.model.SearchModel;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SearchActivity extends BaseActivity {
    private Activity activity = SearchActivity.this;
    private ProgressDialog pd;
    private RelativeLayout rl_back, rl_Menu;
    private FrameLayout rlCart;

    SearchAdapter searchAdapter;

    RecyclerView rvSearch = null;
    private TextView tv_title,tvCartCounter;

    private SharedPreferences pref;
    private SharedPreferences sp;

    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    String text = "";
    ArrayList<SearchModel> searchList = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.activity_search, contentFrameLayout);
        //setContentView(R.layout.activity_search);
        initiViews();
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        setLangRecreate(WhichLanguage);
        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        searchList = new ArrayList<>();
        text = getIntent().getStringExtra("Text");
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvCartCounter.setText(""+HomeActivityNew.cartCounter);

        getSearchData(text);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.toolbar_two_rl_back:
                finish();
                break;

            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;

            case R.id.rl_cart_icon_toolbar:
                Intent intent = new Intent(activity, MyCartActivityNew.class);
                startActivity(intent);
                break;

        }
    }

    private void initiViews() {
        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_back.setOnClickListener(this);
        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tv_title.setText("Search");
        rvSearch = findViewById(R.id.rv_search);
        rl_Menu = findViewById(R.id.toolbar_rl_menutwo);
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        tvCartCounter = findViewById(R.id.tv_cart_counter);
        rlCart.setOnClickListener(this);
        rl_Menu.setOnClickListener(this);


    }

    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");
        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            tv_title.setTypeface(fontAr);
            tvCartCounter.setTypeface(fontAr);


        } else if (langval.equalsIgnoreCase("en")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tv_title.setTypeface(fontEnMedium);
            tvCartCounter.setTypeface(fontEn);


        }
    }


    private void getSearchData(final String textEdit) {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_SEARCH, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                searchList.clear();
                try {
                    Log.e("SearchResponse", response);
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArrayResult = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArrayResult.length(); i++) {
                            JSONObject objSearch = jsonArrayResult.getJSONObject(i);
                            String product_id = objSearch.getString("product_id");
                            String name = objSearch.getString("name");
                            String description = objSearch.getString("description");
                            String price = objSearch.getString("price");
                            String image = objSearch.getString("image");
                            String has_offer = objSearch.getString("has_offer");
                            String offer_price = objSearch.getString("offer_price");
                            String flag = objSearch.getString("flag");

                            SearchModel serarchModel = new SearchModel(product_id, name, description, price, image, has_offer, offer_price, flag);
                            searchList.add(serarchModel);
                        }
                        pd.dismiss();
                        searchAdapter = new SearchAdapter(searchList, R.layout.rowitem_pro, activity);
                        GridLayoutManager layoutManager = new GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false);
                        rvSearch.setLayoutManager(layoutManager);
                        rvSearch.setAdapter(searchAdapter);
                    } else {
                        pd.dismiss();
                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();

                    }
                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Log.e("Exception", e + "");
                    Toast.makeText(activity, "Exception" + e, Toast.LENGTH_LONG).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");
                Toast.makeText(activity, "VolleyError" + error, Toast.LENGTH_LONG).show();


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("key_word", textEdit);
                Log.e("searchParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }

}
