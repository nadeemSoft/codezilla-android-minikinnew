package com.minikin.android.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.adapter.MyOrderAdapter;
import com.minikin.android.model.MyOrderModel;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MyOrderActivity extends BaseActivity {

    private Activity activity = MyOrderActivity.this;
    private RelativeLayout rl_back, rl_menu;
    private FrameLayout rlCart;

    private TextView tv_title, tvCartCounter;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    private ProgressDialog pd = null;

    private RecyclerView rvOrder;
    private SharedPreferences sp = null;
    public  ArrayList<MyOrderModel> myOrderModelList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_framefr); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.act_my_order, contentFrameLayout);
        initViews();

        sp = getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        pref = getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        setLangRecreate(WhichLanguage);
        pd = new ProgressDialog(this);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        myOrderModelList = new ArrayList<>();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getMyOrder();

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {

            case R.id.toolbar_two_rl_back:
                finish();
                break;

            case R.id.toolbar_rl_menutwo:
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    // ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorYellow));
                    drawer.closeDrawer(GravityCompat.START);

                } else {
                    drawer.openDrawer(GravityCompat.START);
                    //ivNavigation.setBackgroundColor(getResources().getColor(R.color.colorLightBlurNavigation));
                }
                break;

            case R.id.rl_cart_icon_toolbar:
                Intent intent = new Intent(activity, MyCartActivityNew.class);
                startActivity(intent);
                break;
        }

    }

    private void initViews() {

        rl_back = findViewById(R.id.toolbar_two_rl_back);
        rl_back.setOnClickListener(this);

        rl_menu = findViewById(R.id.toolbar_rl_menutwo);
        rl_menu.setOnClickListener(this);
        tvCartCounter = findViewById(R.id.tv_cart_counter);

        tv_title = findViewById(R.id.toolbar_two_tv_title);
        tv_title.setText("My Order");
        rlCart = findViewById(R.id.rl_cart_icon_toolbar);
        rlCart.setOnClickListener(this);
        rvOrder = findViewById(R.id.rv_order);

    }


    public void setLangRecreate(String langval) {
        Configuration config = getBaseContext().getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        //Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(activity.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(activity.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ContextCompat.getDrawable(activity, R.drawable.ic_back).setAutoMirrored(true);
            tv_title.setText("طلباتي السابقه");
            tv_title.setTypeface(fontEnMedium);
            tvCartCounter.setTypeface(fontEn);


        } else if (langval.equalsIgnoreCase("en")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tv_title.setTypeface(fontEnMedium);
            tvCartCounter.setTypeface(fontEn);


        }
    }


    /*private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new RecentFragment(), "RECENT");
        //adapter.addFragment(new HistoryFragment(), "HISTORY");
        viewPager.setAdapter(adapter);
    }*/


    private void getMyOrder() {
        pd.show();

        Log.e("url",AppConstants.GET_MY_ORDER);


        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_MY_ORDER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseOrder", response);
                rvOrder.removeAllViews();
                myOrderModelList.clear();
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        JSONArray jsonArray = objects.getJSONArray("result");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObjectResult = jsonArray.getJSONObject(i);
                            String order_id = jsonObjectResult.getString("order_id");
                            String date = jsonObjectResult.getString("date");
                            String statusget = jsonObjectResult.getString("status");

                            MyOrderModel myOrderModel = new MyOrderModel(order_id, date, statusget);
                            myOrderModelList.add(myOrderModel);

                        }


                    } else {
                        pd.dismiss();
                        Toast.makeText(MyOrderActivity.this, message, Toast.LENGTH_SHORT).show();

                    }


                    tvCartCounter.setText("" + HomeActivityNew.cartCounter);

                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
                    rvOrder.setLayoutManager(mLayoutManager);
                    rvOrder.setAdapter(new MyOrderAdapter(myOrderModelList, R.layout.rowitem_my_order, activity));
                    rvOrder.setNestedScrollingEnabled(false);


                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(MyOrderActivity.this, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("customer_id", sp.getString("customerId", ""));
               // params.put("device_id", sp.getString("deviceId", ""));
                Log.e("MyorderParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(MyOrderActivity.this);
        requestQueue.add(request);

    }


    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
