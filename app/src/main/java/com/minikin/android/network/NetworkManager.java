package com.minikin.android.network;

import android.app.Activity;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.util.AppConstants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


public class NetworkManager {
    Activity activity;

    SharedPreferences sp = null;

    SharedPreferences.Editor editorUser = null;

    public NetworkManager(Activity activity) {
        this.activity = activity;

    }

    public void getRefreshToken(Activity activity) {

        sp = activity.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editorUser = sp.edit();
        StringRequest request = new StringRequest(Request.Method.POST, "url", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String responseString = "";

                Log.e("ResponesNetwork", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String accessToken = objects.getString("access_token");
                    String expiresIn = objects.getString("expires_in");
                    String tokenType = objects.getString("token_type");
                    String scope = objects.getString("scope");
                    String refreshToken = objects.getString("refresh_token");

                    editorUser.putString("access_token", accessToken);
                    editorUser.putString("expires_in", expiresIn);
                    editorUser.putString("token_type", tokenType);
                    editorUser.putString("scope", scope);
                    editorUser.putString("refresh_token", refreshToken);
                    editorUser.commit();



                } catch (Exception e) {

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("VolleyError", String.valueOf(error));

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                //params.put("Authorization", sp.getString("token_type", "") +" "+ sp.getString("refresh_token", ""));

                Log.e("networkparamsHeader", params+"");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<String, String>();
                param.put("grant_type", "refresh_token");
                param.put("refresh_token", sp.getString("refresh_token", ""));
                param.put("username", sp.getString("userName", ""));
                param.put("password", sp.getString("password", ""));
                param.put("client_id", sp.getString("client_id", ""));
                param.put("client_secret", sp.getString("client_secret", ""));
                Log.e("networkparams", param+"");

                return param;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(activity);
        requestQueue.add(request);

    }


}
