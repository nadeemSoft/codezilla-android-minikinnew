package com.minikin.android.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.minikin.android.util.AppConstants;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;

public class MyOkHttp {
    Context context;
    OkHttpClient client;
    SharedPreferences sp = null;
    SharedPreferences.Editor editorUser;


    public Response executeAPI(RequestBody requestBody, String url, Context context) throws IOException {

        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        String accessToken = sp.getString("access_token","");
        editorUser = sp.edit();

        Request request = null;
        request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .post(requestBody)
                .build();

        client = new OkHttpClient();
        client.setConnectTimeout(20, TimeUnit.SECONDS);
        client.setReadTimeout(25, TimeUnit.SECONDS);

        //TODO: Here this code call asynchronously need to make it synchronous
        //new BackgroundTask().execute(request);

        Response response = client.newCall(request).execute();
        //Response returnValue = response;
        //String result = response.body().string().toString();
        //Log.e("response code", result);

        try {

            Log.e("code------", response.code() + "");
            if (response.code() == 401) {
                String refreshToken = sp.getString("refresh_token","");
                if (refreshToken == null) {
                    return null;
                }
                //getAuthTokenAgain(context);
                return executeAPI(requestBody, url, context);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;

    }

    public String getAuthTokenAgain(final Context context) {

        //String url = UrlConstant.API_URL + UrlConstant.URL_GET_REFRESH_AUTH_TOKEN;
        String url = "";

        try {
            OkHttpClient client = new OkHttpClient();

            client.setConnectTimeout(20, TimeUnit.SECONDS);
            client.setReadTimeout(25, TimeUnit.SECONDS);
            RequestBody requestBody = null;

            Log.e("old access token",sp.getString("access_token",""));
            Log.e("refresh token1", sp.getString("refresh_token",""));

            requestBody = new FormEncodingBuilder()
                    .add("grant_type", "refresh_token")
                    .add("refresh_token", sp.getString("refresh_token", ""))
                    .add("username", sp.getString("userName", ""))
                    .add("password", sp.getString("password", ""))
                    .add("client_id", sp.getString("client_id", ""))
                    .add("client_secret", sp.getString("client_secret", ""))
                    //.add(KeyConstant.KEY_REFRESH_TOKEN, "")
                    .build();

            Request request = new Request.Builder()
                    .url(url)
                    .post(requestBody)
                    .build();

            Response response = client.newCall(request).execute();
            String result = response.body().string().toString();

            if (response.code() == 401) {
               Log.e("need to logout user", "401 when token refresh");
            } else {
                //Log.e("refresh token", result + "");
                JSONObject responseObj = new JSONObject(result);
                String accessToken = responseObj.optString("access_token");
                String expires_in = responseObj.optString("expires_in");
                String token_type = responseObj.optString("token_type");
                String scope = responseObj.optString("scope");
                String refresh_token = responseObj.optString("refresh_token");

                editorUser.putString("access_token", accessToken);
                editorUser.putString("expires_in", expires_in);
                editorUser.putString("token_type", token_type);
                editorUser.putString("scope", scope);
                editorUser.putString("refresh_token", refresh_token);
                editorUser.commit();

               Log.e("new access token",sp.getString("access_token",""));
                Log.e("refresh token2", sp.getString("refresh_token",""));
            }
           Log.e("responseStr ", result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;

    }

}
