package com.minikin.android.adapter;

/**
 * Created by cube_dev on 10/24/2017.
 */

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.activity.MyCartActivityNew;
import com.minikin.android.model.CartItemModel;
import com.minikin.android.util.AppConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.RestaurantViewHolder> {
    public
    double totalPrice = 0.0;
    long quantitlyLong;
    double productPriceDouble = 0.0;
    double price;

    public ArrayList<CartItemModel> cartItemList;
    private int rowLayout;
    private Context context;
    int count;
    ProgressDialog pd;
    private SharedPreferences sp = null;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    int mainQuantity;
    String WhichLanguage = "";


    public static class RestaurantViewHolder extends RecyclerView.ViewHolder {

        TextView tvCartName, tvStoreName, tvCartPrice, tvSelectQtyCount, tvQuantityDialog,tvPrice,tvQuantity;
        RelativeLayout rlClose, rlSelectQtySub, rlSelectQtyAdd;
        ImageView ivCartImage;
        LinearLayout linearMain;
        ImageView ivQuantitySub,ivQuantityAdd;


        public RestaurantViewHolder(View v) {
            super(v);
            tvCartName = v.findViewById(R.id.tv_cart_name);
            tvStoreName = v.findViewById(R.id.tv_store_name);
            tvCartPrice = v.findViewById(R.id.tv_cart_price);
            tvSelectQtyCount = v.findViewById(R.id.tv_select_qty_count);
            rlClose = v.findViewById(R.id.rl_close);
            rlSelectQtySub = v.findViewById(R.id.lay_select_qty_rl_sub);
            rlSelectQtyAdd = v.findViewById(R.id.lay_select_qty_rl_add);
            ivCartImage = v.findViewById(R.id.iv_cart_image);
            ivQuantitySub = v.findViewById(R.id.iv_quantity_sub);
            ivQuantityAdd = v.findViewById(R.id.iv_quantity_add);
            linearMain = v.findViewById(R.id.linear_main);
            tvPrice = v.findViewById(R.id.tv_cart_new_price);
            tvQuantity = v.findViewById(R.id.tv_quantity);


        }
    }

    public CartAdapter(ArrayList<CartItemModel> cartItemList, int rowLayout, Context context) {
        this.cartItemList = cartItemList;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editor = sp.edit();


    }

    @Override
    public CartAdapter.RestaurantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new RestaurantViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final RestaurantViewHolder holder, final int position) {
        setLangRecreate(WhichLanguage, holder);

        final CartItemModel cartItem = cartItemList.get(position);
        holder.tvCartName.setText(cartItem.getProductName());

        if (WhichLanguage.equalsIgnoreCase("en")){

            holder.tvStoreName.setText("By" + " " + cartItem.getStoreName());
            holder.tvCartPrice.setText("KD" + " " + cartItem.getProductPrice());

        }else if (WhichLanguage.equalsIgnoreCase("ar")){

            holder.tvStoreName.setText("بواسطة" + " " + cartItem.getStoreName());
            holder.tvCartPrice.setText("KD" + " " + cartItem.getProductPrice());

            // holder.tvCartPrice.setText("د.ك" + " " + cartItem.getProductPrice());

        }

        holder.tvSelectQtyCount.setText(cartItem.getQuantity());

        Picasso.with(context).load(cartItem.getProductImage()).fit().placeholder(R.drawable.grey_img_holder)   // optional
                .error(R.drawable.grey_img_holder).into(holder.ivCartImage);



        holder.ivQuantityAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int quan = Integer.parseInt(cartItemList.get(position).getQuantity());
                Log.e("QUANTITYADD", quan + "");
                count = 0;
                count = count + 1;
                Log.e("QUANTITYADDCHEK", count + "");
                mainQuantity = quan + 1;
                getProductStock(cartItem.getProductId(), String.valueOf(count), cartItem.getAttritute1(), cartItem.getAttritute2(), position, holder);





            }
        });


        holder.ivQuantitySub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int quan = Integer.parseInt(cartItemList.get(position).getQuantity());
                Log.e("QUANTITYSUB", quan + "");
                if (quan > 1) {
                    count = 0;
                    count = count - 1;
                    Log.e("QUANTITYSUBCHEK", count + "");
                    mainQuantity = quan - 1;
                    Log.e("quantitychkkmain", mainQuantity + "");

                    getProductStock(cartItem.getProductId(), String.valueOf(count), cartItem.getAttritute1(), cartItem.getAttritute2(), position, holder);


                }else {
                    if (WhichLanguage.equalsIgnoreCase("ar")){
                        Toast.makeText(context, "الكمية أكثر من 1", Toast.LENGTH_SHORT).show();

                    }else {
                        Toast.makeText(context, "Quantity more than 1", Toast.LENGTH_SHORT).show();

                    }
                }


            }
        });


        holder.rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_layout);

                TextView tvTitleDialog = dialog.findViewById(R.id.dialog_titile);
                TextView tvMessage = dialog.findViewById(R.id.dialog_message);
                Button yes = dialog.findViewById(R.id.dialog_positive_btn);
                Button no = dialog.findViewById(R.id.dialog_negative_btn);
                tvTitleDialog.setText("Minikin");
                tvMessage.setText("Are You Sure? You want to remove Item from cart..");

                Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
                Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");


                if (WhichLanguage.equalsIgnoreCase("ar")) {
                    //((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);


                    tvTitleDialog.setText("مينيكن");
                    tvMessage.setText("هل تريد إزالة المنتج من السلة؟");
                    yes.setText("نعم");
                    no.setText("لا");

                    tvTitleDialog.setTypeface(fontEn);
                    tvMessage.setTypeface(fontEn);
                    yes.setTypeface(fontEn);
                    no.setTypeface(fontEn);


                } else if (WhichLanguage.equalsIgnoreCase("en")) {
                    //((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                    tvTitleDialog.setTypeface(fontEn);
                    tvMessage.setTypeface(fontEn);
                    yes.setTypeface(fontEn);
                    no.setTypeface(fontEn);


                }


                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        removeItemFromCart(cartItem.getCartId(), position);
                        dialog.dismiss();


                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });

                dialog.show();


            }
        });


    }

    @Override
    public int getItemCount() {
        //return cartItemList.size();
        return cartItemList == null ? 0 : cartItemList.size();
    }

    private void removeItemFromCart(final String cartId, final int pos) {

        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_REMOVE_ITEM_CART, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseRemoveCart", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        totalPrice = 0.0;
                        pd.dismiss();
                        cartItemList.remove(pos);
                        notifyDataSetChanged();

                        if (cartItemList.size() == 0) {
                            ((Activity) context).finish();


                        }

                        ((MyCartActivityNew)context).computePrice();
                        ((MyCartActivityNew)context).getCartForCounter();
                    } else {
                        pd.dismiss();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(context, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("cart_id", cartId);

                Log.e("MyRemoveParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);

    }


    public void setLangRecreate(String langval, RestaurantViewHolder holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

            holder.tvQuantity.setText("الكميه");
            holder.tvPrice.setText(" السعر");
            holder.tvCartName.setTypeface(fontEn);
            holder.tvCartPrice.setTypeface(fontEn);
            holder.tvStoreName.setTypeface(fontEn);
            holder.tvSelectQtyCount.setTypeface(fontEn);
            holder.tvPrice.setTypeface(fontEn);
            holder.tvQuantity.setTypeface(fontEn);

        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvCartName.setTypeface(fontEn);
            holder.tvCartPrice.setTypeface(fontEn);
            holder.tvStoreName.setTypeface(fontEn);
            holder.tvSelectQtyCount.setTypeface(fontEn);
            holder.tvPrice.setTypeface(fontEn);
            holder.tvQuantity.setTypeface(fontEn);



        }
    }


    public void updateCart(final String productId, final String quant, final String atr1, final String atr2, final int pos, final RestaurantViewHolder holder) {

        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_ADD_ITEM_CARD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseUpdateCartCart", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        //cartItem.setQuantity(String.valueOf(quan));
                        CartItemModel itemModel = cartItemList.get(pos);
                        itemModel.setQuantity(String.valueOf(mainQuantity));
                        String getQuan = itemModel.getQuantity();
                        holder.tvSelectQtyCount.setText("" + getQuan);

                        ((MyCartActivityNew)context).computePrice();
                        ((MyCartActivityNew)context).getCartForCounter();


                        pd.dismiss();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else {
                        pd.dismiss();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(context, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("product_id", productId);
                params.put("customer_id", sp.getString("customerId", ""));
                params.put("attribute_1", atr1);
                params.put("attribute_2", atr2);
                params.put("quantity", quant);
                Log.e("UpdateCartParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);
    }


    private void getProductStock(final String productId, final String quant, final String atr1, final String atr2, final int pos, final RestaurantViewHolder holder) {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_PRODUCT_STOCK, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("StockResponse", response + "");

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    String stock = objects.getString("stock");

                    if (status.equalsIgnoreCase("true") || !stock.equalsIgnoreCase("0")) {
                        updateCart(productId, quant, atr1, atr2, pos, holder);

                    } else {
                        pd.dismiss();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                    }

                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(context, "Exception Two" + e, Toast.LENGTH_SHORT).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("product_id", productId);
                params.put("attribute_1", atr1);
                params.put("attribute_2", atr2);
                Log.e("stockParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);
    }
}
