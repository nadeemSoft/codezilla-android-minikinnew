package com.minikin.android.adapter;

/**
 * Created by cube_dev on 10/24/2017.
 */

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.activity.AddNewAddressActivity;
import com.minikin.android.activity.MyAddressActivity;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class MyAddressAdapter extends RecyclerView.Adapter<MyAddressAdapter.RestaurantViewHolder> {

    private JSONArray jsonArray;
    private int rowLayout;
    private Context context;
    ProgressDialog pd;
    SharedPreferences sp = null;
    private SharedPreferences.Editor editor;
    SharedPreferences pref;
    String WhichLanguage = "";


    public static class RestaurantViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitleName, tvAddress;
        ImageView ivDeleteAddress, ivEditAddress;

        public RestaurantViewHolder(View v) {
            super(v);
            tvTitleName = v.findViewById(R.id.tv_name_address);
            tvAddress = v.findViewById(R.id.tv_addresss_main);
            ivDeleteAddress = v.findViewById(R.id.iv_delete_add);
            ivEditAddress = v.findViewById(R.id.iv_edit_add);


        }
    }

    public MyAddressAdapter(JSONArray jsonArray, int rowLayout, Context context) {
        this.jsonArray = jsonArray;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editor = sp.edit();


    }

    @Override
    public MyAddressAdapter.RestaurantViewHolder onCreateViewHolder(ViewGroup parent,
                                                                    int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new RestaurantViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RestaurantViewHolder holder, final int position) {
        setLangRecreate(WhichLanguage,holder);
        try {
            JSONObject jsonObjectResponse = jsonArray.getJSONObject(position);
            String address_id = jsonObjectResponse.getString("address_id");
            String area_id = jsonObjectResponse.getString("area_id");
            String title = jsonObjectResponse.getString("title");
            String area_name = jsonObjectResponse.getString("area_name");
            String block = jsonObjectResponse.getString("block");
            String street = jsonObjectResponse.getString("street");
            String avenue = jsonObjectResponse.getString("avenue");
            String building = jsonObjectResponse.getString("building");
            String floor = jsonObjectResponse.getString("floor");
            String apartment = jsonObjectResponse.getString("apartment");
            String notes = jsonObjectResponse.getString("notes");
            String delivery_charge = jsonObjectResponse.getString("delivery_charge");

            holder.tvTitleName.setText(title);
            holder.tvAddress.setText("" + area_name + "," + " Block " + block+"," + " " + street + " St.," + " building no. " + building + "," + " floor " + floor);


            holder.ivDeleteAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Dialog dialog = new Dialog(context);
                    dialog.setCancelable(true);
                    dialog.setContentView(R.layout.dialog_layout);

                    TextView tvTitleDialog = dialog.findViewById(R.id.dialog_titile);
                    TextView tvMessage = dialog.findViewById(R.id.dialog_message);
                    Button btnyes = dialog.findViewById(R.id.dialog_positive_btn);
                    Button btnno = dialog.findViewById(R.id.dialog_negative_btn);

                    tvTitleDialog.setText("Delete Address");
                    tvMessage.setText("Are You Sure? You want to Delete Address");
                    btnyes.setText("DELETE");
                    btnno.setText("NO");

                    //Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
                    Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
                    Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
                    Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


                    Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");


                    if (WhichLanguage.equalsIgnoreCase("ar")) {
                        //((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

                       /* tvTitleDialog.setTypeface(fontAr);
                        tvMessage.setTypeface(fontAr);
                        btnyes.setTypeface(fontAr);
                        btnno.setTypeface(fontAr);*/


                        tvTitleDialog.setText("حذف العنوان ");
                        tvMessage.setText("هل انت متأكد من حذف العنوان");
                        btnyes.setText("حذف");
                        btnno.setText("لا");


                    } else if (WhichLanguage.equalsIgnoreCase("en")) {
                        //((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                        tvTitleDialog.setTypeface(fontEnMedium);
                        tvMessage.setTypeface(fontEnMedium);
                        btnyes.setTypeface(fontEnMedium);
                        btnno.setTypeface(fontEnMedium);


                    }


                    btnyes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            JSONObject jsonObjectResponse = null;
                            try {
                                jsonObjectResponse = jsonArray.getJSONObject(position);
                                String address_id = jsonObjectResponse.getString("address_id");
                                deleteAddress(address_id, position);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();


                        }
                    });

                    btnno.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();

                        }
                    });

                    dialog.show();


                }
            });

            holder.ivEditAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        JSONObject jsonObjectResponse = jsonArray.getJSONObject(position);
                        Intent intent = new Intent(context, AddNewAddressActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("Addnew", "EditAddress");
                        extras.putInt("position", position);
                        intent.putExtras(extras);
                        context.startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }



    }

    @Override
    public int getItemCount() {
        //return 1;
        return jsonArray == null ? 0 : jsonArray.length();
    }

    private void deleteAddress(final String idAddress, final int pos) {
        pd.show();

        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_REMOVE_ADDRESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("RemoveAddress", response);
                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        pd.dismiss();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                        jsonArray.remove(pos);
                        notifyDataSetChanged();

                        if(jsonArray.length() == 0){
                            MyAddressActivity.tvAddNewAddress.setText("NO ADDRESS FOUND");
                        }

                    } else {
                        pd.dismiss();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                    }


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(context, "Exception" + e, Toast.LENGTH_SHORT).show();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VolleyError", error + "");
                pd.dismiss();


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("address_id", idAddress);
                params.put("customer_id", sp.getString("customerId", ""));
                Log.e("removeParams", params + "");
                return params;
            }
        };

        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);
    }


    public void setLangRecreate(String langval,RestaurantViewHolder holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
       // Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");



        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvAddress.setTypeface(fontAr);
            holder.tvTitleName.setTypeface(fontAr);

        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvAddress.setTypeface(fontEn);
            holder.tvTitleName.setTypeface(fontEnMedium);



        }
    }
}