package com.minikin.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.minikin.android.R;
import com.minikin.android.activity.CheckOutActivityNew;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class MyAddressAdapterCart extends RecyclerView.Adapter<MyAddressAdapterCart.Cart> {
    private JSONArray jsonArray;
    private int rowLayout;
    private Context context;
    ProgressDialog pd;
    SharedPreferences sp = null;
    private SharedPreferences.Editor editorUser;

    int lastClickedPosition = -1;
    SharedPreferences pref;
    String WhichLanguage = "";


    public MyAddressAdapterCart(JSONArray jsonArray, int rowLayout, Context context) {
        this.jsonArray = jsonArray;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editorUser = sp.edit();

    }

    @Override
    public Cart onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new Cart(view);
    }

    @Override
    public void onBindViewHolder(Cart holder, final int position) {
        setLangRecreate(WhichLanguage, holder);

        JSONObject jsonObjectResponse = null;
        try {
            jsonObjectResponse = jsonArray.getJSONObject(position);
            String address_id = jsonObjectResponse.getString("address_id");
            editorUser.putString("addressIdConfirm", address_id);
            editorUser.commit();
            String area_id = jsonObjectResponse.getString("area_id");
            String title = jsonObjectResponse.getString("title");
            String area_name = jsonObjectResponse.getString("area_name");
            String block = jsonObjectResponse.getString("block");
            String street = jsonObjectResponse.getString("street");
            String avenue = jsonObjectResponse.getString("avenue");
            String building = jsonObjectResponse.getString("building");
            String floor = jsonObjectResponse.getString("floor");
            String apartment = jsonObjectResponse.getString("apartment");
            String notes = jsonObjectResponse.getString("notes");
            String delivery_charge = jsonObjectResponse.getString("delivery_charge");

            holder.tvTitleName.setText("" + title+" :");

            if (WhichLanguage.equalsIgnoreCase("en")){

                holder.tvAddress.setText(" "+area_name+","+"Block"+" "+block+","+street+" St.,"+"Building"+" "+ building+","+"Floor "+floor);


            }else if (WhichLanguage.equalsIgnoreCase("ar")){

                holder.tvAddress.setText(" "+area_name+","+"القطعه "+" "+block+","+street+" الشارع..,"+"بناء"+" "+ building+","+"الطابق "+floor);


            }


            if (lastClickedPosition == position) {

                holder.radioButtonChkNew.setChecked(true);
                JSONObject jsonObjectPrice = jsonArray.getJSONObject(lastClickedPosition);
                editorUser.putString("address_Charge", jsonObjectPrice.getString("delivery_charge"));
                editorUser.putString("selectAddress", String.valueOf(lastClickedPosition));
                Log.e("selectAddress", lastClickedPosition + "");
                editorUser.commit();


              //  ((CheckOutActivityNew)context).getAddressPrice(lastClickedPosition);
               /* holder.radioButtonChk.setChecked(true);
                JSONObject jsonObjectPrice = jsonArray.getJSONObject(lastClickedPosition);
                editorUser.putString("address_Charge", jsonObjectPrice.getString("delivery_charge"));
                editorUser.putString("selectAddress", String.valueOf(lastClickedPosition));
                Log.e("selectAddress", lastClickedPosition + "");
                editorUser.commit();
                ((CheckOutActivityNew)context).getAddressPrice(lastClickedPosition);*/


            } else {
                holder.radioButtonChk.setChecked(false);
                holder.radioButtonChkNew.setChecked(false);

            }

            holder.llParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastClickedPosition = position;
                    JSONObject jsonObjectPrice = null;
                    try {
                        jsonObjectPrice = jsonArray.getJSONObject(lastClickedPosition);
                        editorUser.putString("address_Charge", jsonObjectPrice.getString("delivery_charge"));
                        editorUser.putString("selectAddress", String.valueOf(lastClickedPosition));
                        Log.e("selectAddress", lastClickedPosition + "");
                        editorUser.commit();

                        String address_id = jsonObjectPrice.getString("address_id");
                        editorUser.putString("addressIdConfirm", address_id);

                        Log.e("addressIdConfirm", address_id + "");

                        editorUser.commit();

                        String area_id = jsonObjectPrice.getString("area_id");
                        String title = jsonObjectPrice.getString("title");
                        String area_name = jsonObjectPrice.getString("area_name");
                        String block = jsonObjectPrice.getString("block");
                        String street = jsonObjectPrice.getString("street");
                        String avenue = jsonObjectPrice.getString("avenue");
                        String building = jsonObjectPrice.getString("building");
                        String floor = jsonObjectPrice.getString("floor");
                        String apartment = jsonObjectPrice.getString("apartment");
                        String notes = jsonObjectPrice.getString("notes");
                        String delivery_charge = jsonObjectPrice.getString("delivery_charge");
                        ((CheckOutActivityNew)context).tvSelectShippingAddTitle.setVisibility(View.VISIBLE);
                        ((CheckOutActivityNew)context).tvSelectShippingAddTitle.setText(title);

                        if (WhichLanguage.equalsIgnoreCase("en")){

                            ((CheckOutActivityNew)context).tvSelectShippingAddress.setText("" + area_name + "," + "Block" + " " + block + " ," + street + " St.," + "Building" + " " + building + " ," + "Floor " + floor);


                        }else if (WhichLanguage.equalsIgnoreCase("ar")){
                            ((CheckOutActivityNew)context).tvSelectShippingAddress.setText("" + area_name + "," + "القطعه" + " " + block + " ," + street + " الشارع..," + "بناء" + " " + building + " ," + "الطابق " + floor);

                            //holder.tvAddress.setText(" "+area_name+","+"القطعه "+" "+block+","+street+" الشارع..,"+"بناء"+" "+ building+","+"الطابق "+floor);


                        }
                        ((CheckOutActivityNew)context).dialogAddress.dismiss();
                        ((CheckOutActivityNew)context).getAddressPrice(lastClickedPosition);



                    } catch (JSONException e) {
                        Toast.makeText(context,"ExceptionPrice"+e,Toast.LENGTH_SHORT).show();
                    }

                        notifyDataSetChanged();


                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public class Cart extends RecyclerView.ViewHolder {
        TextView tvTitleName, tvAddress;
        LinearLayout llParent;
        RadioButton radioButtonChk,radioButtonChkNew;

        public Cart(View itemView) {
            super(itemView);
            tvTitleName = itemView.findViewById(R.id.tv_name_address);
            tvAddress = itemView.findViewById(R.id.tv_addresss_main);
            llParent = itemView.findViewById(R.id.ll_parent);
            radioButtonChk = itemView.findViewById(R.id.radio);
            radioButtonChkNew = itemView.findViewById(R.id.radio_new);


        }
    }

    public void setLangRecreate(String langval, Cart holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
       // Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

            holder.tvAddress.setTypeface(fontEn);
            holder.tvTitleName.setTypeface(fontEnMedium);
          /*  holder.tvAddress.setTypeface(fontAr);
            holder.tvTitleName.setTypeface(fontAr);*/


        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvAddress.setTypeface(fontEn);
            holder.tvTitleName.setTypeface(fontEnMedium);


        }
    }
}
