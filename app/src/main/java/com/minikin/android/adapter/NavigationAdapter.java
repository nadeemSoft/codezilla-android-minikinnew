package com.minikin.android.adapter;

/**
 * Created by cube_dev on 10/24/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.minikin.android.activity.BaseActivity;
import com.minikin.android.R;
import com.minikin.android.util.AppConstants;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.RestaurantViewHolder> {

    private String[] strings = {"Home", "Log in / Register", "My Account", "My Favourites", "My Orders", "My Cart", "New Supplier", "Contact Us", "Live Chat", "Terms & Conditions", "Return & Exchange", "Delivery Policy"
            , "Who we are"};

    private String[] stringsLogout = {"Home", "Logout", "My Account", "My Favourites", "My Orders", "My Cart", "New Supplier", "Contact Us", "Live Chat", "Terms & Conditions", "Return & Exchange", "Delivery Policy"
            , "Who we are"};

    private String[] stringsUrdu = {"الصفحة الرئيسيه", "تسجيل الدخول", "الملف الشخصي", "المفضله", "طلباتي السابقه", "سلة المشتريات", "مورد جديد", "اتصل بنا", "تحدث معنا", "الشروط والاحكام", "الاسترجاع والاستبدال", "سياسة التوصيل"
            , "من نحن"};


    private String[] stringsUrduLogout = {"الصفحة الرئيسيه", "خروج", "الملف الشخصي", "المفضله", "طلباتي السابقه", "سلة المشتريات", "مورد جديد", "اتصل بنا", "تحدث معنا", "الشروط والاحكام", "الاسترجاع والاستبدال", "سياسة التوصيل"
            , "من نحن"};



    private int rowLayout;
    private int row_index = 0;
    private Context context;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    String WhichLanguage = "";
    private SharedPreferences sp = null;
    String loginValue;


    public static class RestaurantViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name;
        ImageView ivDrawar;
        LinearLayout llMain;

        public RestaurantViewHolder(View v) {
            super(v);
            tv_name = v.findViewById(R.id.rt_nav_tv_name);
            ivDrawar = v.findViewById(R.id.iv_drawar);
            ivDrawar.setVisibility(View.GONE);
            llMain = v.findViewById(R.id.ll_nav);

        }
    }

    public NavigationAdapter(int rowLayout, Context context) {
        this.rowLayout = rowLayout;
        this.context = context;
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        loginValue = sp.getString("loginValue", "");

    }

    @Override
    public NavigationAdapter.RestaurantViewHolder onCreateViewHolder(ViewGroup parent,
                                                                     int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new RestaurantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RestaurantViewHolder holder, final int position) {
        setLangRecreate(WhichLanguage, holder);

        if (WhichLanguage.equalsIgnoreCase("en")){

            if (loginValue.equalsIgnoreCase("login")) {

                if (position % 3 == 2) {

                    if (position > 9) {

                        holder.tv_name.setText(stringsLogout[position]);
                        holder.llMain.setVisibility(View.GONE);

                    } else {

                        holder.tv_name.setText(stringsLogout[position]);
                        holder.llMain.setVisibility(View.VISIBLE);
                    }

                } else {

                    holder.tv_name.setText(stringsLogout[position]);
                    holder.llMain.setVisibility(View.GONE);
                }


            } else if (!loginValue.equalsIgnoreCase("login")) {

                if (position % 3 == 2) {

                    if (position > 9) {

                        holder.tv_name.setText(strings[position]);
                        holder.llMain.setVisibility(View.GONE);

                    } else {

                        holder.tv_name.setText(strings[position]);
                        holder.llMain.setVisibility(View.VISIBLE);
                    }

                } else {

                    holder.tv_name.setText(strings[position]);
                    holder.llMain.setVisibility(View.GONE);
                }

            }

        }else if (WhichLanguage.equalsIgnoreCase("ar")){



            if (loginValue.equalsIgnoreCase("login")) {

                if (position % 3 == 2) {

                    if (position > 9) {

                        holder.tv_name.setText(stringsUrduLogout[position]);
                        holder.llMain.setVisibility(View.GONE);

                    } else {

                        holder.tv_name.setText(stringsUrduLogout[position]);
                        holder.llMain.setVisibility(View.VISIBLE);
                    }

                } else {

                    holder.tv_name.setText(stringsUrduLogout[position]);
                    holder.llMain.setVisibility(View.GONE);
                }


            } else if (!loginValue.equalsIgnoreCase("login")) {

                if (position % 3 == 2) {

                    if (position > 9) {

                        holder.tv_name.setText(stringsUrdu[position]);
                        holder.llMain.setVisibility(View.GONE);

                    } else {

                        holder.tv_name.setText(stringsUrdu[position]);
                        holder.llMain.setVisibility(View.VISIBLE);
                    }

                } else {

                    holder.tv_name.setText(stringsUrdu[position]);
                    holder.llMain.setVisibility(View.GONE);
                }

            }

        }




        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index = position;
                notifyDataSetChanged();

                ((BaseActivity) context).menuItemClick(position);
                // ((HomeActivity) context).menuItemClick(position);


            }
        });
    }

    @Override
    public int getItemCount() {
        return strings == null ? 0 : strings.length;
    }

    public void setLangRecreate(String langval, RestaurantViewHolder holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        // Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tv_name.setTypeface(fontAr);

        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tv_name.setTypeface(fontEn);


        }
    }
}