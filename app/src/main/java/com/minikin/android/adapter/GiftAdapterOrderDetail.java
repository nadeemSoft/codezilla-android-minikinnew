package com.minikin.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.model.MyGiftModel;
import com.minikin.android.util.AppConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class GiftAdapterOrderDetail extends RecyclerView.Adapter<GiftAdapterOrderDetail.OrderGift> {


    ArrayList<MyGiftModel> myGiftModelsList;

    private int rowLayout;
    private Context context;
    int count = 0;
    ProgressDialog pd;
    int lastClickedPosition = 0;
    SharedPreferences sp = null;
    private SharedPreferences.Editor editor;
    private SharedPreferences.Editor editorSp;

    SharedPreferences pref;
    String WhichLanguage = "";


    public GiftAdapterOrderDetail(ArrayList<MyGiftModel> myGiftModelsList, int rowLayout, Context context) {
        this.myGiftModelsList = myGiftModelsList;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editorSp = sp.edit();

    }


    @Override
    public OrderGift onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new OrderGift(view);
    }

    @Override
    public void onBindViewHolder(OrderGift holder, int position) {

        setLangRecreate(WhichLanguage, holder);


        MyGiftModel myGiftModel = myGiftModelsList.get(position);
        holder.tvPackageName.setText(myGiftModel.getName());
        holder.tvSubName.setText("KD" + " " + myGiftModel.getPrice());
        holder.tvSubName.append(" " + myGiftModel.getBrief());

        // holder.tvPackagePrice.setText("price" + " " + myGiftModel.getPrice());

        Picasso.with(context).load(myGiftModel.getPackagImageId()).fit().into(holder.ivGift);


    }

    @Override
    public int getItemCount() {
        return myGiftModelsList == null ? 0 : myGiftModelsList.size();
    }


    public void setLangRecreate(String langval, OrderGift holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        // Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvSubName.setTypeface(fontEn);
            holder.tvPackageName.setTypeface(fontEn);

        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvSubName.setTypeface(fontEn);
            holder.tvPackageName.setTypeface(fontEn);


        }
    }


    public class OrderGift extends RecyclerView.ViewHolder {

        ImageView ivGift;
        TextView tvPackageName, tvSubName;

        public OrderGift(View itemView) {
            super(itemView);
            ivGift = itemView.findViewById(R.id.iv_image_package);
            tvPackageName = itemView.findViewById(R.id.tv_package_name);
            tvSubName = itemView.findViewById(R.id.tv_package_sub);
        }
    }


}
