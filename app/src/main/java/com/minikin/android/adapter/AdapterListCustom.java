package com.minikin.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.minikin.android.R;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AdapterListCustom extends ArrayAdapter {
    int rowLayout;
    Context context;
    ArrayList<String> arrayList;
    TextView tv;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private String WhichLanguage = "";
    ViewHolder viewHolder;

    public AdapterListCustom(Context context, int rowLayout, ArrayList<String> arrayList) {
        super(context, rowLayout, arrayList);
        this.context = context;
        this.rowLayout = rowLayout;
        this.arrayList = arrayList;
        pref = context.getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        String name = arrayList.get(position);
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(rowLayout, null);
            viewHolder = new ViewHolder();
            viewHolder.tvs = (TextView) convertView.findViewById(R.id.tv_name);
        }
        viewHolder.tvs.setText(name);
        setLangRecreate(WhichLanguage);
        return convertView;

    }

    static class ViewHolder {
        static TextView tvs;
    }

    public void setLangRecreate(String langval) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            viewHolder.tvs.setTypeface(fontAr);


        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            viewHolder.tvs.setTypeface(fontEn);


        }
    }
}
