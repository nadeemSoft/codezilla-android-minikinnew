package com.minikin.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class MyOrderDetailAttributesAdapter extends RecyclerView.Adapter<MyOrderDetailAttributesAdapter.MyOrderDetailAttributes> {
    private Context context;
    JSONArray jsonArray;
    SharedPreferences pref;
    String WhichLanguage = "";
    private SharedPreferences sp = null;


    public MyOrderDetailAttributesAdapter(JSONArray jsonArray, Context context) {
        this.jsonArray = jsonArray;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);

    }

    @Override
    public MyOrderDetailAttributes onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_detail_attribute, parent, false);
        return new MyOrderDetailAttributes(view);
    }

    @Override
    public void onBindViewHolder(MyOrderDetailAttributes holder, int position) {
        setLangRecreate(WhichLanguage, holder);

        try {
            Log.e("SETAttributeLength",jsonArray.isNull(position)+"");


            JSONObject jsonObjectAttribute =  jsonArray.getJSONObject(position);
            String set_name = jsonObjectAttribute.getString("set_name");

            holder.tvOrderDetailSetAttributeSetName.setText(""+set_name);

            JSONArray jsonArraySetAttributes= jsonObjectAttribute.getJSONArray("set_attributes");

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL, false);
            holder.rvOrderDetailsetAttribute.setLayoutManager(mLayoutManager);
            holder.rvOrderDetailsetAttribute.setAdapter(new AdapterOrderDetailSetAttributes(jsonArraySetAttributes, context));
            holder.rvOrderDetailsetAttribute.setNestedScrollingEnabled(false);



        }catch (Exception e){

        }

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public void setLangRecreate(String langval,MyOrderDetailAttributes holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");

        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

            holder.tvOrderDetailSetAttributeSetName.setTypeface(fontAr);



        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            holder.tvOrderDetailSetAttributeSetName.setTypeface(fontEn);
        }
    }



    public class MyOrderDetailAttributes extends RecyclerView.ViewHolder {
        TextView tvOrderDetailSetAttributeSetName;
        RecyclerView rvOrderDetailsetAttribute;
        public MyOrderDetailAttributes(View itemView) {
            super(itemView);
            tvOrderDetailSetAttributeSetName = itemView.findViewById(R.id.tv_my_order_detail_attribute_name);
            rvOrderDetailsetAttribute = itemView.findViewById(R.id.rv_order_detail_set_attribute);
        }
    }
}
