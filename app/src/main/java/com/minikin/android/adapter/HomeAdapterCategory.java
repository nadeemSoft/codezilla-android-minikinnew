package com.minikin.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.model.CategoryModel;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class HomeAdapterCategory  extends RecyclerView.Adapter<HomeAdapterCategory.Cat>{
    Context context;
    ArrayList<CategoryModel> categoriesList;
    public static int poss = -1;
    public boolean val = false;
    public static HomeAdapterCategory homeAdapterCategory;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    String WhichLanguage = "";

    public HomeAdapterCategory(Context context,ArrayList<CategoryModel> categoriesList){
        this.context = context;
        this.categoriesList = categoriesList;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");

    }
    @Override
    public Cat onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_home_category, parent, false);
        return new Cat(view);
    }

    @Override
    public void onBindViewHolder(Cat holder, int position) {
        setLangRecreate(WhichLanguage, holder);

        CategoryModel categoryModel =  categoriesList.get(position);
       holder.tvCategoryName.setText(categoryModel.getCategoryName());
       if(categoryModel.getArrayList().size() != 0){
           holder.ivMenu.setVisibility(View.VISIBLE);


       }else {
           holder.ivMenu.setVisibility(View.GONE);

       }



        if (poss == -1) {
            val = true;
            poss = 0;
            // Toast.makeText(HomeActivity.homeActivity, "poss" + poss, Toast.LENGTH_LONG).show();

            holder.tvCategoryName.setTypeface(null, Typeface.BOLD);
            holder.tvCategoryName.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));


        } else {

            if (position == poss) {
                val = true;
                //Toast.makeText(getContext(), "poss in else " + poss, Toast.LENGTH_LONG).show();
                if (poss == 0) {
                    holder.tvCategoryName.setTypeface(null, Typeface.BOLD);
                    holder.tvCategoryName.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

                } else if (poss == 1) {
                    holder.tvCategoryName.setTypeface(null, Typeface.BOLD);
                    holder.tvCategoryName.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

                }else if(poss == 2){
                    holder.tvCategoryName.setTypeface(null, Typeface.BOLD);
                    holder.tvCategoryName.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

                }else if(poss == 3){
                    holder.tvCategoryName.setTypeface(null, Typeface.BOLD);
                    holder.tvCategoryName.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

                }else if(poss == 4){
                    holder.tvCategoryName.setTypeface(null, Typeface.BOLD);
                    holder.tvCategoryName.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

                }

                holder.tvCategoryName.setTypeface(null, Typeface.BOLD);
                holder.tvCategoryName.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

            } else {
                holder.tvCategoryName.setTypeface(null, Typeface.NORMAL);
                holder.tvCategoryName.setTextColor(context.getResources().getColor(R.color.colorCategoryName));

            }
        }




    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }
    public class Cat extends RecyclerView.ViewHolder {
        TextView tvCategoryName;
        ImageView ivMenu;
        public Cat(View itemView) {
            super(itemView);
            tvCategoryName = itemView.findViewById(R.id.tv_adapter_category);
            ivMenu = itemView.findViewById(R.id.menuIcon);
        }
    }

    public void setLangRecreate(String langval,Cat holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvCategoryName.setTypeface(fontEn);


        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
           holder.tvCategoryName.setTypeface(fontEn);


        }
    }
}
