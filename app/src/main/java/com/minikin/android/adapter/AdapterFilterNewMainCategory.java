package com.minikin.android.adapter;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.model.SetAttributeModel;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AdapterFilterNewMainCategory extends RecyclerView.Adapter<AdapterFilterNewMainCategory.FilterNew> {
    public static int poss = -1;
    Activity context;
    ArrayList<SetAttributeModel> attributeMainList;
    SharedPreferences sp = null;
    private SharedPreferences.Editor editor;
    SharedPreferences pref;
    String WhichLanguage = "";

    public AdapterFilterNewMainCategory(Activity context, ArrayList<SetAttributeModel> attributeMainList) {
        this.context = context;
        this.attributeMainList = attributeMainList;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");


    }

    @Override
    public FilterNew onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_filter_new_layout, parent, false);
        return new FilterNew(view);
    }

    @Override
    public void onBindViewHolder(FilterNew holder, int position) {
        setLangRecreate(WhichLanguage, holder);

        SetAttributeModel attributeModel = attributeMainList.get(position);

        holder.tvMainCategory.setText(attributeModel.getSetName());



        if (poss == -1) {
            poss = 0;

            holder.tvMainCategory.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

        } else {

            if (position == poss) {
                //Toast.makeText(getContext(), "poss in else " + poss, Toast.LENGTH_LONG).show();
                if (poss == 0) {
                    holder.tvMainCategory.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

                } else if (poss == 1) {
                    holder.tvMainCategory.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));
                } else if (poss == 2) {
                    holder.tvMainCategory.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

                } else if (poss == 3) {
                    holder.tvMainCategory.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

                } else if (poss == 4) {
                    holder.tvMainCategory.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

                }

                holder.tvMainCategory.setTextColor(context.getResources().getColor(R.color.colorLightBlurBorder));

            } else {
                holder.tvMainCategory.setTypeface(null, Typeface.NORMAL);
                holder.tvMainCategory.setTextColor(context.getResources().getColor(R.color.colorTollBarName));
            }
        }

    }

    public void setLangRecreate(String langval, FilterNew holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
       // Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvMainCategory.setTypeface(fontEn);

        } else if (langval.equalsIgnoreCase("en")) {
            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvMainCategory.setTypeface(fontEn);


        }
    }

    @Override
    public int getItemCount() {
        return attributeMainList.size();
    }

    public class FilterNew extends RecyclerView.ViewHolder {
        TextView tvMainCategory;

        public FilterNew(View itemView) {
            super(itemView);
            tvMainCategory = itemView.findViewById(R.id.tv_filter_new_main_category);
        }
    }
}
