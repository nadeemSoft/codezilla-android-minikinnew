package com.minikin.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AdapterOrderDetailSetAttributes extends RecyclerView.Adapter<AdapterOrderDetailSetAttributes.SetOrderAttribute>{

    private Context context;
    JSONArray jsonArray;
    SharedPreferences pref;
    String WhichLanguage = "";
    private SharedPreferences sp = null;


    public AdapterOrderDetailSetAttributes(JSONArray jsonArray, Context context) {
        this.jsonArray = jsonArray;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);

    }

    @Override
    public SetOrderAttribute onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapte_order_detail_set_attributes, parent, false);
        return new SetOrderAttribute(view);

    }

    @Override
    public void onBindViewHolder(SetOrderAttribute holder, int position) {
        setLangRecreate(WhichLanguage, holder);


        try {

            JSONObject jsonObjectAttribute =  jsonArray.getJSONObject(position);
            String attribute_name = jsonObjectAttribute.getString("attribute_name");

            holder.tvOrderDetailAttributeSetName.setText(""+attribute_name);




        }catch (Exception e){

        }



    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public void setLangRecreate(String langval,SetOrderAttribute holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");

        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

            holder.tvOrderDetailAttributeSetName.setTypeface(fontAr);



        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            holder.tvOrderDetailAttributeSetName.setTypeface(fontEn);
        }
    }

    public class SetOrderAttribute extends RecyclerView.ViewHolder {

        TextView tvOrderDetailAttributeSetName;

        public SetOrderAttribute(View itemView) {
            super(itemView);
            tvOrderDetailAttributeSetName = itemView.findViewById(R.id.tv_my_order_detail_set_attributes);

        }
    }
}
