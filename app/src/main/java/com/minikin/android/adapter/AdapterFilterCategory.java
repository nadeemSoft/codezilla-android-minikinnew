package com.minikin.android.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.model.AttributesModel;
import com.minikin.android.model.SetAttributeModel;
import com.minikin.android.util.AppConstants;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class AdapterFilterCategory extends RecyclerView.Adapter<AdapterFilterCategory.Cate> {
    Context context;
    ArrayList<SetAttributeModel> attributeMainList;
    ArrayList<AttributesModel> attributesSubCategoryList = new ArrayList<>();
     SharedPreferences sp = null;
    SharedPreferences.Editor editor;


    public AdapterFilterCategory(Context context, ArrayList<SetAttributeModel> attributeMainList) {
        this.context = context;
        this.attributeMainList = attributeMainList;
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editor = sp.edit();
        editor.putString("sizeFilterCategory",attributeMainList.size()+"");
        editor.commit();

        Log.e("sizeFilterCategory",attributeMainList.size()+"");


    }

    @Override
    public Cate onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_filter_category_name, parent, false);
        return new Cate(view);
    }

    @Override
    public void onBindViewHolder(Cate holder, int position) {
        /*SetAttributeModel attributeModel = attributeMainList.get(position);
        attributesSubCategoryList = attributeModel.getAttributesModelsLisy();
        holder.tvCategoryName.setText(attributeModel.getSetName());
        holder.rvSubCategory.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
        holder.rvSubCategory.setAdapter(new AdapterFilterSubCategory(context, attributesSubCategoryList));*/




    }

    @Override
    public int getItemCount() {
        return attributeMainList.size();
    }

    public class Cate extends RecyclerView.ViewHolder {
        TextView tvCategoryName;
        RecyclerView rvSubCategory;

        public Cate(View itemView) {
            super(itemView);

            tvCategoryName = itemView.findViewById(R.id.tv_adapter_filter_category);
            rvSubCategory = itemView.findViewById(R.id.rv_filter_sub_category_name);
        }
    }
}
