package com.minikin.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.activity.CheckOutActivityNew;
import com.minikin.android.model.MyGiftModel;
import com.minikin.android.util.AppConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class GiftAdapter extends RecyclerView.Adapter<GiftAdapter.RestaurantViewHolder> {

    ArrayList<MyGiftModel> myGiftModelsList;
    private int rowLayout;
    private Context context;
    int count = 0;
    ProgressDialog pd;
    int lastClickedPosition = 0;
    SharedPreferences sp = null;
    private SharedPreferences.Editor editor;
    SharedPreferences pref;
    String WhichLanguage = "";


    public static class RestaurantViewHolder extends RecyclerView.ViewHolder {

        TextView tvPackageName, tvStandardName, tvSubPackageName, tvPackagePrice;
        ImageView ivGirtImage;
        LinearLayout llChk, llUnchk;
        RadioGroup radioGroupChk, radioGroupUnChk;
        RadioButton radioButtonChk, radioButtonUnChk;

        public RestaurantViewHolder(View v) {
            super(v);
            tvPackageName = v.findViewById(R.id.tv_package_name);
            tvSubPackageName = v.findViewById(R.id.tv_package_sub);
            tvStandardName = v.findViewById(R.id.tv_standard);
            ivGirtImage = v.findViewById(R.id.iv_image_package);
            radioGroupUnChk = v.findViewById(R.id.radio_group_unchk);
            radioButtonChk = v.findViewById(R.id.chk_mini_pac);
            radioButtonUnChk = v.findViewById(R.id.unchk_mini_pack);
            tvPackagePrice = v.findViewById(R.id.tv_package_price);
            llChk = v.findViewById(R.id.ll_chek);
            llUnchk = v.findViewById(R.id.ll_unchk);


        }
    }

    public GiftAdapter(ArrayList<MyGiftModel> myGiftModelsList, int rowLayout, Context context) {
        this.myGiftModelsList = myGiftModelsList;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editor = sp.edit();


    }

    @Override
    public RestaurantViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new RestaurantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RestaurantViewHolder holder, final int position) {
        setLangRecreate(WhichLanguage, holder);

        holder.llUnchk.setVisibility(View.GONE);
        MyGiftModel myGiftModel = myGiftModelsList.get(position);
        holder.tvPackageName.setText(myGiftModel.getName());
        holder.tvSubPackageName.setText(myGiftModel.getBrief());

        if (WhichLanguage.equalsIgnoreCase("en")){
            holder.tvPackagePrice.setText("price" + " " + myGiftModel.getPrice());

        }else if (WhichLanguage.equalsIgnoreCase("ar")){
            holder.tvPackagePrice.setText("السعر" + " " + myGiftModel.getPrice());

        }

       // Picasso.with(context).load(myGiftModel.getPackagImageId()).into(holder.ivGirtImage);

        Picasso.with(context)
                .load(myGiftModel.getPackagImageId())
                .fit()
                .into(holder.ivGirtImage);

        if (lastClickedPosition == position) {
            holder.radioButtonChk.setChecked(true);
            MyGiftModel giftModel = myGiftModelsList.get(lastClickedPosition);
            String giftPrice = giftModel.getPrice();
            editor.putString("giftPrice",giftPrice );
            editor.putString("selectGirt", String.valueOf(lastClickedPosition));
            editor.putString("packageID", myGiftModel.getPackageId());
            Log.e("checkSelectPackageId", myGiftModel.getPackageId());
            editor.commit();


            ((CheckOutActivityNew)context).getGiftPrice(lastClickedPosition);



        } else {
            holder.radioButtonChk.setChecked(false);
        }

        holder.llChk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastClickedPosition = position;
                editor.putString("selectGirt", String.valueOf(lastClickedPosition));
                MyGiftModel giftModel = myGiftModelsList.get(lastClickedPosition);
                String giftPrice = giftModel.getPrice();
                editor.putString("giftPrice",giftPrice );
                editor.commit();
                notifyDataSetChanged();
            }
        });


    }


    @Override
    public int getItemCount() {
        //return cartItemList.size();
        return myGiftModelsList == null ? 0 : myGiftModelsList.size();
    }

    public void setLangRecreate(String langval, RestaurantViewHolder holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
       // Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

            holder.tvPackagePrice.setTypeface(fontEn);
            holder.tvSubPackageName.setTypeface(fontEn);
            holder.tvPackageName.setTypeface(fontEn);
            holder.tvStandardName.setTypeface(fontEn);
           /* holder.tvPackagePrice.setTypeface(fontAr);
            holder.tvSubPackageName.setTypeface(fontAr);
            holder.tvPackageName.setTypeface(fontAr);
            holder.tvStandardName.setTypeface(fontAr);*/

        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvPackagePrice.setTypeface(fontEn);
            holder.tvSubPackageName.setTypeface(fontEn);
            holder.tvPackageName.setTypeface(fontEn);
            holder.tvStandardName.setTypeface(fontEn);


        }
    }


}
