package com.minikin.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AddressOrderDetail extends RecyclerView.Adapter<AddressOrderDetail.AddressOrder> {
    private JSONArray jsonArray;
    private int rowLayout;
    private Context context;
    ProgressDialog pd;
    SharedPreferences sp = null;
    SharedPreferences pref;
    String WhichLanguage = "";

    public AddressOrderDetail(JSONArray jsonArray, int rowLayout, Context context) {
        this.jsonArray = jsonArray;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);

    }

    @Override
    public AddressOrder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new AddressOrder(view);
    }

    @Override
    public void onBindViewHolder(AddressOrder holder, int position) {

        setLangRecreate(WhichLanguage, holder);


        try {
            JSONObject jsonObjectResponse = jsonArray.getJSONObject(position);
            String address_id = jsonObjectResponse.getString("address_id");
            String title = jsonObjectResponse.getString("title");
            String area_id = jsonObjectResponse.getString("area_id");
            String area_name = jsonObjectResponse.getString("area_name");
            String block = jsonObjectResponse.getString("block");
            String street = jsonObjectResponse.getString("street");
            String building = jsonObjectResponse.getString("building");
            String floor = jsonObjectResponse.getString("floor");
            String apartment = jsonObjectResponse.getString("apartment");

           // String avenue = jsonObjectResponse.getString("avenue");
           // String notes = jsonObjectResponse.getString("notes");

            if (WhichLanguage.equalsIgnoreCase("en")){

                holder.tvTitleName.setText("" + title);
                holder.tvAddress.setText(" "+area_name+","+"Block"+" "+block+","+street+" St.,"+"Building"+" "+ building+","+"Floor "+floor);


            }else if (WhichLanguage.equalsIgnoreCase("ar")){
                holder.tvTitleName.setText("" + title);
                holder.tvAddress.setText(" "+area_name+","+"القطعه "+" "+block+","+street+" الشارع..,"+"بناء"+" "+ building+","+"الطابق "+floor);


            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public class AddressOrder extends RecyclerView.ViewHolder {

        TextView tvTitleName, tvAddress;
        public AddressOrder(View itemView) {
            super(itemView);
            tvTitleName = itemView.findViewById(R.id.tv_name_address);
            tvAddress = itemView.findViewById(R.id.tv_addresss_main);
        }
    }



    public void setLangRecreate(String langval,AddressOrder holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvAddress.setTypeface(fontEn);
            holder.tvTitleName.setTypeface(fontEnMedium);

        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvAddress.setTypeface(fontEn);
            holder.tvTitleName.setTypeface(fontEnMedium);


        }
    }


}
