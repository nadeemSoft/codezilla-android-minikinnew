package com.minikin.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.activity.CheckOutActivityNew;
import com.minikin.android.activity.MyCartGuestMemberCheckout;
import com.minikin.android.model.MyGiftModel;
import com.minikin.android.util.AppConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AdapterGiftSelect extends RecyclerView.Adapter<AdapterGiftSelect.Gift> {
    ArrayList<MyGiftModel> myGiftModelsList;

    private int rowLayout;
    private Context context;
    int count = 0;
    ProgressDialog pd;
    int lastClickedPosition = 0;
    SharedPreferences sp = null;
    private SharedPreferences.Editor editor;
    private SharedPreferences.Editor editorSp;

    SharedPreferences pref;
    String WhichLanguage = "";

    public AdapterGiftSelect(ArrayList<MyGiftModel> myGiftModelsList, int rowLayout, Context context) {
        this.myGiftModelsList = myGiftModelsList;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editorSp = sp.edit();

    }

    @Override
    public Gift onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new Gift(view);
    }

    @Override
    public void onBindViewHolder(final Gift holder, final int position) {


        setLangRecreate(WhichLanguage, holder);

        holder.llUnchk.setVisibility(View.GONE);
        MyGiftModel myGiftModel = myGiftModelsList.get(position);
        holder.tvPackageName.setText(myGiftModel.getName());
        holder.tvSubPackageName.setText(myGiftModel.getBrief());

        if (WhichLanguage.equalsIgnoreCase("en")){
            holder.tvPackagePrice.setText("price" + " " + myGiftModel.getPrice());

        }else if (WhichLanguage.equalsIgnoreCase("ar")){
            holder.tvPackagePrice.setText("السعر" + " " + myGiftModel.getPrice());


        }

       // Picasso.with(context).load(myGiftModel.getPackagImageId()).into(holder.ivGirtImage);

        Picasso.with(context)
                .load(myGiftModel.getPackagImageId())
                .fit()
                .into(holder.ivGirtImage);

        if (lastClickedPosition == position) {
            holder.radioButtonChk.setChecked(true);
            MyGiftModel giftModel = myGiftModelsList.get(lastClickedPosition);
            String giftPrice = giftModel.getPrice();
            editor.putString("giftPriceGuest",giftPrice );
            editor.putString("selectGirtGuest", String.valueOf(lastClickedPosition));
            editorSp.putString("packageIdGuest", myGiftModel.getPackageId());
            Log.e("packageIDGuestAda",myGiftModel.getPackageId());

            editorSp.commit();
            ((MyCartGuestMemberCheckout)context).getGiftPriceGuest(lastClickedPosition);




        } else {
            holder.radioButtonChk.setChecked(false);
        }

        holder.llChk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastClickedPosition = position;
                editor.putString("selectGirt", String.valueOf(lastClickedPosition));
                MyGiftModel giftModel = myGiftModelsList.get(lastClickedPosition);
                String giftPrice = giftModel.getPrice();
                editor.putString("giftPriceGuest",giftPrice );
                editor.commit();
                notifyDataSetChanged();
            }
        });



    }

    @Override
    public int getItemCount() {
        return myGiftModelsList == null ? 0 : myGiftModelsList.size();
    }

    public  class Gift extends RecyclerView.ViewHolder {

        TextView tvPackageName, tvStandardName, tvSubPackageName, tvPackagePrice;
        ImageView ivGirtImage;
        LinearLayout llChk, llUnchk;
        RadioGroup radioGroupChk, radioGroupUnChk;
        RadioButton radioButtonChk, radioButtonUnChk;
        public Gift(View itemView) {
            super(itemView);
            tvPackageName = itemView.findViewById(R.id.tv_package_name);
            tvSubPackageName = itemView.findViewById(R.id.tv_package_sub);
            tvStandardName = itemView.findViewById(R.id.tv_standard);
            ivGirtImage = itemView.findViewById(R.id.iv_image_package);
            radioGroupUnChk = itemView.findViewById(R.id.radio_group_unchk);
            radioButtonChk = itemView.findViewById(R.id.chk_mini_pac);
            radioButtonUnChk = itemView.findViewById(R.id.unchk_mini_pack);
            tvPackagePrice = itemView.findViewById(R.id.tv_package_price);
            llChk = itemView.findViewById(R.id.ll_chek);
            llUnchk = itemView.findViewById(R.id.ll_unchk);
        }
    }

    public void setLangRecreate(String langval,Gift holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        // Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvPackagePrice.setTypeface(fontEn);
            holder.tvSubPackageName.setTypeface(fontEn);
            holder.tvPackageName.setTypeface(fontEn);
            holder.tvStandardName.setTypeface(fontEn);

        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvPackagePrice.setTypeface(fontEn);
            holder.tvSubPackageName.setTypeface(fontEn);
            holder.tvPackageName.setTypeface(fontEn);
            holder.tvStandardName.setTypeface(fontEn);


        }
    }

}
