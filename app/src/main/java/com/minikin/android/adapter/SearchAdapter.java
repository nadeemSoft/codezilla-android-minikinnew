package com.minikin.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.activity.ProductDetailActivity;
import com.minikin.android.model.SearchModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchData> {
    Activity context;
    ArrayList<SearchModel> searchList = null;
    int rowLayout;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    String WhichLanguage = "";

    public SearchAdapter( ArrayList<SearchModel> searchList, int rowLayout, Activity context) {
        this.searchList = searchList;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
    }



    @Override
    public SearchData onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new SearchData(view);
    }

    @Override
    public void onBindViewHolder(SearchData holder, int position) {
        setLangRecreate(WhichLanguage, holder);
        final SearchModel searchModel = searchList.get(position);
        holder.tvProductName.setText(searchModel.getProductName());
        if(searchModel.getProductHasOffer().equalsIgnoreCase("true")){
            holder.tvOldPrice.setVisibility(View.VISIBLE);
            holder.frameLayout.setVisibility(View.VISIBLE);
            holder.tvOfferPerFlag.setVisibility(View.VISIBLE);

            holder.tvPrice.setText("KD" + " " +searchModel.getProductOfferPrice());
            holder.tvOldPrice.setText("KD" + " " +searchModel.getProductPrice());
            holder.tvOfferPerFlag.setText(searchModel.getProductFlag()+" "+"off");

        }else if(searchModel.getProductHasOffer().equalsIgnoreCase("false")){
            holder.tvPrice.setText("KD" + " " + searchModel.getProductPrice());
            holder.tvOldPrice.setVisibility(View.GONE);
            holder.frameLayout.setVisibility(View.GONE);
            holder.tvOfferPerFlag.setVisibility(View.GONE);


        }

       // Picasso.with(context).load(searchModel.getProductImage()).into(holder.ivProductImage);


       /* Picasso.with(context)
                .load(searchModel.getProductImage())
                .resize(110, 110)
                .centerCrop()
                .onlyScaleDown()
                .placeholder(R.drawable.grey_img_holder)
                .into(holder.ivProductImage);*/


        Picasso.with(context)
                .load(searchModel.getProductImage())
                .fit()
                .into(holder.ivProductImage);



        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra("product_id", searchModel.getProductId());
                Log.e("product_idIN ADA", searchModel.getProductId());
                context.startActivity(intent);

            }
        });



    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }



    public void setLangRecreate(String langval,SearchData holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
       // Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvProductName.setTypeface(fontEnMedium);
            holder.tvPrice.setTypeface(fontAr);
            holder.tvAddCard.setTypeface(fontAr);
            holder.tvOldPrice.setTypeface(fontAr);
            holder.tvOfferPerFlag.setTypeface(fontAr);



        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvProductName.setTypeface(fontEnMedium);
            holder.tvPrice.setTypeface(fontEn);
            holder.tvAddCard.setTypeface(fontEn);
            holder.tvOldPrice.setTypeface(fontEn);
            holder.tvOfferPerFlag.setTypeface(fontEn);

        }
    }


    public class SearchData extends RecyclerView.ViewHolder {
        TextView tvProductName, tvAddCard, tvPrice,tvOldPrice,tvOfferPerFlag;
        ImageView ivProductImage;
        LinearLayout llParent;
        FrameLayout frameLayout;

        public SearchData(View itemView) {
            super(itemView);

            tvProductName = itemView.findViewById(R.id.tv_product_name);
            ivProductImage = itemView.findViewById(R.id.iv_product);
            tvAddCard = itemView.findViewById(R.id.tv_add_to_cart);
            tvPrice = itemView.findViewById(R.id.tv_price);
            llParent = itemView.findViewById(R.id.ll_parent_rowItem);
            tvOldPrice = itemView.findViewById(R.id.tv_price_old);
            frameLayout = itemView.findViewById(R.id.fram_old_price);
            tvOfferPerFlag = itemView.findViewById(R.id.tv_flag_offer_percent);

        }
    }



}
