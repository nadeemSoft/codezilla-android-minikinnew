package com.minikin.android.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.minikin.android.R;
import com.minikin.android.model.Advertise;
import com.minikin.android.util.AppConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class ViewPagerAdapter extends PagerAdapter {
    String loginValue;
    SharedPreferences sp = null;
    private SharedPreferences pref;
    private String WhichLanguage = "";

    private Context context;
    private LayoutInflater layoutInflater;
    ArrayList<Advertise> images;

    //  private Integer [] images = {R.drawable.splashh,R.drawable.splashh,R.drawable.splashh,R.drawable.splashh};
    public ViewPagerAdapter(Context context, ArrayList<Advertise> images) {
        this.context = context;
        this.images = images;


    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        Advertise advertise = images.get(position);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        loginValue = sp.getString("LoginValue", "");
        Advertise adObje = images.get(position);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.adapter_viewpager, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
       // Log.e("imageurl",AppConstants.IMAGE_BASE_LIVE +adObje.getImage()+"");

        Picasso.with(context).load(advertise.getImage()).into(imageView);
        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }


}