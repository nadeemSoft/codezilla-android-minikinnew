package com.minikin.android.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.activity.ProductDetailActivity;
import com.minikin.android.model.FavouriteModel;
import com.minikin.android.util.AppConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class AdapterMyFavroit extends RecyclerView.Adapter<AdapterMyFavroit.Favroite> {
    Context context;
    ArrayList<FavouriteModel> favouriteModelList;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    SharedPreferences sp;
    String WhichLanguage = "";
    ProgressDialog pd;


    public AdapterMyFavroit(Context context, ArrayList<FavouriteModel> favouriteModelList) {
        this.context = context;
        this.favouriteModelList = favouriteModelList;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editor = sp.edit();


    }


    @Override
    public Favroite onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_myfavroit, parent, false);
        return new Favroite(view);
    }

    @Override
    public void onBindViewHolder(Favroite holder, final int position) {
        setLangRecreate(WhichLanguage, holder);

        final FavouriteModel favouriteModelModel = favouriteModelList.get(position);
        holder.tvItemName.setText(""+favouriteModelModel.getFavName());
        holder.tvStoreName.setText(""+favouriteModelModel.getFavStore());

        if (WhichLanguage.equalsIgnoreCase("en")){

            holder.tvPrice.setText("KD"+" "+ favouriteModelModel.getFavPrice());

        }else if (WhichLanguage.equalsIgnoreCase("ar")){
            holder.tvPrice.setText("KD"+" "+ favouriteModelModel.getFavPrice());

           // holder.tvPrice.setText("د.ك"+" "+ favouriteModelModel.getFavPrice());


        }

        Picasso.with(context).load(favouriteModelModel.getFavImage()).fit().error(R.drawable.grey_img_holder).placeholder(R.drawable.grey_img_holder).into(holder.ivImage);

       holder.llProductNamestoreName.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {

               Intent intent = new Intent(context, ProductDetailActivity.class);
               intent.putExtra("product_id", favouriteModelModel.getProductId());
               Log.e("product_id in ADA", favouriteModelModel.getProductId());
               context.startActivity(intent);

           }
       });

       holder.ivImage.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(context, ProductDetailActivity.class);
               intent.putExtra("product_id", favouriteModelModel.getProductId());
               Log.e("product_id in ADA", favouriteModelModel.getProductId());
               context.startActivity(intent);

           }
       });

        holder.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(context);
                dialog.setCancelable(true);
                dialog.setContentView(R.layout.dialog_layout);

                TextView tvTitleDialog = dialog.findViewById(R.id.dialog_titile);
                TextView tvMessage = dialog.findViewById(R.id.dialog_message);
                Button yes = dialog.findViewById(R.id.dialog_positive_btn);
                Button no = dialog.findViewById(R.id.dialog_negative_btn);



                yes.setText("Remove");
                tvTitleDialog.setText("Minikin");
                tvMessage.setText("Are You Sure? You want to remove Item from your Favourite list..");

                Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
                Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");


                if (WhichLanguage.equalsIgnoreCase("ar")) {
                    //((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

                    yes.setText("إزالة");
                    tvTitleDialog.setText("مينيكن");
                    tvMessage.setText("هل أنت واثق؟ تريد إزالة عنصر من قائمة المفضلة لديك...");

                    tvTitleDialog.setTypeface(fontEn);
                    tvMessage.setTypeface(fontEn);
                    yes.setTypeface(fontEn);
                    no.setTypeface(fontEn);


                } else if (WhichLanguage.equalsIgnoreCase("en")) {
                    //((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                    tvTitleDialog.setTypeface(fontEn);
                    tvMessage.setTypeface(fontEn);
                    yes.setTypeface(fontEn);
                    no.setTypeface(fontEn);


                }


                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String prdId = favouriteModelList.get(position).getProductId();
                        removeFavroit(prdId, position);
                        dialog.dismiss();


                    }
                });

                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });

                dialog.show();


            }
        });
    }

    @Override
    public int getItemCount() {
        return favouriteModelList.size();
    }

    public class Favroite extends RecyclerView.ViewHolder {
        TextView tvItemName, tvStoreName, tvPrice;
        ImageView ivImage;
        LinearLayout llDelete,llProductNamestoreName;

        public Favroite(View itemView) {
            super(itemView);
            tvItemName = itemView.findViewById(R.id.tv_fav_item_name);
            tvStoreName = itemView.findViewById(R.id.tv_fav_item_store_name);
            tvPrice = itemView.findViewById(R.id.tv_fav_item_price);
            ivImage = itemView.findViewById(R.id.iv_fav_item);
            llDelete = itemView.findViewById(R.id.iv_delete_fav_item);
            llProductNamestoreName = itemView.findViewById(R.id.ll_product_name_store_name);

        }
    }


    public void setLangRecreate(String langval, Favroite holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
       // Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvItemName.setTypeface(fontAr);
            holder.tvStoreName.setTypeface(fontAr);
            holder.tvPrice.setTypeface(fontAr);


        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvItemName.setTypeface(fontEnMedium);
            holder.tvStoreName.setTypeface(fontEn);
            holder.tvPrice.setTypeface(fontEn);


        }
    }

    public void removeFavroit(final String productId, final int pos) {
        pd.show();
        StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_REMOVE_FAVROIT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("REMOVEFavroitResponse", response + "");

                try {
                    JSONObject objects = new JSONObject(response);
                    String status = objects.getString("status");
                    String message = objects.getString("message");
                    if (status.equalsIgnoreCase("true")) {
                        pd.dismiss();
                        favouriteModelList.remove(pos);
                        notifyDataSetChanged();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                    } else {
                        pd.dismiss();
                        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                    }

                    pd.dismiss();


                } catch (Exception e) {
                    pd.dismiss();
                    Toast.makeText(context, "Exception Two" + e, Toast.LENGTH_SHORT).show();


                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                pd.dismiss();
                Log.e("error", error + "");


            }
        })

        {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                Log.e("header", params + "");

                return params;
            }

            @Override
            public Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("lang", sp.getString("WhichLanguage", ""));
                params.put("product_id", productId);
                params.put("customer_id", sp.getString("customerId", ""));
                Log.e("removeFavParams", params + "");
                return params;
            }


        };


        int socketTimeout = 300000;//30 seconds - change to what you want 30000
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);
    }

}
