package com.minikin.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.util.AppConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AdapterProductShowMyOrderOrderDetail extends RecyclerView.Adapter<AdapterProductShowMyOrderOrderDetail.Show> {

    private Context context;
    JSONArray jsonArray;
    SharedPreferences pref;
    String WhichLanguage = "";
    private SharedPreferences sp = null;


    public AdapterProductShowMyOrderOrderDetail(JSONArray jsonArray, Context context) {
        this.jsonArray = jsonArray;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);

    }

    @Override
    public Show onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_detail_product_my_oreder, parent, false);
        return new Show(view);
    }

    @Override
    public void onBindViewHolder(Show holder, int position) {
        setLangRecreate(WhichLanguage, holder);
        try {

            Log.e("Length", jsonArray.length() + "");
            JSONObject jsonObjectProduct = jsonArray.getJSONObject(position);
            String product_id = jsonObjectProduct.getString("product_id");
            String name = jsonObjectProduct.getString("name");
            String main_image = jsonObjectProduct.getString("main_image");
            String price = jsonObjectProduct.getString("price");
            String quantity = jsonObjectProduct.getString("quantity");
            String cart_amount = jsonObjectProduct.getString("cart_amount");
            String store = jsonObjectProduct.getString("store");


            holder.tvProductName.setText(name);
            holder.tvStoreName.setText(store);

            if (WhichLanguage.equalsIgnoreCase("ar")) {
                holder.tvProductPrieLatest.setText("KD" + "  " + cart_amount + " " + " (" + quantity + ")");

                // holder.tvProductPrieLatest.setText("د.ك" + "  " + cart_amount + " " + " (" + quantity + ")");


            } else if (WhichLanguage.equalsIgnoreCase("en")) {
                holder.tvProductPrieLatest.setText("KD" + "  " + cart_amount + " " + " (" + quantity + ")");


            }


            Picasso.with(context).load(main_image).fit().placeholder(R.drawable.grey_img_holder)
                    .error(R.drawable.grey_img_holder).into(holder.ivOrder);


            JSONArray jsonArrayAttributes = jsonObjectProduct.getJSONArray("attributes");
            Log.e("SETAttributeNull", jsonArrayAttributes.isNull(position) + "");


            Log.e("AttributeLength", jsonArrayAttributes.length() + "");
            Log.e("AttributeZEROINDEX", jsonArrayAttributes.get(0).toString().length() + "");


            if (jsonArrayAttributes.length() != 0) {
                holder.rvMyOrderDetailAttribute.setVisibility(View.VISIBLE);

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                holder.rvMyOrderDetailAttribute.setLayoutManager(mLayoutManager);
                holder.rvMyOrderDetailAttribute.setAdapter(new MyOrderDetailAttributesAdapter(jsonArrayAttributes, context));
                holder.rvMyOrderDetailAttribute.setNestedScrollingEnabled(false);
            } else {
                holder.rvMyOrderDetailAttribute.setVisibility(View.GONE);
            }


        } catch (JSONException e) {

            e.printStackTrace();

        }


    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }


    public void setLangRecreate(String langval, Show holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");

        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvProductName.setTypeface(fontAr);
            // holder.tvOfferTitle.setTypeface(fontAr);
            // holder.tvSize.setTypeface(fontAr);
            // holder.tvSizeTitle.setTypeface(fontAr);
            // holder.tvColor.setTypeface(fontAr);
            // holder.tvColorTitle.setTypeface(fontAr);
            //  holder.tvOffer.setTypeface(fontAr);
            holder.tvProductPriceOld.setTypeface(fontAr);
            holder.tvProductPrieLatest.setTypeface(fontAr);


        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvProductName.setTypeface(fontEnMedium);
            // holder.tvOfferTitle.setTypeface(fontEn);
            // holder.tvSize.setTypeface(fontEn);
            // holder.tvSizeTitle.setTypeface(fontEn);
            // holder.tvColor.setTypeface(fontEn);
            // holder.tvColorTitle.setTypeface(fontEn);
            // holder.tvOffer.setTypeface(fontEn);
            holder.tvProductPriceOld.setTypeface(fontEnMedium);
            holder.tvProductPrieLatest.setTypeface(fontEnMedium);
        }
    }


    public class Show extends RecyclerView.ViewHolder {
        TextView tvProductName, tvStoreName, tvProductPriceOld, tvProductPrieLatest;
        ImageView ivOrder;
        RecyclerView rvMyOrderDetailAttribute;

        public Show(View itemView) {
            super(itemView);
            ivOrder = itemView.findViewById(R.id.iv_product_show);

            tvProductName = itemView.findViewById(R.id.tv_my_order_product_name);
            tvStoreName = itemView.findViewById(R.id.tv_my_order_store_name);
            // tvColorTitle = itemView.findViewById(R.id.tv_my_order_color_title);
            // tvColor = itemView.findViewById(R.id.tv_my_order_colore_set);
            //  tvSizeTitle = itemView.findViewById(R.id.tv_my_order_size_title);
            // tvSize = itemView.findViewById(R.id.tv_my_order_size_set);
            // tvOfferTitle = itemView.findViewById(R.id.tv_my_order_offer_title);
            //  tvOffer = itemView.findViewById(R.id.tv_my_order_offer_set);
            tvProductPriceOld = itemView.findViewById(R.id.tv_my_order_amount_old);
            tvProductPrieLatest = itemView.findViewById(R.id.tv_my_order_amount_latest);
            rvMyOrderDetailAttribute = itemView.findViewById(R.id.rv_my_order_detail_attributes);
        }
    }
}
