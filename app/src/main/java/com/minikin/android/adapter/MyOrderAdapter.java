package com.minikin.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.minikin.android.R;
import com.minikin.android.model.MyOrderModel;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.order> {
    private int rowLayout;
    private Context context;
    ArrayList<MyOrderModel> myOrderModelList;
    int click = 0;
    SharedPreferences pref;
    String WhichLanguage = "";
    ProgressDialog pd;
    private SharedPreferences sp = null;
    DecimalFormat precision = null;


    public MyOrderAdapter(ArrayList<MyOrderModel> myOrderModelList, int rowLayout, Context context) {
        this.myOrderModelList = myOrderModelList;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);

        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        precision = new DecimalFormat("0.000");

    }

    @Override
    public order onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new order(view);

    }

    @Override
    public void onBindViewHolder(final order holder, final int position) {
        setLangRecreate(WhichLanguage, holder);
        MyOrderModel myOrderModel = myOrderModelList.get(position);

        holder.tvOrderId.setText(myOrderModel.getOrderID());

        if (WhichLanguage.equalsIgnoreCase("ar")){
            holder.tvOrderPlacedDay.setText("تم الطلب بتاريخ " + myOrderModel.getOrderDate());
        }else if (WhichLanguage.equalsIgnoreCase("en")){
            holder.tvOrderPlacedDay.setText("Placed on " + myOrderModel.getOrderDate());
        }

        holder.tvStatusOrder.setText(myOrderModel.getOrderStatus());


        if (myOrderModelList.get(position).isOpen()) {

            holder.llShowHistory.setVisibility(View.VISIBLE);

            setData(myOrderModelList.get(position).getResultObject(), holder);
        } else {

            holder.llShowHistory.setVisibility(View.GONE);
        }

        holder.llShowOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myOrderModelList.get(position).getSuccess() == 0) {

                    getOrderDetail(myOrderModelList.get(position).getOrderID());

                } else if (myOrderModelList.get(position).getSuccess() == 1) {

                    if (myOrderModelList.get(position).isOpen()) {

                        myOrderModelList.get(position).setOpen(false);
                    } else {

                        myOrderModelList.get(position).setOpen(true);
                    }

                    notifyDataSetChanged();

                } else if (myOrderModelList.get(position).getSuccess() == 2) {

                    getOrderDetail(myOrderModelList.get(position).getOrderID());
                }


            }


            private void getOrderDetail(final String orderId) {
                pd.show();

                StringRequest request = new StringRequest(Request.Method.POST, AppConstants.GET_ORDER_DETAIL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseOrderDetail", response);

                        try {
                            JSONObject objects = new JSONObject(response);
                            String status = objects.getString("status");
                            String message = objects.getString("message");
                            if (status.equalsIgnoreCase("true")) {
                                JSONObject resultJsonObj = objects.getJSONObject("result");

                                myOrderModelList.get(position).setOpen(true);
                                myOrderModelList.get(position).setSuccess(1);
                                myOrderModelList.get(position).setResultObject(resultJsonObj.toString());
                                notifyDataSetChanged();

                                pd.dismiss();


                            } else {

                                myOrderModelList.get(position).setSuccess(2);
                                notifyDataSetChanged();

                                pd.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                            }


                        } catch (Exception e) {

                            myOrderModelList.get(position).setSuccess(2);
                            notifyDataSetChanged();

                            pd.dismiss();
                            Toast.makeText(context, "Exception" + e, Toast.LENGTH_SHORT).show();


                        }


                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VolleyError", error + "");
                        pd.dismiss();


                    }
                }) {

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("Content-Type", "application/x-www-form-urlencoded");

                        return params;
                    }

                    @Override
                    public Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("lang", sp.getString("WhichLanguage", ""));
                        params.put("order_id", orderId);
                        Log.e("OrderDetailParams", params + "");
                        return params;
                    }
                };

                int socketTimeout = 300000;//30 seconds - change to what you want 30000
                RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
                request.setRetryPolicy(policy);

                RequestQueue requestQueue = Volley.newRequestQueue(context);
                requestQueue.add(request);
            }


        });

    }

    private void setData(String data, final order holder) {

        try {

            JSONObject resultJsonObj = new JSONObject(data);
            String coupDiscount = resultJsonObj.getString("coupon_discount");
            String deliveryAddress = resultJsonObj.getString("delivery_charge");
            String subTotal = resultJsonObj.getString("sub_total");
            String total_amount = resultJsonObj.getString("total_amount");
            String crdtype = resultJsonObj.getString("crdtype");
            String as_gift = resultJsonObj.getString("as_gift");
            String card_from = resultJsonObj.getString("card_from");
            String card_to = resultJsonObj.getString("card_to");


            JSONObject addressJsonObject = resultJsonObj.getJSONObject("address");
            String address_id = addressJsonObject.getString("address_id");
            String title = addressJsonObject.getString("title");
            String area_id = addressJsonObject.getString("area_id");
            String area_name = addressJsonObject.getString("area_name");
            String block = addressJsonObject.getString("block");
            String street = addressJsonObject.getString("street");
            String building = addressJsonObject.getString("building");
            String floor = addressJsonObject.getString("floor");
            String apartment = addressJsonObject.getString("apartment");


            JSONArray jsonArrayProduct = resultJsonObj.getJSONArray("products");
            Log.e("PRODUCTArrayDetail", jsonArrayProduct.getJSONObject(0) + "");

            JSONObject objectPackage = resultJsonObj.getJSONObject("package");
            String package_id = objectPackage.getString("package_id");
            String price = objectPackage.getString("price");
            String name = objectPackage.getString("name");
            String brief = objectPackage.getString("brief");
            String image = objectPackage.getString("image");


            holder.tvPaymentType.setText(""+crdtype);

            Log.e("as_giftOrderDetail", as_gift);

            if (WhichLanguage.equalsIgnoreCase("ar")) {

                holder.tvPaymentType.setText("كي-نت");


                if (as_gift.equalsIgnoreCase("0")) {
                    holder.tvGiftCardFromName.setText("NA");
                    holder.tvGiftCardToName.setText("NA");
                } else if (as_gift.equalsIgnoreCase("1")) {
                    holder.tvGiftCardFromName.setText(card_from);
                    holder.tvGiftCardToName.setText(card_to);
                }

            } else if (WhichLanguage.equalsIgnoreCase("en")) {
                holder.tvPaymentType.setText("K-NET");

                if (as_gift.equalsIgnoreCase("0")) {
                    holder.tvGiftCardFromName.setText("NA");
                    holder.tvGiftCardToName.setText("NA");
                } else if (as_gift.equalsIgnoreCase("1")) {
                    holder.tvGiftCardFromName.setText(card_from);
                    holder.tvGiftCardToName.setText(card_to);
                }

            }


            // holder.tvAddressTitleName.setText(title.trim());

            if (WhichLanguage.equalsIgnoreCase("ar")) {

                holder.tvAddressSet.setText(title + "- " + area_name + "," + "القطعه  " + block + ", " + street + " الشارع., " + "رقم المنزل. " + building + ", " + "الطابق  " + floor);

            } else if (WhichLanguage.equalsIgnoreCase("en")) {
                holder.tvAddressSet.setText(title + "- " + area_name + "," + "Block " + block + ", " + street + " St., " + "building no. " + building + ", " + "Floor " + floor);

            }


            holder.tvGift.setText(name);



            if (WhichLanguage.equalsIgnoreCase("en")){

                double afGiftD = Double.parseDouble(price);
                double coupDiscountD = Double.parseDouble(coupDiscount);
                double deliveryAddressD = Double.parseDouble(deliveryAddress);
                double subTotalD = Double.parseDouble(subTotal);
                double totalAmountD = Double.parseDouble(total_amount);


                holder.tvSubTotalAmount.setText("" + precision.format(subTotalD));
                holder.tvDelivery.setText("" + precision.format(deliveryAddressD));
                holder.tvPacakaging.setText("" + precision.format(afGiftD));
                holder.tvOrderCoupon.setText("" + precision.format(coupDiscountD));

                holder.tvTotal.setText("" + precision.format(totalAmountD));


            }else if (WhichLanguage.equalsIgnoreCase("ar")){


                Configuration config = context.getResources().getConfiguration();
                Locale locale = new Locale("en");
                Locale.setDefault(locale);
                config.locale = locale;
                context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());

                precision = new DecimalFormat("0.000");


                double afGiftD = Double.parseDouble(price);
                double coupDiscountD = Double.parseDouble(coupDiscount);
                double deliveryAddressD = Double.parseDouble(deliveryAddress);
                double subTotalD = Double.parseDouble(subTotal);
                double totalAmountD = Double.parseDouble(total_amount);


                holder.tvSubTotalAmount.setText("" + precision.format(subTotalD));
                holder.tvDelivery.setText("" + precision.format(deliveryAddressD));
                holder.tvPacakaging.setText("" + precision.format(afGiftD));
                holder.tvOrderCoupon.setText("" + precision.format(coupDiscountD));

                holder.tvTotal.setText("" + precision.format(totalAmountD));



                Configuration config1 = context.getResources().getConfiguration();
                Locale locale1 = new Locale("ar");
                Locale.setDefault(locale1);
                config1.locale = locale1;
                context.getResources().updateConfiguration(config1, context.getResources().getDisplayMetrics());


            }




            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            holder.rvShowProduct.setLayoutManager(mLayoutManager);
            holder.rvShowProduct.setAdapter(new AdapterProductShowMyOrderOrderDetail(jsonArrayProduct, context));
            holder.rvShowProduct.setNestedScrollingEnabled(false);


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return myOrderModelList.size();
    }


    public void setLangRecreate(String langval, order holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        // Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);


            holder.tvGiftTitle.setText(" هدية");
            holder.tvGiftCardTitle.setText("بطاقه التهادي ");
            holder.tvAddressTitle.setText("العنوان");
            holder.tvPaymentTitle.setText("الدفع");
            holder.tvSubtotalTitle.setText(" حاصل الجمع");
            holder.tvDeliveryTitle.setText("تكلفه التوصيل");
            holder.tvPackagingTitle.setText(" تكلفه التغليف ");
            holder.tvOrderCouponTitle.setText("كود الخصم");
            holder.tvTotaltilte.setText("الاجمالي");

            /*holder.tvKdSubTotalTitle.setText("د.ك ");
            holder.tvConfirmDeliveryKdTitle.setText("د.ك ");
            holder.tvConfirmPackagingKdTitle.setText("د.ك ");
            holder.tvConfirmCouponDiscountKdTitle.setText("د.ك ");
            holder.tvConfirmGrandTotalKdTitle.setText("د.ك ");*/

            holder.tvGiftCardFrom.setText("من ");
            holder.tvGiftCardTo.setText("إلى ");
            holder.tvOrderIdTitle.setText("تفاصيل الطلب");
            holder.tvPaymentType.setText("كي-نت");



            holder.tvGiftTitle.setTypeface(fontEn);
            holder.tvGift.setTypeface(fontEn);
            holder.tvGiftCardTitle.setTypeface(fontEn);
            holder.tvGiftCardFrom.setTypeface(fontEn);
            holder.tvGiftCardFromName.setTypeface(fontEnMedium);
            holder.tvGiftCardTo.setTypeface(fontEn);
            holder.tvGiftCardToName.setTypeface(fontEnMedium);
            holder.tvAddressTitle.setTypeface(fontEn);
            holder.tvAddressTitleName.setTypeface(fontEn);
            holder.tvAddressSet.setTypeface(fontEn);
            holder.tvPaymentTitle.setTypeface(fontEn);
            holder.tvPaymentType.setTypeface(fontEn);
            holder.tvSubtotalTitle.setTypeface(fontEn);
            holder.tvSubTotalAmount.setTypeface(fontEn);
            holder.tvDeliveryTitle.setTypeface(fontEn);
            holder.tvDelivery.setTypeface(fontEn);
            holder.tvPackagingTitle.setTypeface(fontEn);
            holder.tvPacakaging.setTypeface(fontEn);
            //holder.tvTotaltilte.setTypeface(fontEn);
            // holder.tvTotal.setTypeface(fontEn);
            holder.tvKdSubTotalTitle.setTypeface(fontEn);


            holder.tvTotaltilte.setTypeface(fontEnMedium);
            holder.tvTotal.setTypeface(fontEnMedium);


            holder.tvOrderIdTitle.setTypeface(fontEn);
            holder.tvOrderId.setTypeface(fontEn);
            holder.tvOrderPlacedDay.setTypeface(fontEn);
            holder.tvOrderCouponTitle.setTypeface(fontEn);
            holder.tvOrderCoupon.setTypeface(fontEn);

            holder.tvConfirmSubTotalKdTitle.setTypeface(fontEn);
            holder.tvConfirmDeliveryKdTitle.setTypeface(fontEn);
            holder.tvConfirmPackagingKdTitle.setTypeface(fontEn);
            holder.tvConfirmCouponDiscountKdTitle.setTypeface(fontEn);
            holder.tvConfirmGrandTotalKdTitle.setTypeface(fontEn);
            holder.tvStatusOrder.setTypeface(fontEnMedium);



        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            holder.tvGiftTitle.setTypeface(fontEn);
            holder.tvGift.setTypeface(fontEn);
            holder.tvGiftCardTitle.setTypeface(fontEn);
            holder.tvGiftCardFrom.setTypeface(fontEn);
            holder.tvGiftCardFromName.setTypeface(fontEnMedium);
            holder.tvGiftCardTo.setTypeface(fontEn);
            holder.tvGiftCardToName.setTypeface(fontEnMedium);
            holder.tvAddressTitle.setTypeface(fontEn);
            holder.tvAddressTitleName.setTypeface(fontEn);
            holder.tvAddressSet.setTypeface(fontEn);
            holder.tvPaymentTitle.setTypeface(fontEn);
            holder.tvPaymentType.setTypeface(fontEn);
            holder.tvSubtotalTitle.setTypeface(fontEn);
            holder.tvSubTotalAmount.setTypeface(fontEn);
            holder.tvDeliveryTitle.setTypeface(fontEn);
            holder.tvDelivery.setTypeface(fontEn);
            holder.tvPackagingTitle.setTypeface(fontEn);
            holder.tvPacakaging.setTypeface(fontEn);
            //holder.tvTotaltilte.setTypeface(fontEn);
            // holder.tvTotal.setTypeface(fontEn);
            holder.tvKdSubTotalTitle.setTypeface(fontEn);


            holder.tvTotaltilte.setTypeface(fontEnMedium);
            holder.tvTotal.setTypeface(fontEnMedium);


            holder.tvOrderIdTitle.setTypeface(fontEn);
            holder.tvOrderId.setTypeface(fontEn);
            holder.tvOrderPlacedDay.setTypeface(fontEn);
            holder.tvOrderCouponTitle.setTypeface(fontEn);
            holder.tvOrderCoupon.setTypeface(fontEn);

            holder.tvConfirmSubTotalKdTitle.setTypeface(fontEn);
            holder.tvConfirmDeliveryKdTitle.setTypeface(fontEn);
            holder.tvConfirmPackagingKdTitle.setTypeface(fontEn);
            holder.tvConfirmCouponDiscountKdTitle.setTypeface(fontEn);
            holder.tvConfirmGrandTotalKdTitle.setTypeface(fontEn);
            holder.tvStatusOrder.setTypeface(fontEnMedium);


        }
    }

    public class order extends RecyclerView.ViewHolder {
        LinearLayout llShowHistory, llShowOrder;
        TextView tvOrderIdTitle, tvOrderId, tvOrderPlacedDay, tvOrderPlacedDate, tvStatusOrder;
        RecyclerView rvShowProduct;

        private TextView tvConfirmSubTotalKdTitle, tvConfirmDeliveryKdTitle, tvConfirmPackagingKdTitle, tvConfirmCouponDiscountKdTitle, tvConfirmGrandTotalKdTitle;

        TextView tvGiftTitle, tvGift, tvGiftCardTitle, tvGiftCardFrom, tvGiftCardFromName, tvGiftCardTo, tvGiftCardToName, tvAddressTitle, tvAddressTitleName, tvAddressSet;
        TextView tvPaymentTitle, tvPaymentType, tvSubtotalTitle, tvSubTotalAmount, tvDeliveryTitle, tvDelivery, tvPackagingTitle, tvPacakaging, tvTotaltilte, tvTotal, tvOrderCouponTitle, tvOrderCoupon;
        TextView tvKdSubTotalTitle;

        public order(View itemView) {
            super(itemView);

            tvKdSubTotalTitle = itemView.findViewById(R.id.tv_kd_title_subTotal);

            llShowOrder = itemView.findViewById(R.id.ll_show_order);
            rvShowProduct = itemView.findViewById(R.id.rv_show_prod);

            tvOrderIdTitle = itemView.findViewById(R.id.tv_order_id_title_recent);
            tvOrderId = itemView.findViewById(R.id.tv_order_id_recent);
            tvOrderPlacedDay = itemView.findViewById(R.id.tv_order_placed_day_recent);
            tvOrderPlacedDate = itemView.findViewById(R.id.tv_order_placed_date_recent);
            tvStatusOrder = itemView.findViewById(R.id.tv_order_status_);


            tvGiftTitle = itemView.findViewById(R.id.tv_my_order_gift_title);
            tvGift = itemView.findViewById(R.id.tv_my_order_gift);
            tvGiftCardTitle = itemView.findViewById(R.id.tv_my_order_gift_card_title);
            tvGiftCardFrom = itemView.findViewById(R.id.tv_my_order_gift_card_from);
            tvGiftCardFromName = itemView.findViewById(R.id.tv_my_order_gift_card_from_set);
            tvGiftCardTo = itemView.findViewById(R.id.tv_my_order_gift_card_to);
            tvGiftCardToName = itemView.findViewById(R.id.tv_my_order_gift_card_to_set);
            tvAddressTitle = itemView.findViewById(R.id.tv_my_order_address_title);
            tvAddressTitleName = itemView.findViewById(R.id.tv_my_order_address_title_set);
            tvAddressSet = itemView.findViewById(R.id.tv_my_order_address_set);

            tvPaymentTitle = itemView.findViewById(R.id.tv_my_order_payment_title);
            tvPaymentType = itemView.findViewById(R.id.tv_my_order_payment_type);
            tvSubtotalTitle = itemView.findViewById(R.id.tv_my_order_SubTotal_title);
            tvSubTotalAmount = itemView.findViewById(R.id.tv_my_order_SubTotal_amount);
            tvDelivery = itemView.findViewById(R.id.tv_my_order_delivery_amount);
            tvDeliveryTitle = itemView.findViewById(R.id.tv_my_order_delivery_title);

            tvPackagingTitle = itemView.findViewById(R.id.tv_my_order_packaging_service_title);
            tvPacakaging = itemView.findViewById(R.id.tv_my_order_packaging_service_amount);
            tvOrderCouponTitle = itemView.findViewById(R.id.tv_Order_detail_coupon);
            tvOrderCoupon = itemView.findViewById(R.id.tv_order_coupon_discount);
            tvTotaltilte = itemView.findViewById(R.id.tv_my_order_total_title);
            tvTotal = itemView.findViewById(R.id.tv_my_order_totalMain_amount);


            tvConfirmSubTotalKdTitle = itemView.findViewById(R.id.tv_kd_title_subTotal);
            tvConfirmDeliveryKdTitle = itemView.findViewById(R.id.tv_sub_delivery_kd);
            tvConfirmPackagingKdTitle = itemView.findViewById(R.id.tv_sub_wrap_gift_kd);
            tvConfirmCouponDiscountKdTitle = itemView.findViewById(R.id.tv_sub_coupon_discount_kd);
            tvConfirmGrandTotalKdTitle = itemView.findViewById(R.id.tv_sub_total_Main_kd);

            llShowHistory = itemView.findViewById(R.id.ll_show_history);
            llShowHistory.setVisibility(View.GONE);


        }
    }
}
