package com.minikin.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.activity.ProductShowActivity;
import com.minikin.android.model.CategoryModelNew;
import com.minikin.android.util.AppConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AdapterCategoryNew extends RecyclerView.Adapter<AdapterCategoryNew.CategoryNew> {
    int selectPos = 0;
    ArrayList<CategoryModelNew> categoriesList;
    Activity context;
    ProgressDialog pd;
    SharedPreferences sp = null;
    private SharedPreferences.Editor editor;
    SharedPreferences pref;
    String WhichLanguage = "";
    AdapterHome3SubCategory adapterHome3SubCategory;

    public AdapterCategoryNew(Activity context, ArrayList<CategoryModelNew> categoriesList) {
        this.context = context;
        this.categoriesList = categoriesList;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editor = sp.edit();

    }

    @Override
    public CategoryNew onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_home3, parent, false);
        return new CategoryNew(view);
    }

    @Override
    public void onBindViewHolder(final CategoryNew holder, final int position) {
        setLangRecreate(WhichLanguage, holder);
        final CategoryModelNew categoryModel = categoriesList.get(position);
        //holder.tvName.setText(categoryModel.getCategoryName());
        Log.e("CategoryImage..", categoryModel.getCategoryImage() + "");

        Picasso.with(context).load(categoryModel.getCategoryImage()).placeholder(R.drawable.appicon).error(R.drawable.appicon).into(holder.ivCate);


        holder.llHome3Parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CategoryModelNew modelCate = categoriesList.get(position);

                Intent intentHome2 = new Intent(context, ProductShowActivity.class);
                intentHome2.putExtra("id", modelCate.getCategoryId());
                intentHome2.putExtra("titleName", modelCate.getCategoryName());
                intentHome2.putExtra("showDate", "category");
                context.startActivity(intentHome2);
              //  holder.rvSubCategoryHome3.setVisibility(View.GONE);


            }
        });
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    public void setLangRecreate(String langval, CategoryNew holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        //    holder.tvName.setTypeface(fontAr);


        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
         //   holder.tvName.setTypeface(fontEn);


        }
    }

    public class CategoryNew extends RecyclerView.ViewHolder {
       // TextView tvName;
        ImageView ivCate;
        LinearLayout llHome3Parent;
       // RecyclerView rvSubCategoryHome3;

        public CategoryNew(View itemView) {
            super(itemView);
            //tvName = itemView.findViewById(R.id.tv_category_name_home3);
            ivCate = itemView.findViewById(R.id.iv_category_home_3);
            llHome3Parent = itemView.findViewById(R.id.ll_home3_parent);
           // rvSubCategoryHome3 = itemView.findViewById(R.id.rv_sub_cate_adap_home3);
        }
    }
}
