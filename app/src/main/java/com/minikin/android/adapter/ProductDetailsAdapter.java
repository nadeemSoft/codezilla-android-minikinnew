package com.minikin.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.model.ReletedItemModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class ProductDetailsAdapter extends RecyclerView.Adapter<ProductDetailsAdapter.Detail> {
    Context context;
    int rowLayout;
    ArrayList<ReletedItemModel> productDetailList;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    String WhichLanguage = "";


    public ProductDetailsAdapter(ArrayList<ReletedItemModel> productDetailList, int rowLayout, Context context) {
        this.productDetailList = productDetailList;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");

    }

    @Override
    public Detail onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new Detail(view);
    }

    @Override
    public void onBindViewHolder(Detail holder, int position) {
        setLangRecreate(WhichLanguage, holder);

        ReletedItemModel model = productDetailList.get(position);
        Picasso.with(context).load(model.getProductImage()).into(holder.ivProductReleted);
        holder.tvName.setText(model.getProductName());

    }

    @Override
    public int getItemCount() {
        return productDetailList.size();
    }

    public class Detail extends RecyclerView.ViewHolder {
        ImageView ivProductReleted;
        TextView tvName;

        public Detail(View itemView) {
            super(itemView);
            ivProductReleted = itemView.findViewById(R.id.iv_adapter_pro_image);
            tvName = itemView.findViewById(R.id.tv_adapter_name);
        }
    }

    public void setLangRecreate(String langval, Detail holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");

        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvName.setTypeface(fontAr);


        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvName.setTypeface(fontEn);


        }
    }
}
