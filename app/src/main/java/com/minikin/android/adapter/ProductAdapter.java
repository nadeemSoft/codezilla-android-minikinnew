package com.minikin.android.adapter;

/**
 * Created by cube_dev on 10/24/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.activity.ProductDetailActivity;
import com.minikin.android.model.CategoryModel;
import com.minikin.android.model.ProductModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.RestaurantViewHolder> {

    ArrayList<CategoryModel> categoriesList;
    ArrayList<ProductModel> productList;
    private int rowLayout;
    private Context context;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    String WhichLanguage = "";


    public static class RestaurantViewHolder extends RecyclerView.ViewHolder {

        TextView tvProductName, tvAddCard, tvPrice, tvOldPrice,tvOfferPerFlag;
        ImageView ivProductImage;
        LinearLayout llParent;
        FrameLayout frameLayout;


        public RestaurantViewHolder(View v) {
            super(v);
            //tv_order_id = v.findViewById(R.id.rowitem_past_order_tv_order_id);
            tvProductName = v.findViewById(R.id.tv_product_name);
            ivProductImage = v.findViewById(R.id.iv_product);
            tvAddCard = v.findViewById(R.id.tv_add_to_cart);
            tvPrice = v.findViewById(R.id.tv_price);
            llParent = v.findViewById(R.id.ll_parent_rowItem);
            tvOldPrice = v.findViewById(R.id.tv_price_old);
            frameLayout = v.findViewById(R.id.fram_old_price);
            tvOfferPerFlag = v.findViewById(R.id.tv_flag_offer_percent);
        }
    }

    public ProductAdapter(ArrayList<ProductModel> productList, int rowLayout, Context context) {
        this.productList = productList;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
    }

    @Override
    public ProductAdapter.RestaurantViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new RestaurantViewHolder(view);
    }


    @Override
    public void onBindViewHolder(RestaurantViewHolder holder, final int position) {
        setLangRecreate(WhichLanguage, holder);
        final ProductModel productModel = productList.get(position);
        holder.tvProductName.setText(productModel.getProductName());

        if (WhichLanguage.equalsIgnoreCase("en")){

            if (productModel.getProductHasOffer().equalsIgnoreCase("true")) {
                holder.tvOldPrice.setVisibility(View.VISIBLE);
                holder.frameLayout.setVisibility(View.VISIBLE);
                //holder.tvOfferPerFlag.setVisibility(View.VISIBLE);


                holder.tvPrice.setText("KD" + " " + productModel.getProductOfferPrice());
                holder.tvOldPrice.setText("KD" + " " + productModel.getProductPrice());
               // holder.tvOfferPerFlag.setText(productModel.getFlag()+" "+"off");
            } else if (productModel.getProductHasOffer().equalsIgnoreCase("false")) {
                holder.tvPrice.setText("KD" + " " + productModel.getProductPrice());
                holder.tvOldPrice.setVisibility(View.GONE);
                holder.frameLayout.setVisibility(View.GONE);
                holder.tvOfferPerFlag.setVisibility(View.GONE);

            }

        }else if (WhichLanguage.equalsIgnoreCase("ar")){


            if (productModel.getProductHasOffer().equalsIgnoreCase("true")) {
                holder.tvOldPrice.setVisibility(View.VISIBLE);
                holder.frameLayout.setVisibility(View.VISIBLE);
                //holder.tvOfferPerFlag.setVisibility(View.VISIBLE);


                holder.tvPrice.setText("KD" + " " + productModel.getProductOfferPrice());
                holder.tvOldPrice.setText("KD" + " " + productModel.getProductPrice());
                // holder.tvOfferPerFlag.setText(productModel.getFlag()+" "+"off");
            } else if (productModel.getProductHasOffer().equalsIgnoreCase("false")) {
                holder.tvPrice.setText("KD" + " " + productModel.getProductPrice());
                holder.tvOldPrice.setVisibility(View.GONE);
                holder.frameLayout.setVisibility(View.GONE);
                holder.tvOfferPerFlag.setVisibility(View.GONE);

            }

         /*   if (productModel.getProductHasOffer().equalsIgnoreCase("true")) {
                holder.tvOldPrice.setVisibility(View.VISIBLE);
                holder.frameLayout.setVisibility(View.VISIBLE);
                holder.tvOfferPerFlag.setVisibility(View.VISIBLE);


                holder.tvPrice.setText("د.ك" + " " + productModel.getProductOfferPrice());
                holder.tvOldPrice.setText("د.ك" + " " + productModel.getProductPrice());
                holder.tvOfferPerFlag.setText(productModel.getFlag()+" "+"off");
            } else if (productModel.getProductHasOffer().equalsIgnoreCase("false")) {
                holder.tvPrice.setText("د.ك" + " " + productModel.getProductPrice());
                holder.tvOldPrice.setVisibility(View.GONE);
                holder.frameLayout.setVisibility(View.GONE);
                holder.tvOfferPerFlag.setVisibility(View.GONE);

            }*/

        }


        if (productModel.getFlag().equalsIgnoreCase(""))

        //Picasso.with(context).load(productModel.getProductImage()).into(holder.ivProductImage);

      /*  Picasso.with(context)
                .load(productModel.getProductImage())
                .resize(250, 250)
                .centerCrop()
                .onlyScaleDown()
                .placeholder(R.drawable.grey_img_holder)
                .into(holder.ivProductImage);
*/

        Picasso.with(context)
                .load(productModel.getProductImage())
                .fit()
                .into(holder.ivProductImage);



        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductDetailActivity.class);
                intent.putExtra("product_id", productModel.getProductId());
                Log.e("product_idIN ADA", productModel.getProductId());
                context.startActivity(intent);

            }
        });


        //holder.tv_order_id.setText("Order#"+pastOrderList.get(position).getOrder_id());


    }

    @Override
    public int getItemCount() {
        //return productList.size();
        return productList == null ? 0 : productList.size();
    }

    public void setLangRecreate(String langval, RestaurantViewHolder holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());

        //  Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");

        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");

        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvProductName.setTypeface(fontEnMedium);
            holder.tvPrice.setTypeface(fontEn);
            holder.tvAddCard.setTypeface(fontEn);
            holder.tvOldPrice.setTypeface(fontEn);
            holder.tvOfferPerFlag.setTypeface(fontEn);



        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvProductName.setTypeface(fontEnMedium);
            holder.tvPrice.setTypeface(fontEn);
            holder.tvAddCard.setTypeface(fontEn);
            holder.tvOldPrice.setTypeface(fontEn);
            holder.tvOfferPerFlag.setTypeface(fontEn);

        }
    }
}