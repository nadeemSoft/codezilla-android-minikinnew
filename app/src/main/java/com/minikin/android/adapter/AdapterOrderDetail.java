package com.minikin.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.model.OrderDetailProductsModel;
import com.minikin.android.util.AppConstants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AdapterOrderDetail extends RecyclerView.Adapter<AdapterOrderDetail.DetailOrder> {
    Context context;
    ArrayList<OrderDetailProductsModel> orderDetailProductsModelList;
    ProgressDialog pd;
    private SharedPreferences sp = null;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    String WhichLanguage = "";
    long quantitlyLong,price;
    double productPriceDouble;
    double totalPrice = 0.0;


    public AdapterOrderDetail(ArrayList<OrderDetailProductsModel> orderDetailProductsModelList, Context context) {
        this.orderDetailProductsModelList = orderDetailProductsModelList;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editor = sp.edit();


    }

    @Override
    public DetailOrder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_detail, parent, false);
        return new DetailOrder(view);
    }

    @Override
    public void onBindViewHolder(DetailOrder holder, int position) {
        setLangRecreate(WhichLanguage, holder);

        OrderDetailProductsModel itemModel = orderDetailProductsModelList.get(position);

        holder.tvOrderName.setText("" + itemModel.getOrderName());

        if (WhichLanguage.equalsIgnoreCase("en")){
            holder.tvOrderStoreName.setText("By" + "  " + itemModel.getOrderStoreName());

        }else if (WhichLanguage.equalsIgnoreCase("ar")){
            holder.tvOrderStoreName.setText("بواسطة" + "  " + itemModel.getOrderStoreName());

        }
        holder.tvOrderPrice.setText("KD" + "  " + itemModel.getOrderPrice());

        /*Picasso.with(context).load(itemModel.getOrderImage()).placeholder(R.drawable.appicon)   // optional
                .error(R.drawable.appicon).into(holder.ivOrderImage);*/

        Picasso.with(context)
                .load(itemModel.getOrderImage()).fit().placeholder(R.drawable.appicon)
                .error(R.drawable.appicon).into(holder.ivOrderImage);


        quantitlyLong = Long.parseLong(((itemModel.getOrderQuantity())));
        productPriceDouble = Double.valueOf((itemModel.getOrderPrice()));
        Log.e("QUANT...ORDER", quantitlyLong + "");
        Log.e("PRICE..ORDER", productPriceDouble + "");

        totalPrice = (totalPrice + (quantitlyLong * productPriceDouble));
        Log.e("TOTAL..ORDER..PR", totalPrice + "");
        editor.putString("totalPriceOrder", totalPrice + "");
        editor.commit();


    }

    @Override
    public int getItemCount() {
        return orderDetailProductsModelList.size();
    }


    public void setLangRecreate(String langval, DetailOrder holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvOrderName.setTypeface(fontEn);
            holder.tvOrderStoreName.setTypeface(fontEn);
            holder.tvOrderPrice.setTypeface(fontEn);


        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvOrderName.setTypeface(fontEn);
            holder.tvOrderStoreName.setTypeface(fontEn);
            holder.tvOrderPrice.setTypeface(fontEn);


        }
    }


    public class DetailOrder extends RecyclerView.ViewHolder {
        ImageView ivOrderImage;
        TextView tvOrderName, tvOrderStoreName, tvOrderPrice;

        public DetailOrder(View itemView) {
            super(itemView);
            ivOrderImage = itemView.findViewById(R.id.iv_order_detail);
            tvOrderName = itemView.findViewById(R.id.tv_order_name);
            tvOrderStoreName = itemView.findViewById(R.id.tv_order_store_name);
            tvOrderPrice = itemView.findViewById(R.id.tv_oder_detail_price);


        }
    }
}
