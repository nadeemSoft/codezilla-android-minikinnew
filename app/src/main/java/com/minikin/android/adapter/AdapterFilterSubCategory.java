package com.minikin.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.model.AttributesModel;
import com.minikin.android.util.AppConstants;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AdapterFilterSubCategory extends RecyclerView.Adapter<AdapterFilterSubCategory.Sub> {

    Context context;
    ArrayList<AttributesModel> attributesSubCategoryList;
    public static ArrayList<Integer> addIdList = new ArrayList<>();
    SharedPreferences sp = null;
    SharedPreferences.Editor editor;

    SharedPreferences pref;
    String WhichLanguage = "";


    public AdapterFilterSubCategory(Context context, ArrayList<AttributesModel> attributesSubCategoryList) {
        this.context = context;
        this.attributesSubCategoryList = attributesSubCategoryList;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editor = sp.edit();

    }

    @Override
    public Sub onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_filtr_sub_category, parent, false);
        return new Sub(view);
    }

    @Override
    public void onBindViewHolder(Sub holder, final int position) {
        setLangRecreate(WhichLanguage, holder);
        AttributesModel model = attributesSubCategoryList.get(position);

        holder.tvSubCategory.setText(model.getAttribute_name());

        if (attributesSubCategoryList.get(position).isSelected()) {

            holder.radioButton.setChecked(true);

        } else {

            holder.radioButton.setChecked(false);
        }

    }


    public void setLangRecreate(String langval, Sub holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvSubCategory.setTypeface(fontAr);

        } else if (langval.equalsIgnoreCase("en")) {
            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvSubCategory.setTypeface(fontEn);


        }
    }

    @Override
    public int getItemCount() {
        return attributesSubCategoryList.size();
    }

    public class Sub extends RecyclerView.ViewHolder {
        TextView tvSubCategory;
        RadioButton radioButton;
        LinearLayout linearLayout;

        public Sub(View itemView) {
            super(itemView);
            tvSubCategory = itemView.findViewById(R.id.tv_adapter_filter_sub);
            radioButton = itemView.findViewById(R.id.rb_adapter_filter);
            linearLayout = itemView.findViewById(R.id.llParent_Sub_Cate);
        }
    }
}
