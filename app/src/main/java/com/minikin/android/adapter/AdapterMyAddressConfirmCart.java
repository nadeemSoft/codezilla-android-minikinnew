package com.minikin.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.util.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AdapterMyAddressConfirmCart extends RecyclerView.Adapter<AdapterMyAddressConfirmCart.MyAddress> {
    private JSONArray jsonArray;
    private int rowLayout;
    private Context context;
    ProgressDialog pd;
    SharedPreferences sp = null;
    private SharedPreferences.Editor editor;
    int lastClickedPosition = 0;
    SharedPreferences pref;
    String WhichLanguage = "";

    public AdapterMyAddressConfirmCart(JSONArray jsonArray, int rowLayout, Context context) {
        this.jsonArray = jsonArray;
        this.rowLayout = rowLayout;
        this.context = context;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editor = sp.edit();

    }

    @Override
    public MyAddress onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MyAddress(view);
    }

    @Override
    public void onBindViewHolder(MyAddress holder, int position) {
        setLangRecreate(WhichLanguage, holder);



        JSONObject jsonObjectResponse = null;
        try {

            int pos = Integer.parseInt(sp.getString("selectAddress", ""));
            if (pos == position) {
                holder.llParent.setVisibility(View.VISIBLE);
                jsonObjectResponse = jsonArray.getJSONObject(position);
                String address_id = jsonObjectResponse.getString("address_id");
                editor.putString("addressIdConfirm", address_id);
                editor.commit();
                String area_id = jsonObjectResponse.getString("area_id");
                String title = jsonObjectResponse.getString("title");
                String area_name = jsonObjectResponse.getString("area_name");
                String block = jsonObjectResponse.getString("block");
                String street = jsonObjectResponse.getString("street");
                String avenue = jsonObjectResponse.getString("avenue");
                String building = jsonObjectResponse.getString("building");
                String floor = jsonObjectResponse.getString("floor");
                String apartment = jsonObjectResponse.getString("apartment");
                String notes = jsonObjectResponse.getString("notes");
                String delivery_charge = jsonObjectResponse.getString("delivery_charge");

                holder.tvTitleName.setText("" + title);
                holder.tvAddress.setText("" + area_name + "," + "Block" + " " + block + " ," + street + " St.," + "Building" + " " + building + " ," + "Floor " + floor);
            } else {
                holder.llParent.setVisibility(View.GONE);

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    public class MyAddress extends RecyclerView.ViewHolder {
        TextView tvTitleName, tvAddress;
        LinearLayout llParent;

        public MyAddress(View itemView) {
            super(itemView);
            tvTitleName = itemView.findViewById(R.id.tv_name_address);
            tvAddress = itemView.findViewById(R.id.tv_addresss_main);
            llParent = itemView.findViewById(R.id.ll_parent);
        }
    }


    public void setLangRecreate(String langval,MyAddress holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvAddress.setTypeface(fontAr);
            holder.tvTitleName.setTypeface(fontAr);

        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvAddress.setTypeface(fontEn);
            holder.tvTitleName.setTypeface(fontEnMedium);


        }
    }
}
