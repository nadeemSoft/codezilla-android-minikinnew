package com.minikin.android.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.minikin.android.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

/**
 * Created by codezilla-16 on 8/3/18.
 */

public class SlidingImageAdapter extends PagerAdapter {

    // private Integer[] menuIconArr = {R.drawable.dummy_toy_three, R.drawable.dummy_toy_two};
    private JSONArray jsonArray;
    private LayoutInflater inflater;
    private Context mcontext;

    public SlidingImageAdapter(Context context, JSONArray jsonArray) {
        this.mcontext = context;
        this.jsonArray = jsonArray;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return jsonArray.length();
        //return menuIconArr.length;
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.view_pager_layout, view, false);
        assert imageLayout != null;
        ImageView ProductImage = (ImageView) imageLayout.findViewById(R.id.ProductImage);
        Log.e("jsonarray",jsonArray+"");
        Picasso.with(mcontext).load(jsonArray.optString(position)).placeholder(R.drawable.grey_img_holder).error(R.drawable.grey_img_holder).into(ProductImage);


        ProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              /*  Intent intent = new Intent(mcontext, FullScreenActivity.class);
                //intent.putStringArrayListExtra("ImageArrayList", recentMediaList);
                intent.putExtra("position", position);
                mcontext.startActivity(intent);*/

            }
        });


        view.addView(imageLayout, 0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}
