package com.minikin.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.activity.CheckOutActivityNew;
import com.minikin.android.activity.ProductShowActivity;
import com.minikin.android.model.MyGiftModel;
import com.minikin.android.model.SubCategoryModel;
import com.minikin.android.util.AppConstants;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AdapterHome3SubCategory extends RecyclerView.Adapter<AdapterHome3SubCategory.SubHome3> {
    ArrayList<SubCategoryModel> categoriesListSubList;
    Activity context;
    ProgressDialog pd;
    SharedPreferences sp = null;
    private SharedPreferences.Editor editor;
    SharedPreferences pref;
    String WhichLanguage = "";
    int lastClickedPosition = -1;


    public AdapterHome3SubCategory(Activity context,ArrayList<SubCategoryModel> categoriesListSubList) {
        this.context = context;
        this.categoriesListSubList = categoriesListSubList;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");
        pd = new ProgressDialog(context);
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        sp = context.getSharedPreferences(AppConstants.SHAREDPREFERENEKEY, MODE_PRIVATE);
        editor = sp.edit();

    }


    @Override
    public SubHome3 onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_sub_category_home3, parent, false);
        return new SubHome3(view);
    }

    @Override
    public void onBindViewHolder(final SubHome3 holder, final int position) {
        setLangRecreate(WhichLanguage, holder);
        SubCategoryModel model =  categoriesListSubList.get(position);
        holder.tvSubCategory.setText(model.getSubName());


        if (lastClickedPosition == position) {
            holder.radioSelectSub.setChecked(true);
            SubCategoryModel modelSub= categoriesListSubList.get(lastClickedPosition);
            ((ProductShowActivity)context).getProductWithSubCategory(modelSub.getSubCategory(),modelSub.getSubName());


        } else {
            holder.radioSelectSub.setChecked(false);
        }

        holder.llSelectSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.radioSelectSub.setChecked(true);
                lastClickedPosition = position;


                notifyDataSetChanged();


            }
        });

    }

    @Override
    public int getItemCount() {
        return categoriesListSubList.size();
    }



    public void setLangRecreate(String langval,SubHome3 holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());

        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvSubCategory.setTypeface(fontAr);


        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvSubCategory.setTypeface(fontEn);


        }
    }

    public class SubHome3 extends RecyclerView.ViewHolder {
        TextView tvSubCategory;
        LinearLayout llSelectSub;
        RadioButton radioSelectSub;
        public SubHome3(View itemView) {
            super(itemView);
            tvSubCategory = itemView.findViewById(R.id.tv_sub_category_name_home3);
            llSelectSub = itemView.findViewById(R.id.ll_select_sub);
            radioSelectSub = itemView.findViewById(R.id.radio_sub_cate);
        }
    }
}
