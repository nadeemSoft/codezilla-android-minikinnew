package com.minikin.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.model.AreaModel;
import com.minikin.android.util.CountryCallback;

import java.util.ArrayList;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class CountryDialogAdapter extends BaseAdapter implements Filterable {
    private Context context; //context
    ArrayList<AreaModel>  items;
    ArrayList<AreaModel> allItems;
    private CountryFilter countryFilter;
    private CountryCallback countryCallback;
    private SharedPreferences pref;
    private String WhichLanguage = "";
    TextView countryName;


    //public constructor
    public CountryDialogAdapter(Context context, ArrayList<AreaModel> list, CountryCallback callback) {
        this.context = context;
        this.items = list;
        this.allItems = list;
        this.countryCallback = callback;
        pref = context.getApplicationContext().getSharedPreferences("LanguagePref", MODE_PRIVATE);
        WhichLanguage = pref.getString("WhichLanguage", "en");
    }

    @Override
    public int getCount() {
        return items.size(); //returns total of items in the list
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.dialog_country_item, parent, false);
         AreaModel country = items.get(position);
          countryName = (TextView) convertView.findViewById(R.id.country_name);
        countryName.setText(country.getAreaName());

        countryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countryCallback.setItemList(position, items);
            }
        });

        setLangRecreate(WhichLanguage);

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (countryFilter == null)
            countryFilter = new CountryFilter();
        return countryFilter;
    }

    private class CountryFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            // We implement here the filter logic
            if (constraint == null || constraint.length() == 0) {
                // No filter implemented we return all the list
                results.values = allItems;
                results.count = allItems.size();
            } else {
                // We perform filtering operation

                ArrayList filterArray = new ArrayList();
                int length = allItems.size();
                for (int i = 0; i < length; i++) {
                    AreaModel object = allItems.get(i);
                    if (object.getAreaName().toUpperCase().startsWith(constraint.toString().toUpperCase())) {
                        filterArray.add(object);
                    }
                }
                results.values = filterArray;
                results.count = filterArray.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            // Now we have to inform the adapter about the new list filtered
            if (results.count == 0) {
                items = new ArrayList<>();
                notifyDataSetInvalidated();
            } else {
                items = (ArrayList<AreaModel>) results.values;
                notifyDataSetChanged();
            }
        }
    }

    public void setLangRecreate(String langval) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            countryName.setTypeface(fontAr);


        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            countryName.setTypeface(fontEn);



        }
    }
}