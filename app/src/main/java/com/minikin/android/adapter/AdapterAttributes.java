package com.minikin.android.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.model.AttributesModel;
import com.minikin.android.model.ProductDetailsModel;
import com.minikin.android.model.SetAttributeModel;

import java.util.ArrayList;

public class AdapterAttributes extends RecyclerView.Adapter<AdapterAttributes.Attri> {
    Context context;
    ArrayList<ProductDetailsModel> productDetailList;
    ArrayList<SetAttributeModel> attributeModels= new ArrayList<>();
    ArrayList<AttributesModel> attributesModelAttributelist = new ArrayList<>();


    public AdapterAttributes(Context context, ArrayList<ProductDetailsModel> productDetailList) {
        this.context = context;
        this.productDetailList = productDetailList;

    }

    @Override
    public Attri onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_attribute, parent, false);
        return new Attri(view);
    }

    @Override
    public void onBindViewHolder(Attri holder, int position) {

        ProductDetailsModel productDetailsModel = productDetailList.get(position);
        attributeModels = productDetailsModel.getSetAttributeModelsList();

        SetAttributeModel setAttributeModel = attributeModels.get(position);
        Log.e("setatrribute",setAttributeModel.getSetName()+"");

        String setName = setAttributeModel.getSetName();
        attributesModelAttributelist = setAttributeModel.getAttributesModelsLisy();
        AttributesModel model =  attributesModelAttributelist.get(position);
        Log.e("setatrributeName",model.getAttribute_name()+"");

        String attributeName = model.getAttribute_name();
       // model.getAttributeId();

        holder.tvArrributeTitle.setText(setName);
        holder.tvAttributeName.setText(attributeName);

    }

    @Override
    public int getItemCount() {
        return productDetailList.size();
    }

    public class Attri extends RecyclerView.ViewHolder {
        TextView tvArrributeTitle, tvAttributeName;

        public Attri(View itemView) {
            super(itemView);

            tvArrributeTitle = itemView.findViewById(R.id.tv_alltribute_title);
            tvAttributeName = itemView.findViewById(R.id.tv_attribute_name);

        }
    }
}
