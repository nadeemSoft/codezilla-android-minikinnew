package com.minikin.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.minikin.android.R;
import com.minikin.android.model.ProductDetail;

import java.util.List;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class AdapterAttributesTwo extends RecyclerView.Adapter<AdapterAttributesTwo.Attri> {
    Context context;
    public List<ProductDetail.Attributes> attributesList;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    String WhichLanguage = "";

    public AdapterAttributesTwo(Context context, List<ProductDetail.Attributes> attributesList) {
        this.context = context;
        this.attributesList = attributesList;
        pref = context.getSharedPreferences("LanguagePref", MODE_PRIVATE);
        editor = pref.edit();
        WhichLanguage = pref.getString("WhichLanguage", "en");

    }

    @Override
    public Attri onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_attribute, parent, false);
        return new Attri(view);
    }

    @Override
    public void onBindViewHolder(final Attri holder, final int position) {
        setLangRecreate(WhichLanguage, holder);

        final ProductDetail.Attributes attributes = attributesList.get(position);

        holder.tvArrributeTitle.setText(attributes.getSet_name());
        holder.tvAttributeName.setText(attributes.getSet_attributes().get(0).getAttribute_name());
        attributes.getSet_attributes().get(0).setSelected(true);
        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String[] nameArray = new String[attributes.getSet_attributes().size()];
                for (int i = 0; i < attributes.getSet_attributes().size(); i++) {
                    nameArray[i] = attributes.getSet_attributes().get(i).getAttribute_name();
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(context);

                builder.setTitle("Select " + attributes.getSet_name());
                builder.setSingleChoiceItems(nameArray, 0, null);

                // Set the positive button
                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        holder.tvAttributeName.setText(attributes.getSet_attributes().get(selectedPosition).getAttribute_name());

                        for (int i = 0; i < attributes.getSet_attributes().size(); i++) {

                            if (selectedPosition == i) {
                                attributes.getSet_attributes().get(selectedPosition).setSelected(true);
                            } else {
                                attributes.getSet_attributes().get(selectedPosition).setSelected(false);
                            }
                        }

                        // Toast.makeText(context, "ID "+attributes.getSet_attributes().get(selectedPosition).getAttribute_id(), Toast.LENGTH_SHORT).show();

                    }
                })
                        .show();


            }
        });



    }

    @Override
    public int getItemCount() {
        return attributesList.size();
    }

    public class Attri extends RecyclerView.ViewHolder {
        TextView tvArrributeTitle, tvAttributeName;
        LinearLayout llParent;

        public Attri(View itemView) {
            super(itemView);

            tvArrributeTitle = itemView.findViewById(R.id.tv_alltribute_title);
            tvAttributeName = itemView.findViewById(R.id.tv_attribute_name);
            llParent = itemView.findViewById(R.id.ll_parent_attri);

        }
    }

    public void setLangRecreate(String langval, Attri holder) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(langval);
        Locale.setDefault(locale);
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        //Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman Font Download.otf");
        Typeface fontEn = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir.ttc");
        Typeface fontEnBold = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Bold.otf");
        Typeface fontEnMedium = Typeface.createFromAsset(context.getAssets(), "fonts/AmmanV3Serif-Medium.otf");


        Typeface fontAr = Typeface.createFromAsset(context.getAssets(), "fonts/Avenir Roman.otf");

        if (langval.equalsIgnoreCase("ar")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);

            holder.tvArrributeTitle.setTypeface(fontAr);
            holder.tvAttributeName.setTypeface(fontAr);

        } else if (langval.equalsIgnoreCase("en")) {

            ((Activity) context).getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvArrributeTitle.setTypeface(fontEnMedium);
            holder.tvAttributeName.setTypeface(fontEnMedium);


        }
    }
}
